import 'dart:async';

class ReferralCodeModel {
  final String referralCode;
  final String customerMasterId;

  const ReferralCodeModel(this.referralCode,this.customerMasterId);

  static ReferralCodeModel Maping(Map map) {
    String referralCode = map['ReferralCode'];
    String customerMasterId = map['CustomerMasterId'];
    return new ReferralCodeModel(referralCode,customerMasterId);
  }
}

abstract class ReferralCodeGet {
  Future<ReferralCodeModel> referralCodeGet();
}

class FetchDataException implements Exception {
  String _message;

  FetchDataException(this._message);

  String toString() {
    if (_message == null) return "Exception";
    return "Exception: $_message";
  }
}
