class PaytmParameterModel {
  String callBackUrl;
  String channelId;
  String checksum;
  String industryTypeId;
  String merchantKey;
  String merchantId;
  String website;
  bool isProductionMode;


  PaytmParameterModel.fromMap(Map<String, dynamic> map)
      : callBackUrl = map['CALLBACK_URL'],
        channelId = map['CHANNEL_ID'],
        checksum = map['Checksum'],
        industryTypeId = map['INDUSTRY_TYPE_ID'],
        merchantKey = map['MERCHANT_KEY'],
        merchantId = map['MID'],
        website = map['WEBSITE'],
        isProductionMode = map['IS_PRODUCTION'];

  @override
  String toString() {
    return 'PaytmParameterModel{callBackUrl: $callBackUrl, channelId: $channelId, checksum: $checksum, industryTypeId: $industryTypeId, merchantKey: $merchantKey, merchantId: $merchantId, website: $website, isProductionMode: $isProductionMode}';
  }


}
