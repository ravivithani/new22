import 'dart:async';

class StoreListModel {
  String address;
  String area;
  String businessName;
  String businessType;
  String xs_ImageName;
  String openingTime;
  String closingTime;
  String breakStartTime;
  String breakEndTime;
  String totalRating;
  String latitude;
  String longitude;
  String countOffer;
  int businessMasterId;
  int offerUsedCount;
  int vegNonVeg;
  int _kilometer;
  int RedeemPin;
  String UniqueId;

  int get kilometer => _kilometer;

  set kilometer(int kilometer) {
    _kilometer = kilometer;
  }


  StoreListModel(this.address,this.area, this.businessName, this.businessType,
      this.xs_ImageName, this.openingTime, this.closingTime,
      this.breakStartTime, this.breakEndTime, this.totalRating, this.latitude,
      this.longitude, this.businessMasterId, this.countOffer,
      this.offerUsedCount, this.vegNonVeg,this.RedeemPin,this.UniqueId);

  StoreListModel.fromMap(Map<String, dynamic> map) :
        address = map['Address'],
        area = map['Area'],
        businessName = map['BusinessName'],
        businessType = map['BusinessType'],
        xs_ImageName = map['xs_ImageName'],
        openingTime = map['OpeningTime'],
        closingTime = map['ClosingTime'],
        breakStartTime = map['BreakStartTime'],
        breakEndTime = map['BreakEndTime'],
        totalRating = map['TotalRating'],
        latitude = map['Latitude'],
        longitude = map['Longitude'],
        businessMasterId = map['BusinessMasterId'],
        countOffer = map['CountOffer'],
        offerUsedCount = map['OfferUsedCount'],
        RedeemPin = map['RedeemPin'],
        UniqueId = map['UniqueId'],
        vegNonVeg = map['VegNonVeg'];

  @override
  String toString() {
    return 'StoreListModel{address: $address, area: $area, businessName: $businessName, businessType: $businessType, xs_ImageName: $xs_ImageName, openingTime: $openingTime, closingTime: $closingTime, breakStartTime: $breakStartTime, breakEndTime: $breakEndTime, totalRating: $totalRating, latitude: $latitude, longitude: $longitude, countOffer: $countOffer, businessMasterId: $businessMasterId, offerUsedCount: $offerUsedCount, vegNonVeg: $vegNonVeg, _kilometer: $_kilometer, RedeemPin: $RedeemPin, UniqueId: $UniqueId}';
  }


}

