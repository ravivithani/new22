import 'dart:async';

class OneTimePasswordModel{

  final String selectRegisterUserForgotPasswordResult;

  const OneTimePasswordModel(this.selectRegisterUserForgotPasswordResult);

  static OneTimePasswordModel Maping(Map map){
    String selectRegisterUserForgotPasswordResult = map['SelectRegisterUserForgotPasswordResult'];

    return OneTimePasswordModel(selectRegisterUserForgotPasswordResult);
  }

}
abstract class OneTimePasswordGet{
  Future<OneTimePasswordModel> oneTimePasswordGet();
}

class FetchDataException implements Exception{
  String _message;

  FetchDataException(this._message);

  String toString(){
    if (_message == null) return "Exception";
    return "Exception: $_message";
  }
}