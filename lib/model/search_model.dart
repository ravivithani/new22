class SearchModel{
  String searchText;
  String type;
  int rowId;

  SearchModel(this.searchText, this.type, this.rowId);

  SearchModel.fromMap(Map<String, dynamic> map)
      : searchText = map['SearchText'],
        type = map['Type'],
        rowId = map['RowId'];

  @override
  String toString() {
    return 'SearchModel{searchText: $searchText, type: $type, rowId: $rowId}';
  }


}