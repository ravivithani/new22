class OfferMasterObjectModel{
  String areaName;
  String businessName;
  String offerContent;
  String offerTitle;
  String termsAndConditions;
  String toDate;
  String xs_ImageName;
  int businessTypeMasterId;
  int customerOfferTranId;
  int offerMasterId;
  int redeemCount;//customer limit
  int totalRedeemOffer;
  int totalCustomerRedeemOffer;
  String businessType;//total customer used
  bool isAvailableNow;
  bool isFavorite;


  OfferMasterObjectModel(this.areaName, this.businessName, this.offerContent,
      this.offerTitle, this.termsAndConditions, this.toDate, this.xs_ImageName,
      this.businessTypeMasterId, this.customerOfferTranId, this.offerMasterId,
      this.redeemCount, this.totalRedeemOffer,
      this.isAvailableNow, this.isFavorite);

  OfferMasterObjectModel.fromMap(Map map) :
        areaName = map['AreaName'],
        businessName = map['BusinessName'],
        offerContent = map['OfferContent'],
        offerTitle = map['OfferTitle'],
        termsAndConditions = map['TermsAndConditions'],
        toDate = map['ToDate'],
        xs_ImageName = map['xs_ImageName'],
        businessTypeMasterId = map['BusinessTypeMasterId'],
        customerOfferTranId = map['CustomerOfferTranId'],
        offerMasterId = map['OfferMasterId'],
        redeemCount = map['RedeemCount'],
        totalRedeemOffer = map['TotalRedeemOffer'],
        businessType = map['BusinessType'],
        isAvailableNow = map['IsAvailableNow'],
        totalCustomerRedeemOffer = map['TotalCustomerRedeemOffer'],
        isFavorite = map['IsFavorite'];

  @override
  String toString() {
    return 'OfferMasterObjectModel{areaName: $areaName, businessName: $businessName, offerContent: $offerContent, offerTitle: $offerTitle, termsAndConditions: $termsAndConditions, toDate: $toDate, xs_ImageName: $xs_ImageName, businessTypeMasterId: $businessTypeMasterId, customerOfferTranId: $customerOfferTranId, offerMasterId: $offerMasterId, redeemCount: $redeemCount, totalRedeemOffer: $totalRedeemOffer, totalCustomerRedeemOffer: $totalCustomerRedeemOffer, businessType: $businessType, isAvailableNow: $isAvailableNow, isFavorite: $isFavorite}';
  }


}