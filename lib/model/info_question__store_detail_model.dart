class InfoAnswerStoreModel{
  String answer;
  String businessInfoQuestion;
  String questionType;

  InfoAnswerStoreModel(this.answer, this.businessInfoQuestion,
      this.questionType);


  InfoAnswerStoreModel.fromMap(Map<String, dynamic> map):
      answer = map['Answer'],
      businessInfoQuestion = map['BusinessInfoQuestion'],
      questionType = map['QuestionType'];

  @override
  String toString() {
    return 'InfoAnswerStoreModel{answer: $answer, businessInfoQuestion: $businessInfoQuestion, questionType: $questionType}';
  }


}