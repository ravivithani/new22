class PurchasedHistoryModel {
  int Amount;
  String Description;
  String TransactionDateTime;
  List<dynamic> lstPayment;
  PurchasedHistoryModel(this.Amount, this.Description, this.TransactionDateTime);

  PurchasedHistoryModel.fromMap(Map<String, dynamic> map)
      : Amount = map['Amount'],
        Description = map['Description'],
        lstPayment = map['lstPayments'].map((c) => new ListPaymentModel.map(c)).toList(),
        TransactionDateTime = map['TransactionDateTime'];
}

class ListPaymentModel {
  String PaymentModeName;
  int Amount;

  ListPaymentModel(this.PaymentModeName, this.Amount);

  ListPaymentModel.map(Map<String, dynamic> map)
      : PaymentModeName = map['PaymentModeName'],
        Amount = map['Amount'];
}
