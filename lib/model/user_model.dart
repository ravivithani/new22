import 'dart:async';

class UserModel {
  final String androidDeviceId;
  final String birthDate;
  final String customerMasterId;
  final String earnPoint;
  final String email;
  final String fCMToken;
  final String firstName;
  final String lastName;
  final String password;
  final String phone;
  final String gender;
  final bool isEmailVerify;
  final bool isKYCDetails;
  final bool isSMSVerify;
  final String profileImageName;
  final String referralCode;
  final int registeredUserMasterId;
  final String walletBalance;
  final String linktoAreaMasterId;
  final int linktoCityMasterId;
  final String walletWithdrawalLimit;
  final String sessionId;
  final bool isSelectPhoto;
  final bool isUrlPhoto;

  const UserModel(
      this.androidDeviceId,
      this.birthDate,
      this.customerMasterId,
      this.earnPoint,
      this.email,
      this.fCMToken,
      this.firstName,
      this.lastName,
      this.password,
      this.phone,
      this.gender,
      this.isEmailVerify,
      this.isKYCDetails,
      this.isSMSVerify,
      this.profileImageName,
      this.referralCode,
      this.registeredUserMasterId,
      this.walletBalance,
      this.linktoAreaMasterId,
      this.linktoCityMasterId,
      this.walletWithdrawalLimit,
      this.sessionId,
      this.isSelectPhoto,
      this.isUrlPhoto
      );

  @override
  String toString() {
    return 'UserModel{androidDeviceId: $androidDeviceId, birthDate: $birthDate, customerMasterId: $customerMasterId, earnPoint: $earnPoint, email: $email, fCMToken: $fCMToken, firstName: $firstName, lastName: $lastName, password: $password, phone: $phone, gender: $gender, isEmailVerify: $isEmailVerify, isKYCDetails: $isKYCDetails, isSMSVerify: $isSMSVerify, profileImageName: $profileImageName, referralCode: $referralCode, registeredUserMasterId: $registeredUserMasterId, walletBalance: $walletBalance, linktoAreaMasterId: $linktoAreaMasterId, linktoCityMasterId: $linktoCityMasterId, walletWithdrawalLimit: $walletWithdrawalLimit}';
  }

  static UserModel Maping(Map map) {
    String androidDeviceId = map['AndroidDeviceId'];
    String birthDate = map['BirthDate'];
    String customerMasterId = map['CustomerMasterId'];
    String earnPoint = map['EarnPoint'];
    String email = map['Email'];
    String fCMToken = map['FCMToken'];
    String firstName = map['FirstName'];
    String lastName = map['LastName'];
    String password = map['Password'];
    String phone = map['Phone'];
    String gender = map['Gender'];
    bool isEmailVerify = map['IsEmailVerify'];
    bool isKYCDetails = map['IsKYCDetails'];
    bool isSMSVerify = map['IsSMSVerify'];
    String profileImageName = map['ProfileImageName'];
    String referralCode = map['ReferralCode'];
    int registeredUserMasterId = map['RegisteredUserMasterId'];
    String walletBalance = map['WalletBalance'];
    String linktoAreaMasterId = map['linktoAreaMasterId'];
    int linktoCityMasterId = map['linktoCityMasterId'];
    String walletWithdrawalLimit = map['WalletWithdrawalLimit'];
    String sessionId = map['SessionId'];
    bool isSelectPhoto = false;
    bool isUrlPhoto = false;

    return new UserModel(
        androidDeviceId,
        birthDate,
        customerMasterId,
        earnPoint,
        email,
        fCMToken,
        firstName,
        lastName,
        password,
        phone,
        gender,
        isEmailVerify,
        isKYCDetails,
        isSMSVerify,
        profileImageName,
        referralCode,
        registeredUserMasterId,
        walletBalance,
        linktoAreaMasterId,
        linktoCityMasterId,
        walletWithdrawalLimit,
        sessionId,
        isSelectPhoto,
        isUrlPhoto
    );
  }
}

//listener for get UserMasterData
abstract class GetUser {
  Future<UserModel> getUser();
}

//class for handle exceptions on data fetch
class HandleException implements Exception {
  String Exception;

  HandleException(this.Exception);

  String toString() {
    if (Exception == null) return "Exception";
    return "Exception: $Exception";
  }
}
