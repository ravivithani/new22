class WalletHistoryModel{
  int amount;
  int currentBalance;
  String description;
  int walletTransactionType;
  String transactionDate;

  WalletHistoryModel(this.amount, this.currentBalance, this.description,
      this.walletTransactionType,this.transactionDate);

  WalletHistoryModel.fromMap(Map<String,dynamic> map):
      amount = map['Amount'],
      currentBalance = map['CurrentBalance'],
      description = map['Description'],
      walletTransactionType = map['WalletTransactionType'],
      transactionDate = map['TransactionDateTime']
  ;

  @override
  String toString() {
    return 'WalletHistoryModel{amount: $amount, currentBalance: $currentBalance, description: $description, walletTransactionType: $walletTransactionType, transactionDate: $transactionDate}';
  }


}