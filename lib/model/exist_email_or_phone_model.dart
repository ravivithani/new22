import 'dart:async';

class ExistEmailOrPhoneModel{

  final bool selectUserMasterByUserNameIsExistOrNotResult;

  const ExistEmailOrPhoneModel(this.selectUserMasterByUserNameIsExistOrNotResult);

  static ExistEmailOrPhoneModel Maping(Map map){
    bool selectUserMasterByUserNameIsExistOrNotResult = map['SelectUserMasterByUserNameIsExistOrNotResult'];

    return ExistEmailOrPhoneModel(selectUserMasterByUserNameIsExistOrNotResult);
  }
}
abstract class ExistEmailOrPhoneGet{
  Future<ExistEmailOrPhoneModel> existEmailOrPhoneGet();
}

class FetchDataException implements Exception{
  String _message;

  FetchDataException(this._message);

  String toString(){
    if (_message == null) return "Exception";
    return "Exception: $_message";
  }
}