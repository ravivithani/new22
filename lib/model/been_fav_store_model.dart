class BeenFavStoreModel{
  String address;
  String businessName;
  String checkInMasterId;
  String logoImageName;
  String type;

  BeenFavStoreModel(this.address, this.businessName, this.checkInMasterId,
      this.logoImageName, this.type);

  BeenFavStoreModel.fromMap(Map<String, dynamic> map)
      :address = map['Address'],
        businessName = map['BusinessName'],
        checkInMasterId = map['CheckinMasterId'],
        logoImageName = map['LogoImageName'],
        type = map['Type'];

  @override
  String toString() {
    return 'BeenFavStoreModel{address: $address, businessName: $businessName, checkInMasterId: $checkInMasterId, logoImageName: $logoImageName, type: $type}';
  }

}