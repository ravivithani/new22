class ObjectStoreDetailModel {
  String address;
  String area;
  int branchCount;
  String breakEndTime;
  String breakStartTime;
  String businessName;
  String city;
  String closingTime;
  String latitude;
  String longitude;
  String openingTime;
  String phone;
  String reviewCount;
  int savingsAmount;
  String totalRating;
  String weekDay;
  String businessImageName;
  String linkToBusinessTypeMasterId;

  ObjectStoreDetailModel(
      this.address,
      this.area,
      this.branchCount,
      this.breakEndTime,
      this.breakStartTime,
      this.businessName,
      this.city,
      this.closingTime,
      this.latitude,
      this.longitude,
      this.openingTime,
      this.phone,
      this.reviewCount,
      this.savingsAmount,
      this.totalRating,
      this.weekDay,
      this.businessImageName,
      this.linkToBusinessTypeMasterId);

  ObjectStoreDetailModel.fromMap(Map map)
      : address = map['Address'],
        area = map['Area'],
        branchCount = map['BranchCount'],
        breakEndTime = map['BreakEndTime'],
        breakStartTime = map['BreakStartTime'],
        businessName = map['BusinessName'],
        city = map['City'],
        closingTime = map['ClosingTime'],
        latitude = map['Latitude'],
        longitude = map['Longitude'],
        openingTime = map['OpeningTime'],
        phone = map['Phone1'],
        reviewCount = map['ReviewCount'],
        savingsAmount = map['SavingsAmount'],
        totalRating = map['TotalRating'],
        weekDay = map['WeekDay'],
        linkToBusinessTypeMasterId = map['linktoBusinessTypeMasterId'],
        businessImageName = map['sm_ImageName'];

  @override
  String toString() {
    return 'ObjectStoreDetailModel{address: $address, area: $area, branchCount: $branchCount, breakEndTime: $breakEndTime, breakStartTime: $breakStartTime, businessName: $businessName, city: $city, closingTime: $closingTime, latitude: $latitude, longitude: $longitude, openingTime: $openingTime, phone: $phone, reviewCount: $reviewCount, savingsAmount: $savingsAmount, totalRating: $totalRating, weekDay: $weekDay, businessImageName: $businessImageName}';
  }
}
