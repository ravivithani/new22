class ClaimCardListModel{
  String cardActivationDate;
  String cardExpiryDate;
  int cardRenewalBonusPoints;
  String cardRenewalRate;
  String currentDate;
  int customerMembershipTranId;
  int discount;
  String fromDate;
  String toDate;
  bool isActive;
  bool isEnabled;
  bool isDiscountPercentage;
  bool isPurchased;
  bool isRenewNow;
  int membershipLevel;
  int membershipTypeMasterId;
  String membershipTypeName;
  int newCardBonusPoints;
  String newCardRate;
  int totalSavingsAmount;
  int validMonths;

  ClaimCardListModel(this.cardActivationDate, this.cardExpiryDate,
      this.cardRenewalBonusPoints, this.cardRenewalRate, this.currentDate,
      this.customerMembershipTranId, this.discount, this.fromDate, this.toDate,
      this.isActive, this.isEnabled, this.isDiscountPercentage,
      this.isPurchased, this.isRenewNow, this.membershipLevel,
      this.membershipTypeMasterId, this.membershipTypeName,
      this.newCardBonusPoints, this.newCardRate, this.totalSavingsAmount,
      this.validMonths);

  ClaimCardListModel.fromMap(Map<String,dynamic> map)
    : cardActivationDate = map['CardActivationDate'],
        cardExpiryDate = map['CardExpiryDate'],
        cardRenewalBonusPoints = map['CardRenewalBonusPoints'],
        cardRenewalRate = map['CardRenewalRate'],
        currentDate = map['CurrentDate'],
        customerMembershipTranId = map['CustomerMembershipTranId'],
        discount = map['Discount'],
        fromDate = map['FromDate'],
        toDate = map['ToDate'],
        isActive = map['IsActive'],
        isEnabled = map['IsEnabled'],
        isDiscountPercentage = map['IsDiscountPercentage'],
        isPurchased = map['IsPurchased'],
        isRenewNow = map['IsRenewNow'],
        membershipLevel = map['MembershipLevel'],
        membershipTypeMasterId = map['MembershipTypeMasterId'],
        membershipTypeName = map['MembershipTypeName'],
        newCardBonusPoints = map['NewCardBonusPoints'],
        newCardRate = map['NewCardRate'],
        totalSavingsAmount = map['TotalSavingsAmount'],
        validMonths = map['ValidMonths'];

  @override
  String toString() {
    return 'ClaimCardListModel{cardActivationDate: $cardActivationDate, cardExpiryDate: $cardExpiryDate, cardRenewalBonusPoints: $cardRenewalBonusPoints, cardRenewalRate: $cardRenewalRate, currentDate: $currentDate, customerMembershipTranId: $customerMembershipTranId, discount: $discount, fromDate: $fromDate, toDate: $toDate, isActive: $isActive, isEnabled: $isEnabled, isDiscountPercentage: $isDiscountPercentage, isPurchased: $isPurchased, isRenewNow: $isRenewNow, membershipLevel: $membershipLevel, membershipTypeMasterId: $membershipTypeMasterId, membershipTypeName: $membershipTypeName, newCardBonusPoints: $newCardBonusPoints, newCardRate: $newCardRate, totalSavingsAmount: $totalSavingsAmount, validMonths: $validMonths}';
  }

}