class HistoryModel {
  String description;
  int earnPoint;
  int idReferBy;
  int idUsedBy;
  int minimumPointLimit;
  String point;
  String referCustomerFirstName;
  String referCustomerLastName;
  String referDateTime;
  int remainingEarnPoint;
  String usedCustomerFirstName;
  String usedCustomerLastName;
  String linkToMembershipTypeMasterId;

  HistoryModel(this.description, this.earnPoint, this.idReferBy, this.idUsedBy,
      this.minimumPointLimit, this.point, this.referCustomerFirstName,
      this.referCustomerLastName, this.referDateTime, this.remainingEarnPoint,
      this.usedCustomerFirstName, this.usedCustomerLastName,this.linkToMembershipTypeMasterId);

  HistoryModel.fromMap(Map<String, dynamic> map)
      : description = map['Description'],
        earnPoint = map['EarnPoint'],
        idReferBy = map['IdReferBy'],
        idUsedBy = map['IdUsedBy'],
        minimumPointLimit = map['MinimumPointLimit'],
        point = map['Point'],
        referCustomerFirstName = map['ReferCustomerFirstName'],
        referCustomerLastName = map['ReferCustomerLastName'],
        referDateTime = map['ReferDateTime'],
        remainingEarnPoint = map['RemainingEarnPoint'],
        usedCustomerFirstName = map['UsedCustomerFirstName'],
        usedCustomerLastName = map['UsedCustomerLastName'],
        linkToMembershipTypeMasterId = map['linktoMembershipTypeMasterId'];

  @override
  String toString() {
    return 'HistoryModel{description: $description, earnPoint: $earnPoint, idReferBy: $idReferBy, idUsedBy: $idUsedBy, minimumPointLimit: $minimumPointLimit, point: $point, referCustomerFirstName: $referCustomerFirstName, referCustomerLastName: $referCustomerLastName, referDateTime: $referDateTime, remainingEarnPoint: $remainingEarnPoint, usedCustomerFirstName: $usedCustomerFirstName, usedCustomerLastName: $usedCustomerLastName, linkToMembershipTypeMasterId: $linkToMembershipTypeMasterId}';
  }


}