import 'dart:async';

class AppVersionModel {
  final bool selectApplicationVersionExistOrNotResult;

  const AppVersionModel(this.selectApplicationVersionExistOrNotResult);

  static AppVersionModel Maping(Map map) {
    bool selectApplicationVersionExistOrNotResult =
        map['SelectApplicationVersionExistOrNotResult'];

    return new AppVersionModel(selectApplicationVersionExistOrNotResult);
  }
}

abstract class AppVersionGet {
  Future<AppVersionModel> appVersionGet();
}

class FetchDataException implements Exception {
  String _message;

  FetchDataException(this._message);

  String toString() {
    if (_message == null) return "Exception";
    return "Exception: $_message";
  }
}
