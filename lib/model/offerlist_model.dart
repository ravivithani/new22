class OfferListModel{
  String OfferTitle;
  String BusinessName;
  String BusinessType;
  String AreaName;
  String CityName;
  String xs_ImageName;
  String ToDate;//expire date
  String ToTime;//expire time
  int TotalRedeemOffer;//total used time all over
  int RedeemCount;//this is user used limit
  int OfferMasterId;
  int BusinessTypeMasterId;
  int linktoBusinessMasterId;
  bool IsAvailableNow;
  int TotalCustomerRedeemOffer;

  OfferListModel(this.OfferTitle, this.BusinessName, this.BusinessType,
      this.AreaName, this.CityName, this.xs_ImageName, this.ToDate, this.ToTime,
      this.TotalRedeemOffer, this.RedeemCount,
      this.OfferMasterId, this.BusinessTypeMasterId,
      this.linktoBusinessMasterId,
      this.IsAvailableNow,this.TotalCustomerRedeemOffer); //check for offer is available or not



  OfferListModel.fromMap(Map<String, dynamic> map)
      : OfferTitle = map['OfferTitle'],
        BusinessName = map['BusinessName'],
        BusinessType = map['BusinessType'],
        AreaName = map['AreaName'],
        CityName = map['CityName'],
        xs_ImageName = map['xs_ImageName'],
        ToDate = map['ToDate'],
        ToTime = map['ToTime'],
        TotalRedeemOffer = map['TotalRedeemOffer'],
        RedeemCount = map['RedeemCount'],
        OfferMasterId = map['OfferMasterId'],
        BusinessTypeMasterId = map['BusinessTypeMasterId'],
        linktoBusinessMasterId = map['linktoBusinessMasterId'],
        IsAvailableNow = map['IsAvailableNow'],
        TotalCustomerRedeemOffer = map['TotalCustomerRedeemOffer'];

  @override
  String toString() {
    return 'OfferListModel{OfferTitle: $OfferTitle, BusinessName: $BusinessName, BusinessType: $BusinessType, AreaName: $AreaName, CityName: $CityName, xs_ImageName: $xs_ImageName, ToDate: $ToDate, ToTime: $ToTime, TotalRedeemOffer: $TotalRedeemOffer, RedeemCount: $RedeemCount, OfferMasterId: $OfferMasterId, BusinessTypeMasterId: $BusinessTypeMasterId, linktoBusinessMasterId: $linktoBusinessMasterId, IsAvailableNow: $IsAvailableNow, TotalCustomerRedeemOffer: $TotalCustomerRedeemOffer}';
  }


}