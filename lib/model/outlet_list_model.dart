class OutletListModel{
  String area;
  String businessName;
  String businessType;
  String xsImageName;
  String openingTime;
  String closingTime;
  String breakStartTime;
  String breakEndTime;
  String totalRating;
  String latitude;
  String longitude;
  String countOffer;
  int businessMasterId;
  int offerUsedCount;
  int vegNonVeg;
  int _kilometer;

  int get kilometer => _kilometer;

  set kilometer(int kilometer) {
    _kilometer = kilometer;
  }

  OutletListModel(this.area, this.businessName, this.businessType,
      this.xsImageName, this.openingTime, this.closingTime, this.breakStartTime,
      this.breakEndTime, this.totalRating, this.latitude, this.longitude,
      this.countOffer, this.businessMasterId, this.offerUsedCount,
      this.vegNonVeg);

  OutletListModel.fromMap(Map<String,dynamic> map) :
        area = map['Area'],
        businessName = map['BusinessName'],
        businessType= map['BusinessType'],
        xsImageName = map['xs_ImageName'],
        openingTime = map['OpeningTime'],
        closingTime = map['ClosingTime'],
        breakStartTime = map['BreakStartTime'],
        breakEndTime = map['BreakEndTime'],
        totalRating = map['TotalRating'],
        latitude = map['Latitude'],
        longitude = map['Longitude'],
        countOffer = map['CountOffer'],
        businessMasterId = map['BusinessMasterId'],
        offerUsedCount = map['OfferUsedCount'],
        vegNonVeg = map['VegNonVeg'];

  @override
  String toString() {
    return 'OutletListModel{area: $area, businessName: $businessName, businessType: $businessType, xsImageName: $xsImageName, openingTime: $openingTime, closingTime: $closingTime, breakStartTime: $breakStartTime, breakEndTime: $breakEndTime, totalRating: $totalRating, latitude: $latitude, longitude: $longitude, countOffer: $countOffer, businessMasterId: $businessMasterId, offerUsedCount: $offerUsedCount, vegNonVeg: $vegNonVeg, _kilometer: $_kilometer}';
  }


}