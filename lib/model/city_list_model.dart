import 'dart:async';

class CityListModel {
    String cityName;
    int cityMasterId;

    CityListModel(this.cityName, this.cityMasterId);

    CityListModel.fromMap(Map<String, dynamic> map)
        : cityName = map['CityName'],
          cityMasterId = map['CityMasterId'];

    @override
    String toString() {
      return 'CityListModel{cityName: $cityName, cityMasterId: $cityMasterId}';
    }

}
//listener for get UserMasterData
abstract class GetCitySelection {
  Future<List<CityListModel>> getCitySelection();
}

//class for handle exceptions on data fetch
class HandleException implements Exception {
  String Exception;

  HandleException(this.Exception);

  String toString() {
    if (Exception == null) return "Exception";
    return "Exception: $Exception";
  }
}
