import 'dart:async';

class AdvertisementModel{
  int advertisementMasterId;
  int advertisementTypeId;
  int displayForId;
  int linktoBusinessMasterId;
  int linktoOfferMasterId;
  int linktoBusinessTypeMasterId;
  String md_ImageName;
  String businessName;
  String businessTypeName;

  AdvertisementModel(this.advertisementMasterId, this.advertisementTypeId,
      this.displayForId, this.linktoBusinessMasterId, this.linktoOfferMasterId,
      this.linktoBusinessTypeMasterId, this.md_ImageName, this.businessName,
      this.businessTypeName);


  AdvertisementModel.fromMap(Map<String, dynamic> map) :
        advertisementMasterId = map['AdvertisementMasterId'],
        advertisementTypeId = map['AdvertisementTypeId'],
        displayForId = map['DisplayForId'],
        linktoBusinessMasterId = map['linktoBusinessMasterId'],
        linktoOfferMasterId = map['linktoOfferMasterId'],
        linktoBusinessTypeMasterId = map['linktoBusinessTypeMasterId'],
        md_ImageName = map['md_ImageName'],
        businessName = map['BusinessName'],
        businessTypeName = map['BusinessTypeName'];

  @override
  String toString() {
    return 'AdvertisementModel{advertisementMasterId: $advertisementMasterId, advertisementTypeId: $advertisementTypeId, displayForId: $displayForId, linktoBusinessMasterId: $linktoBusinessMasterId, linktoOfferMasterId: $linktoOfferMasterId, linktoBusinessTypeMasterId: $linktoBusinessTypeMasterId, md_ImageName: $md_ImageName, businessName: $businessName, businessTypeName: $businessTypeName}';
  }

}
abstract class GetAdvertisement {
  Future<AdvertisementModel> getAdvertisement();
}