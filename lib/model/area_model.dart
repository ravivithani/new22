class AreaModel {
  int AreaMasterId;
  String AreaName;
  bool _isChecked = false;

  bool get isChecked => _isChecked;

  set isChecked(bool isChecked) {
    _isChecked = isChecked;
  }

  AreaModel.fromMap(Map<String, dynamic> map) :
        AreaMasterId = map['AreaMasterId'],
        AreaName = map['AreaName'];

  AreaModel(this.AreaMasterId, this.AreaName);

  @override
  String toString() {
    return 'AreaModel{AreaMasterId: $AreaMasterId, AreaName: $AreaName}';
  }


}