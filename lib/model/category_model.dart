import 'dart:async';

class CategoryModel {
  String businessType;
  String xs_ImageName;
  int businessTypeMasterId;


  CategoryModel(this.businessType, this.xs_ImageName,
      this.businessTypeMasterId);

  CategoryModel.fromMap(Map<String, dynamic> map) :
        businessType = map['BusinessType'],
        xs_ImageName = map['xs_ImageName'],
        businessTypeMasterId = map['BusinessTypeMasterId'];

  @override
  String toString() {
    return 'CategoryModel{businessType: $businessType, xs_ImageName: $xs_ImageName, businessTypeMasterId: $businessTypeMasterId}';
  }


}

abstract class GetCategory {
  Future<CategoryModel> getCategory();
}