
class NotificationModel {
  String NotificationText;
  String NotificationTitle;
  String NotificationTypeId;
  String NotificationType;
  String NotificationDateTime;
  String NotificationImageName;
  int NotificationTranId;
  String Business;
  String linktoBusinessMasterId;



  NotificationModel(this.NotificationText, this.NotificationTitle,
      this.NotificationTypeId, this.NotificationType, this.NotificationDateTime,
      this.NotificationImageName, this.NotificationTranId);

  NotificationModel.fromMap(Map<String, dynamic> map)
      : NotificationText = map['NotificationText'],
        NotificationTitle = map['NotificationTitle'],
        NotificationTypeId = map['NotificationTypeId'],
        NotificationType = map['NotificationType'],
        NotificationDateTime = map['NotificationDateTime'],
        NotificationImageName = map['NotificationImageName'],
        Business = map['Business'],
        linktoBusinessMasterId = map['linktoBusinessMasterId'],
        NotificationTranId = map['NotificationTranId'];
}
