class HourStoreDetailModel{
  int dayOfWeek;
  String breakEndTime;
  String breakStartTime;
  String closingTime;
  String openingTime;
  String dayName;
  bool isClosed;

  HourStoreDetailModel(this.dayOfWeek, this.breakEndTime, this.breakStartTime,
      this.closingTime, this.openingTime, this.dayName, this.isClosed);

  HourStoreDetailModel.fromMap(Map<String,dynamic> map):
        dayOfWeek = map['DayOfWeek'],
        breakEndTime = map['BreakEndTime'],
        breakStartTime = map['BreakStartTime'],
        closingTime = map['ClosingTime'],
        openingTime = map['OpeningTime'],
        dayName = map['DayName'],
        isClosed = map['IsClosed'];

  @override
  String toString() {
    return 'HourStoreDetailModel{dayOfWeek: $dayOfWeek, breakEndTime: $breakEndTime, breakStartTime: $breakStartTime, closingTime: $closingTime, openingTime: $openingTime, dayName: $dayName, isClosed: $isClosed}';
  }


}