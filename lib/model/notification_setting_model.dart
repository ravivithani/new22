
class NotificationSettingModel{
  bool isEnable;
  int registeredUserSettingTranId;
  int settingTypeId;

  NotificationSettingModel(this.isEnable, this.registeredUserSettingTranId,
      this.settingTypeId);

  NotificationSettingModel.fromMap(Map<String,dynamic> map)
  : isEnable = map['IsEnabled'],
    registeredUserSettingTranId = map['RegisteredUserSettingTranId'],
    settingTypeId = map['SettingTypeId'];

  @override
  String toString() {
    return 'NotificationSettingModel{isEnable: $isEnable, registeredUserSettingTranId: $registeredUserSettingTranId, settingTypeId: $settingTypeId}';
  }

}