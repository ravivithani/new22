class ReviewStoreModel{
  String customerName;
  String businessName;
  String businessImageName;
  String logoImageName;
  String profileImageName;
  String review;
  int reviewMasterId;
  String starRating;
  int customerMasterId;

  ReviewStoreModel(this.customerName, this.businessName, this.businessImageName,
      this.logoImageName, this.profileImageName,this.review,this.reviewMasterId,this.starRating,this.customerMasterId);

  ReviewStoreModel.fromMap(Map<String, dynamic> map)
      :customerName = map['CustomerName'],
        businessName = map['Business'],
        businessImageName = map['BusinessImageName'],
        logoImageName = map['LogoImageName'],
        profileImageName = map['ProfileImageName'],
        review = map['Review'],
        reviewMasterId = map['ReviewMasterId'],
        starRating = map['StarRating'],
        customerMasterId = map['linktoCustomerMasterId'];

  @override
  String toString() {
    return 'ReviewStoreModel{customerName: $customerName, businessName: $businessName, businessImageName: $businessImageName, logoImageName: $logoImageName, profileImageName: $profileImageName, review: $review, reviewMasterId: $reviewMasterId, starRating: $starRating, customerMasterId: $customerMasterId}';
  }


}