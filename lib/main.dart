import 'package:crazyrex/page/city_selection_screen.dart';
import 'package:crazyrex/page/forget_account_screen.dart';
import 'package:crazyrex/page/home_screen.dart';
import 'package:crazyrex/page/intro_screen.dart';
import 'package:crazyrex/page/login_screen.dart';
import 'package:crazyrex/page/offer_description_screen.dart';
import 'package:crazyrex/page/splash_screen.dart';
import 'package:flutter/material.dart';

var api_key = "AIzaSyAkocNdxLVO3m9fx1Gl7_Rz84hwTQQXGa4";

void main() async {
  /*MapView.setApiKey(api_key);*/
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {

  final routes = {
    '/SplashPage': (BuildContext context) => new SplashScreen(),
    '/LoginPage': (BuildContext context) => new LoginScreen1(),
    '/IntroPage': (BuildContext context) => new IntroScreen(),
    '/CitySelectionPage': (BuildContext context) =>
    new CitySelectionScreen(false),
    '/HomePage': (BuildContext context) => new HomeScreen(),
    '/ForgetAccountPage': (BuildContext context) => new ForgetAccountScreen(),
    '/OfferDescription': (BuildContext context) => new OfferDescriptionScreen(),
  };

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'CrazyRex',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
          primaryColorBrightness: Brightness.light,
          primaryColor: Colors.white,
          accentColor: Colors.myColor
      ),
      home: new SplashScreen(),
//      home: new TestPage(),
      routes: routes,
    );
  }
}


