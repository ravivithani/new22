
dependencies:
  flutter:
    sdk: flutter

  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  cupertino_icons: ^0.1.2
  connectivity:
  flutter_statusbarcolor:
  shared_preferences: "^0.4.2"
  http: "^0.11.3+16"
  image_picker:
  date_format: "^1.0.4"
  location: "^1.3.4"
  latlong: "^0.5.3"
  share: "^0.5.2"
  launch_review: "^1.0.1"
  package_info: "^0.3.2"
  url_launcher: "^3.0.3"
  fluttertoast: "^2.0.3"
  geocoder: "^0.0.1"
  photo_view: "^0.0.2"
  flutter_html_view: "^0.5.1"
  flutter_spinkit: "^1.0.0"
  map_view:
  qr_flutter: ^1.1.1
  qr_mobile_vision: path: D:\flutter_sdk\flutter_qr_mobile_vision-master
  firebase_messaging: ^1.0.4
  flutter_local_notifications: "^0.3.5"
  device_info: ^0.2.1
  simple_permissions:
  flutter_webview_plugin: ^0.1.6

dev_dependencies:
  flutter_test:
    sdk: flutter


# For information on the generic Dart part of this file, see the
# following page: https://www.dartlang.org/tools/pub/pubspec

# The following section is specific to Flutter.
flutter:

  # The following line ensures that the Material Icons font is
  # included with your application, so that you can use the icons in
  # the material Icons class.
  uses-material-design: true

  # To add assets to your application, add an assets section, like this:
  assets:
       - images/ic_splash_icon_web.png
       - images/ic_account_multiple_black_48dp.png
       - images/ic_cellphone_message_black_48dp.png
       - images/offer_intro.png
       - images/refer_intro.png
       - images/ic_security_account_black_48dp.png
       - images/ic_table_search_black.png
       - images/ic_crx_icon.png
       - images/ic_currency_usd_black_48dp.png
       - images/bg33.png
       - images/Verified.png
       - images/Verify.png
       - images/refer.png
       - images/whatsapp.png
       - images/instagram.png
       - images/facebook.png
       - images/ic_sort.png
       - images/ic_filter.png
       - images/ic_filter_filled.png
       - images/default1.png
       - images/history.png
       - images/empty_history.png
       - images/label.png
       - images/logo_with_name.png
       - images/ic_buy_now.png
       - images/scan_qr_at_store.png
       - images/keyboard.png
       - images/show_card.png
       - images/ic_veg.png
       - images/nonveg.png
       - images/location.jpg
       - images/Redeem.png


  # An image asset can refer to one or more resolution-specific "variants", see
  # https://flutter.io/assets-and-images/#resolution-aware.

  # For details regarding adding assets from package dependencies, see
  # https://flutter.io/assets-and-images/#from-packages

  # To add custom fonts to your application, add a fonts section here,
  # in this "flutter" section. Each entry in this list should have a
  # "family" key with the font family name, and a "fonts" key with a
  # list giving the asset and other descriptors for the font. For
  # example:
  # fonts:
  #   - family: Schyler
  #     fonts:
  #       - asset: fonts/Schyler-Regular.ttf
  #       - asset: fonts/Schyler-Italic.ttf
  #         style: italic
  #   - family: Trajan Pro
  #     fonts:
  #       - asset: fonts/TrajanPro.ttf
  #       - asset: fonts/TrajanPro_Bold.ttf
  #         weight: 700
  #
  # For details regarding fonts from package dependencies,
  # see https://flutter.io/custom-fonts/#from-packages
