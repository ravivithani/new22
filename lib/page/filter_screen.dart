import 'package:crazyrex/model/area_model.dart';
import 'package:crazyrex/page/all_store_tab_screen.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class FilterScreen extends StatefulWidget {
  final List<AreaModel> areaList;
  final List<AreaModel> selectedAreaList;

  FilterScreen(this.areaList, this.selectedAreaList);

  @override
  _FilterScreenState createState() =>
      _FilterScreenState(areaList, selectedAreaList);
}

class _FilterScreenState extends State<FilterScreen> {
  List<AreaModel> areaList;
  List<AreaModel> selectedAreaList;

  Map<AreaModel, bool> values;

  bool isLoading = false;

  bool isPressedReset = false;

  _FilterScreenState(this.areaList, this.selectedAreaList) {
    if (selectedAreaList.length != 0) {
      for (int i = 0; i < areaList.length; i++) {
        for (int j = 0; j < selectedAreaList.length; j++) {
          if (areaList[i].AreaMasterId == selectedAreaList[j].AreaMasterId) {
            areaList[i].isChecked = true;
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    values = new Map.fromIterable(
      areaList,
      key: (item) => item,
      value: (item) => item.isChecked != null ? item.isChecked : false,
    );
    return Scaffold(
      appBar: new AppBar(
          title: new Text("Filter"),
          leading: new Center(
            child: new InkWell(
              onTap: () {
                if (isPressedReset) {
                  selectedAreaList = new List();
                  Navigator.pop(context, ["", selectedAreaList]);
                } else {
                  Navigator.pop(context);
                }
              },
              child: Icon(Icons.close),
            ),
          )),
      bottomNavigationBar: new Container(
        margin: EdgeInsets.only(bottom: 2.0),
        child: new Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(1.0),
            ),
            new Expanded(
              child: new OutlineButton(
                onPressed: () {
                  setState(() {
                    isPressedReset = true;
                    for (int i = 0; i < areaList.length; i++) {
                      areaList[i].isChecked = false;
                    }
                  });
                },
                borderSide: BorderSide(color: Colors.myColor, width: 2.0),
                child: new Container(
                  padding: EdgeInsets.all(15.0),
                  child: new Text(
                    "RESET",
                    style: Theme
                        .of(context)
                        .textTheme
                        .body1
                        .apply(color: Colors.myColor),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(1.0),
            ),
            new Expanded(
              child: new MaterialButton(
                onPressed: () {
                  String areaIds = "";
                  selectedAreaList = new List();
                  for (int i = 0; i < areaList.length; i++) {
                    if (areaList[i].isChecked) {
                      areaIds += "${areaList[i].AreaMasterId},";
                      selectedAreaList.add(areaList[i]);
                    }
                  }

                  Navigator.pop(context, [areaIds, selectedAreaList]);
                },
                color: Colors.myColor,
                child: new Container(
                  padding: EdgeInsets.all(15.0),
                  child: new Text(
                    "APPLY",
                    style: Theme
                        .of(context)
                        .textTheme
                        .body1
                        .apply(color: Colors.white),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(1.0),
            ),
          ],
        ),
      ),
      body: _AreaList(),
    );
  }

  Widget _AreaList() {
    return new Container(
        child: new Column(
      children: values.keys.map((AreaModel model) {
        return new CheckboxListTile(
          controlAffinity: ListTileControlAffinity.leading,
          title: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 12.0),
                  child: Text(
                    model.AreaName,
                    style: Theme.of(context).textTheme.body1,
                  ),
                ),
                new Container(
                  height: 0.5,
                  color: Colors.black87,
                ),
              ]),
          value: values[model],
          selected: model.isChecked != null ? model.isChecked : false,
          onChanged: (bool value) {
            setState(() {
              values[model] = value;
              model.isChecked = value;
            });
          },
        );
      }).toList(),
    ));
  }
}
