import 'dart:async';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:crazyrex/model/app_version_model.dart';
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/page/registration_screen.dart';
import 'package:crazyrex/parser/app_version_parser.dart';
import 'package:crazyrex/parser/login_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen1 extends StatefulWidget {
  @override
  _LoginScreen1State createState() => _LoginScreen1State();
}

class _LoginScreen1State extends State<LoginScreen1>
    implements AppVersionListener, LoginListener {


  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();

  String packageName;
  String appName;
  String version;
  String buildNumber;
  PackageInfo _packageInfo = new PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  String firebaseToken;

  Future<Null> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      version = info.version;
      packageName = info.packageName;
      appName = info.appName;
      buildNumber = info.buildNumber;

      listener = new SetUpAppVersionListener(this,
          '${Constants.url}SelectApplicationVersionExistOrNot/$version/0');

      if (defaultTargetPlatform == TargetPlatform.iOS) {
        url1 = "http://itunes.apple.com/+91/app/${appName}/id$packageName?mt=8";
      } else if (defaultTargetPlatform == TargetPlatform.android) {
        url1 = "http://play.google.com/store/apps/details?id=${packageName}";
      }
      print("url -----$url1");
    });
  }

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldState = new GlobalKey<ScaffoldState>();

  bool _obscure = true;

  IconData _icon = Icons.remove_red_eye;

  var _userNameController = new TextEditingController();

  var _passwordController = new TextEditingController();
  FocusNode textSecondFocusNode = new FocusNode();

  bool appVersion;
  bool isError = false;

  bool isPhone = false;
  bool isEmail = false;
  String cityName;

  bool _isLoading = true;
  String strPhone, strPassword;

  SetUpAppVersionListener listener;

  SetUpLoginListener listenerLogin;
  UserModel loginModel;
  AppVersionModel appVersionModel;
  var _connectionStatus = 'Unknown';

  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection;

  String url1;

  changeStatusColor(Color color) async {
    await FlutterStatusbarcolor.setStatusBarColor(color);
  }

  _LoginScreen1State() {
    changeStatusColor(Colors.myColor);
    PrefUtil.getStringPreference(Constants.prefPhone).then((String value) {
      strPhone = value;
    });
    PrefUtil.getStringPreference(Constants.prefPassword).then((String value) {
      strPassword = value;
    });
    /*PrefUtil.getStringPreference(Constants.prefFcmToken).then((String value) {
      firebaseToken = value;
    });*/
  }

  @override
  void initState() {
    super.initState();
    getToken();

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    _initPackageInfo();

    PrefUtil.putBoolPreference(Constants.prefIsFirstTimeLaunch, true);

    _internetConnection();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
          _isLoading = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    subscription == null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (_isLoading) {
          listener.loadAppVersion();
        }
        return Scaffold(
          key: scaffoldState,
          body: _isLoading
              ? new Center(
                  child: new CircularProgressIndicator(),
                )
              : new ListView(
                  shrinkWrap: false,
                  children: <Widget>[
                    new Container(
                      height: MediaQuery.of(context).size.height - 85,
                      child: new Stack(
                        children: <Widget>[
                          new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Text(Strings.welcome,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .display1
                                      .apply(color: Colors.myColor)),
                              new Icon(Icons.account_circle,size: 65.0,color: Colors.myColor,),
                              new Form(
                                key: _formKey,
                                child: new Theme(
                                  data: new ThemeData(
                                      primaryColor: Colors.myColor,
                                      accentColor: Colors.myColor,
                                      textSelectionColor: Colors.myColor),
                                  child: new Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        50.0, 0.0, 50.0, 0.0),
                                    child: new Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: <Widget>[
                                        new TextFormField(
                                          controller: _userNameController,
                                          onFieldSubmitted: (String value) {
                                            FocusScope.of(context).requestFocus(
                                                textSecondFocusNode);
                                          },
                                          maxLines: 1,
                                          validator: (value) {
                                            if (_userNameController
                                                .text.isNotEmpty) {
                                              if (_passwordController
                                                  .text.isNotEmpty) {
                                                if (Constants.isValidPhone(
                                                    _userNameController.text)) {
                                                  if (internetConnection == true && internetConnection != null) {
                                                    _progressDialog(true);
                                                    listenerLogin = new SetUpLoginListener(this, '${Constants.url}SelectRegisteredUserMasterByUserName/${_userNameController.text}/${_passwordController.text}/${firebaseToken}/2');
                                                    listenerLogin.loadLogin();
                                                    isPhone = true;
                                                  } else {
                                                    Constants.showSnackBar(scaffoldState, Strings.noInternetToast);
                                                  }
                                                } else if (Constants.isValidEmail(_userNameController.text)) {
                                                  if (internetConnection == true && internetConnection != null) {
                                                    _progressDialog(true);
                                                    listenerLogin = new SetUpLoginListener(this, '${Constants.url}SelectRegisteredUserMasterByUserName/${_userNameController.text}/${_passwordController.text}/${firebaseToken}/1');
                                                    listenerLogin.loadLogin();
                                                    isEmail = true;
                                                  } else {
                                                    Constants.showSnackBar(
                                                        scaffoldState,
                                                        Strings
                                                            .noInternetToast);
                                                  }
                                                } else {
                                                  return Strings
                                                      .invalidUsername;
                                                }
                                              }
                                            } else {
                                              return Strings.emptyUsername;
                                            }
                                          },
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          decoration: new InputDecoration(
                                              labelText: Strings.hintUsername),
                                        ),
                                        new TextFormField(
                                            controller: _passwordController,
                                            focusNode: textSecondFocusNode,
                                            maxLines: 1,
                                            keyboardType: TextInputType.text,
                                            validator: (value) {
                                              if (_userNameController
                                                  .text.isNotEmpty) {
                                                if (_passwordController
                                                    .text.isNotEmpty) {
                                                } else {
                                                  return Strings.emptyPassword;
                                                }
                                              }
                                            },
                                            obscureText: _obscure,
                                            decoration: new InputDecoration(
                                                suffixIcon: _passwordController
                                                        .text.isNotEmpty
                                                    ? new IconButton(
                                                        icon: new Icon(_icon),
                                                        onPressed: () {
                                                          _hidePassword();
                                                        })
                                                    : null,
                                                labelText: "Password")),
                                        new Padding(
                                            padding: const EdgeInsets.only(
                                                top: 27.0)),
                                        new MaterialButton(
                                          color: Colors.myColor,
                                          textColor: Colors.white,
                                          child: new Text(
                                              Strings.labelButtonSignIn),
                                          onPressed: () {
                                            if (_formKey.currentState
                                                .validate()) {}
//                                            _showAlertAppVersion();
                                          },
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              new Container(
                                alignment: Alignment.centerRight,
                                padding: const EdgeInsets.fromLTRB(
                                    0.0, 10.0, 50.0, 0.0),
                                child: new InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, "/ForgetAccountPage");
                                    subscription == null;
                                  },
                                  child: Text(
                                    Strings.labelForgetPassword,
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .caption
                                        .apply(color: Colors.myColor),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      height: 60.0,
                      child: new InkWell(
                        onTap: () {
                          subscription == null;
                          var route = new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new RegistrationScreen());
                          _navigateAndReturn(context, route, 0);
                        },
                        child: new Container(
                          foregroundDecoration: new ViewLineFromRegistration(),
                          padding:
                              new EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Text(
                                Strings.labelDontHaveAccount,
                                style: Theme.of(context).textTheme.body2,
                              ),
                              new Text(
                                Strings.labelDontHaveAccountSignUp,
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .body2
                                    .apply(color: Colors.myColor),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
        );
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return new Scaffold();
    }
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                color: Colors.myColor,
                textColor: Colors.white,
                child: new Text(Strings.labelButtonRetry),
                onPressed: () {
                  setState(() {});
                },
              )
            ],
          ),
        ),
      ],
    ));
  }

  @override
  void onErrorAppVersion() {
    print("Error Appversion Service in Login Page");
    setState(() {
      _isLoading = false;
      isError = true;
    });
  }

  @override
  void onSuccessAppVersion(AppVersionModel model) {
    appVersionModel = model;
    _version();
  }

  void _version() {
    appVersion = appVersionModel.selectApplicationVersionExistOrNotResult;
    if (appVersion == true) {
      print('Appversion $appVersion');

      if ((strPhone != null && strPassword != null) &&
          (strPassword.isNotEmpty && strPhone.isNotEmpty)) {
        isPhone = true;
        String url = '${Constants
            .url}SelectRegisteredUserMasterByUserName/${strPhone}/${strPassword}/${firebaseToken}/2';
        listenerLogin = new SetUpLoginListener(this, url);
        listenerLogin.loadLogin();
      } else {
        setState(() {
          _isLoading = false;
        });
      }
    } else if (appVersion == null) {
      setState(() {
        _isLoading = false;
        isError = true;
      });
    } else if (appVersion == false) {
      _showAlertAppVersion();
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void onErrorLogin(onError) {
    setState(() {
      _isLoading = false;
      _progressDialog(false);
    });

    print("Error Login in login service :- $onError");
  }

  @override
  void onSuccessLogin(UserModel model) {
    print("OK");
    setState(() {
      loginModel = model;
      _isLoading = false;
      _progressDialog(false);
      _login();
    });
  }

  void _login() {
    print(loginModel.toString());

    if (isPhone) {
      if (loginModel.registeredUserMasterId != 0) {
        if (loginModel.isSMSVerify) {
          PrefUtil.putStringPreference(Constants.prefPhone, loginModel.phone);
          PrefUtil.putStringPreference(
              Constants.prefFirstName, loginModel.firstName);
          PrefUtil.putStringPreference(
              Constants.prefLastName, loginModel.lastName);
          PrefUtil.putBoolPreference(Constants.prefLogin, true);
          PrefUtil.putStringPreference(
              Constants.prefPassword, _passwordController.text);
          PrefUtil.putIntPreference(Constants.prefRegisterUserMasterId,
              loginModel.registeredUserMasterId);
          PrefUtil.putStringPreference(
              Constants.prefCustomerMasterId, loginModel.customerMasterId);
          PrefUtil.putBoolPreference(
              Constants.prefKycDetail, loginModel.isKYCDetails);
          PrefUtil.putStringPreference(
              Constants.prefSessionId, loginModel.sessionId);
          PrefUtil.putStringPreference(
              Constants.prefReferralCode, loginModel.referralCode);
          PrefUtil
              .getStringPreference(Constants.prefCityName)
              .then((String value) {
            cityName = value;
            if (cityName != null) {
              subscription == null;
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/HomePage', (Route<dynamic> route) => false);
            } else {
              subscription == null;
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/CitySelectionPage', (Route<dynamic> route) => false);
            }
          });
        } else {
          Constants.showSnackBar(scaffoldState, Strings.wrongUsernameOrPass);
        }
      } else {
        Constants.showSnackBar(scaffoldState, Strings.wrongUsernameOrPass);
      }
    } else if (isEmail) {
      if (loginModel.registeredUserMasterId != 0) {
        if (loginModel.isEmailVerify) {
          PrefUtil.putStringPreference(Constants.prefPhone, loginModel.phone);
          PrefUtil.putBoolPreference(Constants.prefLogin, true);
          PrefUtil.putStringPreference(
              Constants.prefPassword, _passwordController.text);
          PrefUtil.putIntPreference(Constants.prefRegisterUserMasterId,
              loginModel.registeredUserMasterId);
          PrefUtil.putStringPreference(
              Constants.prefCustomerMasterId, loginModel.customerMasterId);
          PrefUtil.putBoolPreference(
              Constants.prefKycDetail, loginModel.isKYCDetails);
          PrefUtil.putStringPreference(
              Constants.prefSessionId, loginModel.sessionId);
          PrefUtil.putStringPreference(
              Constants.prefReferralCode, loginModel.referralCode);
          PrefUtil
              .getStringPreference(Constants.prefCityName)
              .then((String value) {
            cityName = value;
          });
          if (cityName != null) {
            subscription == null;
            Navigator.of(context).pushNamedAndRemoveUntil(
                '/HomePage', (Route<dynamic> route) => false);
          } else {
            subscription == null;
            Navigator.of(context).pushNamedAndRemoveUntil(
                '/CitySelectionPage', (Route<dynamic> route) => false);
          }
        } else {
          Constants.showSnackBar(scaffoldState, Strings.wrongUsernameOrPass);
        }
      } else {
        Constants.showSnackBar(scaffoldState, Strings.wrongUsernameOrPass);
      }
    }
  }

  _hidePassword() {
    setState(() {
      if (_obscure) {
        _obscure = false;
        _icon = Icons.visibility_off;
      } else {
        _obscure = true;
        _icon = Icons.remove_red_eye;
      }
    });
  }

  _showAlertAppVersion() {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: new SimpleDialog(

          contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          children: <Widget>[
            new Container(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    color: Colors.myColor,
                    child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          new Padding(padding: new EdgeInsets.only(top: 30.0)),
                          new Image(
                            image:
                                new AssetImage("images/ic_splash_icon_web.png"),
                            height: 60.0,
                            width: 60.0,
                            color: Colors.white,
                          ),
                          new Padding(padding: new EdgeInsets.only(top: 5.0)),
                          new Text(
                            "New version available",
                            style: Theme
                                .of(context)
                                .textTheme
                                .title
                                .apply(color: Colors.white),
                            textAlign: TextAlign.center,
                          ),
                          new Padding(padding: new EdgeInsets.only(top: 17.0)),
                        ]),
                  ),
                  new Container(
                    padding: new EdgeInsets.fromLTRB(17.0, 17.0, 17.0, 17.0),
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          new Text(
                            Strings.alertAppVersionContent,
                            style: Theme.of(context).textTheme.body1,
                          ),
                          new Padding(padding: new EdgeInsets.only(top: 17.0)),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              new FlatButton(
                                onPressed: () {
//                          exit(0);
                                },
                                child: new Text(Strings.labelAlertCancel),
                              ),
                              new FlatButton(
                                onPressed: () {
//                          _launchInWebViewOrVC(url1);
                                },
                                child: new Text(Strings.labelButtonUdpate),
                                textColor: Colors.myColor,
                              ),
                            ],
                          ),
                          /*new MaterialButton(
                            onPressed: () {
                              _launchInWebViewOrVC(url1);
                            },
                            child: new Text(Strings.labelButtonUdpate),
                            color: Colors.myColor,
                            textColor: Colors.white,
                          ),
                          new Padding(padding: new EdgeInsets.only(top: 10.0)),
                          new FlatButton(
                            onPressed: () {
                              exit(0);
                            },
                            child: new Text(Strings.labelAlertCancel),
                          )*/
                        ]),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  Future<Null> _launchInWebViewOrVC(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  void getToken(){
    firebaseMessaging.getToken().then((token) {
      firebaseToken = token;
      if(firebaseToken == null){
        PrefUtil.putStringPreference(Constants.prefFcmToken, "123456");
      }else {
        PrefUtil.putStringPreference(Constants.prefFcmToken, firebaseToken);
      }
    });
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    final result = await Navigator.push(context, rout);
    onResume();
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }
}
