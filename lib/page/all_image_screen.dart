import 'package:crazyrex/page/full_image_screen.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:flutter/material.dart';

class AllImageScreen extends StatefulWidget {
  final String title;
  final List<String> imageList;

  AllImageScreen(this.title, this.imageList);

  @override
  _AllImageScreenState createState() => _AllImageScreenState();
}

class _AllImageScreenState extends State<AllImageScreen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(title: new Text(widget.title),),
      body: new Stack(
        children: <Widget>[
          new Container(
            child: new GridView.builder(
              padding: EdgeInsets.fromLTRB(3.0, 5.0, 3.0, 5.0),
              itemCount: widget.imageList.length,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                itemBuilder: (BuildContext context,int index){
                  return new InkWell(
                    onTap: (){
                      String title = "";
                      if(widget.title == Strings.labelMenu){
                        title = Strings.labelMenu;
                      }else if(widget.title == Strings.labelPhotos){
                        title = Strings.labelPhoto;
                      }
                      var route = new MaterialPageRoute(builder: (BuildContext context) => new FullImageScreen(widget.imageList,title,index));
                      Navigator.push(context, route);
                    },
                    child: new GridTile(
                        child: new Card(
                          elevation: 1.0,
                          child: new FadeInImage(placeholder: new AssetImage("images/default1.png"), image: new NetworkImage(widget.imageList[index]),fit: BoxFit.cover),
                        )),
                  );
                }),
          )
        ],
      ),
    );
  }
}
