import 'package:flutter/material.dart';
import 'package:crazyrex/page/claim_card_screen.dart';
import 'package:crazyrex/util/strings.dart';

class SuccessBuyClaiamCardScreen extends StatefulWidget {
  final membershipName;

  SuccessBuyClaiamCardScreen(this.membershipName);

  @override
  _SuccessBuyClaiamCardScreenState createState() =>
      _SuccessBuyClaiamCardScreenState();
}

class _SuccessBuyClaiamCardScreenState
    extends State<SuccessBuyClaiamCardScreen> {
  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () {},
        child: new Scaffold(
          body: new Container(
            child: new Center(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    Strings.congratulation,
                    style: Theme
                        .of(context)
                        .textTheme
                        .display1
                        .apply(color: Colors.green),
                  ),
                  new Padding(padding: new EdgeInsets.only(top: 5.0)),
                  new Icon(
                    Icons.check_circle_outline,
                    size: 65.0,
                    color: Colors.myColor,
                  ),
                  Padding(padding: EdgeInsets.fromLTRB(28.0, 28.0, 28.0, 0.0)),
                  new Text("${Strings.buyMembershipSuccessMessage} ${widget.membershipName} ${Strings.buyMembershipSuccessMessage1}",
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.title,
                  ),
                  Padding(padding: EdgeInsets.only(top: 28.0)),
                  new MaterialButton(
                    onPressed: () {
                      Navigator.of(context).pushNamedAndRemoveUntil('/HomePage', (Route<dynamic> route) => false);
                    },
                    child: new Text(Strings.labelAlertOk),
                    textColor: Colors.white,
                    color: Colors.myColor,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

}
