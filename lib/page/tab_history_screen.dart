import 'package:crazyrex/model/history_model.dart';
import 'package:crazyrex/parser/history_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

class TabHistoryScreen extends StatefulWidget {
  final List<HistoryModel> historyList;
  final bool error;
  final String customerId;

  TabHistoryScreen(this.historyList, this.error,this.customerId);

  @override
  _TabHistoryScreenState createState() => _TabHistoryScreenState();
}

class _TabHistoryScreenState extends State<TabHistoryScreen>{
  SetUpHistoryListenerData setUpHistoryListenerData;

  List<HistoryModel> list;
  String _earnPoint;
  String _remainEarnPoint;
  String customerId;
  String _alertMessage;

  @override
  void initState() {
    super.initState();

    list = widget.historyList;
    customerId = widget.customerId;
    _general();
  }

  @override
  Widget build(BuildContext context) {
    if(widget.error){
      return _error();

    }else if(list.length==0){
      return _emptyScreen();
    } else {
      return _build();
    }
  }

  Widget _emptyScreen(){
    return new Stack(
      children: <Widget>[
        new Center(
          child:  new Container(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image(image: AssetImage("images/empty_history.png"),color: Colors.myColor,width: 65.0,height: 65.0,),
                new Padding(padding: new EdgeInsets.only(top: 20.0)),
                new Text(Strings.emptyHistoryScreen,style: Theme.of(context).textTheme.title.apply(color: Colors.myColor),)
              ],
            ),
          ),
        )
      ],
    );
  }
  
  Widget _build(){
    return new Container(
      child: new Center(
        child: new ListView(
          children: <Widget>[
            new Stack(
              children: <Widget>[
                new Image(image: AssetImage("images/history.png")),
                new Container(
                    padding: EdgeInsets.only(top: 70.0),
                    child: new Center(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Text(Strings.labelEarning, style: Theme
                              .of(context)
                              .textTheme
                              .title,),
                          new Text(_earnPoint, style: Theme
                              .of(context)
                              .textTheme
                              .title,),
                          _remainEarnPoint=="0"?new Container():new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new InkWell(
                                onTap: () {_showAlert();},
                                child: new Icon(
                                  Icons.help, color: Colors.myColor,),
                              ),
                              new Text(Strings.labelPendingBalance, style: Theme
                                  .of(context)
                                  .textTheme
                                  .body1,),
                              new Text(_remainEarnPoint, style: Theme
                                  .of(context)
                                  .textTheme
                                  .body1),
                            ],
                          )
                        ],
                      ),
                    )
                )
              ],
            ),
            new Container(
              child: new ListView.builder(
                shrinkWrap: true,
                  primary: false,
                  itemCount: list.length,
                  itemBuilder: (BuildContext context,int index){
                    HistoryModel model = list[index];
                    return list[index].point == "0"? new Container():
                    new Card(elevation:2.0,
                      margin: EdgeInsets.fromLTRB(6.0, 6.0, 6.0, index == list.length-1?6.0:0.0),
                      child: ListTile(
                        title: _description(model),
                        trailing:  new Text("+${model.point}",style: Theme.of(context).textTheme.body1.apply(color: Colors.green),),
                        subtitle: _referDate(model),
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
          ],
        ));
  }

  _general() {
    if(list.length==0){
      _earnPoint = "\u20B9" +"0";
    }else{
        for(int i=0;i<list.length;i++){
          _earnPoint = "\u20B9" + list[0].earnPoint.toString();
          _remainEarnPoint = list[0].remainingEarnPoint.toString();
          _alertMessage = Strings.alertRemainingEarnPointMessage+list[0].minimumPointLimit.toString()+Strings.alertRemainingEarnPointMessage1;
        };
    }
  }

  Widget _description(HistoryModel model){
    if(model.idReferBy == 0){
      return new Text(Strings.labelEarnPointDescriptionFirst);
    }else if(model.idReferBy> 0 && model.idUsedBy> 0 && model.linkToMembershipTypeMasterId!=""  && model.idReferBy.toString() == customerId){
      return new Text("${model.usedCustomerFirstName} ${model.usedCustomerLastName}${Strings.labelEarnPointDescriptionSecond}");
    }else if(model.idReferBy > 0 && model.idUsedBy > 0 && model.idReferBy.toString() == customerId){
      return new Text("${model.usedCustomerFirstName} ${model.usedCustomerLastName}${Strings.labelEarnPointDescriptionThird}");
    }else if(model.idUsedBy.toString() == customerId){
      return new Text("${model.referCustomerFirstName} ${model.referCustomerLastName}${Strings.labelEarnPointDescriptionForth}");
    }else{
      return new Container();
    }
  }

  Widget _referDate(HistoryModel model) {
    return new Text("${formatDate(DateTime.parse(model.referDateTime), [dd, '-', mm, '-', yyyy])}");
  }

   _showAlert() {
   AlertDialog dialog = new AlertDialog(
     content: new Text(_alertMessage),
     actions: <Widget>[
       new FlatButton(onPressed: (){Navigator.pop(context);}, child: new Text(Strings.labelGotIt))
     ],
   );
   showDialog(context: context,child: dialog);
   }

}
