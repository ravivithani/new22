import 'dart:async';
import 'dart:convert';

import 'package:crazyrex/model/purchased_history_model.dart';
import 'package:crazyrex/parser/purchased_history_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:intl/intl.dart';

class PurchasedHistoryScreen extends StatefulWidget {
  @override
  _PurchasedHistoryScreenState createState() => _PurchasedHistoryScreenState();
}

class _PurchasedHistoryScreenState extends State<PurchasedHistoryScreen>
    with PurchasedHistoryListener {
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isLoading;
  bool isError = false;

  List<PurchasedHistoryModel> purchasedHistoryList = [];
  SetUpPurchasedHistoryListener setUpPurchasedHistoryListener;

  String customerMasterId;


  _PurchasedHistoryScreenState() {
    PrefUtil
        .getStringPreference(Constants.prefCustomerMasterId)
        .then((String value) {
      customerMasterId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
  }

  @override
  void dispose() {
    subscription = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          callApi();
        }
        return _build();
      } else {
        return _error();
      }
    } else {
      return _noInternet();
    }
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        setState(() {
          internetConnection = true;
          isLoading = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {
                      _internetConnection();
                    });
                  })
            ],
          ),
        ),
      ],
    ));
  }

  Widget _noData() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image(
                image: new AssetImage("images/ic_table_search_black.png"),
                color: Colors.myColor,
                height: 65.0,
                width: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyPurchasedHistoryList,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme
                      .of(context)
                      .textTheme
                      .title
                      .apply(color: Colors.myColor)),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _build() {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(Strings.purchasedHistoryScreenTitle),
        ),
        body: isLoading
            ? Scaffold(
                body: new Center(
                  child: new CircularProgressIndicator(),
                ),
              )
            : purchasedHistoryList.length != 0 ? listView() : _noData());
  }

  Widget listView() {
    return new Container(
        child: new ListView.builder(
            itemCount: purchasedHistoryList.length,
            itemBuilder: (BuildContext context, int index) {
              final PurchasedHistoryModel model = purchasedHistoryList[index];
              return new Card(
                elevation: 2.0,
                  margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0,
                      index == purchasedHistoryList.length - 1 ? 6.0 : 0.0),
                  child: new InkWell(
                    onTap: () {
//                      _clickOnItem(offer);
                    },
                    child: new Container(
                      padding: EdgeInsets.all(8.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              Expanded(
                                  flex: 5,
                                  child: new Container(
                                    child: new Text(
                                      model.Description,
                                      textAlign: TextAlign.start,
                                      style: Theme.of(context).textTheme.body1,
                                    ),
                                  )),
                              Flexible(
                                  flex: 1,
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      new Text("\u20B9${model.Amount.toString()}",
                                          textAlign: TextAlign.end,
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .body2
                                              .apply(color: Colors.myColor)),
                                    ],
                                  )),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 5.0)),
                          subAmount(model.lstPayment),
                          Padding(padding: EdgeInsets.only(top: 5.0)),
                          _date(model),
                        ],
                      ),
                    ),
                  ));
            }));
  }

  Widget subAmount(List list) {
    try {
      var a = list[0].PaymentModeName;
      ListPaymentModel s = list[0];
      String ss = s.PaymentModeName;
    } catch (e) {
      print(e);
    }
    if (list.length > 1) {
      return new Column(
        children: <Widget>[
          new Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              new Text(
                "${list[0].PaymentModeName} \u20B9${list[0].Amount}",
                textAlign: TextAlign.end,
                style: Theme.of(context).textTheme.caption,
              ),
            ],
          ),
          new Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              new Text(
                  "${list[1].PaymentModeName} \u20B9${list[1].Amount}",
                  textAlign: TextAlign.end,
                  style: Theme.of(context).textTheme.caption),
            ],
          ),
        ],
      );
    } else {
      return new Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          new Text("${list[0].PaymentModeName} \u20B9${list[0].Amount}",
              textAlign: TextAlign.end,
              style: Theme.of(context).textTheme.caption),
        ],
      );
    }
  }

  Widget _date(PurchasedHistoryModel model) {
    String date;
    DateFormat dateFormat = new DateFormat("yyyy-MM-dd");
    DateTime dateTime = dateFormat.parse(model.TransactionDateTime);
    date = new DateFormat("dd-MM-yyyy").format(dateTime);
    return new Text(date,
        textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption);
  }

  void callApi() {
    String url = "${Constants
        .url}SelectAllCustomerPaymentTransactions/$customerMasterId";
    setUpPurchasedHistoryListener =
        new SetUpPurchasedHistoryListener(this, url);
    setUpPurchasedHistoryListener.loadPurchasedHistoryList();
  }

  @override
  void onErrorPurchasedHistory(String error) {
    print(error);
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onGetPurchasedHistoryList(List<PurchasedHistoryModel> list) {
    setState(() {
      isLoading = false;
      purchasedHistoryList = list;
    });
  }
}
