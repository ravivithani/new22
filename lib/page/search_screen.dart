import 'dart:async';

import 'package:crazyrex/model/search_model.dart';
import 'package:crazyrex/page/all_store_tab_screen.dart';
import 'package:crazyrex/page/store_detail_screen.dart';
import 'package:crazyrex/parser/search_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> with SearchListener {
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isLoading;
  bool isError = false;

  List<SearchModel> list = [];
  List<SearchModel> searchResult = [];
  TextEditingController controller = new TextEditingController();

  SetUpSearchListener setUpSearchListener;

  int cityId;

  _SearchScreenState() {
    PrefUtil.getIntPreference(Constants.prefCityId).then((int value) {
      cityId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
  }

  @override
  void dispose() {
    subscription = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          callApi();
        }
        return isLoading == null || isLoading
            ? Scaffold(
                body: new Center(
                  child: new CircularProgressIndicator(),
                ),
              )
            : Scaffold(
                appBar: new AppBar(
                    bottom: new PreferredSize(
                  preferredSize: new Size(0.0, 0.0),
                  child: new Container(
                      padding: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 4.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: new IconButton(
                                icon: Icon(
                                  Icons.arrow_back,
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                }),
                          ),
                          Expanded(
                              flex: 6,
                              child: new Form(
                                  child: new Theme(
                                      data: new ThemeData(
                                          textSelectionColor: Colors.myColor,
                                          primaryColor: Colors.myColor),
                                      child: new Container(
                                          decoration: new BoxDecoration(
                                            borderRadius:
                                                new BorderRadius.circular(5.0),
                                            border: new Border.all(
                                                width: 1.0,
                                                color: Colors.black54),
                                          ),
                                          height: 40.0,
                                          padding: new EdgeInsets.all(8.0),
                                          child: new Container(
                                            child: new TextField(
                                              controller: controller,
                                              decoration:
                                                  new InputDecoration.collapsed(
                                                hintText: Strings.hintSearch,
                                                hintStyle: new TextStyle(
                                                    color: Colors.black54),
                                              ),
                                              onChanged: onSearchTextChanged ,
                                            ),
                                          ))))),
                          new Padding(padding: new EdgeInsets.all(4.0))
                        ],
                      )),
                )),
                body: controller.text.isNotEmpty ? _searchList() : new Container(),
              );
      } else {
        return _error();
      }
    } else {
      return _noInternet();
    }
  }

  Widget _List() {
    return new Container(
      child: new Column(
        children: <Widget>[
          new Flexible(
              child: new ListView.builder(
                  itemCount: list.length,
                  itemBuilder: (BuildContext context, int index) {
                    final SearchModel model = list[index];
                    return new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new ListTile(
                          title: new Container(
                            child: new Text(
                              model.searchText,
                              style: Theme.of(context).textTheme.body1,
                            ),
                          ),
                          onTap: () {
//                            _insertData(city);
                          },
                        ),
                        new Container(
                          foregroundDecoration: new ViewLineFromRegistration(),
                        )
                      ],
                    );
                  }))
        ],
      ),
    );
  }

  onSearchTextChanged(String text) async {
    searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    list.forEach((SearchModel model) {
      if (model.searchText.toLowerCase().contains(text.toLowerCase()))
        searchResult.add(model);
    });
    setState(() {});
  }

  Widget _searchList() {
    if (searchResult.length != 0) {
      return new Container(
        child: new Column(
          children: <Widget>[
            new Flexible(
                child: new ListView.builder(
                    itemCount: searchResult.length,
                    itemBuilder: (BuildContext context, int index) {
                      final SearchModel model = searchResult[index];
                      return new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new ListTile(
                            title: new Container(
                              child: new Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 5,
                                    child: new Text(model.searchText,style: Theme.of(context).textTheme.body1,),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: new Text(model.type,textAlign: TextAlign.end,style: Theme.of(context).textTheme.caption,),
                                  )
                                ],
                              ),
                            ),
                            onTap: () {
                              _itemClick(model.type,model.rowId);
                            },
                          ),
                          new Container(
                            foregroundDecoration:
                                new ViewLineFromRegistration(),
                          )
                        ],
                      );
                    }))
          ],
        ),
      );
    } else {
      return _noSearchFound();
    }
  }

  _itemClick(String type,int rowId ){
    if (type == "Category") {
      var route = new MaterialPageRoute(builder: (BuildContext context) =>new AllStoreTabScreen(false, true, rowId));
      _navigateAndReturn(context, route, 0);
    } else if (type == "Store"){
      var route = new MaterialPageRoute(builder: (BuildContext context) => new StoreDetailScreen(rowId));
      _navigateAndReturn(context, route, 0);
    }
  }

  Widget _noSearchFound() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image(
                image: new AssetImage("images/ic_table_search_black.png"),
                color: Colors.myColor,
                height: 65.0,
                width: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noSearchFound,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title.apply(color: Colors.myColor)),
            ],
          ),
        ),
      ],
    ));
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        setState(() {
          internetConnection = true;
          isLoading = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {
                      _internetConnection();
                    });
                  })
            ],
          ),
        ),
      ],
    ));
  }

  callApi() {
    String url = "${Constants.url}SelectAllSearchDetails/$cityId";
    setUpSearchListener = new SetUpSearchListener(url, this);
    setUpSearchListener.loadAllSearchListData();
  }

  @override
  void onError(String error) {
    print("error in serch $error");
    setState(() {
      isError = true;
      isLoading = false;
    });
  }

  @override
  void onSearchListener(List<SearchModel> searchList) {
    setState(() {
      isLoading = false;
      list = searchList;
    });
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);
          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            setState(() {
              internetConnection = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }
}
