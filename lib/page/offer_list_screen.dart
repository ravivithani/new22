import 'dart:async';

import 'package:crazyrex/model/advertisement_model.dart';
import 'package:crazyrex/model/offerlist_model.dart';
import 'package:crazyrex/page/all_store_tab_screen.dart';
import 'package:crazyrex/page/offer_description_screen.dart';
import 'package:crazyrex/page/store_detail_screen.dart';
import 'package:crazyrex/parser/advertisement_parser.dart';
import 'package:crazyrex/parser/offer_list_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/CarouselImageSlider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';
import 'package:share/share.dart';

enum MenuItems { share, store }

class OfferListScreen extends StatefulWidget {
  final String title;
  final int businessTypeMasterId;
  final int businessMasterId;
  List<OfferListModel> offerListOfStore = new List();
  bool isStore = false;

  OfferListScreen({this.title, this.businessTypeMasterId, this.businessMasterId,this.offerListOfStore}){
    if(offerListOfStore != null){
      isStore = true;
    }
  }

  @override
  _OfferListScreenState createState() => _OfferListScreenState(offerListOfStore,isStore);
}

class _OfferListScreenState extends State<OfferListScreen>with OfferListListener, AdvertisementListener, HandleClickOnImage {
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isLoading;
  bool isError = false;

  int cityId;
  String customerMasterId;
  List<OfferListModel> offerList = [];
  List<AdvertisementModel> advertisementList = [];
  SetUpOfferListListener setUpOfferListListener;
  SetUpAdvertisementListener setUpAdvertisementListener;
  bool isStore = false;

  //region share
  String packageName;
  String appName;

  String buildNumber;
  PackageInfo _packageInfo = new PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<Null> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      packageName = info.packageName;
      appName = info.appName;
      buildNumber = info.buildNumber;
    });
  }

  //endregion


  _OfferListScreenState(this.offerList,this.isStore) {
    PrefUtil.getIntPreference(Constants.prefCityId).then((int value) {
      cityId = value;
    });
    PrefUtil
        .getStringPreference(Constants.prefCustomerMasterId)
        .then((String value) {
      customerMasterId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
    _initPackageInfo();
  }

  @override
  void dispose() {
    subscription = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          callApi();
        }
        return _build();
      } else {
        return _error();
      }
    } else {
      return _noInternet();
    }
  }

  Widget _build() {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
        actions: <Widget>[],
      ),
      body: isLoading
          ? Scaffold(
              body: new Center(
                child: new CircularProgressIndicator(),
              ),
            )
          : offerList!=null
              ? offerList.length != 0
              ? new ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    advertisementList.isNotEmpty?_imageSlider():new Container(),
                    new Container(
                        height: 63.0,
                        child: new InkWell(
                          onTap: () {
                            _clickOnAllStore();
                          },
                          child: new Card(
                            elevation: 2.0,
                            margin: new EdgeInsets.fromLTRB(6.0, 8.0, 6.0, 0.0),
                            child: new Container(
                              child: new Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: new Row(
                                      children: <Widget>[
                                        Padding(
                                            padding: new EdgeInsets.fromLTRB(
                                                15.0, 0.0, 0.0, 0.0)),
                                        new Icon(
                                          Icons.store_mall_directory,
                                          size: 27.0,
                                          color: Colors.black54,
                                        ),
                                        Padding(
                                            padding: new EdgeInsets.fromLTRB(
                                                15.0, 0.0, 0.0, 0.0)),
                                        new Text("All Stores",
                                            softWrap: true,
                                            textAlign: TextAlign.center,
                                            style: Theme
                                                .of(context)
                                                .textTheme
                                                .body2),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: new Container(
                                        margin: new EdgeInsets.only(right: 8.0),
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: <Widget>[
                                            new Text(Strings.labelShowMe,
                                                softWrap: true,
                                                textAlign: TextAlign.end,
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .body2
                                                    .apply(
                                                        color: Colors.myColor)),
                                            new Icon(
                                              Icons.navigate_next,
                                              color: Colors.myColor,
                                            ),
                                          ],
                                        )),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )),
                    offerListView(),
                  ],
                )
              : _noData()
              : new Container(),
    );
  }

  Widget _imageSlider() {
    var images = [];
    for (int i = 0; i < advertisementList.length; i++) {
      images.add(new NetworkImage(advertisementList[i].md_ImageName));
    }
    return new Container(
        margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
        height: 130.0,
        child: new ClipRRect(
          borderRadius: BorderRadius.circular(5.0),
          child: Carousel(
            boxFit: BoxFit.cover,
            autoplay: true,
            clickListener: this,
            image: images,
            indicatorBgPadding: 10.0,
            dotSize: 0.0,
            dotSpacing: 15.0,
            dotBgColor: Colors.transparent,
            animationCurve: Curves.easeInOut,
            animationDuration: Duration(seconds: 2),
            autoplayDuration: Duration(seconds: 4),
          ),
        ));
  }

  Widget offerListView() {
    return new Container(
        child: new ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: offerList.length,
            itemBuilder: (BuildContext context, int index) {
              final OfferListModel offer = offerList[index];
              return new Card(elevation: 2.0,
                  margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, index == offerList.length-1?6.0:0.0),
                  child: new InkWell(
                    onTap: () {
                      _clickOnItem(offer);
                    },
                    child: new Container(
                      padding: new EdgeInsets.all(12.0),
                      child: new Stack(
                        alignment: Alignment(1.1, -2.0),
                        children: <Widget>[
                          new PopupMenuButton<MenuItems>(
                            icon: Icon(
                              Icons.more_vert,
                              color: Colors.black54,
                            ),
                            onSelected: (MenuItems result) {
                              if (result == MenuItems.share) {
                                share(offer);
                              } else if (result == MenuItems.store) {
                                var route = new MaterialPageRoute(builder: (BuildContext context) => new StoreDetailScreen(offer.linktoBusinessMasterId));
                                _navigateAndReturn(context, route, 0);
                              }
                            },
                            itemBuilder: (BuildContext context) =>
                                <PopupMenuEntry<MenuItems>>[
                                  PopupMenuItem<MenuItems>(
                                    value: MenuItems.share,
                                    child: Text(
                                      'Share',
                                      textAlign: TextAlign.end,
                                    ),
                                  ),
                                  PopupMenuItem<MenuItems>(
                                    value: MenuItems.store,
                                    child: Text("Show all ${offer.BusinessName} offers"),
                                  ),
                                ],
                          ),
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: new Text(
                                      offer.OfferTitle,
                                      softWrap: false,
                                      style: Theme.of(context).textTheme.body2,
                                      maxLines: 2,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: new Container(
                                      height: 10.0,
                                      width: 10.0,
                                      child: new Container(),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(padding: EdgeInsets.only(top: 10.0)),
                              new Row(
                                children: <Widget>[
                                  new Flexible(
                                    child: new Row(
                                      children: <Widget>[
                                        new Container(
                                          height: 40.0,
                                          width: 40.0,
                                          decoration: new BoxDecoration(
                                            border: Border.all(width: 0.3),
                                            image: DecorationImage(
                                              image: offer.xs_ImageName != null
                                                  ? NetworkImage(
                                                      offer.xs_ImageName)
                                                  : AssetImage(
                                                      "images/default1.png"),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                            padding:
                                                EdgeInsets.only(left: 8.0)),
                                        new Flexible(
                                          child: new Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              new Text(
                                                offer.BusinessName,
                                                maxLines: 2,
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .body1,
                                              ),
                                              new Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 5.0)),
                                              new Text(
                                                offer.BusinessType,
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .caption,
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    margin:
                                        EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                                    width: 0.5,
                                    height: 50.0,
                                    color: Colors.black54,
                                  ),
                                  new Expanded(
                                    child: new Container(
                                      height: 40.0,
                                      child: new Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          new Text(
                                            offer.TotalRedeemOffer!=0?"${offer.TotalRedeemOffer} times used":Strings.beTheFirstUser,
                                            textAlign: TextAlign.end,
                                            maxLines: 2,
                                            softWrap: true,
                                            style: Theme
                                                .of(context)
                                                .textTheme
                                                .caption,
                                          ),
                                          validTillText(offer),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ));
            })
    );
  }

  Widget _noData() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.local_offer,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyOfferList,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme
                      .of(context)
                      .textTheme
                      .title
                      .apply(color: Colors.myColor)),
            ],
          ),
        ),
      ],
    ));
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        setState(() {
          internetConnection = true;
          isLoading = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {
                      _internetConnection();
                    });
                  })
            ],
          ),
        ),
      ],
    ));
  }

  Widget validTillText(OfferListModel model) {
    String date;
    if (model.ToDate != null && model.ToDate.isNotEmpty) {
      DateFormat dateFormat = new DateFormat("yyyy-MM-dd");
      DateTime dateTime = dateFormat.parse(model.ToDate);
      date = new DateFormat("dd-MM-yyyy").format(dateTime);
    }
    String text = date.isEmpty ? "Valid till \u221E" : "Valid till $date";
    return new Text(text,
        textAlign: TextAlign.end, style: Theme.of(context).textTheme.caption);
  }

  callApi() {
    String url = "${Constants.url}SelectAllOfferMasterByBusinessType/${widget.businessTypeMasterId}/${cityId}/${customerMasterId}";
    String returnURL = "SelectAllOfferMasterByBusinessTypeResult";
    String advertisementURL;
    if (!isStore) {
      advertisementURL ="${Constants.url}SelectAllAdvertisementMasterDAL/3/${widget.businessTypeMasterId}/${cityId}";
      setUpOfferListListener = new SetUpOfferListListener(url, returnURL, this);
      setUpOfferListListener.loadOfferList();
    } else {
      advertisementURL ="${Constants.url}SelectAllAdvertisementMasterDAL/2/${widget.businessTypeMasterId}/${cityId}";
    }
    setUpAdvertisementListener = new SetUpAdvertisementListener(advertisementURL, this);
    setUpAdvertisementListener.loadAds();
  }

  share(OfferListModel model) {
    String appUrlForAndroid = "http://play.google.com/store/apps/details?id=${packageName}";
    String appUrlForIphone = "http://itunes.apple.com/+91/app/${_packageInfo.appName}/id$packageName?mt=8";
    final RenderBox box = context.findRenderObject();
    Share.share(
        "${model.OfferTitle} at ${model.AreaName}, ${model
            .CityName} .\nDownload CrazyRex: ${defaultTargetPlatform ==
            TargetPlatform.iOS ? appUrlForIphone : appUrlForAndroid}",
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  // region click
  _clickOnItem(OfferListModel model) {
    var route = new MaterialPageRoute(builder: (BuildContext context) => new OfferDescriptionScreen(isFromStore: false,offerId:model.OfferMasterId,businessMasterId:model.linktoBusinessMasterId,businessName:model.BusinessName));
    _navigateAndReturn(context, route, 0);
  }

  _clickOnAllStore() {
    var route = new MaterialPageRoute(builder: (BuildContext context) =>new AllStoreTabScreen(false, true, widget.businessTypeMasterId));
    _navigateAndReturn(context, route, 0);
  }

  @override
  void onClickItemOfSlider(int position) {
    print("taped image- ${advertisementList[position].toString()}");
    if (advertisementList[position].advertisementTypeId == 1) {
      var route = new MaterialPageRoute(builder: (BuildContext context) => new OfferDescriptionScreen(isFromStore: false,offerId:advertisementList[position].linktoOfferMasterId,businessMasterId:advertisementList[position].linktoBusinessMasterId,businessName:advertisementList[position].businessName));
      _navigateAndReturn(context, route, 0);
    } else if (advertisementList[position].advertisementTypeId == 2) {
      var route = new MaterialPageRoute(
          builder: (BuildContext context) => new StoreDetailScreen(advertisementList[position].linktoBusinessMasterId));
      _navigateAndReturn(context, route, 0);
    }
  }

  //endregion

  @override
  void onErrorOfferListListener(String error) {
    print("error in offerlist $error");
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onOfferListener(List<OfferListModel> list) {
    print(list);
    setState(() {
      offerList = list;
    });
  }

  @override
  void onErrorAdvertisement(String error) {
    print("Error in advertisement $error");
    setState(() {
      isError = true;
    });
  }

  @override
  void onGetAdvertisementList(List<AdvertisementModel> adList) {
    print("AdList $adList");
    setState(() {
      isLoading = false;
      advertisementList = adList;
    });
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }
}
