import 'dart:async';

import 'package:crazyrex/page/tab_offer_screen.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class MyOfferScreen extends StatefulWidget {
  final bool isFromOffer;

  MyOfferScreen({this.isFromOffer}):assert(isFromOffer!=null);

  @override
  _MyOfferScreenState createState() => _MyOfferScreenState();
}

class _MyOfferScreenState extends State<MyOfferScreen> with SingleTickerProviderStateMixin{

  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isLoading;
  bool isError = false;

  TabController tabController;
  int cityId;
  String customerMasterId;

  _MyOfferScreenState(){
    PrefUtil.getIntPreference(Constants.prefCityId).then((int value) {
      cityId = value;
    });
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((String value) {
      customerMasterId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    tabController = new TabController(length: 2, vsync: this);
    _internetConnection();
  }

  @override
  void dispose() {
    subscription = null;
    tabController = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if(widget.isFromOffer){
          tabController.animateTo(1);
        }
        return new Scaffold(
          appBar: new AppBar(
            title: new Text(Strings.labelForTitle),
            bottom: new TabBar(
              controller: tabController,
              labelColor: Colors.myColor,
              unselectedLabelColor: Colors.black54,
              labelStyle: Theme.of(context).textTheme.body1,
              unselectedLabelStyle: Theme.of(context).textTheme.body1,
                tabs: <Tab>[
                  new Tab(text: Strings.labelForTabSaved ),
                  new Tab(text: Strings.labelForTabUsed,)
                ]
            ),
          ),
         body: new TabBarView(
          controller: tabController,
          children: <Widget>[
            new TabOfferScreen(cityId,customerMasterId,false),
            new TabOfferScreen(0,customerMasterId,true),
          ],
        ),
        );
      }else{
        return _error();
      }
    }else{
      return _noInternet();
    }
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            setState(() {
              internetConnection = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {
                          _internetConnection();
                        });
                      })
                ],
              ),
            ),
          ],
        ));
  }

}
