import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math' as math;

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

class TestPage extends StatefulWidget {

  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> with TickerProviderStateMixin {
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();

  String message = '',
      title = '',
      notificationImage = '',
      BusinessName = '';
  int id = 0;
  int type = 0;
  int notificationId = 0;
  int linktoBusinessMasterId = 0;

  String groupKey = 'com.arraybit.crazyRex';
  String groupChannelId = 'crazyRex';
  String groupChannelName = 'crazyRex Flutter';
  String groupChannelDescription = 'crazyRex Flutter Offer App';

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

  AnimationController controller;

  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void initState() {
    super.initState();
    //    var initializationSettingsAndroid = new AndroidInitializationSettings('mipmap/ic_launcher');
//    var initializationSettingsIOS = new IOSInitializationSettings();
//    var initializationSettings = new InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
//    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
//    flutterLocalNotificationsPlugin.initialize(
//        initializationSettings,
//        selectNotification: onSelectNotification
//    );
//
//    configureFirebase();
//
//    getToken();
//
//    iosSetting();
    controller = AnimationController(vsync: this,
      duration: Duration(seconds: 60),
    );

    if(controller.isAnimating){
      controller.stop();
    }else{
      controller.reverse(from: controller.value == 0.0 ? 1.0: controller.value);
    }
  }

  void configureFirebase() {
    firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> msg) {
        print(msg);
        setData(msg);
      },
      onResume: (Map<String, dynamic> msg) {
        print(msg);
        setData(msg);
      },
      onMessage: (Map<String, dynamic> msg) {
        print(msg);
        setData(msg);
      },
    );
  }

  void setData(Map<String, dynamic> map) {
    dynamic data = map['data'];
    Map msg = JSON.decode(data);
    if (msg.containsKey('message')) {
      message = msg['message'];
    }
    if (msg.containsKey('title')) {
      title = msg['title'];
    }
    if (msg.containsKey('image')) {
      notificationImage = msg['image'];
    }
    if (msg.containsKey('type')) {
      type = msg['type'];
    }
    if (msg.containsKey('id')) {
      id = msg['id'];
    }
    if (msg.containsKey('notificationId')) {
      notificationId = msg['notificationId'];
    }
    if (msg.containsKey('linktoBusinessMasterId')) {
      linktoBusinessMasterId = msg['linktoBusinessMasterId'];
    }
    if (msg.containsKey('BusinessName')) {
      BusinessName = msg['BusinessName'];
    }
    if (notificationImage.isEmpty) {
      _showNotification();
    } else {
      _showBigPictureNotification();
    }
    setState(() {});
  }

  void getToken() {
    firebaseMessaging.getToken().then((token) {
      print(token);
      Fluttertoast.showToast(msg: "firebase Token : $token");
    });
  }

  void iosSetting() {
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, alert: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings setting) {
      print("registerd setting ${setting}");
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: new AppBar(
        title: new Text("Test Claim Card Design"),
        actions: <Widget>[
          new FlatButton(onPressed: (){
            if(controller.isAnimating){
              controller.stop();
            }else{
              controller.reverse(from: controller.value == 0.0 ? 1.0: controller.value);
            }
          }, child: new Text("Play"))
        ],
      ),
      body: AnimatedBuilder(
        animation: controller,
        builder: (BuildContext context,Widget child){
          return new Center(
            child: Text(
              timerString,
              style: Theme.of(context).textTheme.body2.apply(color: Colors.myColor),
            ),
          );
        },
      ),
    );
  }
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: new AppBar(
//        title: new Text("Test Claim Card Design"),
//        actions: <Widget>[
//          new FlatButton(onPressed: (){
//            if(controller.isAnimating){
//              controller.stop();
//            }else{
//              controller.reverse(from: controller.value == 0.0 ? 1.0: controller.value);
//            }
//          }, child: new Text("Play"))
//        ],
//      ),
//      body: Padding(padding: EdgeInsets.all(8.0),
//        child: new Column(
//            children: <Widget>[
//              Expanded(
//                child: Align(
//                  alignment: FractionalOffset.center,
//                  child: AspectRatio(aspectRatio: 1.0,
//                    child: Stack(
//                      children: <Widget>[
//                        Positioned.fill(child: AnimatedBuilder(
//                            animation: controller,
//                            builder: (BuildContext context, Widget child) {
//                              return new CustomPaint(
//                                painter: TimerPainter(
//                                  animation: controller,
//                                  backgroundcolor: Colors.yellowAccent,
//                                  color: Colors.myColor,
//                                ),
//                              );
//                            }))
//                      ],
//                    )
//                    ,),
//                ),
//              ),
//              Align(
//                alignment: FractionalOffset.center,
//                child: Column(
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                  crossAxisAlignment: CrossAxisAlignment.center,
//                  children: <Widget>[
//                    Text(
//                      "count down"
//                      , style: Theme
//                        .of(context)
//                        .textTheme
//                        .subhead,
//                    ),
//                    AnimatedBuilder(
//                      animation: controller,
//                      builder: (BuildContext context,Widget child){
//                        return new Text(
//                          timerString,
//                          style: Theme.of(context).textTheme.display4,
//                        );
//                      },
//                    )
//
//                  ],
//                )
//                ,)
//            ]),
//      ),);
//  }

  Widget claimCard() {
    double height = MediaQuery
        .of(context)
        .size
        .height / 1.5;
    double width = MediaQuery
        .of(context)
        .size
        .width / 1.3;
    print("$height - $width");
    return new Center(
      child: new Container(
        child: new Card(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(25.0)),
          child: new SizedBox(
              height: height,
              width: width,
              child: new Stack(
                children: <Widget>[
                  new Image.asset(
                    "images/bg33.png",
                    height: height,
                    fit: BoxFit.fitHeight,
                  ),
                  new Column(
                    children: <Widget>[
                      new Flexible(
                          flex: 3,
                          child: new Container(
                            color: Colors.black12,
                            padding: EdgeInsets.all(10.0),
                            child: new Column(
                              children: <Widget>[
                                new Flexible(
                                    flex: 1,
                                    child: new Container(
                                      child: new Image.asset(
                                        "images/logo_with_name.png",
//                                   height: 65.0,
                                      ),
                                    )
                                ),
                                new Flexible(
                                    flex: 2,
                                    child: new Container(
                                      child: new Row(
                                        children: <Widget>[
                                          new Flexible(
                                              flex: 3,
                                              child: new Container(
                                                child: new Column(
                                                  mainAxisAlignment: MainAxisAlignment
                                                      .center,
                                                  crossAxisAlignment: CrossAxisAlignment
                                                      .start,
                                                  children: <Widget>[
                                                    new Row(
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .start,
                                                      children: <Widget>[
                                                        new Icon(Icons.person),
                                                        new Padding(
                                                            padding: EdgeInsets
                                                                .only(
                                                                left: 5.0)),
                                                        new Flexible(
                                                          child: new Text(
                                                            "Vithani Chandesh",
                                                            textAlign: TextAlign
                                                                .start,
                                                            softWrap: true,
                                                            maxLines: 1,
                                                            overflow: TextOverflow
                                                                .fade,),),
                                                      ],
                                                    ),
                                                    new Row(
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .start,
                                                      children: <Widget>[
                                                        new Icon(Icons.phone),
                                                        new Padding(
                                                            padding: EdgeInsets
                                                                .only(
                                                                left: 5.0)),
                                                        new Flexible(
                                                          child: new Text(
                                                            "9662882578",
                                                            textAlign: TextAlign
                                                                .start,
                                                            softWrap: true,
                                                            maxLines: 1,
                                                            overflow: TextOverflow
                                                                .fade,),)
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              )
                                          ),
                                          new Flexible(
                                              flex: 2,
                                              child: new Container(
                                                child: new Center(
                                                  child: new Icon(
                                                      Icons.account_circle,
                                                      size: height / 4.5),
                                                ),
                                              )
                                          ),
                                        ],
                                      ),
                                    )
                                ),
                              ],
                            ),
                          )
                      ),
                      new Flexible(
                          flex: 3,
                          child: new Container(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text(
                                    "DMD4 7758 1123",
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .headline
                                        .apply(color: Colors.myColor),
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                  ),
                                  new Padding(
                                      padding: EdgeInsets.only(
                                          top: height / 35)),
                                  new QrImage(
                                    data: "DMD4 7758 1123",
                                    size: 100.0,
                                  ),
                                ],
                              )
                          )
                      ),
                      new Expanded(
                          flex: 1,
                          child: new Container(
                            padding: EdgeInsets.only(left: 20.0, right: 20.0),
                            color: Colors.black12,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Expanded(
                                    child: new Text(
                                      "Silver",
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .body1,
                                      maxLines: 1,)),
                                new Text(
                                  "Valid up to 07-08-2018",
                                  style:
                                  Theme
                                      .of(context)
                                      .textTheme
                                      .body1,
                                  maxLines: 1,)
                              ],
                            ),
                          )
                      ),
                    ],
                  ),
                ],
              )
          ),
        ),
      ),
    );
  }

  Future _showNotification() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        groupChannelId, groupChannelName, groupChannelDescription,
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, title, message, platformChannelSpecifics);
  }

  Future _showBigPictureNotification() async {
    //region big picture
    var directory = await getApplicationDocumentsDirectory();

    //region large icon
    var largeIconResponse = await http.get(notificationImage);
    var largeIconPath = '${directory.path}/largeIcon';
    var file = new File(largeIconPath);
    await file.writeAsBytes(largeIconResponse.bodyBytes);
    //endregion

    //region big image
    var bigPictureResponse = await http.get(notificationImage);
    var bigPicture = '${directory.path}/bigPicture';
    file = new File(bigPicture);
    await file.writeAsBytes(bigPictureResponse.bodyBytes);
    var bigPictureStyleInformation = new BigPictureStyleInformation(
        bigPicture,
        BitmapSource.FilePath,
        largeIcon: /*largeIconPath*/'mipmap/ic_launcher',
        largeIconBitmapSource: BitmapSource.Drawable,
        contentTitle: title,
        htmlFormatContentTitle: true,
        summaryText: message,
        htmlFormatSummaryText: true);
    //endregion

    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        groupChannelId, groupChannelName, groupChannelDescription,
        style: AndroidNotificationStyle.BigPicture,
        styleInformation: bigPictureStyleInformation);
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, null);
    await flutterLocalNotificationsPlugin.show(
        0, '', '', platformChannelSpecifics);
  }

  showNotification() async {
    var android = new AndroidNotificationDetails(
        groupChannelId, groupChannelName, groupChannelDescription,
        groupKey: groupKey,
        importance: Importance.Max,
        priority: Priority.High,
        largeIcon: notificationImage,
        playSound: true,
        enableVibration: true);
    var ios = new IOSNotificationDetails();
    var platform = new NotificationDetails(android, ios);
    await flutterLocalNotificationsPlugin.show(0, title, "body", platform);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
  }
}

//class TimerPainter extends CustomPainter {
//  final Animation<double> animation;
//
//  final Color backgroundcolor, color;
//
//  TimerPainter({
//    this.animation,
//    this.backgroundcolor,
//    this.color
//  }) : super (repaint: animation);
//
//  @override
//  void paint(Canvas canvas, Size size) {
//    Paint paint = new Paint()
//      ..color = backgroundcolor
//      ..strokeWidth = 5.0
//      ..strokeCap = StrokeCap.round
//      ..style = PaintingStyle.stroke;
//
//    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
//    paint.color = color;
//    double progress = (1.0 - animation.value) * 2 * math.PI;
//    canvas.drawArc(Offset.zero & size, math.PI * 1.5, -progress, false, paint);
//  }
//
//  @override
//  bool shouldRepaint(TimerPainter old) {
//    return animation.value != old.animation.value || color != old.color;
//  }
//}
