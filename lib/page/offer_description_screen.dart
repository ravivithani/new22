import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:crazyrex/model/offer_master_obj_model.dart';
import 'package:crazyrex/model/store_list_model.dart';
import 'package:crazyrex/page/claim_card_list_screen.dart';
import 'package:crazyrex/page/my_offer_screen.dart';
import 'package:crazyrex/page/offer_list_screen.dart';
import 'package:crazyrex/page/scan_qr_screen.dart';
import 'package:crazyrex/page/store_detail_screen.dart';
import 'package:crazyrex/parser/offer_description_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:flutter_html_view/flutter_html_text.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';
import 'package:share/share.dart';
import 'package:simple_permissions/simple_permissions.dart';

enum MenuItems { share, category, store }

class OfferDescriptionScreen extends StatefulWidget {
  final int offerId;
  final int businessMasterId;
  final String businessName;
  final bool isFromStore;

  OfferDescriptionScreen({this.offerId,
    this.businessMasterId,
    this.businessName,
    this.isFromStore})
      : assert(offerId != null),
        assert(businessMasterId != null),
        assert(isFromStore != null),
        assert(businessName != null);

  @override
  _OfferDescriptionScreenState createState() => _OfferDescriptionScreenState();
}

class _OfferDescriptionScreenState extends State<OfferDescriptionScreen>
    with OfferDescriptionListener {
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isLoading;
  bool isError = false;

  String _sessionId;
  var iconOfFavorite = Icons.favorite_border;

  OfferMasterObjectModel offerModel;
  List<StoreListModel> businessList;

  int customerMembershipTranId = 0;
  int selectedBusiness = 0;
  StoreListModel selectedStore;
  StoreListModel store;

  String strCustomerId;
  String cityName;
  String groupValue;

  //region share
  String packageName;
  String appName;

  String buildNumber;
  PackageInfo _packageInfo = new PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<Null> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      packageName = info.packageName;
      appName = info.appName;
      buildNumber = info.buildNumber;
    });
  }

  //endregion

  _OfferDescriptionScreenState() {
    PrefUtil
        .getStringPreference(Constants.prefCustomerMasterId)
        .then((String value) {
      strCustomerId = value;
    });
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value) {
      _sessionId = value;
    });
    PrefUtil.getStringPreference(Constants.prefCityName).then((String value) {
      cityName = value;
    });
    PrefUtil.getIntPreference(Constants.prefMembershipTranId).then((int value) {
      customerMembershipTranId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
    _initPackageInfo();
  }

  @override
  void dispose() {
    subscription = null;
    super.dispose();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            //    _isLoading = true;
            setState(() {
              internetConnection = true;
              isLoading = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          callApi();
        }
        return _build();
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return new Scaffold();
    }
  }

  Widget _build() {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(Strings.offerDescTitle),
        actions: <Widget>[
          !isLoading ? popUpMenu() : new Container(),
        ],
      ),
      floatingActionButton: !isLoading ? floatingButton() : new Container(),
      body: isLoading
          ? new Center(
        child: new CircularProgressIndicator(),
      )
          : offerDescScreen(),
    );
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .title),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {
                          _internetConnection();
                        });
                      })
                ],
              ),
            ),
          ],
        ));
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text("Please wait")
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  Widget popUpMenu() {
    return new PopupMenuButton<MenuItems>(
      icon: Icon(
        Icons.more_vert,
        color: Colors.black54,
      ),
      onSelected: (MenuItems result) {
        if (result == MenuItems.share) {
          share(offerModel);
        } else if (result == MenuItems.category) {
          var route = new MaterialPageRoute(
              builder: (BuildContext context) =>
              new OfferListScreen(
                title: "${offerModel.businessType} Offers",
                businessTypeMasterId: offerModel.businessTypeMasterId,
              ));
          _navigateAndReturn(context, route, 0);
        } else if (result == MenuItems.store) {
          var route = new MaterialPageRoute(
              builder: (BuildContext context) =>
              new StoreDetailScreen(widget.businessMasterId));
          _navigateAndReturn(context, route, 0);
        }
      },
      itemBuilder: (BuildContext context) =>
      <PopupMenuEntry<MenuItems>>[
        PopupMenuItem<MenuItems>(
          value: MenuItems.share,
          child: Text(
            'Share',
            textAlign: TextAlign.end,
          ),
        ),
        PopupMenuItem<MenuItems>(
          value: MenuItems.category,
          child: Text("Show all ${offerModel.businessType != null ? offerModel
              .businessType : "Category"} offers"),
        ),
        PopupMenuItem<MenuItems>(
          value: MenuItems.store,
          child: Text("Show all ${widget.businessName} offers"),
        ),
      ],
    );
  }

  Widget offerDescScreen() {
    return new Container(
      child: new ListView(
        children: <Widget>[
          offerCard(offerModel),
          offerUsageCard(offerModel),
          branchInfoCard(),
          customerMembershipTranId == 0 ? membershipCard() : new Container(),
          (offerModel.termsAndConditions != null &&
              offerModel.termsAndConditions.isNotEmpty)
              ? termsAndCondition()
              : new Container(),
        ],
      ),
    );
  }

  Widget offerCard(OfferMasterObjectModel model) {
    return new Card(
        elevation: 2.0,
        margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
        child: new Container(
          padding: EdgeInsets.all(10.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Stack(
                alignment: Alignment(1.1, -2.0),
                children: <Widget>[
                  new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      model.xs_ImageName != null
                          ? new FadeInImage(
                        placeholder: AssetImage("images/default1.png"),
                        image: NetworkImage(model.xs_ImageName),
                        width: 30.0,
                        height: 30.0,
                      )
                          : new Image.asset(
                        "images/default1.png",
                        width: 20.0,
                        height: 20.0,
                      ),
                      Padding(padding: EdgeInsets.only(left: 15.0)),
                      new Flexible(
                        child: new Text(
                          model.offerTitle,
                          style: Theme
                              .of(context)
                              .textTheme
                              .body2,
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(right: 30.0)),
                    ],
                  ),
                  favorite(model),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 5.0)),
              new Text(
                model.offerContent,
                style: Theme
                    .of(context)
                    .textTheme
                    .body1,
              ),
              Padding(padding: EdgeInsets.only(top: 10.0)),
              new Container(
                height: 0.5,
                color: Colors.black38,
              ),
              Padding(padding: EdgeInsets.only(top: 10.0)),
              new Row(
                children: <Widget>[
                  Expanded(
                    child: new Text(
                      model.totalRedeemOffer != 0 ? "${model
                          .totalRedeemOffer} times used" : Strings
                          .beTheFirstUser,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .caption,
                    ),
                  ),
                  Expanded(
                    child: txtValidity(model.toDate),
                  ),
                ],
              )
            ],
          ),
        ));
  }

  Widget txtValidity(String date) {
    String txtDate;
    if (date != null && date.isNotEmpty) {
      DateFormat dateFormat = new DateFormat("yyyy-MM-dd");
      DateTime dateTime = dateFormat.parse(date);
      txtDate = new DateFormat("dd-MM-yyyy").format(dateTime);
    }
    String text = txtDate.isEmpty ? "Valid till \u221E" : "Valid till $txtDate";
    return new Text(
      text,
      textAlign: TextAlign.end,
      style: Theme
          .of(context)
          .textTheme
          .caption,
    );
  }

  Widget offerUsageCard(OfferMasterObjectModel model) {
    return new Card(
      elevation: 2.0,
      margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
      child: new Container(
        padding: EdgeInsets.all(10.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new Row(
              children: <Widget>[
                Expanded(
                  child: new Text(Strings.cardOfferUsageTitle, style: Theme
                      .of(context)
                      .textTheme
                      .body2,),
                ),
                Expanded(
                  child: new InkWell(
                    onTap: () {
                      showHowToUseDialog();
                    },
                    child: new Text(
                      Strings.txtHowToUseOffer, textAlign: TextAlign.end,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .apply(color: Colors.myColor),),
                  ),
                ),
              ],
            ),
            !model.isAvailableNow
                ? Padding(padding: EdgeInsets.only(top: 10.0))
                : new Container(),
            !model.isAvailableNow
                ? new Text(
              Strings.offerNotAvailable,
              style: Theme
                  .of(context)
                  .textTheme
                  .body1
                  .apply(color: Colors.red),
            )
                : new Container(),
            Padding(padding: EdgeInsets.only(top: 10.0)),
            rowOfOfferUsage(model),
          ],
        ),
      ),
    );
  }

  Widget rowOfOfferUsage(OfferMasterObjectModel model) {
    return new Row(
      children: <Widget>[
        Expanded(
            child: new Text(
                "${model.totalCustomerRedeemOffer}/${model.redeemCount} used.",
                style: Theme
                    .of(context)
                    .textTheme
                    .body1)),
        Expanded(
            child: new InkWell(
              onTap: () {
                clickOnHistory();
              },
              child: model.totalCustomerRedeemOffer != 0
                  ? new Text(
                Strings.txtHistory,
                textAlign: TextAlign.end,
                style: Theme
                    .of(context)
                    .textTheme
                    .body1
                    .apply(color: Colors.myColor),
              )
                  : new Container(),
            )),
      ],
    );
  }

  Widget branchInfoCard() {
    return new Card(
        elevation: 2.0,
        margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
        child: new Container(
          padding: EdgeInsets.all(10.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Text(
                Strings.branchInfoCardTitle,
                textAlign: TextAlign.start,
                style: Theme
                    .of(context)
                    .textTheme
                    .body2,
              ),
              Padding(padding: EdgeInsets.only(top: 10.0)),
              new Container(
                  child: new Column(
                    children: buildRadio(),
                  )),
            ],
          ),
        ));
  }

  buildRadio() {
    List<Row> rows = new List<Row>.generate(businessList.length, (int index) {
      StoreListModel model = businessList[index];
      return new Row(
        children: <Widget>[
          radioButton(model),
          new Expanded(
              flex: 8,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.fromLTRB(5.0, 5.0, 0.0, 0.0)),
                  new Text(
                    "${model.businessName} (${model.area})",
                    style: Theme
                        .of(context)
                        .textTheme
                        .body2,
                  ),
                  Padding(padding: EdgeInsets.fromLTRB(5.0, 5.0, 0.0, 0.0)),
                  new Text(
                    "${model.address}",
                    style: Theme
                        .of(context)
                        .textTheme
                        .caption,
                  ),
                  index != businessList.length - 1
                      ? Padding(
                      padding: EdgeInsets.fromLTRB(5.0, 5.0, 0.0, 0.0))
                      : new Container(),
                  index != businessList.length - 1
                      ? new Container(
                    height: 0.5,
                    color: Colors.black38,
                  )
                      : new Container(),
                ],
              ))
        ],
      );
    });

    return rows;
  }

  Widget floatingButton() {
    if (customerMembershipTranId != 0 &&
        (offerModel != null &&
            offerModel.isAvailableNow != null &&
            offerModel.isAvailableNow) &&
        (offerModel.totalCustomerRedeemOffer != offerModel.redeemCount &&
            offerModel.totalCustomerRedeemOffer < offerModel.redeemCount)) {
      return new Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          new FloatingActionButton(
            onPressed: () {
              onClickRedeemButton();
            },
            child: new Image.asset("images/Redeem.png", height: 30.0,
              width: 30.0,
              color: Colors.white,),
            tooltip: "Redeem",
          ),
          new Text(
            "REDEEM",
            style:
            Theme
                .of(context)
                .textTheme
                .body2
                .apply(color: Colors.myColor),
          )
        ],
      );
    } else {
      return new Container();
    }
  }

  Widget radioButton(StoreListModel model) {
    store = model;
    if (customerMembershipTranId != 0) {
      if (offerModel.isAvailableNow) {
        if (offerModel.totalCustomerRedeemOffer != offerModel.redeemCount &&
            offerModel.totalCustomerRedeemOffer < offerModel.redeemCount) {
          //region condition for selection
          if (businessList.length == 1) {
            selectedBusiness = businessList[0].businessMasterId;
            selectedStore = businessList[0];
          } else if (widget.isFromStore != null && widget.isFromStore) {
            businessList.forEach((StoreListModel store) {
              if (store.businessMasterId == widget.businessMasterId) {
                selectedBusiness = widget.businessMasterId;
                selectedStore = store;
              }
            });
          }
          return new Expanded(
              flex: 1,
              child: new Radio(
                groupValue: selectedBusiness,
                value: model.businessMasterId,
                onChanged: businessList.length != 1 && (!widget.isFromStore)
                    ? (dynamic value) {
                  onSelectStoreRadio(value,model);
                }
                    : null,
              ));
          //endregion

        } else {
          return new Container();
        }
      } else {
        return new Container();
      }
    } else {
      return new Container();
    }
  }

  onSelectStoreRadio(int value,StoreListModel model) {
    setState(() {
      selectedBusiness = value;
      selectedStore = model;
    });
  }

  Widget membershipCard() {
    return new Card(
      elevation: 2.0,
      margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
      child: new InkWell(
        onTap: () {
          _clickOnClimCard();
        },
        child: new Container(
          padding: EdgeInsets.all(10.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Image(
                image: new AssetImage("images/ic_buy_now.png"),
                color: Colors.black54,
                height: 22.0,
                width: 22.0,
              ),
              Padding(padding: new EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0)),
              new Text(Strings.labelBeCrazyMember,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme
                      .of(context)
                      .textTheme
                      .body2),
            ],
          ),
        ),
      ),
    );
  }

  Widget termsAndCondition() {
    return new Card(
      elevation: 2.0,
      margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 6.0),
      child: new Container(
        padding: EdgeInsets.all(10.0),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new Text(
              Strings.termsAndConditionCardTitle,
              textAlign: TextAlign.start,
              style: Theme
                  .of(context)
                  .textTheme
                  .body2,
            ),
            Padding(padding: EdgeInsets.only(top: 10.0)),
            new HtmlText(
              data: offerModel.termsAndConditions,
            ),
          ],
        ),
      ),
    );
  }

  Widget favorite(OfferMasterObjectModel model) {
    if (model.isFavorite) {
      iconOfFavorite = Icons.favorite;
    } else {
      iconOfFavorite = Icons.favorite_border;
    }
    return new InkWell(
      onTap: () {
        onClickFavorite(model);
      },
      child: new Container(
        height: 20.0,
        width: 20.0,
        margin: EdgeInsets.fromLTRB(0.0, 4.0, 20.0, 0.0),
        child: new Icon(
          iconOfFavorite,
          size: 20.0,
          color: Colors.red,
        ),
      ),
    );
  }

  //region click

  onClickRedeemButton() {
    if (selectedBusiness != 0 && selectedStore != null) {
      SimplePermissions.requestPermission(Permission.Camera).then((isGranted) {
        print(selectedStore.RedeemPin);
        print(isGranted);
        var route = new MaterialPageRoute(
            builder: (BuildContext context) => new ScanQrScreen(
                selectedStore, offerModel));
        _navigateAndReturn(context, route, 0);
      });
    } else {
      Fluttertoast.showToast(msg: "Select an outlet");
    }
  }

  onClickFavorite(OfferMasterObjectModel model) {
    if (model.isFavorite) {
      callDeleteFavorite();
    } else {
      callFavorite();
    }
  }

  void _clickOnClimCard() {
    var route = new MaterialPageRoute(
        builder: (BuildContext context) => new ClaimCardListScreen());
    _navigateAndReturn(context, route, 1);
  }

  clickOnHistory() {
    var route = new MaterialPageRoute(
        builder: (BuildContext context) =>
        new MyOfferScreen(
          isFromOffer: true,
        ));
    _navigateAndReturn(context, route, 2);
  }

  //endregion

  void callApi() {
    String url = "${Constants.url}SelectOfferMaster/${widget
        .offerId}/$strCustomerId/$customerMembershipTranId";
    SetUpOfferDescriptionListener setUpOfferDescriptionListener =
    new SetUpOfferDescriptionListener(this, url);
    setUpOfferDescriptionListener.loadOffer();
  }

  void callFavorite() {
    _progressDialog(true);
    var data = {
      'CustomerOfferTran': {
        'SessionId': _sessionId,
        'linktoCustomerMasterId': strCustomerId,
        'linktoOfferMasterId': offerModel.offerMasterId,
        'CustomerOfferTranId': offerModel.customerOfferTranId,
        'IsFavourite': true,
      },
    };
    var encoder = JSON.encode(data);
    var url = '${Constants.url}InsertCustomerOfferTran';
    Future<int> result = OfferDescriptionParser.offerFavorite(url, encoder);
    result.then((c) {
      var errorCode = c;
      _successFavorite(errorCode, true);
    }).catchError((onError) {
      setState(() {
        isError = true;
        _progressDialog(false);
      });
    });
  }

  void callDeleteFavorite() {
    _progressDialog(true);
    var data = {
      'CustomerOfferTran': {
        'SessionId': _sessionId,
        'linktoCustomerMasterId': strCustomerId,
        'linktoOfferMasterId': offerModel.offerMasterId,
        'CustomerOfferTranId': offerModel.customerOfferTranId,
        'IsFavourite': false,
      },
    };
    var encoder = JSON.encode(data);
    var url = '${Constants.url}DeleteCustomerOfferTran';
    Future<int> result =
    OfferDescriptionParser.offerDeleteFavorite(url, encoder);
    result.then((c) {
      var errorCode = c;
      _successFavorite(errorCode, false);
    }).catchError((onError) {
      setState(() {
        isError = true;
        _progressDialog(false);
      });
    });
  }

  void _successFavorite(int errorCode, bool isFavorite) {
    _progressDialog(false);
    if (errorCode == 0) {
      if (isFavorite) {
        offerModel.isFavorite = true;
        Fluttertoast.showToast(
            msg: "Added to 'Favourite'", toastLength: Toast.LENGTH_SHORT);
        setState(() {
          iconOfFavorite = Icons.favorite;
        });
      } else {
        offerModel.isFavorite = false;
        Fluttertoast.showToast(
            msg: "Removed from 'Favourite'", toastLength: Toast.LENGTH_SHORT);
        setState(() {
          iconOfFavorite = Icons.favorite_border;
        });
      }
    } else {
      setState(() {
        isError = true;
      });
    }
  }

  share(OfferMasterObjectModel model) {
    String appUrlForAndroid = "http://play.google.com/store/apps/details?id=${packageName}";
    String appUrlForIphone = "http://itunes.apple.com/+91/app/${_packageInfo
        .appName}/id$packageName?mt=8";
    final RenderBox box = context.findRenderObject();
    Share.share(
        "${model.offerTitle} at ${model
            .areaName}, ${cityName} .\nDownload CrazyRex: ${defaultTargetPlatform ==
            TargetPlatform.iOS ? appUrlForIphone : appUrlForAndroid}",
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  @override
  void onErrorOfferDescriptionListener(String error) {
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onGetOfferMasterObjectModel(
      OfferMasterObjectModel offerMasterObjectModel) {
    setState(() {
      offerModel = offerMasterObjectModel;
      isLoading = false;
    });
  }

  @override
  void onGetStoreList(List<StoreListModel> storeList) {
    setState(() {
      businessList = storeList;
    });
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume(result);
  }

  onResume(var onBackResult) {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);
          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            setState(() {
              internetConnection = true;
              if (onBackResult != null) {
                List r = onBackResult;
                if (r[0] == 1) {
                  isLoading = true;
                }
              }
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }

  void showHowToUseDialog() {
    showDialog(
        context: context,
        child: new SimpleDialog(
          children: <Widget>[
            new Container(
              child: new Column(
                children: <Widget>[
                  new Container(
                    padding: new EdgeInsets.fromLTRB(25.0, 0.0, 16.0, 0.0),
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                            flex: 12,
                            child: new Text(
                              Strings.txtHowToUseOffer, style: Theme
                                .of(context)
                                .textTheme
                                .title,)),
                        new Expanded(flex: 2,
                            child: new IconButton(icon: new Icon(
                              Icons.close, color: Colors.black45, size: 25.0,),
                              onPressed: () {
                                Navigator.pop(context);
                              },)),
                      ],
                    ),
                  ),
                  new Container(
                      padding: new EdgeInsets.fromLTRB(25.0, 35.0, 16.0, 16.0),
                      foregroundDecoration: new ViewLineFromRegistration(),
                      child: new Column(
                        children: <Widget>[
                          new Text(Strings.txtHowToUseFirst,
                            textAlign: TextAlign.center, style: Theme
                                .of(context)
                                .textTheme
                                .body1,),
                          Padding(padding: EdgeInsets.only(top: 10.0)),
                          new Text(Strings.txtHowToUseSecond,
                            textAlign: TextAlign.center, style: Theme
                                .of(context)
                                .textTheme
                                .body1,),
                        ],
                      )
                  ),
                  new Container(
                    padding: new EdgeInsets.fromLTRB(25.0, 0.0, 16.0, 16.0),
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(
                            child: new Center(
                              child: new Column(
                                children: <Widget>[
                                  new Container(
                                    height: 80.0,
                                    width: 80.0,
                                    decoration: new BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      border: new Border.all(
                                        color: Colors.myColor,
                                        width: 2.0,),),
                                    child: new Center(child: new Image.asset(
                                      "images/scan_qr_at_store.png",
                                      height: 50.0,
                                      width: 50.0,
                                      color: Colors.myColor,)),
                                  ),
                                  new Padding(
                                      padding: new EdgeInsets.only(top: 13.0)),
                                  new Text(Strings.txtScanQRCodeAtStore,
                                    textAlign: TextAlign.center, style: Theme
                                        .of(context)
                                        .textTheme
                                        .body1,)
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          height: 0.5,
                          width: 50.0,
                          color: Colors.black54,
                        ),
                        new Container(
                          margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                          child: new Text("OR"),
                        ),
                        new Container(
                          height: 0.5,
                          width: 50.0,
                          color: Colors.black54,
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    padding: new EdgeInsets.fromLTRB(25.0, 6.0, 16.0, 16.0),
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(
                            child: new Center(
                              child: new Column(
                                children: <Widget>[
                                  new Container(
                                    height: 80.0,
                                    width: 80.0,
                                    decoration: new BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      border: new Border.all(
                                        color: Colors.myColor,
                                        width: 2.0,),),
                                    child: new Center(child: new Image(
                                      image: new AssetImage(
                                          "images/keyboard.png"),
                                      width: 50.0,
                                      height: 50.0,
                                      color: Colors.myColor,)),
                                  ),
                                  new Padding(
                                      padding: new EdgeInsets.only(top: 13.0)),
                                  new Text(Strings.txtEnterPinForRedeem,
                                    textAlign: TextAlign.center, style: Theme
                                        .of(context)
                                        .textTheme
                                        .body1,)
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          height: 0.5,
                          width: 50.0,
                          color: Colors.black54,
                        ),
                        new Container(
                          margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                          child: new Text("OR"),
                        ),
                        new Container(
                          height: 0.5,
                          width: 50.0,
                          color: Colors.black54,
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    padding: new EdgeInsets.fromLTRB(25.0, 6.0, 16.0, 16.0),
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(
                            child: new Center(
                              child: new Column(
                                children: <Widget>[
                                  new Container(
                                    height: 80.0,
                                    width: 80.0,
                                    decoration: new BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      border: new Border.all(
                                        color: Colors.myColor,
                                        width: 2.0,),),
                                    child: new Center(child: new Image(
                                      image: new AssetImage(
                                          "images/show_card.png"),
                                      width: 50.0,
                                      height: 50.0,
                                      color: Colors.myColor,)),
                                  ),
                                  new Padding(
                                      padding: new EdgeInsets.only(top: 13.0)),
                                  new Text(Strings.txtShowClaimCardToStore,
                                    textAlign: TextAlign.center, style: Theme
                                        .of(context)
                                        .textTheme
                                        .body1,)
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }

}
