import 'dart:async';

import 'package:crazyrex/model/offerlist_model.dart';
import 'package:crazyrex/page/offer_description_screen.dart';
import 'package:crazyrex/page/offer_list_screen.dart';
import 'package:crazyrex/page/store_detail_screen.dart';
import 'package:crazyrex/parser/offer_list_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';
import 'package:share/share.dart';

enum MenuItems { share, category, store }

class TabOfferScreen extends StatefulWidget {
  final int cityId;
  final String customerMasterId;
  final bool isForUsed;
  TabOfferScreen(this.cityId, this.customerMasterId,this.isForUsed);

  @override
  _TabOfferScreenState createState() => _TabOfferScreenState();
}

class _TabOfferScreenState extends State<TabOfferScreen> with OfferListListener {
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;

  bool isLoading;
  bool isError = false;
  List<OfferListModel> offerList = [];

  //region share variables
  String packageName;
  String appName;

  String buildNumber;
  PackageInfo _packageInfo = new PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  SetUpOfferListListener setUpOfferListListener;

  Future<Null> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      packageName = info.packageName;
      appName = info.appName;
      buildNumber = info.buildNumber;
    });
  }

  //endregion

  @override
  void initState() {
    super.initState();
    _internetConnection();
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (!isLoading) {
          return new Scaffold(
            body: offerList.length != 0 ? offerListView() : _noData(),
          );
        } else {
          callApi();
          return new Scaffold(
            body: new Center(
              child: new CircularProgressIndicator(),
            ),
          );
        }
      } else {
        return _error();
      }
    } else {
      return _noInternet();
    }
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        setState(() {
          internetConnection = true;
          isLoading = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {
                          _internetConnection();
                        });
                      })
                ],
              ),
            ),
          ],
        ));
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _noData() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.local_offer,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(widget.isForUsed?
                  Strings.emptyUsed
                  :Strings.emptySaved,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme
                      .of(context)
                      .textTheme
                      .title
                      .apply(color: Colors.myColor)),
            ],
          ),
        ),
      ],
    ));
  }

  Widget offerListView() {
    return new Container(
        child: new ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: offerList.length,
            itemBuilder: (BuildContext context, int index) {
              final OfferListModel offer = offerList[index];
              return new Card(elevation: 2.0,
                  margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, index == offerList.length -1 ? 6.0:0.0),
                  child: new InkWell(
                    onTap: () {
                      _clickOnItem(offer);
                    },
                    child: new Container(
                      padding: new EdgeInsets.all(12.0),
                      child: new Stack(
                        alignment: Alignment(1.1, -2.0),
                        children: <Widget>[
                          new PopupMenuButton<MenuItems>(
                            icon: Icon(
                              Icons.more_vert,
                              color: Colors.black54,
                            ),
                            onSelected: (MenuItems result) {
                              if (result == MenuItems.share) {
                                share(offer);
                              } else if (result == MenuItems.category) {
                                var route = new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        new OfferListScreen(title: "${offer.BusinessType} Offers",businessTypeMasterId: offer.BusinessTypeMasterId,));
                                _navigateAndReturn(context, route, 0);
                              } else if (result == MenuItems.store) {
                                var route = new MaterialPageRoute(builder: (BuildContext context) => new StoreDetailScreen(offer.linktoBusinessMasterId));
                                _navigateAndReturn(context, route, 0);
                              }
                            },
                            itemBuilder: (BuildContext context) =>
                                <PopupMenuEntry<MenuItems>>[
                                  PopupMenuItem<MenuItems>(
                                    value: MenuItems.share,
                                    child: Text(
                                      'Share',
                                      textAlign: TextAlign.end,
                                    ),
                                  ),
                                  PopupMenuItem<MenuItems>(
                                    value: MenuItems.category,
                                    child: Text(
                                      "Show all ${offer.BusinessType} offers",
                                      textAlign: TextAlign.end,
                                    ),
                                  ),
                                  PopupMenuItem<MenuItems>(
                                    value: MenuItems.store,
                                    child: Text("Show all ${offer
                                    .BusinessName} offers"),
                                  ),
                                ],
                          ),
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              new Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: new Text(
                                      offer.OfferTitle,
                                      softWrap: false,
                                      style: Theme.of(context).textTheme.body2,
                                      maxLines: 2,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: new Container(
                                      height: 10.0,
                                      width: 10.0,
                                      child: new Container(),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(padding: EdgeInsets.only(top: 10.0)),
                              new Row(
                                children: <Widget>[
                                  new Flexible(
                                    child: new Row(
                                      children: <Widget>[
                                        new Container(
                                          height: 40.0,
                                          width: 40.0,
                                          decoration: new BoxDecoration(
                                            border: Border.all(width: 0.3),
                                            image: DecorationImage(
                                              image: offer.xs_ImageName != null
                                                  ? NetworkImage(
                                                      offer.xs_ImageName)
                                                  : AssetImage(
                                                      "images/default1.png"),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                            padding:
                                                EdgeInsets.only(left: 8.0)),
                                        new Flexible(
                                          child: new Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              new Text(
                                                offer.BusinessName,
                                                maxLines: 2,
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .body1,
                                              ),
                                              new Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 5.0)),
                                              new Text(
                                                offer.BusinessType,
                                                style: Theme
                                                    .of(context)
                                                    .textTheme
                                                    .caption,
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    margin:
                                        EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                                    width: 0.5,
                                    height: 50.0,
                                    color: Colors.black54,
                                  ),
                                  new Expanded(
                                    child: new Container(
                                      height: 40.0,
                                      child: new Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          new Text(
                                            offer.TotalRedeemOffer!=0?"${offer.TotalRedeemOffer} times used":Strings.beTheFirstUser,
                                            textAlign: TextAlign.end,
                                            maxLines: 2,
                                            softWrap: true,
                                            style: Theme
                                                .of(context)
                                                .textTheme
                                                .caption,
                                          ),
                                          validTillText(offer),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ));
            }));
  }

  Widget validTillText(OfferListModel model) {
    String date;
    if (model.ToDate != null && model.ToDate.isNotEmpty) {
      DateFormat dateFormat = new DateFormat("yyyy-MM-dd");
      DateTime dateTime = dateFormat.parse(model.ToDate);
      date = new DateFormat("dd-MM-yyyy").format(dateTime);
    }
    String text = date.isEmpty ? "Valid till \u221E" : "Valid till $date";
    return new Text(text,
        textAlign: TextAlign.end, style: Theme.of(context).textTheme.caption);
  }

  share(OfferListModel model) {
    String appUrlForAndroid =
        "http://play.google.com/store/apps/details?id=${packageName}";
    String appUrlForIphone = "http://itunes.apple.com/+91/app/${_packageInfo
        .appName}/id$packageName?mt=8";
    final RenderBox box = context.findRenderObject();
    Share.share(
        "${model.OfferTitle} at ${model.AreaName}, ${model
        .CityName} .\nDownload CrazyRex: ${defaultTargetPlatform ==
        TargetPlatform.iOS ? appUrlForIphone : appUrlForAndroid}",
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  callApi() {
    String url;
    String returnURL;
    if(widget.isForUsed){
      url = "${Constants.url}SelectAllOfferRedeemTranByCustomerId/${widget.customerMasterId}/null/null";
      returnURL = "SelectAllOfferRedeemTranByCustomerIdResult";
    }else{
      url = "${Constants.url}SelectAllOfferMasterByCustomerFavoriteOffer/${widget.customerMasterId}/${widget.cityId}";
      returnURL = "SelectAllOfferMasterByCustomerFavoriteOfferResult";
    }
    print(url);
    setUpOfferListListener = new SetUpOfferListListener(url, returnURL, this);
    setUpOfferListListener.loadOfferList();
  }

  // region click
  _clickOnItem(OfferListModel model) {
    var route = new MaterialPageRoute(builder: (BuildContext context) => new OfferDescriptionScreen(isFromStore: false,offerId:model.OfferMasterId,businessMasterId:model.linktoBusinessMasterId,businessName:model.BusinessName));
    _navigateAndReturn(context, route, 0);
  }

  //endregion

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    final result = await Navigator.push(context, rout);
    onResume();
  }

  onResume(){
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
          isLoading = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

  @override
  void onErrorOfferListListener(String error) {
    print("error in saved offer $error");
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onOfferListener(List<OfferListModel> list) {
    subscription = null;
    print(list);
    setState(() {
      isLoading = false;
      offerList = list;
    });
  }
}
