import 'dart:async';
import 'dart:convert';

import 'package:crazyrex/model/review_store_model.dart';
import 'package:crazyrex/page/edit_review_store_screen.dart';
import 'package:crazyrex/parser/delete_review_store_parser.dart';
import 'package:crazyrex/parser/review_store_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/star_rating.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';


class ViewAllReview extends StatefulWidget {
  final int storeId;

  ViewAllReview(this.storeId);

  @override
  _ViewAllReviewState createState() => _ViewAllReviewState();
}

class _ViewAllReviewState extends State<ViewAllReview> implements ReviewStoreDataListener {


  final GlobalKey<ScaffoldState> scaffoldStateViewAllReview = new GlobalKey<ScaffoldState>();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  String prefSessionId;
  bool isError=false;
  bool isLoading=false;

  SetUpReviewStoreListenerData setUpReviewStoreListenerData;

  int _storeId;
  List<ReviewStoreModel> reviewList1;
  String _customerId;

  int _reviewCustomerId;

  _ViewAllReviewState(){
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((String value){
      _customerId = value;
    });
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value){
      prefSessionId = value;
    });
  }

  @override
  void initState() {
    super.initState();

    _storeId = widget.storeId;

    _internetConnection();
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        internetConnection = true;
        setState(() {
          isLoading = true;
        });

      } else {
        internetConnection = false;
        setState(() {
        });
      }
    });
  }
//endregion

  @override
  void dispose() {
    subscription = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(internetConnection!=null && internetConnection){
      if(!isError){
        if (isLoading){
          _callApis();
        }
        return isLoading
            ? new Scaffold(body: new Center(child: new CircularProgressIndicator(),),)
            : reviewList1.length==0?_emptyScreen():_build();
      }else{
        return _error();
      }
    }else if(internetConnection!=null && !internetConnection){
      return _noInternet();
    }else{
      return new Scaffold();
    }

  }

  _callApis() {
    setUpReviewStoreListenerData = new SetUpReviewStoreListenerData("${Constants.url}SelectAllReviewMaster/$_storeId/null",this);
    setUpReviewStoreListenerData.loadReviewStoreData();
  }

  Widget _build() {
    return new Scaffold(
      appBar: AppBar(title: new Text(Strings.labelTitleViewAllScreen),),
      body: new ListView(
        children: <Widget>[
          new ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: reviewList1.length,
              itemBuilder: (BuildContext context, int index) {
                ReviewStoreModel model = reviewList1[index];
            return
              reviewList1[index].customerMasterId != int.parse(_customerId)
                  ? new Container()
                  : new Dismissible(key: new ObjectKey(reviewList1[index]),
                  background:new Container(
                    color: Colors.orangeAccent,

                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Icon(Icons.edit,color: Colors.white,),
                              new Text(Strings.labelEdit,style: Theme.of(context).textTheme.caption.apply(color: Colors.white),)
                            ],
                          ),
                          padding: EdgeInsets.only(left: 12.0),
                        )

                      ],
                    ),
                  ),
                  secondaryBackground: new Container(
                    color: Colors.red,

                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Container(
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Icon(Icons.delete,color: Colors.white,),
                              new Text(Strings.labelDelete,style: Theme.of(context).textTheme.caption.apply(color: Colors.white),)
                            ],
                          ),
                          padding: EdgeInsets.only(right: 12.0),
                        )

                      ],
                    ),
                  ),
                  onDismissed: (DismissDirection direction){
                    if(direction == DismissDirection.endToStart){
                      reviewList1.removeAt(index);
                      _delete(model.reviewMasterId);
                    }else if(direction == DismissDirection.startToEnd){
                      reviewList1.removeAt(index);
                      var route = new MaterialPageRoute(builder: (BuildContext context) => new EditReviewStoreScreen(editReviewStoreModel: model,));
                      _navigateAndReturn(context, route, 1);
                    }
                  },
                  child: new Container(
                    padding: new EdgeInsets.fromLTRB(12.0,12.0,12.0,0.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                                reviewList1[index].profileImageName != null
                                    ? new Container(
                                    width: 40.0,
                                    height: 40.0,
                                    decoration: new BoxDecoration(
                                      color: Colors.transparent,
                                      shape: BoxShape.circle,

                                      image: new DecorationImage(
                                          image: new NetworkImage(
                                              reviewList1[index].profileImageName),
                                          fit: BoxFit.cover

                                      ),
                                    ))
                                    : new Icon(Icons.account_circle,size: 40.0,color: Colors.black54,),
                            new Padding(padding: new EdgeInsets.only(left: 8.0)),
                            new Expanded(child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(model.customerName,style: Theme.of(context).textTheme.body2,maxLines: 1,),
                                new Padding(padding: new EdgeInsets.only(top: 2.0)),
                                new StarRating(
                                  rating: double.parse(model.starRating),
                                  color: Colors.myColor,
                                ),
                                new Padding(padding: new EdgeInsets.only(top: 2.0)),
                                new Text(model.review,style: Theme.of(context).textTheme.caption,),
                              ],
                            ),flex: 1,),
                          ],
                        ),
                        new Padding(padding: new EdgeInsets.only(top: 8.0)),

                      ],
                    ),
                  ));
          }),
          new ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: reviewList1.length,
              itemBuilder: (BuildContext context, int index) {
                return
                  reviewList1[index].customerMasterId == int.parse(_customerId)
                      ? new Container()
                      : new Card(
                    elevation: 2.0,
                    margin: index == reviewList1.length-1
                        ? new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 6.0)
                        : new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
                    child: new Container(
                      margin: new EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                      child:
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              reviewList1[index].profileImageName != null
                                  ? new Container(
                                  width: 40.0,
                                  height: 40.0,
                                  decoration: new BoxDecoration(
                                    color: Colors.transparent,
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                        image: new NetworkImage(reviewList1[index].profileImageName), fit: BoxFit.cover,),
                                  ))
                                  : new Icon(Icons.account_circle,size: 40.0,color: Colors.black54,),
                              new Padding(padding: new EdgeInsets.only(left: 8.0)),
                              new Expanded(child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(reviewList1[index].customerName,style: Theme.of(context).textTheme.body2,maxLines: 1,),
                                  new Padding(padding: new EdgeInsets.only(top: 2.0)),
                                  new StarRating(
                                    rating: double.parse(reviewList1[index].starRating),
                                    color: Colors.myColor,
                                  ),
                                  new Padding(padding: new EdgeInsets.only(top: 2.0)),
                                  new Text(reviewList1[index].review,style: Theme.of(context).textTheme.caption,),
                                ],
                              ))
                            ],
                          )
                    ),
                  );
              }
          )
        ],
      ),

    );
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(padding: EdgeInsets.only(top: 16.0),),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(padding: EdgeInsets.only(top: 27.0),),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _emptyScreen(){
    return new Scaffold(
      appBar: new AppBar(title: new Text(Strings.labelTitleReviewStoreScreen),),
      body: new Stack(
        children: <Widget>[
          new Center(
            child:  new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(Icons.tag_faces,color: Colors.myColor,size: 65.0,),
                  new Padding(padding: new EdgeInsets.only(top: 20.0)),
                  new Text(Strings.emptyReviewScreen,style: Theme.of(context).textTheme.title.apply(color: Colors.myColor),)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  void onErrorReviewStore(onError) {
    print("Error in View All review Screen ----- $onError");

    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onReviewStoreListener(List<ReviewStoreModel> reviewList) {
    reviewList1 = reviewList;

    for(int i=0;i<reviewList1.length;i++){
      _reviewCustomerId = reviewList1[i].customerMasterId;
      print("Customer Id $_reviewCustomerId");
    }

    setState(() {
      isLoading = false;
      isError = false;
    });
  }

  _delete(int reviewMasterId) {
    setState(() {
      int errorCode;
      Map<String,dynamic> body2 = {
        'ReviewMaster':{
          'ReviewMasterId':reviewMasterId,
          'SessionId':prefSessionId
        }
      };
      var encoder = JSON.encode(body2);
      var url = '${Constants.url}DeleteReviewMaster';

      DeleteReviewParser deleteReviewParser = new DeleteReviewParser();
      Future<int> result = deleteReviewParser.deleteReview(url, encoder);
      result.then((c) {
        errorCode = c;
        _handleResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        setState(() {
        });
      });
    });
  }

  _handleResult(int errorCode) {
    setState(() {
    });
    if (errorCode == 0) {
      setState(() {

      });
    }else{
      setState(() {
        reviewList1.clear();
        Constants.showSnackBar(scaffoldStateViewAllReview, Strings.errorInServices);
        isLoading = true;
      });
    }
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
    setState(() {
      isLoading = true;
    });
  }

  onResume(){
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);
      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }
}
