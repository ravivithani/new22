import 'package:crazyrex/util/strings.dart';
import 'package:flutter/material.dart';

class SuccessBuyMembershipScreen extends StatefulWidget {
  final String membershipName;


  SuccessBuyMembershipScreen(this.membershipName);

  @override
  _SuccessBuyMembershipScreenState createState() => _SuccessBuyMembershipScreenState();
}

class _SuccessBuyMembershipScreenState extends State<SuccessBuyMembershipScreen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        child: new Center(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment:  MainAxisAlignment.center,
            children: <Widget>[
              new Text(Strings.congratulation,style: Theme.of(context).textTheme.display1.apply(color: Colors.green),),
              new Padding(padding: new EdgeInsets.only(top: 5.0)),
              new Icon(Icons.check_circle_outline,size: 65.0,color: Colors.myColor,),
              Padding(padding: EdgeInsets.fromLTRB(28.0, 28.0, 28.0, 0.0)),
              new Text(
                "${Strings.buyMembershipSuccessMessage} ${widget.membershipName} ${Strings.buyMembershipSuccessMessage1}",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.title,
              ),
              Padding(padding: EdgeInsets.only(top: 28.0)),
              new MaterialButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text(Strings.labelAlertOk),
                textColor: Colors.white,
                color: Colors.myColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
