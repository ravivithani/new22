import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:crazyrex/page/claim_card_list_screen.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info/package_info.dart';
import 'package:share/share.dart';
//import 'package:advanced_share/advanced_share.dart';

class TabReferAndEarnScreen extends StatefulWidget {
  final bool isMembership;
  final String earnPoint;
  final String referralCode;
  final int membershipLevel;

  TabReferAndEarnScreen({this.isMembership,this.membershipLevel, this.earnPoint, this.referralCode});

  @override
  _TabReferAndEarnScreenState createState() => _TabReferAndEarnScreenState();
}

class _TabReferAndEarnScreenState extends State<TabReferAndEarnScreen> {
  String _earnPoint;

  String _referralCode;

  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isLoading;
  bool isError = false;

  //region share
  String packageName;
  String appName;
  String buildNumber;
  PackageInfo _packageInfo = new PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<Null> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      packageName = info.packageName;
      appName = info.appName;
      buildNumber = info.buildNumber;
    });
  }

  static const platform = const MethodChannel("CRXChannel");

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
    _general();
    _internetConnection();
  }

  @override
  void dispose() {
    subscription = null;
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    if(internetConnection != null && internetConnection){
    return new Stack(
      children: <Widget>[
        new ListView(
          padding: EdgeInsets.only(bottom: 15.0),
          children: <Widget>[
            new Container(
              child: new Column(
                children: <Widget>[
                  new Stack(
                    children: <Widget>[
                      new Image(image: AssetImage("images/refer.png")),
                      new Container(
                          padding:
                              new EdgeInsets.fromLTRB(27.0, 142.0, 0.0, 10.0),
                          child: new Column(
                            children: <Widget>[
                              new Text(
                                _earnPoint,
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .body2
                                    .apply(color: Colors.myColor),
                              )
                            ],
                          ))
                    ],
                  ),
                  new Container(
                    padding: new EdgeInsets.fromLTRB(27.0, 0.0, 27.0, 0.0),
                    child: new Column(
                      children: <Widget>[
                        new Padding(padding: new EdgeInsets.only(top: 15.0)),
                        new Text(
                          Strings.labelYourReferralCode,
                          style: Theme.of(context).textTheme.body2,
                        ),
                        new Padding(padding: new EdgeInsets.only(top: 8.0)),
                        new Container(
                          child: new Row(
                            children: <Widget>[
                              new Expanded(
                                  flex: 1,
                                  child: new Text(
                                    _referralCode,
                                    textAlign: TextAlign.center,
                                  ))
                            ],
                          ),
                          padding: EdgeInsets.only(top: 14.0, bottom: 14.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: Border.all(
                                  color: Colors.myColor,
                                  width: 0.5,
                                  style: BorderStyle.solid)),
                        ),
                        new Row(
                          children: <Widget>[
                            widget.isMembership && widget.membershipLevel != 0
                                ? new Container()
                                : new Expanded(
                                    child: new Text(
                                    Strings.labelCurrentlyNotVisible,
                                    textAlign: TextAlign.end,
                                    style: Theme.of(context).textTheme.caption,
                                  ))
                          ],
                        ),
                        widget.isMembership && widget.membershipLevel != 0
                            ? new Container()
                            : new Padding(
                                padding: new EdgeInsets.only(top: 8.0)),
                        widget.isMembership && widget.membershipLevel != 0
                            ? new Container()
                            : new Text(
                                Strings.labelOnlyActiveMember,
                                style: Theme.of(context).textTheme.body1,
                              ),
                        widget.isMembership && widget.membershipLevel != 0
                            ? new Container()
                            : new Padding(
                                padding: new EdgeInsets.only(top: 8.0)),
                        widget.isMembership && widget.membershipLevel != 0
                            ? new Container()
                            : new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new InkWell(
                                    onTap: () {
                                      var route = new MaterialPageRoute( builder: (BuildContext context) => new ClaimCardListScreen());
                                      _navigateAndReturn(context, route, 0);
                                    },
                                    child: new Text(Strings.labelBuyMembership,
                                        textAlign: TextAlign.center,
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .body1
                                            .apply(color: Colors.myColor)),
                                  ),
                                  new Text(Strings.labelFirstGetReferralCode,
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context).textTheme.body1),
                                ],
                              ),
                        new Padding(padding: new EdgeInsets.only(top: 16.0)),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new RaisedButton(
                              onPressed: widget.isMembership && widget.membershipLevel != 0
                                  ? () {
                                      share();
                                    }
                                  : null,
                              child: new Text(Strings.labelShare),
                              color: Colors.myColor,
                              textColor: Colors.white,
                            ),
                            new Padding(
                                padding: new EdgeInsets.only(left: 8.0)),
                            new IconButton(
                              icon: Image(
                                image: new AssetImage("images/whatsapp.png"),
                                height: 25.0,
                                width: 25.0,
                              ),
                              onPressed: widget.isMembership && widget.membershipLevel != 0
                                  ? () {
                                      _shareMessage(true, false, false);
                                    }
                                  : null,
                              iconSize: 25.0,
                            ),
                            new IconButton(
                              icon: Image(
                                image: new AssetImage("images/instagram.png"),
                                height: 25.0,
                                width: 25.0,
                              ),
                              onPressed: widget.isMembership && widget.membershipLevel != 0
                                  ? () {
                                      _shareMessage(false, true, false);
                                    }
                                  : null,
                              iconSize: 25.0,
                            ),
                            new IconButton(
                              icon: Image(
                                image: new AssetImage("images/facebook.png"),
                                height: 25.0,
                                width: 25.0,
                              ),
                              onPressed: widget.isMembership && widget.membershipLevel != 0
                                  ? () {
                                      _shareMessage(false, false, true);
                                    }
                                  : null,
                              iconSize: 25.0,
                            )
                          ],
                        ),
                        new Padding(padding: new EdgeInsets.only(top: 16.0)),
                        new Text(
                          Strings.labelEarnMoneyBySharing,
                          style: Theme.of(context).textTheme.body1,
                        ),
                        new Padding(padding: new EdgeInsets.only(top: 23.0)),
                        new InkWell(
                          onTap: () {
                            _showDialogue();
                          },
                          child: new Text(
                            Strings.labelHowDoesItWork,
                            style: Theme
                                .of(context)
                                .textTheme
                                .body1
                                .apply(color: Colors.myColor),
                          ),
                        ),
                        new Padding(padding: new EdgeInsets.only(top: 23.0)),
                        new Text(
                          Strings.labelRewardCommission,
                          style: Theme
                              .of(context)
                              .textTheme
                              .body1
                              .apply(color: Colors.myColor),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ],
    );
    }else if(internetConnection!=null && !internetConnection){
      return _noInternet();
    }else{
      return new Scaffold();
    }
  }



  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            //    _isLoading = true;
            setState(() {
              internetConnection = true;
              isLoading = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {
                          _internetConnection();
                        });
                      })
                ],
              ),
            ),
          ],
        ));
  }

  _general() {
    if (widget.earnPoint == "null") {
      _earnPoint = Strings.labelYourEarning + "\u20B9" + "0";
    } else if(widget.earnPoint == null){
      _earnPoint = Strings.labelYourEarning + "\u20B9" + "0";
    }else{
      _earnPoint = Strings.labelYourEarning + "\u20B9" + widget.earnPoint;
    }
    if (widget.isMembership && widget.membershipLevel != 0) {
        _referralCode = widget.referralCode;
    } else {
      _referralCode = Strings.labelDefaultReferCode;
    }
  }

  _shareMessage(bool isWhatsApp, bool isInstagram, bool isFacebook) {
    if (isWhatsApp) {
      shareApp("com.whatsapp");
    } else if (isInstagram) {
      shareApp("com.instagram.android");
    } else if (isFacebook) {
      shareApp("com.facebook.orca");
    }
  }

  void share(){
    String appUrlForAndroid ="http://play.google.com/store/apps/details?id=${packageName}";
    String appUrlForIphone = "http://itunes.apple.com/+91/app/${_packageInfo.appName}/id$packageName?mt=8";
    final RenderBox box =context.findRenderObject();
    Share.share("${Strings.labelShareReferralCodeMessage}${widget.referralCode}${Strings.labelShareReferralCodeMessage1}\n ${defaultTargetPlatform ==TargetPlatform.iOS ? appUrlForIphone : appUrlForAndroid}",
        sharePositionOrigin:
        box.localToGlobal(Offset.zero) &
        box.size);
  }

  Future shareApp(String packageName) async {
    try {
      String appUrlForAndroid ="http://play.google.com/store/apps/details?id=${packageName}";
      String appUrlForIphone = "http://itunes.apple.com/+91/app/${_packageInfo.appName}/id$packageName?mt=8";
      String msg = "${Strings.labelShareReferralCodeMessage}${widget.referralCode}${Strings.labelShareReferralCodeMessage1}\n ${defaultTargetPlatform ==TargetPlatform.iOS ? appUrlForIphone : appUrlForAndroid}";
      var map = <String, dynamic>{'msg': msg, 'packageName': packageName};
      var value = await platform.invokeMethod("ShareApp", map).then((value){
        Map<dynamic,dynamic> map = value;
        if(map['errorCode'] == '-1'){
          Fluttertoast.showToast(msg: Strings.toastNotInstalled);
        }
      });
    } catch (e) {
      print(e);
      if(e.message.contains('No Activity found')){
        Fluttertoast.showToast(msg: Strings.toastNotInstalled);
      }
    }
    return null;
  }

  void _showDialogue(){
    showDialog(context: context,child: new SimpleDialog(
      contentPadding: EdgeInsets.fromLTRB(0.0,0.0,0.0,10.0),
      children: <Widget>[
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 5.0),
          child: new Row(
            children: <Widget>[
              new Expanded(child: new Text(Strings.labelHowDoesItWork,style: Theme.of(context).textTheme.title,)),
              new InkWell(
                onTap: (){Navigator.pop(context);},
                child: new Icon(Icons.close),
              )
            ],
          ),
        ),
        new Container(
          foregroundDecoration: new ViewLineFromRegistration(),
        ),
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  child: new Center(child: Text("1",textAlign: TextAlign.center,style: Theme.of(context).textTheme.subhead,),),
                  width: 0.0,
                  height: 35.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color: Colors.myColor,
                      width: 0.5, ), ),
                ),),
              new Expanded(
                  flex: 7,
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 80.0,
                          width: 80.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.myColor,
                              width: 2.0, ), ),
                          child: new Center(child: new Icon(Icons.share,size: 50.0,color: Colors.myColor,)),
                        ),
                      ],
                    ),
                  )),
              new Expanded(
                  flex: 1,
                  child: new Text(" "))
            ],
          ),
        ),
        new Text(Strings.labelFirst,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  child: new Center(child: Text("2",textAlign: TextAlign.center,style: Theme.of(context).textTheme.subhead,),),
                  width: 0.0,
                  height: 35.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color: Colors.myColor,
                      width: 0.5, ), ),
                ),),
              new Expanded(
                  flex: 7,
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 80.0,
                          width: 80.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.myColor,
                              width: 2.0, ), ),
                          child: new Center(child: new Image(image: new AssetImage("images/ic_splash_icon_web.png"),width: 50.0,height: 50.0,color: Colors.myColor,)),
                        ),
                      ],
                    ),
                  )),
              new Expanded(
                  flex: 1,
                  child: new Text(" "))
            ],
          ),
        ),
        new Text(Strings.labelSecond,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  child: new Center(child: Text("3",textAlign: TextAlign.center,style: Theme.of(context).textTheme.subhead,),),
                  width: 0.0,
                  height: 35.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color: Colors.myColor,
                      width: 0.5, ), ),
                ),),
              new Expanded(
                  flex: 7,
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 80.0,
                          width: 80.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.myColor,
                              width: 2.0, ), ),
                          child: new Center(child: new Image(image: new AssetImage("images/ic_currency_usd_black_48dp.png"),width: 50.0,height: 50.0,color: Colors.myColor,)),
                        ),
                      ],
                    ),
                  )),
              new Expanded(
                  flex: 1,
                  child: new Text(" "))
            ],
          ),
        ),
        new Text(Strings.labelThird,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
      ],
    ));
  }

  void handleResponse(response, {String appName}) {
    if (response == 0) {
      print("failed.");
    } else if (response == 1) {
      print("success");
    } else if (response == 2) {
      print("application isn't installed");
      if (appName != null) {
        Fluttertoast.showToast(msg: "${appName} isn't installed.");
      }
    }
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);
          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            setState(() {
              internetConnection = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }
}
