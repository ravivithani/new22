import 'dart:async';

import 'package:crazyrex/model/exist_email_or_phone_model.dart';
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/model/otp_model.dart';
import 'package:crazyrex/page/one_time_password.dart';
import 'package:crazyrex/parser/exist_email_parser.dart';
import 'package:crazyrex/parser/exist_phone_parser.dart';
import 'package:crazyrex/parser/otp_parser.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class ForgetAccountScreen extends StatefulWidget {
  @override
  _ForgetAccountScreenState createState() => _ForgetAccountScreenState();
}

class _ForgetAccountScreenState extends State<ForgetAccountScreen> implements ExistPhoneListener,ExistEmailListener,OneTimePasswordListener{

  final GlobalKey<FormState> _formKeyForget = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldStateForget = new GlobalKey<ScaffoldState>();
  var _userNameController = new TextEditingController();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection;

  SetUpExistEmailListener listenerExistEmail;
  SetUpExistPhoneListener listenerExistPhone;
  SetUpOneTimePasswordListener listenerOneTimePassword;

  ExistEmailOrPhoneModel existPhoneModel;
  ExistEmailOrPhoneModel existEmailModel;
  OneTimePasswordModel oneTimePasswordModel;
  UserModel userModel;


  @override
  void initState() {
    _internetConnection();
  }
  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }
//endregion

  @override
  void dispose() {
    subscription == null;
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldStateForget,
      body: new Stack(
        children: <Widget>[
          new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(Strings.labelForgetPasswordTitle, style: Theme.of(context).textTheme.display1.apply(color: Colors.myColor)),
                new Image(image: new AssetImage("images/ic_security_account_black_48dp.png"), width: 65.0, height: 65.0, color: Colors.myColor,),
                new Container(
                  padding: new EdgeInsets.fromLTRB(50.0, 15.0, 50.0, 0.0),
                  child: Text(Strings.labelForgetPasswordSubTitle,textAlign: TextAlign.center,style: Theme.of(context).textTheme.body1,),
                ),
                new Form(
                  key: _formKeyForget,
                    child: new Theme(data: new ThemeData(
                      primaryColor: Colors.myColor,
                      accentColor: Colors.myColor,
                      textSelectionColor: Colors.myColor
                    ), child: new Container(
                      padding: const EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 0.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          new TextFormField(
                            controller: _userNameController,
                            maxLines: 1,
                            keyboardType: TextInputType.emailAddress,
                            validator: (value){
                              if(_userNameController.text.isNotEmpty){
                                if(Constants.isValidPhone(_userNameController.text)){

                                  if(internetConnection !=null && internetConnection){
                                    _progressDialog(true);
                                    listenerExistPhone = new SetUpExistPhoneListener(this, '${Constants.url}SelectUserMasterByUserNameIsExistOrNot/${_userNameController.text}/2');
                                    listenerExistPhone.loadExistPhone();
                                  }else if(internetConnection !=null && !internetConnection){
                                    Constants.showSnackBar(scaffoldStateForget, Strings.noInternetToast);
                                  }
                                }else if(Constants.isValidEmail(_userNameController.text)){
                                  if(internetConnection !=null && internetConnection){
                                    _progressDialog(true);
                                    listenerExistEmail = new SetUpExistEmailListener(this, '${Constants.url}SelectUserMasterByUserNameIsExistOrNot/${_userNameController.text}/1');
                                    listenerExistEmail.loadExistEmail();
                                  }else if(internetConnection !=null && !internetConnection){
                                    Constants.showSnackBar(scaffoldStateForget, Strings.noInternetToast);
                                  }
                                }else {
                                  return Strings.invalidUsername;
                                }
                              }else{
                                return Strings.emptyUsername;
                              }
                            },
                            decoration: new InputDecoration(
                              labelText: Strings.hintUsername
                            ),
                          ),
                          new Padding(padding: new EdgeInsets.only(top: 27.0)),
                          new MaterialButton(
                            color: Colors.myColor,
                            textColor: Colors.white,
                            child: new Text(Strings.labelButtonNext),
                            onPressed: () {
                              _formKeyForget.currentState.validate();
                            },
                          )
                        ],
                      ),
                    )))
              ]),
        ],
      ),
    );
  }

  @override
  void onErrorExistEmail() {
    print("Error Exist Email in Forget Screen");
    _progressDialog(false);
  }

  @override
  void onErrorExistPhone() {
    print("Error Exist phone in Forget Screen");
    _progressDialog(false);
  }

  @override
  void onErrorOneTimePassword(onError) {
    print("Error generate otp in Forget Screen -  $onError");
    _progressDialog(false);
  }

  @override
  void onSuccessExistEmail(ExistEmailOrPhoneModel model) {
    existEmailModel = model;
    if(existEmailModel.selectUserMasterByUserNameIsExistOrNotResult){
      listenerOneTimePassword = new SetUpOneTimePasswordListener(this, '${Constants.url}SelectRegisterUserForgotPassword/${_userNameController.text}/Email');
      listenerOneTimePassword.loadOneTimePassword();
    }else{
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStateForget, Strings.errorAccountNotExist);
    }
  }

  @override
  void onSuccessExistPhone(ExistEmailOrPhoneModel model) {
    existPhoneModel = model;
    if(existPhoneModel.selectUserMasterByUserNameIsExistOrNotResult){
      listenerOneTimePassword = new SetUpOneTimePasswordListener(this, '${Constants.url}SelectRegisterUserForgotPassword/${_userNameController.text}/Phone');
      listenerOneTimePassword.loadOneTimePassword();
    }else{
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStateForget, Strings.errorAccountNotExist);
    }
  }

  @override
  void onSuccessOneTimePassword(OneTimePasswordModel model) {
    oneTimePasswordModel = model;
    _progressDialog(false);
    if(oneTimePasswordModel.selectRegisterUserForgotPasswordResult != null){
      Constants.OTP = oneTimePasswordModel.selectRegisterUserForgotPasswordResult;
//      Constants.showSnackBar(scaffoldStateForget, Constants.OTP);
      subscription == null;
      var route = new MaterialPageRoute(builder: (BuildContext context) => new OneTimePasswordScreen(userNameForget: _userNameController.text));
      _navigateAndReturn(context, route, 0);
    }
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    // 1 for edit account
    // 2 for change password
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
  }
  onResume(){
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);
      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;  
        });
      }
    });
  }
}
