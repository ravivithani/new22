import 'package:crazyrex/util/constants.dart';
import 'package:flutter/material.dart';

class ShortStoreListScreen extends StatefulWidget {
  final String groupValue;


  ShortStoreListScreen(this.groupValue);

  @override
  _ShortStoreListScreenState createState() => _ShortStoreListScreenState(groupValue);
}

class _ShortStoreListScreenState extends State<ShortStoreListScreen> {
  int nearest = 1;
  int newest = 2;
  int popular = 3;

  String groupValue;


  _ShortStoreListScreenState(this.groupValue);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Short By"),
          leading: new Center(
            child: new InkWell(
              onTap: (){
                Navigator.pop(context);
              },
              child: Icon(Icons.close),
            ),
          )
      ),
      body: new Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Column(
          children: <Widget>[
            new RadioListTile<String>(
              title: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 12.0),
                      child: Text("Nearest",style: Theme.of(context).textTheme.body1,),
                    ),
                    new Container(
                      height: 0.5,
                      color: Colors.black87,
                    ),
                  ]
              ),
              groupValue: groupValue,
              value: "nearest",
              onChanged: shortList,
            ),
            new RadioListTile<String>(
              title: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 12.0),
                      child: Text("Alphabetic",style: Theme.of(context).textTheme.body1,),
                    ),
                    new Container(
                      height: 0.5,
                      color: Colors.black87,
                    ),
                  ]
              ),
              groupValue: groupValue,
              value: "alphabetic",
              onChanged: shortList,
            ),
            new RadioListTile<String>(
              title: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 12.0),
                      child: Text("Newest",style: Theme.of(context).textTheme.body1,),
                    ),
                    new Container(
                      height: 0.5,
                      color: Colors.black87,
                    ),
                  ]
              ),
              groupValue: groupValue,
              value: "newest",
              onChanged: shortList,
            ),
            new RadioListTile<String>(
              title: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 12.0),
                      child: Text("Popular",style: Theme.of(context).textTheme.body1,),
                    ),
                    new Container(
                      height: 0.5,
                      color: Colors.black87,
                    ),
                  ]
              ),
              groupValue: groupValue,
              value: "popular",
              onChanged: shortList,
            )
          ],
        ),
      ),
      bottomNavigationBar: new Container(
        child: new RawMaterialButton(
            padding: EdgeInsets.all(14.0),
            onPressed: (){
              Navigator.pop(context,groupValue);
              },
            fillColor: Colors.myColor,
            child: new Text("APPLY",style: Theme.of(context).textTheme.body2.apply(color: Colors.white),),
            ),

      ),
    );
  }

  shortList(String type){
    setState(() {
      groupValue = type;
    });
  }

}
