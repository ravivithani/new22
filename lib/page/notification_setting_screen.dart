import 'dart:async';
import 'dart:convert';

import 'package:crazyrex/model/notification_setting_model.dart';
import 'package:crazyrex/parser/notification_setting_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class NotificationSettingScreen extends StatefulWidget {
  @override
  _NotificationSettingScreenState createState() => _NotificationSettingScreenState();
}

class _NotificationSettingScreenState extends State<NotificationSettingScreen> implements NotificationDataListener{

  final GlobalKey<ScaffoldState> scaffoldStateNotificationScreen = new GlobalKey<ScaffoldState>();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  bool isError=false;
  bool isLoading=false;
  int prefUserId;
  String prefSessionId;
  bool _valueStore = true;
  bool _valueOffer = true;

  SetUpNotificationSettingListenerData setUpNotificationSettingListenerData;

  List<NotificationSettingModel> notificationList;

  int _settingStoreId;
  int _settingOfferId;

  _NotificationSettingScreenState(){
    PrefUtil.getIntPreference(Constants.prefRegisterUserMasterId).then((int value){
      prefUserId = value;
    });
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value){
      prefSessionId = value;
    });
  }

  @override
  void initState() {
    _internetConnection();
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        internetConnection = true;
        setState(() {
          isLoading = true;
        });

      } else {
        internetConnection = false;
        setState(() {
        });

      }
    });
  }
//endregion

  @override
  void dispose() {
    subscription == null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(internetConnection!=null && internetConnection){
      if(!isError){
        if(isLoading){
          _callApis();
        }
        return isLoading
            ? new Scaffold(
          body: new Center(
            child: new CircularProgressIndicator(),
          ),
        )
            :_build();
      }else{
        return _error();
      }
    }else if(internetConnection!=null && !internetConnection){
      return _noInternet();
    }else{
      return new Scaffold();
    }
  }

  void _onChangeOffer(bool value){
    setState(() {
      _valueOffer = value;
      _progressDialog(true);
      _editReview(_settingOfferId,value);
    });
  }
void _onChangeStore(bool value){
    setState(() {
      _valueStore = value;
      _progressDialog(true);
      _editReview(_settingStoreId,value);
    });
  }

  Widget _build(){
    return new Scaffold(
      appBar: AppBar(title: new Text(Strings.labelTitleNotificationSettingScreen),),
      body: new Stack(
        children: <Widget>[
          new Column(
            children: <Widget>[
              new Padding(padding: new EdgeInsets.only(top: 18.0)),
              new ListTile(
                title: new Text(Strings.labelOfferNotification),
                subtitle: new Text(Strings.labelNotifyOffer,style: Theme.of(context).textTheme.caption,),
                trailing: new Switch(value: _valueOffer, onChanged: (bool value){
                  _onChangeOffer(value);
                }),
              ),
              new ListTile(
                title: new Text(Strings.labeStoreNotification),
                subtitle: new Text(Strings.labelNotifyStore,style: Theme.of(context).textTheme.caption),
                trailing: new Switch(value: _valueStore, onChanged: (bool value){_onChangeStore(value);}),
              ),
            ],
          ),
        ],
      ),
    );
  }
  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(padding: EdgeInsets.only(top: 16.0),),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(padding: EdgeInsets.only(top: 27.0),),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }
  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
          ],
        ));
  }

  _callApis() {
    setUpNotificationSettingListenerData = new SetUpNotificationSettingListenerData("${Constants.url}SelectAllRegisteredUserSettingTran/$prefUserId",this);
    setUpNotificationSettingListenerData.loadNotificationSettingData();
  }

  @override
  void onErrorNotificationSetting(onError) {
    print("error in notification setting screen");
    isLoading = false;
    isError = true;
    setState(() {

    });
  }

  @override
  void onNotificationSettingListener(List<NotificationSettingModel> notificationSettingList) {
    notificationList =  notificationSettingList;

    _setData();

    isLoading = false;
    isError = false;
    setState(() {

    });
  }

  _editReview(int settingTranId,bool isEnable) {
    setState(() {
      int errorCode;

      Map<String, dynamic> body2;

        body2 = {
          'RegisteredUserSettingTran':{
            'SessionId':prefSessionId,
            'RegisteredUserSettingTranId':settingTranId,
            'IsEnabled':isEnable,
          }
        };

      var encoder = JSON.encode(body2);
      var url = '${Constants.url}UpdateRegisteredUserSettingTran';

      Future<int> result = NotificationSettingParser.updateNotificationSetting(url, encoder);
      result.then((c) {
        errorCode = c;
        _handleResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        _progressDialog(false);
      });
    });
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  _handleResult(int errorCode) {
    if (errorCode == 0) {
      _progressDialog(false);
    } else if (errorCode == -1) {
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStateNotificationScreen, Strings.errorInServices);
    }
  }

  _setData() {
    if(notificationList!=null){
      for(int i=0;i<notificationList.length;i++){
        if(notificationList[i].settingTypeId == 1){
          _valueOffer = notificationList[i].isEnable;
          _settingOfferId = notificationList[i].registeredUserSettingTranId;
        }else if(notificationList[i].settingTypeId == 2){
          _valueStore = notificationList[i].isEnable;
          _settingStoreId = notificationList[i].registeredUserSettingTranId;
        }
      }
    }
  }
}
