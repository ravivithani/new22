import 'dart:async';

import 'package:flutter/services.dart';
import 'package:crazyrex/model/exist_email_or_phone_model.dart';
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/model/otp_model.dart';
import 'package:crazyrex/page/one_time_password.dart';
import 'package:crazyrex/page/privacy_policy_screen.dart';
import 'package:crazyrex/parser/exist_email_parser.dart';
import 'package:crazyrex/parser/exist_phone_parser.dart';
import 'package:crazyrex/parser/otp_parser.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen>
    implements ExistPhoneListener, ExistEmailListener, OneTimePasswordListener {
  final GlobalKey<FormState> _formRegisterKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldStateRegister =
      new GlobalKey<ScaffoldState>();


  changeStatusColor(Color color) async {
    await FlutterStatusbarcolor.setStatusBarColor(color);
  }

  _RegistrationScreenState() {
    changeStatusColor(Colors.myColor);
  }

  FocusNode textFirstFocusNode = new FocusNode();
  FocusNode textSecondFocusNode = new FocusNode();
  FocusNode textThirdFocusNode = new FocusNode();
  FocusNode textForthFocusNode = new FocusNode();

  SetUpExistEmailListener listenerExistEmail;
  SetUpExistPhoneListener listenerExistPhone;
  SetUpOneTimePasswordListener listenerOneTimePassword;

  var _firstNameController = new TextEditingController();
  var _lastNameController = new TextEditingController();
  var _phoneNumberController = new TextEditingController();
  var _emailAddressController = new TextEditingController();
  var _referralCodeController = new TextEditingController();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection;

  ExistEmailOrPhoneModel existPhoneModel;
  ExistEmailOrPhoneModel existEmailModel;
  OneTimePasswordModel oneTimePasswordModel;

  bool isEmail;

  @override
  void initState() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    _internetConnection();
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        internetConnection = true;
      } else {
        internetConnection = false;
      }
    });
  }

//endregion

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    subscription == null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldStateRegister,
      body: new ListView(
        shrinkWrap: false,
        children: <Widget>[
          new Container(
            height: MediaQuery.of(context).size.height -85,
            child:  new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(Strings.labelSignUp,
                      style: Theme
                          .of(context)
                          .textTheme
                          .display1
                          .apply(color: Colors.myColor)),
                  new Image(
                    image: new AssetImage(
                        "images/ic_account_multiple_black_48dp.png"),
                    width: 65.0,
                    height: 65.0,
                    color: Colors.myColor,
                  ),
                  new Form(
                      key: _formRegisterKey,
                      child: new Theme(
                          data: new ThemeData(
                              primaryColor: Colors.myColor,
                              textSelectionColor: Colors.myColor,
                              accentColor: Colors.myColor),
                          child: new Container(
                            padding: const EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 0.0),
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment:
                              CrossAxisAlignment.stretch,
                              children: <Widget>[
                                new Row(
                                  children: <Widget>[
                                    new Expanded(
                                      child: new Container(
                                        padding: const EdgeInsets.only(
                                            right: 8.0),
                                        child: new TextFormField(
                                          controller: _firstNameController,
                                          keyboardType: TextInputType.text,
                                          onFieldSubmitted: (String value) {
                                            FocusScope.of(context).requestFocus(
                                                textFirstFocusNode);
                                          },
                                          maxLines: 1,
                                          validator: (value) {
                                            if (_firstNameController
                                                .text.isEmpty) {
                                              return Strings.emptyFirstName;
                                            }
                                          },
                                          decoration: new InputDecoration(
                                              labelText: Strings.hintFirstName),
                                        ),
                                      ),
                                      flex: 1,
                                    ),
                                    new Expanded(
                                      child: new Container(
                                        padding: const EdgeInsets.only(
                                            left: 8.0),
                                        child: new TextFormField(
                                          maxLines: 1,
                                          focusNode: textFirstFocusNode,
                                          keyboardType: TextInputType.text,
                                          onFieldSubmitted: (String value) {
                                            FocusScope
                                                .of(context)
                                                .requestFocus(
                                                textSecondFocusNode);
                                          },
                                          controller: _lastNameController,
                                          validator: (value) {
                                            if (_firstNameController
                                                .text.isNotEmpty) {
                                              if (_lastNameController
                                                  .text.isEmpty) {
                                                return Strings.emptyLastName;
                                              }
                                            }
                                          },
                                          decoration: new InputDecoration(
                                              labelText: Strings.hintLastName),
                                        ),
                                      ),
                                      flex: 1,
                                    )
                                  ],
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                ),
                                new TextFormField(
                                  controller: _phoneNumberController,
                                  focusNode: textSecondFocusNode,
                                  keyboardType: TextInputType.phone,
                                  onFieldSubmitted: (String value) {
                                    FocusScope
                                        .of(context)
                                        .requestFocus(textThirdFocusNode);
                                  },
                                  maxLines: 1,
                                  validator: (value) {
                                    if (_firstNameController
                                        .text.isNotEmpty) {
                                      if (_lastNameController
                                          .text.isNotEmpty) {
                                        if (_phoneNumberController
                                            .text.isNotEmpty) {
                                          if (Constants.isValidPhone(
                                              _phoneNumberController
                                                  .text)) {
                                            if (_emailAddressController
                                                .text.isEmpty) {
                                              if (internetConnection &&
                                                  internetConnection != null) {
                                                isEmail = false;
                                                _progressDialog(true);
                                                listenerExistPhone =
                                                new SetUpExistPhoneListener(
                                                    this, '${Constants
                                                    .url}SelectUserMasterByUserNameIsExistOrNot/${_phoneNumberController
                                                    .text}/2');
                                                listenerExistPhone
                                                    .loadExistPhone();
                                              } else {
                                                Constants.showSnackBar(
                                                    scaffoldStateRegister,
                                                    Strings.noInternetToast);
                                              }
                                            } else if (_emailAddressController
                                                .text.isNotEmpty) {
                                              if (Constants.isValidEmail(
                                                  _emailAddressController
                                                      .text)) {
                                                if (internetConnection &&
                                                    internetConnection !=
                                                        null) {
                                                  isEmail = true;
                                                  _progressDialog(true);
                                                  listenerExistPhone =
                                                  new SetUpExistPhoneListener(
                                                      this, '${Constants
                                                      .url}SelectUserMasterByUserNameIsExistOrNot/${_phoneNumberController
                                                      .text}/2');
                                                  listenerExistPhone
                                                      .loadExistPhone();
                                                } else {
                                                  Constants.showSnackBar(
                                                      scaffoldStateRegister,
                                                      Strings.noInternetToast);
                                                }
                                              }
                                            }
                                          } else {
                                            return Strings.invalidPhone;
                                          }
                                        } else {
                                          return Strings.emptyPhone;
                                        }
                                      }
                                    }
                                  },
                                  decoration: new InputDecoration(
                                      labelText: Strings.hintPhoneNumber),
                                ),
                                new TextFormField(
                                  maxLines: 1,
                                  controller: _emailAddressController,
                                  focusNode: textThirdFocusNode,
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (value) {
                                    if (_firstNameController
                                        .text.isNotEmpty) {
                                      if (_lastNameController
                                          .text.isNotEmpty) {
                                        if (_lastNameController
                                            .text.isNotEmpty) {
                                          if (Constants.isValidPhone(
                                              _phoneNumberController
                                                  .text)) {
                                            if (_emailAddressController
                                                .text.isNotEmpty) {
                                              if (Constants.isValidEmail(
                                                  _emailAddressController
                                                      .text) ==
                                                  false) {
                                                return Strings.invalidEmail;
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  },
                                  onFieldSubmitted: (String value) {
                                    FocusScope
                                        .of(context)
                                        .requestFocus(textForthFocusNode);
                                  },
                                  decoration: new InputDecoration(
                                      labelText:
                                      Strings.hintEmailAddressOptional),
                                ),
                                new TextFormField(
                                  maxLines: 1,
                                  controller: _referralCodeController,
                                  keyboardType: TextInputType.text,
                                  focusNode: textForthFocusNode,
                                  decoration: new InputDecoration(
                                      labelText:
                                      Strings.hintReferCode),
                                ),
                                new InkWell(
                                  onTap: () {
                                    _showDialogue();
                                  },
                                  child: new Container(
                                      alignment: Alignment.centerRight,
                                      padding: const EdgeInsets.fromLTRB(
                                          0.0, 8.0, 0.0, 0.0),
                                      child: new Text(
                                        Strings.labelHowDoesItWork,
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .caption
                                            .apply(color: Colors.myColor),
                                      )),
                                ),
                                new Padding(
                                    padding:
                                    new EdgeInsets.only(top: 17.0)),
                                new MaterialButton(
                                  color: Colors.myColor,
                                  textColor: Colors.white,
                                  child: new Text(Strings.labelButtonContinue),
                                  onPressed: () {
                                    _formRegisterKey.currentState
                                        .validate();
                                  },
                                ),
                              ],
                            ),
                          ))),
                  new Padding(padding: new EdgeInsets.only(top: 20.0)),
                  new Text(
                      Strings.labelAgreement,
                      textAlign: TextAlign.center,
                      style:
                      Theme
                          .of(context)
                          .textTheme
                          .body1),
                  new Padding(
                      padding: new EdgeInsets.only(top: 0.0)),
                  new InkWell(
                    onTap: (){
                      var route = new MaterialPageRoute(builder: (BuildContext context) =>new PrivacyPolicyScreen(Constants.PRIVACY_URL,"Privacy Policy"));
                      _navigateAndReturn(context, route, 0);
                    },
                    child: new Text(
                      Strings.labelAgreementPrivacyPolicy,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .apply(color: Colors.myColor),
                    ),
                  )
                ],
              ),
          ),
          new Container(
            height: 60.0,
            child: new InkWell(
              onTap: () {
                subscription == null;
                Navigator.pushNamedAndRemoveUntil(context, '/LoginPage', (Route<dynamic> route) => false);
              },
              child: new Container(
                foregroundDecoration: new ViewLineFromRegistration(),
                padding: new EdgeInsets.fromLTRB(0.0, 20.0, 10.0, 20.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text(
                      Strings.labelAlreadyHaveAccount,
                      style: Theme.of(context).textTheme.body2,
                    ),
                    new Text(
                      Strings.labelAlreadyHaveAccountSignIn,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.myColor),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void onErrorExistPhone() {
    print('Error Phone Exist in Registration Screen');
    _progressDialog(false);
  }

  @override
  void onSuccessExistPhone(ExistEmailOrPhoneModel model) {
    existPhoneModel = model;
    _existPhone();
  }

  @override
  void onErrorExistEmail() {
    print('Error Email Exist in Registration Screen');
    _progressDialog(false);
  }

  @override
  void onSuccessExistEmail(ExistEmailOrPhoneModel model) {
    existEmailModel = model;
    _existEmail();
  }

  @override
  void onErrorOneTimePassword(onError) {
    print('Error OTP in Registration Screen  - $onError');
    _progressDialog(false);
  }

  @override
  void onSuccessOneTimePassword(OneTimePasswordModel model) {
    oneTimePasswordModel = model;
    /*isLoading = false;*/
    _progressDialog(false);
    if (oneTimePasswordModel.selectRegisterUserForgotPasswordResult != null) {
      Constants.OTP =
          oneTimePasswordModel.selectRegisterUserForgotPasswordResult;
//      Constants.showSnackBar(scaffoldStateRegister, Constants.OTP);
      UserModel loginModel = new UserModel(null, null, null, null, _emailAddressController.text, null, _firstNameController.text, _lastNameController.text, null, _phoneNumberController.text, null, false, null, true, null, _referralCodeController.text, null, null, null, null, null, null,false,false);

      var route = new MaterialPageRoute(builder: (BuildContext context) =>new OneTimePasswordScreen(userModelRegistration: loginModel));
      _navigateAndReturn(context, route, 0);
    }
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  void _existPhone() {
    if (existPhoneModel.selectUserMasterByUserNameIsExistOrNotResult == false) {
      if (isEmail == true) {
        listenerExistEmail = new SetUpExistEmailListener(this, '${Constants.url}SelectUserMasterByUserNameIsExistOrNot/${_emailAddressController.text}/1');
        listenerExistEmail.loadExistEmail();
      } else {
        print('Generate OTP');
        listenerOneTimePassword = new SetUpOneTimePasswordListener(this, '${Constants.url}SelectRegisterUserForgotPassword/${_phoneNumberController.text}/Phone');
        listenerOneTimePassword.loadOneTimePassword();
      }
    } else {
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStateRegister, Strings.errorAlreadyHaveAccount);
    }
  }

  void _existEmail() {
    if (existEmailModel.selectUserMasterByUserNameIsExistOrNotResult == false) {
      print('Generate OTP');
      listenerOneTimePassword = new SetUpOneTimePasswordListener(
          this,
          '${Constants
              .url}SelectRegisterUserForgotPassword/${_phoneNumberController
              .text}/Phone');
      listenerOneTimePassword.loadOneTimePassword();
    } else {
      _progressDialog(false);
      Constants.showSnackBar(
          scaffoldStateRegister, Strings.errorAlreadyEmailExist);
    }
  }

  void _showDialogue(){
    showDialog(context: context,child: new SimpleDialog(
      contentPadding: EdgeInsets.fromLTRB(0.0,10.0,0.0,10.0),
      children: <Widget>[
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 15.0),
          child: new Row(
            children: <Widget>[
              new Expanded(child: new Text(Strings.labelHowDoesItWork,style: Theme.of(context).textTheme.title,)),
              new InkWell(
                onTap: (){Navigator.pop(context);},
                child: new Icon(Icons.close),
              )
            ],
          ),
        ),
        new Container(
          foregroundDecoration: new ViewLineFromRegistration(),
        ),
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  child: new Center(child: Text("1",textAlign: TextAlign.center,style: Theme.of(context).textTheme.subhead,),),
                  width: 0.0,
                  height: 35.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color: Colors.myColor,
                      width: 0.5, ), ),
                ),),
              new Expanded(
                  flex: 7,
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 80.0,
                          width: 80.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.myColor,
                              width: 2.0, ), ),
                          child: new Center(child: new Icon(Icons.share,size: 50.0,color: Colors.myColor,)),
                        ),
                      ],
                    ),
                  )),
              new Expanded(
                  flex: 1,
                  child: new Text(" "))
            ],
          ),
        ),
        new Text(Strings.labelFirst,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  child: new Center(child: Text("2",textAlign: TextAlign.center,style: Theme.of(context).textTheme.subhead,),),
                  width: 0.0,
                  height: 35.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color: Colors.myColor,
                      width: 0.5, ), ),
                ),),
              new Expanded(
                  flex: 7,
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 80.0,
                          width: 80.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.myColor,
                              width: 2.0, ), ),
                          child: new Center(child: new Image(image: new AssetImage("images/ic_splash_icon_web.png"),width: 50.0,height: 50.0,color: Colors.myColor,)),
                        ),
                      ],
                    ),
                  )),
              new Expanded(
                  flex: 1,
                  child: new Text(" "))
            ],
          ),
        ),
        new Text(Strings.labelSecond,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  child: new Center(child: Text("3",textAlign: TextAlign.center,style: Theme.of(context).textTheme.subhead,),),
                  width: 0.0,
                  height: 35.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color: Colors.myColor,
                      width: 0.5, ), ),
                ),),
              new Expanded(
                  flex: 7,
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 80.0,
                          width: 80.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.myColor,
                              width: 2.0, ), ),
                          child: new Center(child: new Image(image: new AssetImage("images/ic_currency_usd_black_48dp.png"),width: 50.0,height: 50.0,color: Colors.myColor,)),
                        ),
                      ],
                    ),
                  )),
              new Expanded(
                  flex: 1,
                  child: new Text(" "))
            ],
          ),
        ),
        new Text(Strings.labelThird,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
        new Padding(padding: EdgeInsets.only(top: 10.0))
      ],
    ));
  }


  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    // 1 for edit account
    // 2 for change password
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
  }

  onResume(){
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);
      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

}
