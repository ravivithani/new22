import 'package:flutter/material.dart';
import 'package:crazyrex/model/claim_card_list_model.dart';
import 'package:crazyrex/page/claim_card_detail_screen.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ClaimCardActivationScreen extends StatefulWidget {
  final ClaimCardListModel model;
  final String cardExpiryDate;
  final String activeMembershipName;
  ClaimCardActivationScreen(this.model,this.cardExpiryDate,this.activeMembershipName);

  @override
  _ClaimCardActivationScreenState createState() => _ClaimCardActivationScreenState();
}

class _ClaimCardActivationScreenState extends State<ClaimCardActivationScreen> {
  String _groupValue;
  bool _groupValue1;

  String _activeNow = "true";
  String _activeLater = "false";

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Container(
          padding: EdgeInsets.all(20.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset("images/logo_with_name.png",height: 65.0,),
              new Padding(padding: new EdgeInsets.only(top: 15.0)),
              new Text("${Strings.labelCurrently} ${widget.activeMembershipName} ${Strings.labelMembershipIsActiveTill} ${widget.cardExpiryDate}",style: Theme.of(context).textTheme.title,textAlign: TextAlign.center,),
              new Padding(padding: new EdgeInsets.only(top: 5.0)),
              new Text("${Strings.labelMembershipSinceOnly}",style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
              new Padding(padding: new EdgeInsets.only(top: 40.0)),
              new RadioListTile(title: new Text(Strings.labelActiveNow),value: _activeNow.toString(), groupValue: _groupValue, onChanged: _onChange),
              new Padding(padding: new EdgeInsets.only(top: 5.0)),
              new Text("OR",textAlign: TextAlign.center,style: Theme.of(context).textTheme.caption,),
              new Padding(padding: new EdgeInsets.only(top: 5.0)),
              new RadioListTile(title: new Text(Strings.labelActiveLater),value: _activeLater.toString(), groupValue: _groupValue, onChanged: _onChange),
              new Padding(padding: new EdgeInsets.only(top: 27.0)),
              new Container(
                padding: EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
                child: new MaterialButton(onPressed: (){
                  if(_groupValue!=null){
                    var route = new MaterialPageRoute(builder: (BuildContext context) => new ClaimCardDetailScreen(claimCardModel: widget.model,buyOptions: 3,instantActive: _groupValue1,isRenewal: false,));
                    _navigateAndReturn(context, route, 0);
                  }else{
                    Fluttertoast.showToast(msg: "Please select valid option",toastLength: Toast.LENGTH_LONG);
                  }
                },child: new Text(Strings.labelButtonContinue,),color: Colors.myColor,textColor: Colors.white,)
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onChange(String active) {
    setState(() {
      _groupValue = active;
      print("_groupvalue  $_groupValue");
      if(_groupValue == "true"){
        _groupValue1 = true;
      }else if(_groupValue == "false"){
        _groupValue1  = false;
      }
      print(_groupValue);
    });
  }
  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    final result = await Navigator.push(context, rout);
  }


}
