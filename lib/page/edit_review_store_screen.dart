reviimport 'dart:async';
import 'dart:convert';

import 'package:crazyrex/model/review_store_model.dart';
import 'package:crazyrex/parser/insert_update_review_store_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/star_rating.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

class EditReviewStoreScreen extends StatefulWidget {
  final ReviewStoreModel editReviewStoreModel;
  final int storeId;
  final String businessImageName;

  EditReviewStoreScreen({Key key,this.editReviewStoreModel,this.storeId,this.businessImageName}) : super(key: key);

  @override
  _EditReviewStoreScreenState createState() => _EditReviewStoreScreenState();
}

class _EditReviewStoreScreenState extends State<EditReviewStoreScreen> {


  final GlobalKey<FormState> _formAddReviewKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldStateReview = new GlobalKey<ScaffoldState>();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  ReviewStoreModel model;
  var _reviewListener = new TextEditingController();
  String prefSessionId;
  String prefCustomerId;
  double rating = 0.0;

  bool isEditReview=false;


  int _storeId;
  String _businessImage;

  _EditReviewStoreScreenState(){
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value){
      prefSessionId = value;
    });
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((String value){
      prefCustomerId = value;
    });
  }

  @override
  void initState() {
    _internetConnection();

    _general();
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        internetConnection = true;
        setState(() {
        });

      } else {
        internetConnection = false;
        setState(() {
        });
      }
    });
  }
  //endregion

  @override
  void dispose() {
    subscription == null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new CustomScrollView(
        slivers: <Widget>[
          new SliverAppBar(
            expandedHeight: 250.0,
            floating: false,
            pinned: false,
            title: new Text(Strings.labelTitleAddReviewStoreScreen,style: Theme.of(context).textTheme.title.apply(color: Colors.white),),
            leading: new InkWell(
              child: new Icon(Icons.arrow_back,color: Colors.white,),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            flexibleSpace: new FlexibleSpaceBar(
              background: new Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  _businessImage!=null?new Image.network(_businessImage,fit: BoxFit.cover,color: Colors.black54,colorBlendMode: BlendMode.darken,)
                      :new Image.asset("images/bg33.png",fit: BoxFit.cover,color: Colors.black54,colorBlendMode: BlendMode.darken,),
                ],
              ),
            ),
          ),
          new SliverToBoxAdapter(
              child: new Stack(
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.all(18.0),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        new Text(Strings.labelHowWasExperience,textAlign: TextAlign.center,style: Theme.of(context).textTheme.title,),
                        new Padding(padding: new EdgeInsets.only(top: 30.0)),
                        new Text(rating==0.0?"0":rating.toString(),textAlign: TextAlign.center,style: Theme.of(context).textTheme.title,),
                        new Padding(padding: new EdgeInsets.only(top: 30.0)),
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new StarRating(
                              color: Colors.myColor,
                              rating: rating,
                              size: 45.0,
                              onRatingChanged: (rating) => setState(() => this.rating = rating),
                            )
                          ],
                        ),
                        new Padding(padding: new EdgeInsets.only(top: 30.0)),
                        new Text(Strings.labelWriteReview,textAlign: TextAlign.center,style: Theme.of(context).textTheme.body2,),
                        new Padding(padding: new EdgeInsets.only(top: 30.0)),
                        new Form(key: _formAddReviewKey ,
                            child: new Theme(data: new ThemeData(
                          primaryColor: Colors.myColor,
                          accentColor: Colors.myColor,
                          textSelectionColor: Colors.myColor
                        ),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                new TextFormField(
                                  keyboardType: TextInputType.multiline,
                                  maxLines: 2,
                                  controller: _reviewListener,
                                  validator: (String value){
                                    if(_reviewListener.text.isEmpty){
                                      Fluttertoast.showToast(msg: Strings.labelToastWriteDescription,toastLength: Toast.LENGTH_LONG);
                                    }else{
                                      if(rating == 0){
                                        Fluttertoast.showToast(msg: Strings.labelToastAddStarRate,toastLength: Toast.LENGTH_LONG);
                                      }else{
                                        if(internetConnection!=null && internetConnection) {
                                          _progressDialog(true);
                                          _editReview();
                                        }else if(internetConnection!=null && !internetConnection){
                                          Constants.showSnackBar(scaffoldStateReview, Strings.noInternetToast);
                                        }
                                      }
                                    }
                                  },
                                  decoration: InputDecoration(
                                    labelText: Strings.labelDescription,
                                    border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(4.0))),
                                  ),
                                ),
                                new Padding(padding: new EdgeInsets.only(top: 27.0)),
                                new MaterialButton(onPressed: (){
                                  _formAddReviewKey.currentState.validate();
                                },child: new Text(Strings.labelButtonSubmit),color: Colors.myColor,textColor: Colors.white,)
                              ],
                            )))
                      ],
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }

  _general() {
    if(widget.editReviewStoreModel!=null){
      model = widget.editReviewStoreModel;
      _businessImage = model.businessImageName;
      isEditReview = true;
      rating = double.parse(model.starRating);
      _reviewListener.text = model.review;
    }else{
      isEditReview = false;
      _businessImage = widget.businessImageName;
      _storeId = widget.storeId;
    }
  }

  _editReview() {
    setState(() {
      int errorCode;

      Map<String, dynamic> body2;

      if(isEditReview){
        body2 = {
          'ReviewMasterData':{
            'SessionId':prefSessionId,
            'ReviewMasterId':model.reviewMasterId,
            'Review':_reviewListener.text,
            'StarRating':rating,
          }
        };
      }else{
        body2 = {
          'ReviewMasterData':{
            'Review':_reviewListener.text,
            'SessionId':prefSessionId,
            'StarRating':rating,
            'linktoCustomerMasterId':prefCustomerId,
            'linktoBusinessMasterId':_storeId,
            'IsShow':true,
          }
        };
      }

      var encoder = JSON.encode(body2);
      var url;
      if(isEditReview){
        url = '${Constants.url}UpdateReviewMaster';
      }else{
        url = '${Constants.url}InsertReviewMaster';
      }


      EditReviewStoreParser  editReviewStoreParser = new EditReviewStoreParser ();
      Future<int> result = editReviewStoreParser.editReviewStore(url, encoder,isEditReview);
      result.then((c) {
        errorCode = c;
        _handleResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        _progressDialog(false);
      });
    });
  }

  _handleResult(int errorCode) {
    if (errorCode == 0) {
      setState(() {
        _progressDialog(false);
        Fluttertoast.showToast(msg: Strings.labelToastSuccessAddReview,toastLength: Toast.LENGTH_LONG);
        subscription == null;
        Navigator.pop(context);

      });
    } else if (errorCode == -1) {
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStateReview, Strings.errorInServices);
    }
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

}
