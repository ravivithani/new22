import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:crazyrex/model/outlet_list_model.dart';
import 'package:crazyrex/page/store_detail_screen.dart';
import 'package:crazyrex/parser/outlet_list_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:flutter/material.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';
import 'package:latlong/latlong.dart';
import 'package:location/location.dart';
import 'package:fluttertoast/fluttertoast.dart';

class OutletStoreListScreen extends StatefulWidget {
  final int storeId;


  OutletStoreListScreen(this.storeId);

  @override
  _OutletStoreListScreenState createState() => _OutletStoreListScreenState();
}

class _OutletStoreListScreenState extends State<OutletStoreListScreen> implements OutletListDataListener{
  List<OutletListModel> outletList1;

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  bool isError = false;
  bool isLoading = false;

  int _cityId;
  int _storeId;

  SetUpOutletListData setUpOutletListData;

  Map<String, double> _startLocation;
  Map<String, double> _currentLocation;
  StreamSubscription<Map<String, double>> _locationSubscription;
  Location _location = new Location();
  double latitude, longitude;

  _OutletStoreListScreenState(){
    PrefUtil.getIntPreference(Constants.prefCityId).then((int value) {
      _cityId = value;
    });
  }


  @override
  void initState() {

    _storeId = widget.storeId;

    initPlatformState();

    _locationSubscription = _location.onLocationChanged.listen((Map<String, double> result) {
      _currentLocation = result;
      if (latitude == null || longitude == null) {
        latitude = _startLocation['latitude'];
        longitude = _startLocation['longitude'];
      }
    });
    _internetConnection();
  }

  initPlatformState() async {
    Map<String, double> location;
    // Platform messages may fail, so we use a try/catch PlatformException.

    try {
      location = await _location.getLocation;
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        Fluttertoast.showToast(msg: Strings.appNeedLocationPermission,toastLength: Toast.LENGTH_LONG);
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        Fluttertoast.showToast(msg: Strings.appNeedLocationPermission,toastLength: Toast.LENGTH_LONG);
      }
      location = null;
    }

    _startLocation = location;
    if (latitude == null || longitude == null) {
      latitude = _startLocation['latitude'];
      longitude = _startLocation['longitude'];
    }
  }
  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
            internetConnection = true;
            setState(() {
              isLoading = true;
            });
          } else {
            internetConnection = false;
            setState(() {});
          }
        });
  }
//endregion

  @override
  void dispose() {
    if (subscription != null) {
      subscription == null;
    }
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          _callApi();
        }
        return isLoading
            ? new Scaffold(
          body: new Center(
            child: new CircularProgressIndicator(),
          ),
        )
            : _build();
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return Scaffold();
    }
  }
  Widget _build(){
    return new Scaffold(
      appBar: AppBar(title: new Text(Strings.labelTitleOutletListScreen),),
      body: new ListView.builder(shrinkWrap: false,
          itemCount: outletList1.length,
          itemBuilder: (BuildContext context,int index){
            OutletListModel model = outletList1[index];
            return new InkWell(
              onTap: (){handleClickOfItem(model);},
              child: new Card(
                elevation: 2.0,
                margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, index == outletList1.length-1?6.0: 0.0),
                child: new Container(
                  margin: EdgeInsets.fromLTRB(12.0, 10.0, 10.0, 10.0),
                  child: new Column(
                    children: <Widget>[
                      new Container(
                        child: new Row(
                          children: <Widget>[
                            new Card(
                              child: new Container(
                                height: 75.0,
                                width: 75.0,
                                child: new FadeInImage(
                                    fit: BoxFit.cover,
                                    placeholder:
                                    new AssetImage("images/bg33.png"),
                                    image: new NetworkImage(model.xsImageName)),
                              ),
                            ),
                            new Expanded(child: new Container(
                              margin: EdgeInsets.only(left: 6.0),
                              child: new Column(
                                children: <Widget>[
                                  new Row(
                                    children: <Widget>[
                                      new Expanded(flex:4,child: new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Padding(padding: EdgeInsets.only(top: 5.0)),
                                          new Text(model.businessName, textAlign: TextAlign.start, softWrap: false, style: Theme.of(context).textTheme.body2, maxLines: 1,),
                                          new Padding(padding: EdgeInsets.only(top: 4.0)),
                                          new Row(
                                            children: <Widget>[
                                              new Text(model.businessType, textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption,),
                                              Padding(padding: EdgeInsets.only(left: 3.0)),
                                              vegNonVeg(model)
                                            ],
                                          ),
                                          Padding(padding: new EdgeInsets.only(top: 4.0),),
                                          new Text(model.area, textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption,),
                                          Padding(padding: new EdgeInsets.only(top: 4.0),),
                                        ],
                                      )),
                                      model.totalRating != "0.0"
                                          ? new Expanded(flex: 1,
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.end,
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: <Widget>[
                                              new Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  model.totalRating != "0.0"
                                                      ? new Icon(Icons.star, color: Colors.green,)
                                                      : new Container(),
                                                  model.totalRating != "0.0"
                                                      ? new Text(model.totalRating, textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption,)
                                                      : new Container(),
                                                ],
                                              ),
                                            ],
                                          ))
                                          : new Container()
                                    ],
                                  ),
                                  new Row(
                                    children: <Widget>[
                                      new Expanded(child: _txtOpenClose(model)),
                                      model.kilometer != null
                                          ? new Text("${model.kilometer} km", textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption,)
                                          : new Container(),
                                    ],
                                  ),
                                ],
                              ),
                            ))
                          ],
                        ),
                      ),
                      new Padding(padding: EdgeInsets.only(top: 10.0)),
                      new Container(
                        foregroundDecoration: ViewLineFromRegistration(),
                      ),
                      new Padding(padding: EdgeInsets.only(top: 10.0)),
                      new Container(
                        child: new Row(
                          children: <Widget>[
                            new Padding(padding: EdgeInsets.only(left: 8.0)),
                            new Expanded(child: new Text(
                                "${model.countOffer} ${Strings.labelOffers}",
                                textAlign: TextAlign.start,
                                softWrap: false,
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(color: Colors.myColor))),
                            new Text("${model.offerUsedCount} ${Strings.labelTimeUsed}", textAlign: TextAlign.start, softWrap: false, style: Theme.of(context).textTheme.caption)
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(padding: EdgeInsets.only(top: 16.0),),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(padding: EdgeInsets.only(top: 27.0),),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }
  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
          ],
        ));
  }

  Widget vegNonVeg(OutletListModel model) {
    if (model.vegNonVeg == 1) {
      //veg
      return new Image.asset("images/ic_veg.png",height: 10.0,width: 10.0,);
    } else if (model.vegNonVeg == 2) {
      //non veg
      return new Image.asset("images/nonveg.png",height: 10.0,width: 10.0,);
    } else if (model.vegNonVeg == 3) {
      // veg and non veg
      return new Row(
        children: <Widget>[
          new Image.asset("images/ic_veg.png",height: 10.0,width: 10.0,),
          Padding(padding: EdgeInsets.only(left: 2.0)),
          new Image.asset("images/nonveg.png",height: 10.0,width: 10.0,),
        ],
      );
    }
    return new Image.asset("");
  }

  Widget _txtOpenClose(OutletListModel model) {
    String label = "";
    Color color;
    DateFormat dateFormat = new DateFormat.Hm();

    DateTime now = DateTime.now();
    DateTime open = dateFormat.parse(model.openingTime);
    open = new DateTime(now.year, now.month, now.day, open.hour, open.minute);

    DateTime breakStart = dateFormat.parse(model.breakStartTime);
    breakStart = new DateTime(now.year, now.month, now.day, open.hour, open.minute);

    DateTime breakEnd = dateFormat.parse(model.breakEndTime);
    breakEnd = new DateTime(now.year, now.month, now.day, open.hour, open.minute);

    DateTime close = dateFormat.parse(model.closingTime);
    close = new DateTime(now.year, now.month, now.day, close.hour, close.minute);

    if (now.isAfter(open) && now.isBefore(breakStart)) {
      label = "Open now";
      color = Colors.green;
    } else if(now.isAfter(breakEnd) && now.isBefore(close)){
      label = "Open now";
      color = Colors.green;
    }else{
      label = "Closed";
      color = Colors.red;
    }

    return new Text(
      label,
      textAlign: TextAlign.start,
      style: Theme.of(context).textTheme.caption.apply(color: color),
    );
  }

  void setKilometer() {
    if (longitude != null || latitude != null) {
      for (int i = 0; i < outletList1.length; i++) {
        outletList1[i].kilometer = calculateDistance(latitude, longitude, double.parse(outletList1[i].latitude), double.parse(outletList1[i].longitude));
      }
    }
  }

  int calculateDistance(double currentLat, double currentLng, double destinationLat, double destinationLng) {
    final Distance distance = new Distance();
    double km;
    km = distance.as(LengthUnit.Kilometer, new LatLng(currentLat, currentLng),
        new LatLng(destinationLat, destinationLng));
    print("km1 cail ${km.ceil()}");
    print("km1 floor ${km.floor()}");
    print("km1 round ${km.round()}");

    return km.round();
  }
  @override
  void onErrorOutletList(String error) {
    print("Error in Outlet List Screen $error");

    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  _callApi(){
    setUpOutletListData = new SetUpOutletListData("${Constants.url}SelectAllBusinessMasterByGroup/$_storeId/$_cityId",this);
    setUpOutletListData.voidLoadOutletListData();
  }
  @override
  void onOutletListListener(List<OutletListModel> outletList) {
    outletList1 = outletList;

    setState(() {
      isLoading = false;
      isError = false;
    });

  }

  handleClickOfItem(OutletListModel model) {
    var route = new MaterialPageRoute(
        builder: (BuildContext context) =>
        new StoreDetailScreen(model.businessMasterId));
    _navigateAndReturn(context, route, 0);
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);
          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            setState(() {
              internetConnection = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }
}
