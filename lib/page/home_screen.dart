import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:crazyrex/model/advertisement_model.dart';
import 'package:crazyrex/model/category_model.dart';
import 'package:crazyrex/model/store_list_model.dart';
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/page/about_us_screen.dart';
import 'package:crazyrex/page/all_store_tab_screen.dart';
import 'package:crazyrex/page/city_selection_screen.dart';
import 'package:crazyrex/page/claim_card_list_screen.dart';
import 'package:crazyrex/page/claim_card_screen.dart';
import 'package:crazyrex/page/my_account_screen.dart';
import 'package:crazyrex/page/my_offer_screen.dart';
import 'package:crazyrex/page/notification_screen.dart';
import 'package:crazyrex/page/offer_description_screen.dart';
import 'package:crazyrex/page/offer_list_screen.dart';
import 'package:crazyrex/page/refer_and_earn_screen.dart';
import 'package:crazyrex/page/search_screen.dart';
import 'package:crazyrex/page/store_detail_screen.dart';
import 'package:crazyrex/page/suggestion_screen.dart';
import 'package:crazyrex/page/wallet_history_screen.dart';
import 'package:crazyrex/parser/home_screeen_data_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/CarouselImageSlider.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:package_info/package_info.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with
        HandleClickOnImage,
        HomeScreenDataListener,
        HandleLifeCycle,
        WidgetsBindingObserver {
  //region internet var
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isError = false;
  bool isLoading = false;

  //endregion

  //region share
  String packageName;
  String appName;
  String buildNumber;
  PackageInfo _packageInfo = new PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  int _membershipLevel;

  Future<Null> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      packageName = info.packageName;
      appName = info.appName;
      buildNumber = info.buildNumber;
    });
  }

  //endregion

  bool isKycDetail = false;
  bool isMembershipBought = false;

  UserModel userModel;
  int userId, cityId;
  String customerId, sessionId, cityName;

  setUpHomeScreenListner homeScreenListener;

  List<AdvertisementModel> advertisementList;

  List<CategoryModel> categoryList;

  List<StoreListModel> recentlyJoinedList;

  int catListPosition = 0;

  int recentlyJoinedListPosition = 0;

  changeStatusColor(Color color) async {
    await FlutterStatusbarcolor.setStatusBarColor(color);
  }

  _HomeScreenState() {
    changeStatusColor(Colors.myColor);
    PrefUtil.getIntPreference(Constants.prefRegisterUserMasterId).then((
        int value) {
      userId = value;
    });
    _initPackageInfo();
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((
        String value) {
      customerId = value;
    });
    PrefUtil.getStringPreference(Constants.prefCityName).then((String value) {
      cityName = value;
    });
    PrefUtil.getIntPreference(Constants.prefCityId).then((int value) {
      cityId = value;
    });
    PrefUtil.getBoolPreference(Constants.prefKycDetail).then((bool value) {
      isKycDetail = value;
    });
  }

  @override
  void initState() {
    super.initState();
    catListPosition = 0;
    recentlyJoinedListPosition = 0;
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    WidgetsBinding.instance.addObserver(new LifecycleEventHandler(this));
    _internetConnection();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            //    _isLoading = true;
            setState(() {
              internetConnection = true;
              isLoading = true;
              catListPosition = 0;
              recentlyJoinedListPosition = 0;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return _layoutMain();
  }

  Widget _layoutMain() {
    //region build

    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          _callApis();
        }
        return _build();
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return new Scaffold();
    }
    //endregion
  }

  Widget _build() {
    return isLoading
        ? new Scaffold(
        body: new Center(
          child: new CircularProgressIndicator(),
        ))
        : new Scaffold(
      drawer: _drawer(),
      appBar: new AppBar(
        primary: true,
        title: new Text(Strings.labelHomeScreenTitle),
        actions: <Widget>[
          new Container(
            height: kToolbarHeight,
            width: kToolbarHeight,
            child: new InkWell(
              onTap: () {
                var route = new MaterialPageRoute(builder: (BuildContext context) => new WalletHistoryScreen());
                _navigateAndReturn(context, route, 0);
              },
                child: Center(
                  child: new Text(
                      userModel.walletBalance.isNotEmpty
                          ? "\u20B9${userModel
                          .walletBalance}"
                          : "\u20B9 0",
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .apply(color: Colors.myColor)),
                )
            ),
          ),
          new InkWell(
            onTap: () {
//                        shortDialog();
              subscription == null;
              var route = new MaterialPageRoute(
                  builder: (BuildContext context) => new SearchScreen());
              _navigateAndReturn(context, route, 0);
            },
            child: new Container(
              margin: EdgeInsets.all(10.0),
              child: new Icon(
                Icons.search,
                size: 30.0,
//                          color: Colors.black54,
              ),
            ),
          )
        ],
      ),
      body: new RefreshIndicator(
        child: new ListView(
          children: <Widget>[_homeWidget()],
        ),
        onRefresh: refresh,
      ),
    );
  }

  Future<Null> refresh() async {
    await Future.delayed(Duration(seconds: 1));
    setState(() {
      isLoading = true;
    });
    return null;
  }

  _callApis() {
    String url = "${Constants.url}SelectAllHomeDetails/$cityId/1/$userId/$customerId";
    print("url :- $url");
    homeScreenListener = new setUpHomeScreenListner(this, url);
    homeScreenListener.loadHomeScreenData();
  }

  Widget _drawer() {
    return new Drawer(
      child: new ListView(
        padding: const EdgeInsets.all(0.0),
        children: <Widget>[
          new Container(
            child: new DrawerHeader(
                decoration: BoxDecoration(color: Colors.myColor),
                child: new Container(
                  child: new Column(
                    children: <Widget>[
                      Flexible(
                          flex: 3,
                          child: new InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                              var route = new MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                  new MyAccountScreen());
                              _navigateAndReturn(context, route, 0);
                            },
                            child: new Row(
                              children: <Widget>[
                                new Flexible(
                                  child: userModel.profileImageName != null
                                      ? new Container(
                                      width: 50.0,
                                      height: 50.0,
                                      decoration: new BoxDecoration(
                                        color: Colors.transparent,
                                        shape: BoxShape.circle,
                                        image: new DecorationImage(
                                            image: new NetworkImage(
                                                userModel.profileImageName),
                                            fit: BoxFit.cover),
                                      ))
                                      : new Icon(
                                    Icons.account_circle, size: 50.0,
                                    color: Colors.white70,),
                                ),
                                Flexible(child: new Container(
                                  margin: EdgeInsets.fromLTRB(
                                      5.0, 33.0, 0.0, 0.0),
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: <Widget>[
                                      new Text(
                                          (userModel == null ||
                                              userModel.firstName.isEmpty ||
                                              userModel.lastName.isEmpty)
                                              ? Strings.labelUserName
                                              : "${userModel
                                              .firstName} ${userModel
                                              .lastName}",
                                          textAlign: TextAlign.start,
                                          softWrap: false,
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .body2
                                              .apply(color: Colors.white)),
                                      new Padding(
                                          padding: EdgeInsets.only(top: 3.0)),
                                      new Text(
                                          userModel == null ||
                                              userModel
                                                  .phone.isEmpty
                                              ? Strings.labelPhone
                                              : "${userModel.phone}",
                                          softWrap: true,
                                          textAlign: TextAlign.start,
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .body1
                                              .apply(
                                              color:
                                              Colors.white70)),
                                    ],
                                  ),
                                )),
                              ],
                            ),
                          )),
                      Flexible(
                        flex: 1,
                        child: new Container(
                          child: new Row(
                            children: <Widget>[
                              Expanded(
                                flex: 4,
                                child: Container(
                                    child: new InkWell(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                        var route = new MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                            new CitySelectionScreen(false));
                                        _navigateAndReturn(context, route, 0);
                                      },
                                      child: new Row(
                                        children: <Widget>[
                                          new Icon(
                                            Icons.location_on,
                                            size: 18.0,
                                            color: Colors.white,
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left: 5.0),
                                          ),
                                          new Text(
                                              cityName.isNotEmpty
                                                  ? cityName
                                                  : Strings.labelCity,
                                              softWrap: true,
                                              textAlign: TextAlign.center,
                                              style: Theme
                                                  .of(context)
                                                  .textTheme
                                                  .body1
                                                  .apply(color: Colors.white)),
                                        ],
                                      ),
                                    )),
                              ),
                              Expanded(
                                child: Container(
                                  child: new Row(
                                    children: <Widget>[
                                      new InkWell(
                                        onTap: () {
                                          _logOut();
                                        },
                                        child: new Text(Strings.labelLogOut,
                                            softWrap: true,
                                            textAlign: TextAlign.center,
                                            style: Theme
                                                .of(context)
                                                .textTheme
                                                .body1
                                                .apply(color: Colors.white)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )),
          ),
          new Container(
            child: new Column(
              children: <Widget>[
                new ListTile(
                  leading: new Icon(Icons.person),
                  title: new Text(Strings.labelMyAccount,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.black54)),
                  onTap: () {
                    setState(() {
                      Navigator.of(context).pop();
                      var route = new MaterialPageRoute(
                          builder: (BuildContext context) =>
                          new MyAccountScreen());
                      _navigateAndReturn(context, route, 0);
                    });
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.local_offer),
                  title: new Text(Strings.labelMyOffer,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.black54)),
                  onTap: () {
                    setState(() {
                      Navigator.of(context).pop();
                      var route = new MaterialPageRoute(
                          builder: (BuildContext context) =>
                          new MyOfferScreen(
                            isFromOffer: false,
                          ));
                      _navigateAndReturn(context, route, 0);
                    });
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.notifications),
                  title: new Text(Strings.labelNotifications,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.black54)),
                  onTap: () {
                    setState(() {
                      Navigator.of(context).pop();
                      var route = new MaterialPageRoute(
                          builder: (BuildContext context) =>
                          new NotificationScreen());
                      _navigateAndReturn(context, route, 0);
                    });
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.account_balance_wallet),
                  title: new Text(Strings.referAndEarn,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.black54)),
                  onTap: () {
                    setState(() {
                      Navigator.of(context).pop();
                      var route = new MaterialPageRoute(
                          builder: (BuildContext context) => new ReferAndEarnScreen(isMembership: isMembershipBought,membershipLevel :_membershipLevel,earnPoint: userModel.earnPoint));
                      _navigateAndReturn(context, route, 0);
                    });
                  },
                ),
                new ListTile(
                  leading: new Image.asset(
                    "images/ic_crx_icon.png",
                    color: Colors.black54,
                    height: 25.0,
                  ),
                  title: new Text(Strings.joinCrx,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.black54)),
                  onTap: () {
                    setState(() {
                      Navigator.of(context).pop();
                      var route = new MaterialPageRoute(builder: (
                          BuildContext context) => new ClaimCardListScreen());
                      _navigateAndReturn(context, route, 0);
                    });
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.store),
                  title: new Text(Strings.labelSuggestion,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.black54)),
                  onTap: () {
                    setState(() {
                      Navigator.of(context).pop();
                      var route = new MaterialPageRoute(
                          builder: (BuildContext context) =>
                          new SuggestScreen());
                      _navigateAndReturn(context, route, 0);
                    });
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.share),
                  title: new Text(Strings.labelSmallShare,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.black54)),
                  onTap: () {
                    setState(() {
                      Navigator.of(context).pop();
                      share();
                    });
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.star),
                  title: new Text(Strings.labelRateApp,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.black54)),
                  onTap: () {
                    setState(() {
                      String appUrlForAndroid ="http://play.google.com/store/apps/details?id=${packageName}";
                      String appUrlForIphone = "http://itunes.apple.com/+91/app/${_packageInfo.appName}/id$packageName?mt=8";
                      Navigator.of(context).pop();
                      _launchInWebViewOrVC(defaultTargetPlatform ==TargetPlatform.iOS ? appUrlForIphone : appUrlForAndroid);
                    });
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.info),
                  title: new Text(Strings.labelAboutUs,
                      textAlign: TextAlign.start,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.black54)),
                  onTap: () {
                    setState(() {
                      Navigator.of(context).pop();
                      var route = new MaterialPageRoute(
                          builder: (BuildContext context) =>
                          new AboutUsScreen());
                      _navigateAndReturn(context, route, 0);
                    });
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .title),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _homeWidget() {
    return new Container(
      child: new Column(
        children: <Widget>[
          advertisementList.length != 0
              ? new Container(
              height: 180.0, child: new Container(child: _imageSlider()))
              : new Container(),
          new Container(
              height: 63.0,
              child: new InkWell(
                onTap: () {
                  _clickOnAllStore();
                },
                child: new Card(
                  elevation: 2.0,
                  margin: new EdgeInsets.fromLTRB(6.0, 8.0, 6.0, 0.0),
                  child: new Container(
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: new Row(
                            children: <Widget>[
                              Padding(
                                  padding: new EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0)),
                              new Icon(
                                Icons.store_mall_directory,
                                size: 27.0,
                                color: Colors.black54,
                              ),
                              Padding(
                                  padding: new EdgeInsets.fromLTRB(
                                      15.0, 0.0, 0.0, 0.0)),
                              new Text("All Stores",
                                  softWrap: true,
                                  textAlign: TextAlign.center,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .body2),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: new Container(
                              margin: new EdgeInsets.only(right: 8.0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  new Text(Strings.labelShowMe,
                                      softWrap: true,
                                      textAlign: TextAlign.end,
                                      style: Theme
                                          .of(context)
                                          .textTheme
                                          .body2
                                          .apply(color: Colors.myColor)),
                                  new Icon(
                                    Icons.navigate_next,
                                    color: Colors.myColor,
                                  ),
                                ],
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          categoryList.length != 0 ? categoryListWidget() : new Container(),
          recentlyJoinedList.length != 0
              ? recentlyJoinedListWidget()
              : new Container(),
          new Container(
              height: 63.0,
              child: new Card(
                elevation: 2.0,
                margin: new EdgeInsets.fromLTRB(6.0, 8.0, 6.0, 8.0),
                child: new InkWell(
                  onTap: () {
                    _clickOnClimCard();
                  },
                  child: new Container(
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                            padding:
                            new EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0)),
                        new Image(
                          image: new AssetImage("images/ic_crx_icon.png"),
                          color: Colors.black54,
                          height: 27.0,
                        ),
                        Padding(
                            padding:
                            new EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0)),
                        new Text(
                            isMembershipBought
                                ? Strings.labelShowClaimCard
                                : Strings.labelBeCrazyMember,
                            softWrap: true,
                            textAlign: TextAlign.center,
                            style: Theme
                                .of(context)
                                .textTheme
                                .body2),
                      ],
                    ),
                  ),
                ),
              )),
        ],
      ),
    );
  }

  Widget categoryListWidget() {
    catListPosition = 0;
    return new Card(
        margin: new EdgeInsets.fromLTRB(6.0, 8.0, 6.0, 0.0),
        elevation: 2.0,
        child: new Column(
          children: <Widget>[
            new Container(
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                      padding: new EdgeInsets.fromLTRB(15.0, 40.0, 0.0, 0.0)),
                  new Icon(
                    Icons.local_offer,
                    color: Colors.black54,
                    size: 27.0,
                  ),
                  Padding(
                      padding: new EdgeInsets.fromLTRB(15.0, 40.0, 0.0, 0.0)),
                  new Text(Strings.labelOfferCategory,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2),
                ],
              ),
              margin: new EdgeInsets.fromLTRB(0.0, 6.0, 0.0, 0.0),
            ),
            new Column(
              children: _buildGridTiles(),
            ),
            Padding(
              padding: new EdgeInsets.only(bottom: 6.0),
            )
          ],
        ));
  }

  Widget recentlyJoinedListWidget() {
    recentlyJoinedListPosition = 0;
    return new Card(
        margin: new EdgeInsets.fromLTRB(6.0, 8.0, 6.0, 0.0),
        elevation: 2.0,
        child: new Column(
          children: <Widget>[
            new Container(
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                            padding:
                            new EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0)),
                        new Icon(
                          Icons.access_time,
                          size: 27.0,
                          color: Colors.black54,
                        ),
                        Padding(
                            padding:
                            new EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0)),
                        new Text(Strings.labelRecentlyJoined,
                            softWrap: true,
                            textAlign: TextAlign.center,
                            style: Theme
                                .of(context)
                                .textTheme
                                .body2),
                      ],
                    ),
                  ),
                  Expanded(
                      flex: 1,
                      child: new GestureDetector(
                        onTap: () {
                          _onClickViewAllRecently();
                        },
                        child: new Container(
                          margin: new EdgeInsets.only(right: 15.0),
                          child: new Text(Strings.labelViewAll,
                              softWrap: true,
                              textAlign: TextAlign.end,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .body2
                                  .apply(color: Colors.myColor)),
                        ),
                      )),
                ],
              ),
              margin: new EdgeInsets.fromLTRB(0.0, 6.0, 0.0, 0.0),
            ),
            new Container(
              margin: new EdgeInsets.all(6.0),
              child: new Column(
                children: _buildGridTileForRecentlyJoined(),
//                    Padding(padding: new EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),),
              ),
            ),
          ],
        ));
  }

  Widget _imageSlider() {
    var images = [];
    for (int i = 0; i < advertisementList.length; i++) {
      images.add(new NetworkImage(advertisementList[i].md_ImageName));
    }
    return new Center(
      child: new Scaffold(
        backgroundColor: Colors.white,
//        body: ImageCarousel(),
        body: new Container(
            margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
//            padding: new EdgeInsets.all(5.0),
            height: 195.0,
            child: new ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: Carousel(
                boxFit: BoxFit.cover,
                autoplay: true,
                clickListener: this,
                image: images,
                indicatorBgPadding: 10.0,
                dotSize: 5.0,
                dotSpacing: 15.0,
                dotBgColor: Colors.transparent,
                animationCurve: Curves.fastOutSlowIn,
                animationDuration: Duration(seconds: 2),
                autoplayDuration: Duration(seconds: 4),
              ),
            )),
      ),
    );
  }

  _buildGridTiles() {
    int column = (categoryList.length / 3).ceil();
    print("Colunm $column");
    List<GestureDetector> rows =
    new List<GestureDetector>.generate(column, (int index) {
      return new GestureDetector(
          onTap: () {
            print("index perent $index");
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _buildGridItem(index),
          ));
    });

    return rows;
  }

  _buildGridItem(int superIndex) {
    List<Container> containers = new List<Container>.generate(3, (int index) {
      catListPosition == 0 ? catListPosition = index : catListPosition++;
      if (catListPosition <= categoryList.length - 1) {
        return new Container(
          margin: new EdgeInsets.only(top: 10.0),
          child: new InkWell(
            onTap: () {
              int position = (3 * superIndex) + (index);
              _onPressCategoryItem(position);
            },
            child: new Container(
              height: 100.0,
              width: MediaQuery
                  .of(context)
                  .size
                  .width / 3.2,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Image.network(
                    categoryList[catListPosition].xs_ImageName,
                    height: 55.0,
                    color: Colors.myColor,
                  ),
                  Padding(
                    padding: new EdgeInsets.only(top: 10.0),
                  ),
                  new Text(categoryList[catListPosition].businessType,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1),
                ],
              ),
            ),
          ),
        );
      } else {
        return new Container();
      }
    });
    return containers;
  }

  _buildGridTileForRecentlyJoined() {
    int column = (categoryList.length / 3).ceil();
    print("Colunm $column");
    List<GestureDetector> rows =
    new List<GestureDetector>.generate(column, (int index) {
      return new GestureDetector(
          onTap: () {
            print("index perent $index");
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _buildGridItemForRecentlyJoined(index),
          ));
    });

    return rows;
  }

  _buildGridItemForRecentlyJoined(int superIndex) {
    List<Container> containers = new List<Container>.generate(3, (int index) {
      recentlyJoinedListPosition == 0
          ? recentlyJoinedListPosition = index
          : recentlyJoinedListPosition++;
      if (recentlyJoinedListPosition <= recentlyJoinedList.length - 1) {
        return new Container(
            child: new Expanded(
              child: new InkWell(
                  onTap: () {
                    int position = (3 * superIndex) + (index);
                    _onPressStoreItem(position);
                  },
                  child: new Card(
                    child: Container(
                      height: 150.0,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Image.network(
                            recentlyJoinedList[recentlyJoinedListPosition]
                                .xs_ImageName,
                            height: 100.0,
                            width: MediaQuery
                                .of(context)
                                .size
                                .width,
                            fit: BoxFit.cover,
                          ),
                          Padding(
                            padding: new EdgeInsets.only(top: 5.0),
                          ),
                          new Text(
                            recentlyJoinedList[recentlyJoinedListPosition]
                                .businessName,
                            textAlign: TextAlign.center,
                            style: Theme
                                .of(context)
                                .textTheme
                                .body1,
                            maxLines: 1,
                          ),
                          Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                          ),
                          new Text(
                              recentlyJoinedList[recentlyJoinedListPosition]
                                  .area,
                              textAlign: TextAlign.center,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .caption),
                        ],
                      ),
                    ),
                  )),
            ));
      } else {
        return new Container();
      }
    });
    return containers;
  }

  //region clicks

  _clickOnClimCard() {
    if(isMembershipBought){
      var route = new MaterialPageRoute( builder: (BuildContext context) => new ClaimCardScreen());
      _navigateAndReturn(context, route, 0);
    }else{
      var route = new MaterialPageRoute( builder: (BuildContext context) => new ClaimCardListScreen());
      _navigateAndReturn(context, route, 0);
    }
  }

  _clickOnAllStore() {
    var route = new MaterialPageRoute(
        builder: (BuildContext context) =>
            new AllStoreTabScreen(false, false, 0));
    _navigateAndReturn(context, route, 0);
  }

  _onPressCategoryItem(int position) {
    print("position $position");
    print("${categoryList[position].toString()}");
    subscription == null;
    var route = new MaterialPageRoute(
        builder: (BuildContext context) => new OfferListScreen(
            title: "${categoryList[position].businessType} Offers",
            businessTypeMasterId: categoryList[position].businessTypeMasterId));
    _navigateAndReturn(context, route, 0);
  }

  _onPressStoreItem(int position) {
    print("position $position");
    print("${recentlyJoinedList[position].toString()}");
    var route = new MaterialPageRoute(
        builder: (BuildContext context) => new StoreDetailScreen(
            recentlyJoinedList[position].businessMasterId));
    _navigateAndReturn(context, route, 0);
  }

  _onClickViewAllRecently() {
    var route = new MaterialPageRoute(
        builder: (BuildContextcontext) =>
            new AllStoreTabScreen(true, false, 0));
    _navigateAndReturn(context, route, 0);
  }

  _logOut() {
    setState(() {
      PrefUtil.clearPreference();
      PrefUtil.putBoolPreference(Constants.prefIsFirstTimeLaunch, true);
      subscription == null;
      Navigator.of(context).pushReplacementNamed('/LoginPage');
    });
  }

  @override
  void onClickItemOfSlider(int position) {
    print("taped image- ${advertisementList[position].toString()}");
    if (advertisementList[position].advertisementTypeId == 1) {
      var route = new MaterialPageRoute(
          builder: (BuildContext context) => new OfferDescriptionScreen(
                offerId: advertisementList[position].linktoOfferMasterId,
                businessMasterId:
                    advertisementList[position].linktoBusinessMasterId,
                businessName: advertisementList[position].businessName,
                isFromStore: false,
              ));
      _navigateAndReturn(context, route, 0);
    } else if (advertisementList[position].advertisementTypeId == 2) {
      var route = new MaterialPageRoute(
          builder: (BuildContext context) => new StoreDetailScreen(
              advertisementList[position].linktoBusinessMasterId));
      _navigateAndReturn(context, route, 0);
    }
  }

  //endregion

  @override
  void onAdvertisementListener(List<AdvertisementModel> list) {
    print("$list");
    advertisementList = list;
  }

  @override
  void onCategoryListener(List<CategoryModel> catList) {
    print("$catList");
    categoryList = catList;
  }

  @override
  void onStoreListener(List<StoreListModel> storeList) {
    print("$storeList");
    recentlyJoinedList = storeList;
  }

  @override
  void onUserListener(UserModel model) {
    userModel = model;
    print("${userModel.toString()}");
  }

  @override
  void onCustomerMembershipListener(int membershipTranId,int membershipLevel) {
    PrefUtil.putIntPreference(Constants.prefMembershipTranId, membershipTranId);
    print("$membershipTranId");
    setState(() {
      isLoading = false;
      Constants.isLoadingOnResume = false;
      if (membershipTranId != 0) {
        _membershipLevel = membershipLevel;
        isMembershipBought = true;
      }
    });
  }

  @override
  void dispose() {
    subscription == null;
    WidgetsBinding.instance.removeObserver(new LifecycleEventHandler(this));
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  void onResume() {
    if (internetConnection) {
      if (Constants.isLoadingOnResume) {
        setState(() {
          isLoading = true;
          catListPosition = 0;
          recentlyJoinedListPosition = 0;
        });
      }
    }
  }

  share() {
    String appUrlForAndroid ="http://play.google.com/store/apps/details?id=${packageName}";
    String appUrlForIphone = "http://itunes.apple.com/+91/app/${_packageInfo.appName}/id$packageName?mt=8";
      final RenderBox box = context.findRenderObject();
      Share.share("I just joined ${Constants.appName}.\nGet freebies and SAVE in local stores.As well EARN on sign up and referring.\n${defaultTargetPlatform ==TargetPlatform.iOS ? appUrlForIphone : appUrlForAndroid}",
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  Future<Null> _launchInWebViewOrVC(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }
  _navigateAndReturn(BuildContext context, var route, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, route);
    List<Object> result1 = result;
    if (Constants.isLoadingOnResume) {
      setState(() {
        _internetConnection();
        catListPosition = 0;
        recentlyJoinedListPosition = 0;
      });
    }
  }

  @override
  void onErrorHomeScreenDataListener(String error) {
    setState(() {
      isError = true;
      isLoading = false;
    });
  }
}

abstract class HandleLifeCycle {
  void onResume();
}

class LifecycleEventHandler extends WidgetsBindingObserver {
  HandleLifeCycle listener;

  LifecycleEventHandler(this.listener);

  @override
  didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        print("inactive");
        break;
      case AppLifecycleState.paused:
        print("paused");
        break;
      case AppLifecycleState.suspending:
        print("suspending");
        break;
      case AppLifecycleState.resumed:
        print("resumed");
        listener.onResume();
        break;
    }
  }
}
