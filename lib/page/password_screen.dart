import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:crazyrex/model/referral_code_model.dart';
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/page/my_account_screen.dart';
import 'package:crazyrex/parser/insert_register_parser.dart';
import 'package:crazyrex/parser/login_parser.dart';
import 'package:crazyrex/parser/referral_code_parser.dart';
import 'package:crazyrex/parser/update_password_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:device_info/device_info.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PasswordScreen extends StatefulWidget {
  final UserModel userModelRegistration;
  final String userNameForget;

  @override
  _PasswordScreenState createState() => _PasswordScreenState();

  PasswordScreen({Key key, this.userModelRegistration, this.userNameForget})
      : super(key: key);
}

class _PasswordScreenState extends State<PasswordScreen>implements ReferralCodeListener, LoginListener {
  final GlobalKey<FormState> _formPasswordKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldStatePassword = new GlobalKey<ScaffoldState>();

  FocusNode textZeroFocusNode = new FocusNode();
  FocusNode textFirstFocusNode = new FocusNode();
  FocusNode textSecondFocusNode = new FocusNode();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection;

  var _oldPasswordController = new TextEditingController();
  var _newPasswordController = new TextEditingController();
  var _confirmPasswordController = new TextEditingController();

  UserModel loginModel;
  ReferralCodeModel referralCodeModel;
  SetUpReferralCodeListener listenerReferralCode;
  SetUpLoginListener listenerLogin;

  bool isReferralCode = false;
  String cityName;

  String firebaseToken;
  bool _obscure = true;
  bool _obscure1 = true;
  bool _obscure2 = true;
  bool isRegistration = false;
  bool isForgetAccount = false;
  bool isChangePassword = false;
  bool isPhone = false;
  bool isEmail = false;
  String userName;
  String oldPassword;
  String _titlePassword;
  String _buttonText;

  IconData _icon = Icons.remove_red_eye;
  IconData _icon1 = Icons.remove_red_eye;
  IconData _icon2 = Icons.remove_red_eye;

  String _deviceid;


  _PasswordScreenState(){
    PrefUtil.getStringPreference(Constants.prefFcmToken).then((String value){
      firebaseToken = value;
    });
  }

  @override
  void initState() {


    _deviceInfo();
    _internetConnection();
    _general();
  }

  Future _deviceInfo() async {
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();

    if(defaultTargetPlatform == TargetPlatform.android){
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      _deviceid = androidInfo.id;
    }else if(defaultTargetPlatform == TargetPlatform.iOS){
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      _deviceid = iosInfo.identifierForVendor;
      print('Running on ${iosInfo.utsname.machine}');
    }

  }

  _general() {
    if (widget.userModelRegistration != null) {
      //region registration

      loginModel = widget.userModelRegistration;
      isRegistration = true;
      isPhone = true;
      userName = loginModel.phone;
      _titlePassword = Strings.labelPasswordScreenTitleForCreatePassword;
      _buttonText = Strings.labelForButtonSignUp;
      //endregion
    } else if (widget.userNameForget != null) {
      //region Forget
      _titlePassword = Strings.labelPasswordScreenTitleForResetPassword;
      _buttonText = Strings.labelForButtonSave;
      isForgetAccount = true;
      userName = widget.userNameForget;
      if (Constants.isValidPhone(userName)) {
        isPhone = true;
      } else if (Constants.isValidEmail(userName)) {
        isEmail = true;
      } else {
        Constants.showSnackBar(scaffoldStatePassword, Strings.invalidUserName);
      }
      //endregion
    } else {
      //region ChangePassword
      _titlePassword = Strings.labelPasswordScreenTitleForResetPassword;
      _buttonText = Strings.labelForButtonSave;
      isChangePassword = true;
      isPhone = true;
      PrefUtil.getStringPreference(Constants.prefPhone).then((String value) {
        userName = value;
      });
      PrefUtil.getStringPreference(Constants.prefPassword).then((String value) {
        oldPassword = value;
      });
      //endregion
    }
  }

//region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        internetConnection = true;

        if (widget.userModelRegistration != null) {
          if (loginModel.referralCode.isNotEmpty) {
            listenerReferralCode = new SetUpReferralCodeListener(this, '${Constants.url}SelectRegisteredUserMasterByReferralCode/${loginModel.referralCode}');
            listenerReferralCode.loadReferralCode();
          }
        }
      } else {
        internetConnection = false;
      }
    });
  }
  //endregion

  Future<bool> _back() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
                content: new Text(Strings.labelAlertTitleOfCancelRegistration),
                actions: <Widget>[
                  new FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop(false);
                        subscription == null;
                      },
                      child: new Text(Strings.alertNegativeButton)),
                  new FlatButton(
                      onPressed: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            "/LoginPage", (Route<dynamic> route) => false);
                        subscription == null;
                      },
                      child: new Text(Strings.alertPositiveButton)),
                ],
              ),
        ) ??
        false;
  }

  Future<bool> _back1() {
    Navigator.pop(context, [true]);
    subscription == null;
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: isRegistration ? _back : isChangePassword ? _back1 : null,
        child: new Scaffold(
          key: scaffoldStatePassword,
          resizeToAvoidBottomPadding: true,
          body: new Stack(
            fit: StackFit.expand,
            children: <Widget>[
              new Center(
                child: new ListView(
                  padding: EdgeInsets.only(bottom: 15.0),
                  shrinkWrap: true,
                  children: <Widget>[
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text(_titlePassword,
                            style: Theme
                                .of(context)
                                .textTheme
                                .display1
                                .apply(color: Colors.myColor)),
                        new Icon(Icons.account_circle,size: 65.0,color: Colors.myColor,),
                        new Form(
                            key: _formPasswordKey,
                            child: new Theme(
                                data: new ThemeData(
                                    primaryColor: Colors.myColor,
                                    accentColor: Colors.myColor,
                                    textSelectionColor: Colors.myColor),
                                child: new Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        50.0, 0.0, 50.0, 0.0),
                                    child: new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: <Widget>[
                                        new Container(
                                          child: isChangePassword
                                              ? new TextFormField(
                                                  controller:
                                                      _oldPasswordController,
                                                  maxLines: 1,
                                                  onFieldSubmitted:
                                                      (String value) {
                                                    FocusScope
                                                        .of(context)
                                                        .requestFocus(
                                                            textZeroFocusNode);
                                                  },
                                                  validator: (value) {
                                                    //region changePassword Process
                                                    if (_oldPasswordController
                                                        .text.isNotEmpty) {
                                                      if (_newPasswordController
                                                          .text.isNotEmpty) {
                                                        if (_confirmPasswordController
                                                            .text.isNotEmpty) {
                                                          if (oldPassword ==
                                                              _oldPasswordController
                                                                  .text) {
                                                            if (_newPasswordController
                                                                    .text ==
                                                                _confirmPasswordController
                                                                    .text) {
                                                              if (_newPasswordController
                                                                      .text
                                                                      .length >
                                                                  5) {
                                                                if (internetConnection != null && internetConnection) {
                                                                  _progressDialog(true);
                                                                  _updatePassword(userName, "Phone");
                                                                } else if (internetConnection != null && !internetConnection) {
                                                                  Constants.showSnackBar(scaffoldStatePassword, Strings.noInternetToast);
                                                                }
                                                              }
                                                            }
                                                          } else {
                                                            return Strings
                                                                .incorrectPassword;
                                                          }
                                                        }
                                                      }
                                                    } else {
                                                      return Strings
                                                          .emptyOldPassword;
                                                    }
                                                    //endregion
                                                  },
                                                  obscureText: _obscure2,
                                                  decoration:
                                                      new InputDecoration(
                                                          suffixIcon: _oldPasswordController
                                                                  .text
                                                                  .isNotEmpty
                                                              ? new IconButton(
                                                                  icon: new Icon(
                                                                      _icon2),
                                                                  onPressed:
                                                                      () {
                                                                    _hidePassword2();
                                                                  })
                                                              : null,
                                                          labelText: Strings
                                                              .hintOldPassword),
                                                )
                                              : null,
                                        ),
                                        new TextFormField(
                                          controller: _newPasswordController,
                                          maxLines: 1,
                                          focusNode: textZeroFocusNode,
                                          onFieldSubmitted: (String value) {
                                            FocusScope.of(context).requestFocus(
                                                textFirstFocusNode);
                                          },
                                          validator: (value) {
                                            //region new password
                                            if (isChangePassword) {
                                              if (_oldPasswordController
                                                  .text.isNotEmpty) {
                                                if (_newPasswordController
                                                    .text.isNotEmpty) {
                                                  if (_confirmPasswordController
                                                      .text.isNotEmpty) {
                                                    if (oldPassword ==
                                                        _oldPasswordController
                                                            .text) {
                                                      if (_newPasswordController
                                                              .text ==
                                                          _confirmPasswordController
                                                              .text) {
                                                        if (_newPasswordController
                                                                .text.length >
                                                            5) {
                                                          if (internetConnection !=
                                                                  null &&
                                                              internetConnection) {
                                                          } else if (internetConnection !=
                                                                  null &&
                                                              !internetConnection) {
                                                            Constants.showSnackBar(
                                                                scaffoldStatePassword,
                                                                Strings
                                                                    .noInternetToast);
                                                          }
                                                        } else {
                                                          return Strings
                                                              .invalidPassword;
                                                        }
                                                      }
                                                    }
                                                  }
                                                } else {
                                                  return Strings
                                                      .emptyNewPassword;
                                                }
                                              }
                                            } else if (isRegistration ||
                                                isForgetAccount) {
                                              if (_newPasswordController
                                                  .text.isNotEmpty) {
                                                if (_confirmPasswordController
                                                    .text.isNotEmpty) {
                                                  if (_newPasswordController
                                                          .text ==
                                                      _confirmPasswordController
                                                          .text) {
                                                    if (_newPasswordController
                                                            .text.length >
                                                        5) {
                                                      if (internetConnection !=
                                                              null &&
                                                          internetConnection) {
                                                        if (isRegistration) {
                                                          _progressDialog(true);
                                                          _insertData();
                                                        } else if (isForgetAccount) {
                                                          _progressDialog(true);
                                                          if (isPhone) {
                                                            _updatePassword(
                                                                userName,
                                                                "Phone");
                                                          } else if (isEmail) {
                                                            _updatePassword(
                                                                userName,
                                                                "Email");
                                                          }
                                                        }
                                                      } else if (internetConnection !=
                                                              null &&
                                                          !internetConnection) {
                                                        Constants.showSnackBar(
                                                            scaffoldStatePassword,
                                                            Strings
                                                                .noInternetToast);
                                                      }
                                                    } else {
                                                      return Strings
                                                          .invalidPassword;
                                                    }
                                                  }
                                                }
                                              } else {
                                                return Strings.requirePassword;
                                              }
                                            }
                                            //endregion
                                          },
                                          obscureText: _obscure,
                                          decoration: new InputDecoration(
                                              suffixIcon: _newPasswordController
                                                      .text.isNotEmpty
                                                  ? new IconButton(
                                                      icon: new Icon(_icon),
                                                      onPressed: () {
                                                        _hidePassword();
                                                      })
                                                  : null,
                                              labelText:
                                                  Strings.hintNewPassword),
                                        ),
                                        new TextFormField(
                                          controller:
                                              _confirmPasswordController,
                                          maxLines: 1,
                                          focusNode: textFirstFocusNode,
                                          validator: (value) {
                                            //region confirm password
                                            if (isChangePassword) {
                                              if (_oldPasswordController
                                                  .text.isNotEmpty) {
                                                if (_newPasswordController
                                                    .text.isNotEmpty) {
                                                  if (_confirmPasswordController
                                                      .text.isNotEmpty) {
                                                    if (oldPassword ==
                                                        _oldPasswordController
                                                            .text) {
                                                      if (_newPasswordController
                                                              .text ==
                                                          _confirmPasswordController
                                                              .text) {
                                                        if (_confirmPasswordController
                                                                .text.length >
                                                            5) {
                                                        } else {
                                                          return Strings
                                                              .invalidPassword;
                                                        }
                                                      } else {
                                                        return Strings
                                                            .rePasswordNotMatch;
                                                      }
                                                    }
                                                  } else {
                                                    return Strings
                                                        .emptyRePassword;
                                                  }
                                                }
                                              }
                                            } else if (isForgetAccount ||
                                                isRegistration) {
                                              if (_newPasswordController.text.isNotEmpty) {
                                                if (_confirmPasswordController.text.isNotEmpty) {
                                                  if (_newPasswordController.text == _confirmPasswordController.text) {
                                                    if (_confirmPasswordController.text.length > 5) {
                                                    } else {
                                                      return Strings.invalidPassword;
                                                    }
                                                  } else {
                                                    return Strings.rePasswordNotMatch;
                                                  }
                                                } else {
                                                  return Strings.emptyRePassword;
                                                }
                                              }
                                            }
                                            //endregion
                                          },
                                          obscureText: _obscure1,
                                          decoration: new InputDecoration(
                                              suffixIcon:
                                                  _confirmPasswordController.text.isNotEmpty
                                                      ? new IconButton(
                                                          icon: new Icon(_icon1),
                                                          onPressed: () {
                                                            _hidePassword1();
                                                          })
                                                      : null,
                                              labelText: Strings.hintRePassword),
                                        ),
                                        new Padding(
                                            padding:
                                                new EdgeInsets.only(top: 27.0)),
                                        new MaterialButton(
                                          color: Colors.myColor,
                                          textColor: Colors.white,
                                          child: new Text(_buttonText),
                                          onPressed: () {
                                            _formPasswordKey.currentState
                                                .validate();
                                          },
                                        ),
                                        new Padding(padding: EdgeInsets.only(top: 6.30)),
                                      ],
                                    ))))
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  _updatePassword(String userName, String userType) {
    setState(() {
      int errorCode;
      Map<String, dynamic> body2;
      if (isChangePassword) {
        body2 = {
          'RegisteredUserMasterData': {
            'UserNameType': userType,
            'Phone': userName,
            'Password': _newPasswordController.text,
            'FCMToken': firebaseToken,
          }
        };
      } else if (isForgetAccount) {
        if (isPhone) {
          body2 = {
            'RegisteredUserMasterData': {
              'UserNameType': userType,
              'Phone': userName,
              'Password': _newPasswordController.text,
              'FCMToken': firebaseToken,
            }
          };
        } else if (isEmail) {
          body2 = {
            'RegisteredUserMasterData': {
              'UserNameType': userType,
              'Email': userName,
              'Password': _newPasswordController.text,
              'FCMToken': firebaseToken,
            }
          };
        }
      }

      var encoder = JSON.encode(body2);
      var url = '${Constants.url}UpdateUserMasterForgotPassword';

      UpdatePasswordParser updatePasswordPassword = new UpdatePasswordParser();
      Future<int> result = updatePasswordPassword.updatePassword(url, encoder);
      result.then((c) {
        errorCode = c;
        _handleUpdateResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        _progressDialog(false);
      });
    });
  }

  _insertData() {
    setState(() {
      int errorCode;
      Map<String, dynamic> body2;
      if (isReferralCode == true) {
        body2 = {
          'RegisteredUserMasterData': {
            'FirstName': loginModel.firstName,
            'LastName': loginModel.lastName,
            'Email': loginModel.email,
            'Phone': loginModel.phone,
            'Password': _newPasswordController.text,
            'IsEmailVerify': loginModel.isEmailVerify,
            'IsSMSVerify': loginModel.isSMSVerify,
            'FCMToken': firebaseToken,
            'linktoSourceMasterId': Constants.SourceMasterId,
            'AndroidDeviceId': _deviceid,
          },
          'ReferAndEarnTran': referralCodeModel.customerMasterId.isNotEmpty
              ? {
                  'linktoCustomerMasterIdReferBy': referralCodeModel.customerMasterId,
                }
              : null
        };
      } else {
        body2 = {
          'RegisteredUserMasterData': {
            'FirstName': loginModel.firstName,
            'LastName': loginModel.lastName,
            'Email': loginModel.email,
            'Phone': loginModel.phone,
            'Password': _newPasswordController.text,
            'IsEmailVerify': loginModel.isEmailVerify,
            'IsSMSVerify': loginModel.isSMSVerify,
            'FCMToken': firebaseToken,
            'AndroidDeviceId': _deviceid,
            'linktoSourceMasterId': Constants.SourceMasterId,
          },
          'ReferAndEarnTran': null
        };
      }

      var encoder = JSON.encode(body2);
      print(body2);
      var url = '${Constants.url}InsertRegisteredUserMaster';

      InsertRegisterParser insertRegisterParser = new InsertRegisterParser();
      Future<int> result = insertRegisterParser.insertRegister(url, encoder);
      result.then((c) {
        errorCode = c;
        _handleResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        _progressDialog(false);
      });
    });
  }

  _handleUpdateResult(int errorCode) {
    _progressDialog(false);
    if (errorCode == 0) {
      if (isChangePassword) {
        PrefUtil.putStringPreference(Constants.prefPassword, _newPasswordController.text);
        Fluttertoast.showToast(msg: Strings.toastSuccessPassword);
        Navigator.pop(context, [true]);
        subscription == null;
      } else if (isForgetAccount) {
        subscription == null;
        Navigator.pushNamedAndRemoveUntil(
            context, "/LoginPage", (Route<dynamic> route) => false);
      }
    } else if (errorCode == -1) {
      Constants.showSnackBar(scaffoldStatePassword, Strings.errorInServices);
    }
  }

  _handleResult(int errorCode) {
    bool isAndroid = false;
    if (errorCode == 0) {
      if (isReferralCode) {
        _progressDialog(false);
          showAlert(Strings.alertDialogTitleForSuccessEarn);
      } else {
        if (loginModel.referralCode.toString().isEmpty) {
          listenerLogin = new SetUpLoginListener(this, '${Constants.url}SelectRegisteredUserMasterByUserName/${loginModel.phone}/${_newPasswordController.text}/${firebaseToken}/2');
          listenerLogin.loadLogin();
        } else {
          showAlert(Strings.alertDialogTitleForFailEarn);
        }
      }
    } else if (errorCode == -2) {
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStatePassword, Strings.errorAlreadyHaveAccount);
    } else if (errorCode == -1) {
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStatePassword, Strings.errorInServices);
    }
  }

  void showAlert(String value) {
    AlertDialog dialog = new AlertDialog(
      content: new Text(
        value,
        style: Theme.of(context).textTheme.body2,
      ),
      contentPadding: new EdgeInsets.all(15.0),
      actions: <Widget>[
        new FlatButton(
            onPressed: () {
              listenerLogin = new SetUpLoginListener(this, '${Constants.url}SelectRegisteredUserMasterByUserName/${loginModel.phone}/${_newPasswordController.text}/${firebaseToken}/2');
              listenerLogin.loadLogin();
            },
            child: new Text("OK"))
      ],
    );
    showDialog(context: context, child: dialog);
  }

  @override
  void onErrorReferralCode() {
    print('Error referral code in password screen');
  }

  @override
  void onSuccessReferralCode(ReferralCodeModel model) {
    referralCodeModel = model;
    if (referralCodeModel.referralCode != null) {
      listenerReferralCode = new SetUpReferralCodeListener(this, '${Constants.url}SelectRegisterUserMasterAndroidDeviceIdExistOrNot/$_deviceid');
      listenerReferralCode.loadDeviceExist();
    } else {
      isReferralCode = false;
    }
  }

  @override
  void onErrorLogin(onError) {
    print('Error Login in password screen :- $onError');
    _progressDialog(false);
  }

  @override
  void onSuccessLogin(UserModel model) {
    loginModel = model;

    setState(() {
      _progressDialog(false);
      _login();
    });
  }

  void _login() {
    print(loginModel.toString());
    if (loginModel.registeredUserMasterId != 0) {
      if (loginModel.isSMSVerify) {
        PrefUtil.putStringPreference(Constants.prefPhone, loginModel.phone);
        PrefUtil.putStringPreference(Constants.prefFirstName, loginModel.firstName);
        PrefUtil.putStringPreference(Constants.prefLastName, loginModel.lastName);
        PrefUtil.putStringPreference(Constants.prefPassword, _newPasswordController.text);
        PrefUtil.putIntPreference(Constants.prefRegisterUserMasterId, loginModel.registeredUserMasterId);
        PrefUtil.putStringPreference(Constants.prefCustomerMasterId, loginModel.customerMasterId);
        PrefUtil.putBoolPreference(Constants.prefKycDetail, loginModel.isKYCDetails);
        PrefUtil.putStringPreference(Constants.prefSessionId, loginModel.sessionId);
        PrefUtil.getStringPreference(Constants.prefCityName).then((String value) {
          cityName = value;
        });
        if (cityName != null) {
          subscription == null;
          Navigator.of(context).pushNamedAndRemoveUntil('/HomePage', (Route<dynamic> route) => false);
        } else {
          subscription == null;
          Navigator.of(context).pushNamedAndRemoveUntil('/CitySelectionPage', (Route<dynamic> route) => false);
        }
      } else {
        Constants.showSnackBar(
            scaffoldStatePassword, Strings.wrongUsernameOrPass);
      }
    } else {
      Constants.showSnackBar(
          scaffoldStatePassword, Strings.wrongUsernameOrPass);
    }
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  _hidePassword() {
    setState(() {
      if (_obscure) {
        _obscure = false;

        _icon = Icons.visibility_off;
      } else {
        _obscure = true;
        _icon = Icons.remove_red_eye;
      }
    });
  }

  _hidePassword1() {
    setState(() {
      if (_obscure1) {
        _obscure1 = false;
        _icon1 = Icons.visibility_off;
      } else {
        _obscure1 = true;
        _icon1 = Icons.remove_red_eye;
      }
    });
  }

  _hidePassword2() {
    setState(() {
      if (_obscure2) {
        _obscure2 = false;
        _icon2 = Icons.visibility_off;
      } else {
        _obscure2 = true;
        _icon2 = Icons.remove_red_eye;
      }
    });
  }

  @override
  void onSuccessDeviceExist(bool isDeviceExist) {
    print("DeviceExist $isDeviceExist");
    if(!isDeviceExist){
      isReferralCode = true;
      print("Successsss");
    }else{
      isReferralCode = false;
      print("Failed ****");
    }
  }
}
