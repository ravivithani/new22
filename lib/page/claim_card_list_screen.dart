import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:crazyrex/model/claim_card_list_model.dart';
import 'package:crazyrex/page/claim_card_activation_screen.dart';
import 'package:crazyrex/page/claim_card_detail_screen.dart';
import 'package:crazyrex/page/claim_card_screen.dart';
import 'package:crazyrex/page/privacy_policy_screen.dart';
import 'package:crazyrex/page/success_buy_membership_screen.dart';
import 'package:crazyrex/parser/claim_card_list_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ClaimCardListScreen extends StatefulWidget{
  @override
  _ClaimCardListScreenState createState() => _ClaimCardListScreenState();
}

class _ClaimCardListScreenState extends State<ClaimCardListScreen> implements ClaimCardListDataListener{

  final GlobalKey<ScaffoldState> scaffoldStateClaimCardList = new GlobalKey<ScaffoldState>();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  bool isError = false;
  bool isLoading = false;

  String _customerId;
  SetUpClaimCardListListenerData setUpClaimCardListListenerData;
  List<ClaimCardListModel> claimCardList1;
  int type=0;
  bool isBuyNow=true;
  int _membershipLevel;
  String _sessionId;
  bool isPurchased = false;
  String _cardExpiryDate;
  String _activeMembershipName;

  _ClaimCardListScreenState(){
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((String value){
      _customerId = value;
    });
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value) {
      _sessionId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {

            setState(() {
              internetConnection = true;
              isLoading = true;
            });
          } else {
            internetConnection = false;
            setState(() {});
          }
        });
  }
  //endregion

  @override
  void dispose() {
    if (subscription != null) {
      subscription == null;
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(internetConnection!=null && internetConnection){
      if(!isError){
        if(isLoading){
          _callApi();
        }
        return isLoading
            ? new Scaffold(body: new Center(child: new CircularProgressIndicator(),),)
            : _build();
      }else{
        return _error();
      }
    }else if(internetConnection!=null && !internetConnection){
      return _noInternet();
    }else{
      return new Scaffold(body: new Container(),);
    }
  }

  _callApi(){
    setUpClaimCardListListenerData = SetUpClaimCardListListenerData("${Constants.url}SelectAllMembershipType/$_customerId",this);
    print("${Constants.url}SelectAllMembershipType/$_customerId");
    setUpClaimCardListListenerData.loadClaimCardListData();
  }

  Widget _build(){
    return new Scaffold(
      appBar: AppBar(title: new Text(Strings.labelTitleMembershipListScreen),),
      body: new ListView(
        shrinkWrap: true,
        padding: EdgeInsets.fromLTRB(13.0, 30.0, 13.0, 18.0),
        children: <Widget>[
          new Column(
            children: <Widget>[
              new Text(Strings.labelBenefitsOfBuyingMembership,style: Theme.of(context).textTheme.title,textAlign: TextAlign.center,),
              new Container(
                padding: EdgeInsets.fromLTRB(35.0, 0.0, 35.0, 0.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    new Padding(padding: EdgeInsets.only(top: 40.0)),
                    new Text(Strings.labelCrazyPrice,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.start,),
                    new Padding(padding: EdgeInsets.only(top: 2.0)),
                    new Text(Strings.labelCrazySaver,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.start,),
                    new Padding(padding: EdgeInsets.only(top: 2.0)),
                    new Text(Strings.labelCrazyEarn,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.start,),
                    new Padding(padding: EdgeInsets.only(top: 2.0)),
                    new Text(Strings.labelDontMissExclusive,style: Theme.of(context).textTheme.body1.apply(color: Colors.black),textAlign: TextAlign.end,),
                    new Padding(padding: EdgeInsets.only(top: 25.0)),
                  ],
                ),
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(Strings.labelBeCrazyOneAndSave,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
                  new Text(" \u20B9${claimCardList1[0].totalSavingsAmount}+",style: Theme.of(context).textTheme.body1.apply(color: Colors.green), )
                ],),
              new ListView.builder(
                shrinkWrap: true,
                  primary: false,
                  itemCount: claimCardList1.length,
                  padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 13.0),
                  itemBuilder: (BuildContext context,int index){
                    ClaimCardListModel model = claimCardList1[index];
                    return new Card(
                      shape: Border.all(width: 0.3),
                      elevation: 2.0,
                      margin: EdgeInsets.fromLTRB(0.0, 6.0, 0.0, 6.0),
                      child: new Stack(
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              new Image.asset("images/bg33.png",height: 210.0,fit: BoxFit.fitWidth,),
                            ],
                          ),
                          new Container(
                            padding: new EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
                            child: new Column(
                              children: <Widget>[
                                new Row(
                                  children: <Widget>[
                                    new Expanded(child: new Text(model.isActive? Strings.labelCurrentlyActive:"",style: Theme.of(context).textTheme.body2.apply(color: Colors.green),),),
                                    new InkWell(
                                      onTap: (){
                                        if(model.isActive){
                                          var route = new MaterialPageRoute(builder: (BuildContext context) => new ClaimCardScreen());
                                          _navigateAndReturn(context,route,1);
                                        }
                                      },
                                      child: new Text(model.isActive?Strings.labelViewClaimCard:"",style: Theme.of(context).textTheme.body2.apply(color: Colors.myColor),),
                                    )
                                  ],
                                ),
                                new Padding(padding: new EdgeInsets.only(top: 12.0)),
                                new Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Expanded(child: new Text(model.membershipTypeName,style: Theme.of(context).textTheme.title.apply(color:Colors.black54),),),
                                    new Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        _txtPackageActualPrice(model),
                                        new Padding(padding: new EdgeInsets.only(top: 2.0)),
                                        _txtDiscount(model),
                                      ],
                                    )
                                ],)
                              ],
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.fromLTRB(12.0, 117.0, 12.0, 12.0),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                model.membershipLevel == 0?new Text(""): _txtExpiryDate(model),
                                new Padding(padding: new EdgeInsets.only(top: 3.0)),
                                new Row(
                                  children: <Widget>[
                                    new Expanded(child: model.membershipLevel == 0
                                        ? new Container()
                                        : new Text("${Strings.labelValidity} ${model.validMonths} ${model.validMonths>1?Strings.labelMonths:Strings.labelMonth}",style: Theme.of(context).textTheme.body1,),),
                                    model.membershipLevel==0
                                        ? new Container()
                                        : _txtPackagePrice(model)
                                  ],
                                )
                              ],
                            ),
                          ),
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              new Padding(padding: new EdgeInsets.only(top: 160.0)),
                              model.membershipLevel == 0 ? new Container(): new Container(
                                foregroundDecoration: ViewLineFromRegistration(),
                              ),
                              new Container(
                                height: 50.0,
                                child: model.membershipLevel ==0 
                                    ? new Column(mainAxisAlignment: MainAxisAlignment.end,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Container(
                                            padding: EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 12.0),
                                            child: new Row(
                                              children: <Widget>[
                                                new Expanded(child: _txtExpiryDate(model)),
                                                _txtPackagePrice(model)
                                              ],
                                            ),
                                          )
                                        ],
                                        )
                                    :_btnBuy(model),
                              )
                            ],
                          )
                        ],
                      ),
                    );

                  }),
              new Text(Strings.labelByJoiningCrzyRex,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
              new Padding(padding: EdgeInsets.only(top: 2.0)),
              new InkWell(
                onTap: (){
                  var route = new MaterialPageRoute(builder: (BuildContext context) =>new PrivacyPolicyScreen(Constants.TERMS_URL,"Terms And Condition"));
                  _navigateAndReturn(context, route, 0);
                },
                child: new Text(Strings.labelTermsCondition,style: Theme.of(context).textTheme.body1.apply(color: Colors.myColor),textAlign: TextAlign.start,),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .title),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _btnBuy(ClaimCardListModel model) {

      if (model.isActive) {
        if (model.isRenewNow) {
          return new FlatButton(onPressed: () {
            var route = new MaterialPageRoute(builder: (BuildContext context) => new ClaimCardDetailScreen(claimCardModel: model, buyOptions: 2, instantActive: false, isRenewal: true,));
            _navigateAndReturn(context, route, 1);
          }, child: new Text(Strings.labelButtonRenew), textColor: Colors.red,);
        } else {
          return new FlatButton(onPressed: null,
            child: new Text(Strings.labelButtonRenew),
            textColor: Colors.red,);
        }
      } else if (!model.isActive && model.isPurchased) {
        return new FlatButton(onPressed: () {
          _showPurchasedAlertDialogue(model);
        },
          child: new Text(Strings.labelButtonPurchased),
          textColor: Colors.green,);
      } else if (!isBuyNow) {
        if (_membershipLevel < model.membershipLevel) {
          return new FlatButton(onPressed: () {
            if (isPurchased) {
              _showDoubleBuyAlertDialogue();
            } else {
              if(_membershipLevel == 0){
                var route = new MaterialPageRoute(builder: (BuildContext context) => new ClaimCardDetailScreen(claimCardModel: model, buyOptions: 3, instantActive: true, isRenewal: false,));
                _navigateAndReturn(context, route, 1);
              }else{
                var route = new MaterialPageRoute(builder: (BuildContext context) => new ClaimCardActivationScreen(model, _cardExpiryDate, _activeMembershipName));
                _navigateAndReturn(context, route, 1);
              }

            }
          },
            child: new Text(Strings.labelButtonUpgrade),
            textColor: Colors.myColor,);
        } else {
          return new FlatButton(onPressed: () {
            if (isPurchased) {
              _showDoubleBuyAlertDialogue();
            } else {
              var route = new MaterialPageRoute(builder: (
                  BuildContext context) =>
              new ClaimCardActivationScreen(
                  model, _cardExpiryDate, _activeMembershipName));
              _navigateAndReturn(context, route, 1);
            }
          },
            child: new Text(Strings.labelButtonDowngrade),
            textColor: Colors.myColor,);
        }
      } else {
        return new FlatButton(onPressed: () {
          var route = new MaterialPageRoute(builder: (BuildContext context) => new ClaimCardDetailScreen(claimCardModel: model, buyOptions: 1, instantActive: true, isRenewal: false,));
          _navigateAndReturn(context, route, 1);
        },
          child: new Text(Strings.labelButtonBuyNow),
          textColor: Colors.myColor,);
      }
    }


  Widget _txtDiscount(ClaimCardListModel model) {
    if(model.isActive || model.isPurchased){
      if(int.parse(model.cardRenewalRate) < int.parse(model.newCardRate)){
        int discount = 100 - ((int.parse(model.cardRenewalRate) * 100)/int.parse(model.newCardRate)).round();
        if(discount!=0){
          return new Text("${Strings.labelDiscount} ${discount.toString()}%",style: Theme.of(context).textTheme.body1.apply(color: Colors.green),);
        }else{
          return new Text("");
        }
      }else{
        return new Text("");
      }
    }else{
      if(model.discount!=0){
        if(model.toDate!=null && model.fromDate!=null){
          if(model.toDate.compareTo(DateTime.now().toString()) * DateTime.now().toString().compareTo(model.fromDate) >= 0){
            if(model.isDiscountPercentage){
              return new Text("${Strings.labelDiscount} ${model.discount}%",style: Theme.of(context).textTheme.body1.apply(color: Colors.green));
            }else{
              return new Text("${Strings.labelDiscount} \u20B9${model.discount}",style: Theme.of(context).textTheme.body1.apply(color: Colors.green));
            }
          }else{
            return new Text("");
          }
        }else{
          if(model.isDiscountPercentage){
            return new Text("${Strings.labelDiscount} ${model.discount}%",style: Theme.of(context).textTheme.body1.apply(color: Colors.green));
          }else{
            return new Text("${Strings.labelDiscount} \u20B9${model.discount}",style: Theme.of(context).textTheme.body1.apply(color: Colors.green));
          }
        }
      }else{
        return new Text("");
      }
    }
  }

  Widget _txtExpiryDate(ClaimCardListModel model) {
    String date;
    if(model.cardExpiryDate!=null) {
      date = new DateFormat("dd-MM-yyyy").format(new DateFormat("yyyy-MM-dd").parse(model.cardExpiryDate));
    }
    if(model.isActive){
      return new Text("${Strings.labelExpireOn} $date");
    }else if(!model.isActive && model.isPurchased){
      return new Text("${Strings.labelActiveFrom} $date");
    }else {
      return new Text("");
    }
  }

  Widget _txtPackageActualPrice(ClaimCardListModel model) {
    if(model.isActive || model.isPurchased){
      if(int.parse(model.cardRenewalRate) < int.parse(model.newCardRate)){
        return new Text("\u20B9${model.newCardRate}",style: Theme.of(context).textTheme.body2,);
      }else{
        return new Text("",style: Theme.of(context).textTheme.body2,);
      }
    }else{
      if(model.discount!=0){
        if(model.toDate != null && model.fromDate != null){
          if(model.toDate.compareTo(DateTime.now().toString()) * DateTime.now().toString().compareTo(model.fromDate) >= 0){
            return new Text("\u20B9${model.newCardRate}",style: Theme.of(context).textTheme.body2,);
          }else{
            return new Text("",style: Theme.of(context).textTheme.body2,);
          }
        }else{
          return new Text("\u20B9${model.newCardRate}",style: Theme.of(context).textTheme.body2,);
        }
      }else{
        return new Text("",style: Theme.of(context).textTheme.body2,);
      }
      return new Text("\u20B9${model.newCardRate}",style: Theme.of(context).textTheme.body2,);
    }
  }

  Widget _txtPackagePrice(ClaimCardListModel model) {
    if(model.isActive || model.isPurchased){
      return new Text("${Strings.labelAmount} \u20B9${model.cardRenewalRate}",style: Theme.of(context).textTheme.body2,);
    }else {
      if(model.discount!=0){
        if(model.toDate!=null && model.fromDate!=null){
          if(model.toDate.compareTo(DateTime.now().toString() * DateTime.now().toString().compareTo(model.fromDate)) >= 0){
            if(model.isDiscountPercentage){
              return new Text("${Strings.labelAmount} \u20B9${int.parse(model.newCardRate) - ((int.parse(model.newCardRate) * model.discount)/100).round()}",style: Theme.of(context).textTheme.body2);
            }else{
              return new Text("${Strings.labelAmount} \u20B9${(int.parse(model.newCardRate) - model.discount)}",style: Theme.of(context).textTheme.body2);
            }
          }else{
            return new Text("${Strings.labelAmount} \u20B9${model.newCardRate}",style: Theme.of(context).textTheme.body2);
          }
        }else{
          if(model.isDiscountPercentage){
            return new Text("${Strings.labelAmount} \u20B9${int.parse(model.newCardRate) - ((int.parse(model.newCardRate) * model.discount)/100).round()}",style: Theme.of(context).textTheme.body2);
          }else{
            return new Text("${Strings.labelAmount} \u20B9${(int.parse(model.newCardRate) - model.discount)}",style: Theme.of(context).textTheme.body2);
          }
        }
      }else{
        return new Text("${Strings.labelAmount} \u20B9${model.newCardRate}",style: Theme.of(context).textTheme.body2);
      }
    }
  }

  @override
  void onClaimCardListListener(List<ClaimCardListModel> claimCardList) {
    claimCardList1 = claimCardList;
    print(claimCardList);

    for(int i=0;i<claimCardList1.length;i++){
      if(claimCardList1[i].isActive){
        isBuyNow = false;
        _membershipLevel = claimCardList1[i].membershipLevel;

      }
      if(!claimCardList1[i].isActive && claimCardList1[i].isPurchased){
        isPurchased = true;
      }
      if(claimCardList1[i].isActive){

        _cardExpiryDate = new DateFormat("dd-MM-yyyy").format(new DateFormat("yyyy-MM-dd").parse(claimCardList1[i].cardExpiryDate));
        _activeMembershipName = claimCardList1[i].membershipTypeName;
      }
    }

    //_setData();

    setState(() {
      isError = false;
      isLoading = false;
    });
  }

  @override
  void onErrorClaimCardList(onError) {
    print("Error in claim card list screen $onError");

    setState(() {
      isError = true;
      isLoading = false;
    });
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
    switch(navigateTo){
      case 0:
        setState(() {
          isLoading = false;
        });
        break;
      case 1:
        setState(() {
          isLoading = true;
        });
        break;
    }
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);
          if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
            setState(() {
              internetConnection = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }

  _showDoubleBuyAlertDialogue() {
    showDialog(context: context,child: new AlertDialog(
      contentPadding: EdgeInsets.all(15.0),
      content: new Text("${Strings.labelAlertDoubleBuyContent}"),
      actions: <Widget>[
        new FlatButton(onPressed: (){Navigator.pop(context);}, child: new Text(Strings.labelAlertOk)),
      ],
    ));
  }

  _showPurchasedAlertDialogue(ClaimCardListModel model) {
    showDialog(context: context,child: new AlertDialog(
      contentPadding: EdgeInsets.all(15.0),
      title: new Text(Strings.labelAlertPurchaseTitle),
      content: new Text("${Strings.labelAlertPurchaseContent} ${model.membershipTypeName} ${Strings.labelAlertPurchaseContent1}"),
      actions: <Widget>[
        new FlatButton(onPressed: (){Navigator.pop(context);}, child: new Text(Strings.labelAlertCancel)),
        new FlatButton(onPressed: (){
          Navigator.pop(context);
          if(internetConnection!=null && internetConnection){
            _progressDialog(true);
            _updateCustomerMembership(model);
          }else if(internetConnection!=null && !internetConnection){
            Constants.showSnackBar(scaffoldStateClaimCardList, Strings.noInternetToast);
          }
        }, child: new Text(Strings.labelAlertActivate))
      ],
    ));
  }

  _updateCustomerMembership(ClaimCardListModel model) {
    int errorCode;
    Map<String, dynamic>
      body2 = {
        'CustomerMembershipTran': {
          'SessionId': _sessionId,
          'CustomerMembershipTranId': model.customerMembershipTranId,
          'linktoMembershipTypeMasterId': model.membershipTypeMasterId,
          'IsActiveNow': true,
          'IsEnabled': true,
          'IsPaymentSuccess': true,
          'linktoCustomerMasterId':_customerId,
          'BuyOptions':3
        }
      };

    var encoder = JSON.encode(body2);
    var url = '${Constants.url}UpdateCustomerMembershipTran';
    Future<int> result = ClaimCardListParser.updateCustomerMembershipTran(url, encoder);
    result.then((c) {
      errorCode = c;
      _handleUpdateResult(errorCode,model);
    }).catchError((onError) {
      errorCode = -1;
      print(onError);
    });
  }

  _handleUpdateResult(int errorCode,ClaimCardListModel model) {
    _progressDialog(false);
    if(errorCode ==0){
      var route = new MaterialPageRoute(builder: (BuildContext context) => new SuccessBuyMembershipScreen(model.membershipTypeName));
      isPurchased = false;
      _navigateAndReturn(context, route,1);
    }else{
      Constants.showSnackBar(scaffoldStateClaimCardList, Strings.errorInServices);
    }
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

}
