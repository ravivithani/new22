import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/widget/StrikeThroughDecoration.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  final PageController controller = new PageController();
  int currentPage = 0;
  bool lastPage = false;
  Color c = Colors.myColor;

  changeStatusColor(Color color) async {
    await FlutterStatusbarcolor.setStatusBarColor(color);
  }

  _IntroScreenState() {
    changeStatusColor(Colors.myColor);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        //region stack
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: c),
          ),
          Column(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(),
              ),
              Expanded(
                //region expanded 2 flex: 4,
                flex: 5,
                child: PageView(
                  children: <Widget>[
                    new Container(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CircleAvatar(
                                  backgroundColor: Colors.white,
                                  radius: 64.0,
                                  child: Image(
                                    image: new AssetImage(
                                        "images/offer_intro.png"),
                                    color: c,
                                    width: 72.0,
                                    height: 72.0,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 20.0),
                                ),
                                new Text(Strings.offersAndMore,
                                    softWrap: true,
                                    textAlign: TextAlign.center,
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .headline
                                        .apply(color: Colors.white)),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      new Text(
                                        Strings.saveMoney,
                                        softWrap: true,
                                        textAlign: TextAlign.right,
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .display1
                                            .apply(color: Colors.white),
                                      ),
                                      new Text(Strings.throughOurNetwork,
                                          softWrap: true,
                                          textAlign: TextAlign.right,
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .body1
                                              .apply(color: Colors.white70)),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 14.0),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 14.0),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
//                      child: new Text("Offers & More",softWrap: true, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0,  color: Colors.white),),
                    ),
                    new Container(
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                CircleAvatar(
                                  backgroundColor: Colors.white,
                                  radius: 64.0,
                                  child: Image(
                                    image: new AssetImage(
                                        "images/refer_intro.png"),
                                    color: c,
                                    width: 72.0,
                                    height: 72.0,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 20.0),
                                ),
                                new Text(Strings.referAndEarn,
                                    softWrap: true,
                                    textAlign: TextAlign.center,
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .headline
                                        .apply(color: Colors.white)),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      new Text(Strings.makeMoney,
                                          softWrap: true,
                                          textAlign: TextAlign.right,
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .display1
                                              .apply(color: Colors.white)),
                                      new Text(Strings.throughYourNetwork,
                                          softWrap: true,
                                          textAlign: TextAlign.right,
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .body1
                                              .apply(color: Colors.white70)),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 14.0),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 14.0),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
//                      child: new Text("Offers & More",softWrap: true, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0,  color: Colors.white),),
                    ),
                  ],
                  controller: controller,
                  onPageChanged: _onPageChanged,
                ),
                //endregion
              ),
              Expanded(
                //region expanded 4 flex: 1
                flex: 1,

                child: Container(
                  foregroundDecoration: new StrikeThroughDecoration(),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        child: Text(lastPage ? "" : "SKIP",
                            style:
                                TextStyle(color: Colors.white, fontSize: 14.0)),
                        onPressed: () => lastPage
                            ? null
                            : Navigator.pushReplacementNamed(context, "/LoginPage"),
                      ),
                      FlatButton(
                        child: Text(lastPage ? "START" : "NEXT",
                            style:
                                TextStyle(color: Colors.white, fontSize: 14.0)),
                        onPressed: () => lastPage
                            ? Navigator.pushReplacementNamed(
                                context, "/LoginPage")
                            : controller.nextPage(
                                duration: Duration(milliseconds: 300),
                                curve: Curves.easeIn),
                      ),
                    ],
                  ),
                ),
                //endregion
              )
            ],
          )
        ],
        //endregion
      ),
    );
  }

  void _onPageChanged(int page) {
    setState(() {
      currentPage = page;
      if (currentPage == 1) {
        lastPage = true;
      } else {
        lastPage = false;
      }
    });
  }
}
