import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:crazyrex/model/city_list_model.dart';
import 'package:crazyrex/parser/select_city_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:simple_permissions/simple_permissions.dart';

class CitySelectionScreen extends StatefulWidget {
  bool isFromMyAcccount;

  @override
  _CitySelectionScreenState createState() => _CitySelectionScreenState();

  CitySelectionScreen(this.isFromMyAcccount);
}

class _CitySelectionScreenState extends State<CitySelectionScreen>
    implements CityListener {
  SetUpCityListener cityListener;
  bool isLoading;
  bool isError = false;

  List<CityListModel> _cities;
  List<CityListModel> searchResult = [];

  String _sessionId;
  String CustomerMasterId;
  Map<String, dynamic> data;

  TextEditingController controller = new TextEditingController();

  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;

  final GlobalKey<ScaffoldState> scaffoldState = new GlobalKey<ScaffoldState>();

  Map<String, double> _startLocation;
  Map<String, double> _currentLocation;
  StreamSubscription<Map<String, double>> _locationSubscription;
  Location _location = new Location();
  double latitude, longitude;

  changeStatusColor(Color color) async {
    await FlutterStatusbarcolor.setStatusBarColor(color);
  }

  _CitySelectionScreenState() {
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value) {
      _sessionId = value;
    });

    PrefUtil
        .getStringPreference(Constants.prefCustomerMasterId)
        .then((String value) {
      CustomerMasterId = value;
    });

    cityListener =
        new SetUpCityListener(this, "${Constants.url}SelectAllCityMaster");
    changeStatusColor(Colors.myColor);
  }

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    initPlatformState();

    SimplePermissions.checkPermission(Permission.AccessFineLocation).then((isGranted){
      if(isGranted){
        _locationSubscription = _location.onLocationChanged.listen((Map<String, double> result) {
          _currentLocation = result;
          if (latitude == null || longitude == null) {
            if (_startLocation != null ) {
              latitude = _startLocation['latitude'];
              longitude = _startLocation['longitude'];
            }
          }
        });
      }
    });
    _internetConnection();
  }

  @override
  void dispose() {
    subscription == null;
    _locationSubscription = null;
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  initPlatformState() {
    Map<String, double> location;
    // Platform messages may fail, so we use a try/catch PlatformException.
    SimplePermissions.requestPermission(Permission.AccessFineLocation).then((isGranted) async{
      if(isGranted){
        location = await _location.getLocation;
        _startLocation = location;
        if (latitude == null || longitude == null) {
          latitude = _startLocation['latitude'];
          longitude = _startLocation['longitude'];
        }
      }else{
        Fluttertoast.showToast(msg: Strings.appNeedLocationPermission);
      }
    }
    );


  }


  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        internetConnection = true;
        isLoading = true;
        setState(() {});
      } else {
        internetConnection = false;
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          cityListener.loadCities();
        }
        return Scaffold(
          key: scaffoldState,
          appBar: new AppBar(
              title: new Text(Strings.labelCitySelectionScreenTitle),
              bottom: isLoading
                  ? null
                  : new PreferredSize(
                      preferredSize: new Size(55.0, 55.0),
                      child: new Container(
                          padding: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 4.0),
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: new IconButton(
                                    icon: Icon(
                                      Icons.my_location,
                                      color: Colors.myColor,
                                      size: 30.0,
                                    ),
                                    onPressed: () {
                                      onPressLoction();
                                    }),
                              ),
                              Expanded(
                                  flex: 6,
                                  child: new Form(
                                      child: new Theme(
                                          data: new ThemeData(
                                              textSelectionColor:
                                                  Colors.myColor,
                                              primaryColor: Colors.myColor),
                                          child: new Container(
                                              decoration: new BoxDecoration(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        5.0),
                                                border: new Border.all(
                                                    width: 1.0,
                                                    color: Colors.black54),
                                              ),
                                              height: 40.0,
                                              padding: new EdgeInsets.all(8.0),
                                              child: new Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: new Icon(
                                                      Icons.search,
                                                      color: Colors.myColor,
                                                    ),
                                                    flex: 0,
                                                  ),
                                                  Expanded(
                                                      flex: 1,
                                                      child: new Container(
                                                        margin: new EdgeInsets.fromLTRB(2.0, 3.0, 0.0, 0.0),
                                                        child: new TextField(controller: controller, decoration: new InputDecoration.collapsed(hintText: Strings.hintCityName, hintStyle: new TextStyle(color: Colors.black54),),
                                                          onChanged: onSearchTextChanged,
                                                        ),
                                                      ))
                                                ],
                                              ))))),
                              new Padding(padding: new EdgeInsets.all(4.0))
                            ],
                          )),
                    )),
          body: isLoading
              ? new Center(
                  child: new CircularProgressIndicator(),
                )
              : (searchResult.length != 0 ? _searchList() : controller.text.isNotEmpty?_noSearchFound():_cityList()),
        );
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return new Scaffold();
    }
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    _btnRetryOnPress();
                  })
            ],
          ),
        ),
      ],
    ));
  }

  _btnRetryOnPress() {
    setState(() {});
  }

  onSearchTextChanged(String text) async {
    searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _cities.forEach((CityListModel city) {
      if (city.cityName.toLowerCase().contains(text.toLowerCase()))
        searchResult.add(city);
    });
    setState(() {});
  }

  Widget _cityList() {
    return new Container(
      child: new Column(
        children: <Widget>[
          new Flexible(
              child: new ListView.builder(
                  itemCount: _cities.length,
                  itemBuilder: (BuildContext context, int index) {
                    final CityListModel city = _cities[index];
                    return new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new ListTile(
                          title: new Container(
                            child: new Text(
                              city.cityName,
                              style: Theme.of(context).textTheme.body1,
                            ),
                          ),
                          onTap: () {
                            _insertData(city);
                          },
                        ),
                        new Container(
                          foregroundDecoration: new ViewLineFromRegistration(),
                        )
                      ],
                    );
                  }))
        ],
      ),
    );
  }

  Widget _searchList() {
    if (searchResult.length != 0) {
      return new Container(
        child: new Column(
          children: <Widget>[
            new Flexible(
                child: new ListView.builder(
                    itemCount: searchResult.length,
                    itemBuilder: (BuildContext context, int index) {
                      final CityListModel city = searchResult[index];
                      return new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new ListTile(
                            title: new Container(
                              child: new Text(
                                city.cityName,
                                style: Theme.of(context).textTheme.body1,
                              ),
                            ),
                            onTap: () {
                              _insertData(city);
                            },
                          ),
                          new Container(
                            foregroundDecoration:
                                new ViewLineFromRegistration(),
                          )
                        ],
                      );
                    }))
          ],
        ),
      );
    } else {
      return _noSearchFound();
    }
  }

  Widget _noSearchFound() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image(
                image: new AssetImage("images/ic_table_search_black.png"),
                color: Colors.myColor,
                height: 65.0,
                width: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text("No result found.",
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title.apply(color: Colors.myColor)),
            ],
          ),
        ),
      ],
    ));
  }

  @override
  void onErrorCityList() {
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onSuccessCityList(List<CityListModel> items) {
    setState(() {
      _cities = items;
      isLoading = false;
    });
  }

  _getCityName(double latitude, double longitude) async {
    final coordinates = new Coordinates(latitude, longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    if (addresses.length != 0) {
      var first = addresses.first;
      print("${first.featureName} : ${first.locality}");
      String city = first.locality;
      controller.text = city;
      onSearchTextChanged(first.locality);
    } else {
      Fluttertoast.showToast(
          msg: "No store available in this city",
          toastLength: Toast.LENGTH_SHORT);
    }
  }

  _insertData(CityListModel model) {
    _progressDialog(true);
    int errorCode;

    data = {
      'CustomerMaster': {
        'SessionId': _sessionId,
        'CustomerMasterId': CustomerMasterId,
        'linktoCityMasterId': model.cityMasterId,
      },
    };

    var encoder = JSON.encode(data);
    var url = '${Constants.url}UpdateCustomerMasterCity';
    Future<int> result = SelectCityParser.insertCity(url, encoder);
    result.then((c) {
      errorCode = c;
      _successUpdate(errorCode, model);
    }).catchError((onError) {
      print("UpdateCustomerMasterCity Error :- ${onError.toString()}");
    });
  }

  _successUpdate(int errorCode, CityListModel model) {
    _progressDialog(false);
    if (errorCode == 0) {
      PrefUtil.putStringPreference(Constants.prefCityName, model.cityName);
      PrefUtil.putIntPreference(Constants.prefCityId, model.cityMasterId);
      subscription == null;
      if (!widget.isFromMyAcccount) {
        Navigator.of(context).pushNamedAndRemoveUntil('/HomePage', (Route<dynamic> route) => false);
      } else {
        Navigator.pop(context);
      }

      subscription == null;
    } else if (errorCode == -1) {
      Constants.showSnackBar(
          scaffoldState, "somthings went wrong contact your service provider");
    }
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  onPressLoction() {
    if (latitude != null && longitude != null) {
      _getCityName(latitude, longitude);
    }
  }
}
