import 'dart:async';
import 'dart:convert';

import 'package:crazyrex/model/been_fav_store_model.dart';
import 'package:crazyrex/model/hour_store_detail_model.dart';
import 'package:crazyrex/model/info_question__store_detail_model.dart';
import 'package:crazyrex/model/object_store_detail_model.dart';
import 'package:crazyrex/model/offerlist_model.dart';
import 'package:crazyrex/model/review_store_model.dart';
import 'package:crazyrex/page/all_image_screen.dart';
import 'package:crazyrex/page/edit_review_store_screen.dart';
import 'package:crazyrex/page/full_image_screen.dart';
import 'package:crazyrex/page/offer_description_screen.dart';
import 'package:crazyrex/page/offer_list_screen.dart';
import 'package:crazyrex/page/outlet_store_list_screen.dart';
import 'package:crazyrex/page/view_all_review.dart';
import 'package:crazyrex/parser/store_detail_screen_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/StrikeThroughDecoration.dart';
import 'package:crazyrex/widget/star_rating.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:latlong/latlong.dart';
import 'package:map_view/polyline.dart';
import 'package:package_info/package_info.dart';
import 'package:share/share.dart';
import 'package:map_view/map_view.dart';
import 'package:location/location.dart' as latlong;
import 'package:url_launcher/url_launcher.dart';

class StoreDetailScreen extends StatefulWidget {
  final int storeId;

  StoreDetailScreen(this.storeId);

  @override
  _StoreDetailScreenState createState() => _StoreDetailScreenState();
}

class _StoreDetailScreenState extends State<StoreDetailScreen> implements StoreDetailDataListener {
  final GlobalKey<ScaffoldState> scaffoldStateStoreDetail = new GlobalKey<ScaffoldState>();

  MapView mapView = new MapView();
  CameraPosition cameraPosition;
  var staticMapProvider = new StaticMapProvider(Constants.api_key);
  Uri staticMapUri;

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  bool isError = false;
  bool isLoading = false;
  int _storeId;
  String _customerId;
  String _sessionId;
  int _cityId;

  //region share
  String packageName;
  String appName;

  String buildNumber;
  PackageInfo _packageInfo = new PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  static String _storeName;
  static double _latitude;
  static double _longitude;

  List<Marker> markers;

  Future<Null> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      packageName = info.packageName;
      appName = info.appName;
      buildNumber = info.buildNumber;
    });
  }

  //endregion
  SetUpStoreDetailScreenListener setUpStoreDetailScreenListener;

  List<ReviewStoreModel> reviewList1;
  List<OfferListModel> offerList1;
  ObjectStoreDetailModel objectModel;
  List<String> menuList1;
  List<InfoAnswerStoreModel> infoQuestionList1;
  List<HourStoreDetailModel> hourList1;
  List<BeenFavStoreModel> checkInList1;
  List<String> galleryList1;

  bool isRate = false;
  bool isBeen = false;
  bool isFavourite = false;
  String _checkInFavId;
  String _checkInBeenId;

  double expandedHeight = 250.0;

  //region kilometer
  Map<String, double> _startLocation;
  Map<String, double> _currentLocation;
  StreamSubscription<Map<String, double>> _locationSubscription;
  latlong.Location _location = new latlong.Location();
  double latitude, longitude;
  //endregion

  _StoreDetailScreenState() {
    MapView.setApiKey(Constants.api_key);
    PrefUtil
        .getStringPreference(Constants.prefCustomerMasterId)
        .then((String value) {
      _customerId = value;
    });
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value) {
      _sessionId = value;
    });
    PrefUtil.getIntPreference(Constants.prefCityId).then((int value) {
      _cityId = value;
    });
  }

  @override
  void initState() {
    super.initState();

    //region kilometer
    initPlatformState();

    _locationSubscription =
        _location.onLocationChanged.listen((Map<String, double> result) {
          _currentLocation = result;
          if (latitude == null || longitude == null) {
            latitude = _startLocation['latitude'];
            longitude = _startLocation['longitude'];
          }
        });
    //endregion
    _storeId = widget.storeId;

    _internetConnection();
    _initPackageInfo();
  }

  initPlatformState() async {
    Map<String, double> location;
    // Platform messages may fail, so we use a try/catch PlatformException.

    try {
      location = await _location.getLocation;
    } catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        Fluttertoast.showToast(
            msg: Strings.appNeedLocationPermission,
            toastLength: Toast.LENGTH_LONG);
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        Fluttertoast.showToast(
            msg: Strings.appNeedLocationPermission,
            toastLength: Toast.LENGTH_LONG);
      }
      location = null;
    }

    _startLocation = location;
    if (latitude == null || longitude == null) {
      latitude = _startLocation['latitude'];
      longitude = _startLocation['longitude'];
    }
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            internetConnection = true;
            setState(() {
              isLoading = true;
            });
          } else {
            internetConnection = false;
            setState(() {});
          }
        });
  }

//endregion

  @override
  void dispose() {
    if (subscription != null) {
      subscription == null;
    }
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          _callApis();
        }
        return isLoading
            ? new Scaffold(
          body: new Center(
            child: new CircularProgressIndicator(),
          ),
        )
            : _build();
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return Scaffold();
    }
  }

  Widget _build() {
    return new Scaffold(
      body: new CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          _collapse(),
          offerList1.length==0
              ? SliverToBoxAdapter(child: Container())
              : _offerCard(),
          menuList1.length==0
              ? SliverToBoxAdapter(child: Container())
              : _menuCard(),
          infoQuestionList1.length==0
              ? SliverToBoxAdapter(child: Container())
              : _detailsCard(),
          galleryList1.length == 0
              ? SliverToBoxAdapter(child: Container())
              : _photoCard(),
          objectModel.address==null
              ? SliverToBoxAdapter(child: Container())
              : _addressCard(),
          hourList1.length == 0
              ? SliverToBoxAdapter(child: Container())
              : _openingHourCard(),
          _reviewCard()
        ],
      ),
    );
  }

  Widget _collapse() {
    //region set date (am:pm)
    DateFormat dateFormat = new DateFormat.Hm();

    DateTime now = DateTime.now();
    DateTime open = dateFormat.parse(objectModel.openingTime);
    open = new DateTime(now.year, now.month, now.day, open.hour, open.minute);

    DateTime breakStart = dateFormat.parse(objectModel.breakStartTime);
    breakStart = new DateTime(
        now.year, now.month, now.day, breakStart.hour, breakStart.minute);

    DateTime breakEnd = dateFormat.parse(objectModel.breakEndTime);
    breakEnd = new DateTime(
        now.year, now.month, now.day, breakEnd.hour, breakEnd.minute);

    DateTime close = dateFormat.parse(objectModel.closingTime);
    close = new DateTime(now.year, now.month, now.day, close.hour, close.minute);
    //endregion

    return new SliverAppBar(
      backgroundColor:
      expandedHeight != 0.0 ? Colors.transparent : Colors.white,
      actions: <Widget>[
        new InkWell(
          onTap: () {
            _share();
          },
          child: new Container(
            child: new Icon(
              Icons.share,
              color: expandedHeight != 0.0 ? Colors.white : Colors.black,
            ),
            padding: EdgeInsets.only(right: 15.0),
          ),
        )
      ],
      leading: new InkWell(
        child: new Icon(
          Icons.arrow_back,
          color: expandedHeight != 0.0 ? Colors.white : Colors.black,
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
      title: expandedHeight == 0.0
          ? new Text(objectModel.businessName)
          : new Container(),
      automaticallyImplyLeading: true,
      expandedHeight: expandedHeight,
      pinned: false,
      floating: false,
      flexibleSpace: new FlexibleSpaceBar(
        background: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            objectModel.businessImageName != null
                ? new Image.network(
              objectModel.businessImageName,
              fit: BoxFit.cover,
              color: Colors.black54,
              colorBlendMode: BlendMode.darken,
            )
                : new Image.asset("images/bg33.png",
                fit: BoxFit.cover,
                color: Colors.black54,
                colorBlendMode: BlendMode.darken),
            new Image.asset("images/bg33.png",fit: BoxFit.cover,color: Colors.black54,colorBlendMode: BlendMode.darken),
            objectModel.businessImageName!=null
                ? new Image.network(objectModel.businessImageName,fit: BoxFit.cover,color: Colors.black54,colorBlendMode: BlendMode.darken,)
                : new Image.asset("images/bg33.png",fit: BoxFit.cover,color: Colors.black54,colorBlendMode: BlendMode.darken),
            new Container(
              padding: new EdgeInsets.fromLTRB(18.0, 50.0, 18.0, 18.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    objectModel.businessName,
                    maxLines: 1,
                    style: Theme
                        .of(context)
                        .textTheme
                        .title
                        .apply(color: Colors.white),
                  ),
                  new Padding(padding: new EdgeInsets.only(top: 10.0)),
                  new Text(
                    "${objectModel.area}, ${objectModel.city}",
                    maxLines: 1,
                    style: Theme
                        .of(context)
                        .textTheme
                        .caption
                        .apply(color: Colors.white),
                  ),
                  new Container(
                    padding: EdgeInsets.only(
                      left: 80.0,
                      right: 80.0,
                      top: 10.0,
                      bottom: 5.0,
                    ),
                    child: new Container(
                      foregroundDecoration: StrikeThroughDecoration(),
                    ),
                  ),
                  new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _txtOpenClose(now, open, breakStart, breakEnd, close),
                      new Text(
                        " - ${new DateFormat('h:mm a').format(open)} to ${new DateFormat('h:mm a (EEE)').format(close)}",
                        style: Theme
                            .of(context)
                            .textTheme
                            .caption
                            .apply(color: Colors.white),
                      ),
                    ],
                  ),
                  new Container(
                    padding: EdgeInsets.only(
                      left: 80.0,
                      right: 80.0,
                      top: 5.0,
                      bottom: 15.0,
                    ),
                    child: new Container(
                      foregroundDecoration: StrikeThroughDecoration(),
                    ),
                  ),
                  new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Expanded(
                        flex: 3,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 35.0,
                              width: 35.0,
                              decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                color: isRate
                                    ? Colors.orangeAccent
                                    : Colors.transparent,
                                border: new Border.all(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              child: new Center(
                                child: new FloatingActionButton(
                                    backgroundColor: Colors.transparent,
                                    elevation: 0.0,
                                    child: new Icon(Icons.star_border),
                                    onPressed: () {
                                      _rateClick();
                                    }),
                              ),
                            ),
                            new Padding(padding: new EdgeInsets.only(top: 5.0)),
                            new Text(
                              Strings.labelRate,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .caption
                                  .apply(color: Colors.white),
                            )
                          ],
                        ),
                      ),
                      new Expanded(
                        flex: 2,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 35.0,
                              width: 35.0,
                              decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                color: isBeen
                                    ? Colors.lightBlue
                                    : Colors.transparent,
                                border: new Border.all(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              child: new Center(
                                child: new FloatingActionButton(
                                    elevation: 0.0,
                                    backgroundColor: Colors.transparent,
                                    child: new Icon(Icons.beenhere),
                                    onPressed: () {
                                      if (internetConnection != null &&
                                          internetConnection) {
                                        _checkInIconClick(true, false);
                                      } else if (internetConnection != null &&
                                          !internetConnection) {
                                        Constants.showSnackBar(
                                            scaffoldStateStoreDetail,
                                            Strings.noInternetToast);
                                      }
                                    }),
                              ),
                            ),
                            new Padding(padding: new EdgeInsets.only(top: 5.0)),
                            new Text(
                              Strings.labelBeenHere,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .caption
                                  .apply(color: Colors.white),
                            )
                          ],
                        ),
                      ),
                      new Expanded(
                        flex: 3,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              height: 35.0,
                              width: 35.0,
                              decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                color: isFavourite
                                    ? Colors.red
                                    : Colors.transparent,
                                border: new Border.all(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              child: new Center(
                                child: new FloatingActionButton(
                                    elevation: 0.0,
                                    backgroundColor: Colors.transparent,
                                    child: new Icon(Icons.favorite_border),
                                    onPressed: () {
                                      if (internetConnection != null &&
                                          internetConnection) {
                                        _checkInIconClick(false, true);
                                      } else if (internetConnection != null &&
                                          !internetConnection) {
                                        Constants.showSnackBar(
                                            scaffoldStateStoreDetail,
                                            Strings.noInternetToast);
                                      }
                                    }),
                              ),
                            ),
                            new Padding(padding: new EdgeInsets.only(top: 5.0)),
                            new Text(
                              Strings.labelFavourite,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .caption
                                  .apply(color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _offerCard() {
    return new SliverToBoxAdapter(
      child: new Card(
        margin: EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
        elevation: 2.0,
        child: new Container(
          margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Text(
                    Strings.labelOffers,
                    style: Theme
                        .of(context)
                        .textTheme
                        .body2,
                  ),
                  objectModel.savingsAmount != null &&
                      objectModel.savingsAmount != 0
                      ? new Expanded(
                    child: new Text(
                      "${Strings.labelEstimatedSaving}\u20B9${objectModel
                          .savingsAmount}",
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.green),
                      textAlign: TextAlign.end,
                    ),
                  )
                      : new Container()
                ],
              ),
              new ListView.builder(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                  shrinkWrap: true,
                  primary: false,
                  itemCount: offerList1.length > 8 ? 8 : offerList1.length,
                  itemBuilder: (BuildContext context, int index) {
                    OfferListModel offerModel = offerList1[index];
                    return new InkWell(
                      onTap: () {
                        _offerClick(index);
                      },
                      child: new Column(
                        children: <Widget>[
                          new Padding(padding: EdgeInsets.only(top: 10.0)),
                          new Row(
                            children: <Widget>[
                              new Image.asset(
                                "images/label.png",
                                height: 18.0,
                                width: 18.0,
                                color: Colors.black54,
                              ),
                              new Padding(
                                  padding: new EdgeInsets.only(left: 10.0)),
                              new Expanded(
                                  child: new Column(
                                    children: <Widget>[
                                      new Row(
                                        children: <Widget>[
                                          new Expanded(
                                              child: new Text(
                                                offerModel.OfferTitle,
                                                style:
                                                Theme
                                                    .of(context)
                                                    .textTheme
                                                    .body1,
                                              )),
                                          offerModel.IsAvailableNow
                                              ? new Container()
                                              : new Text(
                                            Strings.labelNA,
                                            style: Theme.of(context).textTheme.caption.apply(color: Colors.red),
                                          )
                                        ],
                                      ),
                                      new Padding(
                                          padding: new EdgeInsets.only(
                                              top: 4.0)),
                                      new Row(
                                        children: <Widget>[
                                          new Expanded(
                                              child: new Text(
                                                "${offerModel.TotalRedeemOffer
                                                    .toString()} ${Strings
                                                    .labelRedeemed}",
                                                style:
                                                Theme
                                                    .of(context)
                                                    .textTheme
                                                    .caption,
                                              )),
                                          new Expanded(
                                              child: new Text(
                                                "${offerModel
                                                    .TotalCustomerRedeemOffer
                                                    .toString()}/${offerModel
                                                    .RedeemCount} ${Strings
                                                    .labelUsed}",
                                                style:
                                                Theme
                                                    .of(context)
                                                    .textTheme
                                                    .caption,
                                                textAlign: TextAlign.end,
                                              )),
                                        ],
                                      )
                                    ],
                                  ))
                            ],
                          ),
                          offerList1.length > 8
                              ? index == 7
                              ? new Container()
                              : new Container(
                            padding: EdgeInsets.only(top: 10.0),
                            child: new Container(
                              foregroundDecoration:
                              ViewLineFromRegistration(),
                            ),
                          )
                              : index == offerList1.length - 1
                              ? new Container()
                              : new Container(
                            padding: EdgeInsets.only(top: 10.0),
                            child: new Container(
                              foregroundDecoration:
                              ViewLineFromRegistration(),
                            ),
                          )
                        ],
                      ),
                    );
                  }),
              offerList1.length > 8
                  ? Padding(padding: new EdgeInsets.only(top: 10.0))
                  : new Container(),
              offerList1.length > 8
                  ? new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  new InkWell(
                    onTap: () {
                      _viewAllOfferClick();
                    },
                    child: new Text(
                      Strings.labelViewAll,
                      textAlign: TextAlign.end,
                      style: Theme
                          .of(context)
                          .textTheme
                          .caption
                          .apply(color: Colors.myColor),
                    ),
                  )
                ],
              )
                  : new Container()
            ],
          ),
        ),
      ),
    );
  }

  Widget _menuCard() {
    return new SliverToBoxAdapter(
      child: new Card(
        margin: EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
        elevation: 2.0,
        child: new Container(
          margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                Strings.labelMenu,
                style: Theme
                    .of(context)
                    .textTheme
                    .body2,
              ),
              new GridView.builder(
                  padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                  shrinkWrap: true,
                  primary: false,
                  itemCount: menuList1.length > 4 ? 4 : menuList1.length,
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4),
                  itemBuilder: (BuildContext context, int index) {
                    String menuModel = menuList1[index];
                    return new InkWell(
                      onTap: () {
                        print(index);
                        _menuClick(index);
                      },
                      child: new Card(
                        elevation: 1.0,
                        child: new GridTile(
                          footer: menuList1.length > 4
                              ? index == 3
                              ? new Container(
                            color: Colors.black54,
                            child: new Text(
                              "More",
                              textAlign: TextAlign.center,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .caption
                                  .apply(color: Colors.white),
                            ),
                          )
                              : new Container()
                              : new Container(),
                          child: menuModel != null
                              ? new FadeInImage(
                              placeholder:
                              new AssetImage("images/default1.png"),
                              image: new NetworkImage(menuModel),
                              height: 45.0,
                              fit: BoxFit.cover)
                              : new Image.asset(
                            "images/default1.png",
                            height: 45.0,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }

  Widget _detailsCard() {
    return new SliverToBoxAdapter(
      child: new Card(
        elevation: 2.0,
        margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
        child: new Container(
          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                Strings.labelDetails,
                style: Theme
                    .of(context)
                    .textTheme
                    .body2,
              ),
              new Padding(padding: new EdgeInsets.only(top: 10.0)),
              new Text(
                Strings.labelCall,
                style: Theme
                    .of(context)
                    .textTheme
                    .caption,
              ),
              new InkWell(
                onTap: () {
                  launch("tel://${objectModel.phone}");
                },
                child: new Row(
                  children: <Widget>[
                    new Text(
                      objectModel.phone,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .apply(color: Colors.green),
                    ),
                    new Padding(padding: new EdgeInsets.only(left: 5.0)),
                    new Icon(
                      Icons.call,
                      color: Colors.green,
                      size: 15.0,
                    )
                  ],
                ),
              ),
              new ListView.builder(
                  padding: EdgeInsets.fromLTRB(0.0, 7.0, 0.0, 0.0),
                  shrinkWrap: true,
                  primary: false,
                  itemCount: infoQuestionList1.length,
                  itemBuilder: (BuildContext context, int index) {
                    InfoAnswerStoreModel model = infoQuestionList1[index];
                    return model.answer==""?new Container(): new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          model.businessInfoQuestion,
                          style: Theme
                              .of(context)
                              .textTheme
                              .caption,
                        ),
                        new Padding(padding: new EdgeInsets.only(bottom: 2.0)),
                        new Text(model.answer,
                            style: Theme
                                .of(context)
                                .textTheme
                                .body1),
                        index == infoQuestionList1.length - 1
                            ? new Container()
                            : new Padding(
                            padding: new EdgeInsets.only(bottom: 7.0))
                      ],
                    );
                  }),
              objectModel.branchCount > 1
                  ? new Padding(padding: new EdgeInsets.only(top: 7.0))
                  : new Container(),
              objectModel.branchCount> 1
                  ? new InkWell(onTap: (){
                var route = new MaterialPageRoute(builder: (BuildContext context) => new OutletStoreListScreen(_storeId,));
                    _navigateAndReturn(context, route, 0);
              },child: new Text("${objectModel.branchCount.toString()} ${Strings.labelOutlet} ${objectModel.city}",style: Theme.of(context).textTheme.caption.apply(color: Colors.myColor),),)
                  : new Container()
            ],
          ),
        ),
      ),
    );
  }

  Widget _photoCard() {
    return new SliverToBoxAdapter(
      child: new Card(
        margin: EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
        elevation: 2.0,
        child: new Container(
          margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                Strings.labelPhotos,
                style: Theme
                    .of(context)
                    .textTheme
                    .body2,
              ),
              new GridView.builder(
                  padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                  shrinkWrap: true,
                  primary: false,
                  itemCount: galleryList1.length > 4 ? 4 : galleryList1.length,
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4),
                  itemBuilder: (BuildContext context, int index) {
                    String galleryModel = galleryList1[index];
                    return new InkWell(
                      onTap: () {
                        print(index);
                        _photoClick(index);
                      },
                      child: new Card(
                        elevation: 1.0,
                        child: new GridTile(
                          footer: galleryList1.length > 4
                              ? index == 3
                              ? new Container(
                            color: Colors.black54,
                            child: new Text(
                              "More",
                              textAlign: TextAlign.center,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .caption
                                  .apply(color: Colors.white),
                            ),
                          )
                              : new Container()
                              : new Container(),
                          child: galleryModel != null
                              ? new FadeInImage(
                              placeholder:
                              new AssetImage("images/default1.png"),
                              image: new NetworkImage(galleryModel),
                              height: 45.0,
                              fit: BoxFit.cover)
                              : new Image.asset(
                            "images/default1.png",
                            height: 45.0,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }

  Widget _addressCard() {
    return new SliverToBoxAdapter(
      child: new Card(
        elevation: 2.0,
        margin: EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
        child: new Container(
          margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        Strings.labelAddress,
                        style: Theme
                            .of(context)
                            .textTheme
                            .body2,
                      ),
                      new Padding(padding: new EdgeInsets.only(top: 10.0)),
                      new Text(objectModel.address,
                          style: Theme
                              .of(context)
                              .textTheme
                              .caption)
                    ],
                  )),
              new Column(
                children: <Widget>[
                  new InkWell(
                    child: new Card(
                      elevation: 2.0,
                      child: new Center(
                        child: new Image.asset("images/location.jpg", width: 50.0, height: 50.0,fit: BoxFit.cover,),
                      ),
                    ),
                    onTap: _showMap,
                  ),
                  kilometer(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  //region kilometer
  Widget kilometer() {
    int kilometer;
    if (latitude != null && longitude != null) {
      kilometer = calculateDistance(
          latitude,
          longitude,
          double.parse(objectModel.latitude),
          double.parse(objectModel.longitude));
    }
    return new Text(
      kilometer!=null?"${kilometer.toString()} km":"",
      style: Theme.of(context).textTheme.caption.apply(color: Colors.myColor),
    );
  }

  int calculateDistance(double currentLat, double currentLng,double destinationLat, double destinationLng) {
    final Distance distance = new Distance();
    double km;
    km = distance.as(LengthUnit.Kilometer, new LatLng(currentLat, currentLng),
        new LatLng(destinationLat, destinationLng));
    print("km1 cail ${km.ceil()}");
    print("km1 floor ${km.floor()}");
    print("km1 round ${km.round()}");

    return km.round();
  }

  //endregion

  Widget _openingHourCard() {
    return new SliverToBoxAdapter(
      child: new Card(
        elevation: 2.0,
        margin: EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 0.0),
        child: new Container(
          margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(
                Strings.labelOpeningHours,
                style: Theme
                    .of(context)
                    .textTheme
                    .body2,
              ),
              new ListView.builder(
                  itemCount: hourList1.length,
                  shrinkWrap: true,
                  primary: false,
                  padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                  itemBuilder: (BuildContext context, int index) {
                    HourStoreDetailModel model = hourList1[index];

                    //region Date (am / pm)
                    DateFormat dateFormat = new DateFormat.Hm();

                    DateTime now = DateTime.now();
                    DateTime open = dateFormat.parse(model.openingTime);
                    open = new DateTime(
                        now.year, now.month, now.day, open.hour, open.minute);

                    DateTime breakStart;
                    DateTime breakEnd;
                    if (model.breakStartTime != null && model.breakEndTime != null) {
                      breakStart = dateFormat.parse(model.breakStartTime);
                      breakStart = new DateTime(now.year, now.month, now.day,
                          breakStart.hour, breakStart.minute);

                      breakEnd = dateFormat.parse(model.breakEndTime);
                      breakEnd = new DateTime(now.year, now.month, now.day,
                          breakEnd.hour, breakEnd.minute);
                    }
                    DateTime close = dateFormat.parse(model.closingTime);
                    close = new DateTime(
                        now.year, now.month, now.day, close.hour, close.minute);
                    //endregion

                    return !model.isClosed ?
                      model.breakStartTime != null &&
                        model.breakEndTime != null
                        ? Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(
                            flex: 2,
                            child: new Text(model.dayName,
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                        new Expanded(
                            flex: 3,
                            child: new Text(
                                new DateFormat('h:mm a').format(open),
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                        new Expanded(
                            child: new Text(" to ",
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                        new Expanded(
                            flex: 3,
                            child: new Text(
                                new DateFormat('h:mm a')
                                    .format(breakStart),
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                        new Expanded(
                            child: new Text(" - ",
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                        new Expanded(
                            flex: 3,
                            child: new Text(
                                new DateFormat('h:mm a').format(breakEnd),
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                        new Expanded(
                            child: new Text(" to ",
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                        new Expanded(
                            flex: 4,
                            child: new Text(
                                new DateFormat('h:mm a').format(close),
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                      ],
                    )
                        : Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(
                            flex: 2,
                            child: new Text(
                              model.dayName,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .caption
                                  .apply(
                                  color: model.dayOfWeek ==
                                      DateTime
                                          .now()
                                          .weekday
                                      ? Colors.myColor
                                      : null),
                              textAlign: TextAlign.start,
                            )),
                        new Expanded(
                            flex: 3,
                            child: new Text(
                                new DateFormat('h:mm a').format(open),
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                        new Expanded(
                            child: new Text(" to",
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                        new Expanded(
                            flex: 12,
                            child: new Text(
                                new DateFormat('h:mm a').format(close),
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .apply(
                                    color: model.dayOfWeek ==
                                        DateTime
                                            .now()
                                            .weekday
                                        ? Colors.myColor
                                        : null),
                                textAlign: TextAlign.start)),
                      ],
                    )
                    : new Row(
                      children: <Widget>[
                        new Expanded(flex: 1 ,child: new Text(
                          model.dayName,
                          style: Theme
                              .of(context)
                              .textTheme
                              .caption
                              .apply(
                              color: model.dayOfWeek ==
                                  DateTime
                                      .now()
                                      .weekday
                                  ? Colors.myColor
                                  : null),
                          textAlign: TextAlign.start,
                        )),
                        new Expanded(flex: 8,child: new Text(Strings.labelClosed,style: Theme.of(context).textTheme.caption.apply(color: Colors.red),),
                        )
                      ],
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }

  Widget _reviewCard() {
    return new SliverToBoxAdapter(
      child: new Card(
        elevation: 2.0,
        margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, 6.0),
        child: new Container(
          margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 12.0),
          child: new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Card(
                    color: Colors.green,
                    elevation: 1.0,
                    child: new Container(
                      width: 30.0,
                      height: 20.0,
                      child: new Center(
                        child: new Text(
                          objectModel.totalRating == null ||
                              objectModel.totalRating == "0.0"
                              ? "0"
                              : objectModel.totalRating,
                          style: Theme
                              .of(context)
                              .textTheme
                              .caption
                              .apply(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  new Padding(padding: EdgeInsets.only(left: 10.0)),
                  new Text(
                    Strings.labelBasedOn,
                    style: Theme
                        .of(context)
                        .textTheme
                        .body1,
                  ),
                  new Expanded(
                      child: new InkWell(
                          onTap: () {
                            objectModel.reviewCount == "0"
                                ? null
                                : _viewAllReviewClick();
                          },
                          child: new Text(
                            " ${objectModel.reviewCount} ${Strings
                                .labelReviews}",
                            style: Theme
                                .of(context)
                                .textTheme
                                .body2
                                .apply(color: Colors.myColor),
                          ))),
                ],
              ),
              !isRate
                  ? new Container()
                  : new Padding(padding: new EdgeInsets.only(top: 10.0)),
              !isRate
                  ? new Container()
                  : new Container(
                foregroundDecoration: ViewLineFromRegistration(),
              ),
              !isRate
                  ? new Container()
                  : new Padding(padding: new EdgeInsets.only(top: 10.0)),
              !isRate
                  ? new Container()
                  : new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  reviewList1[0].profileImageName != null
                      ? new Container(
                      width: 40.0,
                      height: 40.0,
                      decoration: new BoxDecoration(
                        color: Colors.transparent,
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            image: new NetworkImage(
                                reviewList1[0].profileImageName),
                            fit: BoxFit.cover),
                      )) //
                      : new Icon(Icons.account_circle,size: 40.0,color: Colors.black54,),
                  new Padding(padding: EdgeInsets.only(left: 10.0)),
                  new Expanded(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            reviewList1[0].customerName,
                            style: Theme
                                .of(context)
                                .textTheme
                                .body1,
                            maxLines: 1,
                          ),
                          new Padding(padding: new EdgeInsets.only(top: 1.0)),
                          new StarRating(
                            size: 15.0,
                            rating: double.parse(reviewList1[0].starRating),
                            color: Colors.myColor,
                          ),
                          new Padding(padding: new EdgeInsets.only(top: 1.0)),
                          new Text(
                            reviewList1[0].review /*model.review*/,
                            style: Theme
                                .of(context)
                                .textTheme
                                .caption,
                          ),
                        ],
                      )),
                  new IconButton(
                      icon: new Icon(Icons.delete, color: Colors.black45),
                      onPressed: () {
                        _deleteReviewClick();
                      })
                ],
              ),
              new Padding(padding: new EdgeInsets.only(top: 10.0)),
              new Container(
                foregroundDecoration: ViewLineFromRegistration(),
              ),
              new Padding(padding: new EdgeInsets.only(top: 10.0)),
              new Row(
                children: <Widget>[
                  new Expanded(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(Strings.labelHowDidVisitGo,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .body1),
                          new Padding(padding: new EdgeInsets.only(top: 3.0)),
                          new Text(
                            Strings.labelTellEveryOneAbout,
                            style: Theme
                                .of(context)
                                .textTheme
                                .caption,
                          )
                        ],
                      )),
                  new MaterialButton(
                    onPressed: () {
                      _rateClick();
                    },
                    child: new Text(!isRate
                        ? Strings.labelButtonAddReview
                        : Strings.labelButtonEditReview),
                    color: Colors.myColor,
                    textColor: Colors.white,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .title),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _txtOpenClose(DateTime now, DateTime open, DateTime breakStart,
      DateTime breakEnd, DateTime close) {
    String label = "";
    Color color;

    if (now.isAfter(open) && now.isBefore(breakStart)) {
      label = Strings.labelOpenNow;
      color = Colors.green;
    } else if (now.isAfter(breakEnd) && now.isBefore(close)) {
      label = Strings.labelOpenNow;
      color = Colors.green;
    } else {
      label = Strings.labelClosed;
      color = Colors.red;
    }

    return new Text(
      label,
      textAlign: TextAlign.start,
      style: Theme
          .of(context)
          .textTheme
          .caption
          .apply(color: color),
    );
  }

  _offerClick(int position) {
    var route = new MaterialPageRoute(builder: (BuildContext context) => new OfferDescriptionScreen(offerId: offerList1[position].OfferMasterId, businessMasterId: _storeId, businessName: objectModel.businessName, isFromStore: true,));
    _navigateAndReturn(context, route, 1);
  }

  _deleteReviewClick() {
    showDialog(
        context: context,
        child: new AlertDialog(
          content: new Text(Strings.labelAlertReviewContent),
          contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text(Strings.labelAlertCancel)),
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  if (internetConnection != null && internetConnection) {
                    _deleteReview();
                  } else if (internetConnection != null &&
                      !internetConnection) {
                    Constants.showSnackBar(
                        scaffoldStateStoreDetail, Strings.noInternetToast);
                  }
                },
                child: new Text(Strings.labelAlertDelete)),
          ],
        ));
  }

  _menuClick(int index) {
    if (menuList1.length > 4) {
      if (index == 3) {
        var route = new MaterialPageRoute(
            builder: (BuildContext context) =>
            new AllImageScreen(Strings.labelMenu, menuList1));
        _navigateAndReturn(context, route, 0);
      } else {
        var route = new MaterialPageRoute(
            builder: (BuildContext context) =>
            new FullImageScreen(menuList1, Strings.labelMenu, index));
        _navigateAndReturn(context, route, 0);
      }
    } else {
      var route = new MaterialPageRoute(
          builder: (BuildContext context) =>
          new FullImageScreen(menuList1, Strings.labelMenu, index));
      _navigateAndReturn(context, route, 0);
    }
  }

  _photoClick(int index) {
    if (galleryList1.length > 4) {
      if (index == 3) {
        var route = new MaterialPageRoute(
            builder: (BuildContext context) =>
            new AllImageScreen(Strings.labelPhotos, galleryList1));
        _navigateAndReturn(context, route, 0);
      } else {
        var route = new MaterialPageRoute(
            builder: (BuildContext context) =>
            new FullImageScreen(galleryList1, Strings.labelPhoto, index));
        _navigateAndReturn(context, route, 0);
      }
    } else {
      var route = new MaterialPageRoute(
          builder: (BuildContext context) =>
          new FullImageScreen(galleryList1, Strings.labelPhoto, index));
      _navigateAndReturn(context, route, 0);
    }
  }

  _viewAllOfferClick() {
    var route = new MaterialPageRoute(
        builder: (BuildContext context) =>
        new OfferListScreen(
          title: "${objectModel.businessName}'s Offer",
          businessMasterId: _storeId,
          offerListOfStore: offerList1,
          businessTypeMasterId:
          int.parse(objectModel.linkToBusinessTypeMasterId),
        ));
    _navigateAndReturn(context, route, 1);
  }

  _viewAllReviewClick() {
    var route = new MaterialPageRoute(
        builder: (BuildContext context) => new ViewAllReview(_storeId));
    subscription == null;
    _navigateAndReturn(context, route, 1);
  }

  _showMap() {
    if (objectModel.latitude != null && objectModel.longitude != null) {
      Marker marker = new Marker("1", _storeName, _latitude, _longitude, color: Colors.red);
      mapView.onMapReady.listen((_) {
        setState(() {
          mapView.removeMarker(marker);
          mapView.addMarker(marker);
        });
      });

      mapView.show(

          new MapOptions(
              mapViewType: MapViewType.normal,
              initialCameraPosition: new CameraPosition(new Location(double.parse(objectModel.latitude), double.parse(objectModel.longitude)), 16.0),
              showUserLocation: true,
              title: "Map"
          )
      );
    }else{
      Fluttertoast.showToast(msg: Strings.labelDirectionNotAvailable,toastLength: Toast.LENGTH_LONG);
    }
  }

  _rateClick() {
    if (isRate) {
      var route = new MaterialPageRoute(
          builder: (BuildContext context) =>
          new EditReviewStoreScreen(
            editReviewStoreModel: reviewList1[0],
          ));
      subscription == null;

      _navigateAndReturn(context, route, 1);
    } else {
      var route = new MaterialPageRoute(
          builder: (BuildContext context) =>
          new EditReviewStoreScreen(
            storeId: _storeId,
            businessImageName: objectModel.businessImageName,
          ));
      subscription == null;

      _navigateAndReturn(context, route, 1);
    }
  }

  _checkInIconClick(bool isBeenClick, bool isFavouriteClick) {
    if (isBeenClick) {
      if (isBeen) {
        _deleteCheckIn(true, false);
      } else {
        _insertCheckIn(true, false);
      }
    } else if (isFavouriteClick) {
      if (isFavourite) {
        _deleteCheckIn(false, true);
      } else {
        _insertCheckIn(false, true);
      }
    }
  }

  _deleteCheckIn(bool isBeenClick, isFavouriteClick) {
    _progressDialog(true);
    int errorCode;
    Map<String, dynamic> body2;
    if (isBeenClick) {
      body2 = {
        'CheckinMaster': {'CheckinMasterId': _checkInBeenId}
      };
    } else if (isFavouriteClick) {
      body2 = {
        'CheckinMaster': {'CheckinMasterId': _checkInFavId}
      };
    }
    var encoder = JSON.encode(body2);
    var url = '${Constants.url}DeleteCheckinMaster';

    Future<int> result = StoreDetailParser.deleteCheckIn(url, encoder);
    result.then((c) {
      errorCode = c;
      _handleDeleteCheckInResult(isBeenClick, isFavouriteClick, errorCode);
    }).catchError((onError) {
      errorCode = -1;
      setState(() {});
    });
  }

  _deleteReview() {
    _progressDialog(true);
    int errorCode;
    Map<String, dynamic> body2;
    body2 = {
      'ReviewMaster': {
        'SessionId': _sessionId,
        'ReviewMasterId': reviewList1[0].reviewMasterId
      }
    };

    var encoder = JSON.encode(body2);
    var url = '${Constants.url}DeleteReviewMaster';

    Future<int> result = StoreDetailParser.deleteReview(url, encoder);
    result.then((c) {
      errorCode = c;
      _handleDeleteReview(errorCode);
    }).catchError((onError) {
      errorCode = -1;
      _progressDialog(false);
    });
  }

  _insertCheckIn(bool isBeenClick, bool isFavouriteClick) {
    _progressDialog(true);
    Map errorCode;
    Map<String, dynamic> body2;
    if (isBeenClick) {
      body2 = {
        'CheckinMaster': {
          'SessionId': _sessionId,
          'linktoBusinessMasterId': _storeId,
          'linktoCustomerMasterId': _customerId,
          'Type': 1
        }
      };
    } else if (isFavouriteClick) {
      body2 = {
        'CheckinMaster': {
          'SessionId': _sessionId,
          'linktoBusinessMasterId': _storeId,
          'linktoCustomerMasterId': _customerId,
          'Type': 2
        }
      };
    }

    var encoder = JSON.encode(body2);
    var url = '${Constants.url}InsertCheckinMaster';
    Future<Map> result = StoreDetailParser.insertCheckIn(url, encoder);
    result.then((c) {
      errorCode = c;
      int error = errorCode['InsertCheckinMasterResult']['ErrorCode'];
      int rowId = errorCode['InsertCheckinMasterResult']['RowId'];

      _handleInsertCheckInResult(isBeenClick, isFavouriteClick, error, rowId);
    }).catchError((onError) {
      print(onError);
    });
  }

  _handleInsertCheckInResult(bool isBeenClick, bool isFavouriteClick, int errorCode, int checkInId) {
    _progressDialog(false);
    if (errorCode == 0) {
      if (isBeenClick) {
        setState(() {
          isBeen = true;
          _checkInBeenId = checkInId.toString();
          Fluttertoast.showToast(
              msg: Strings.toastAddBeen, toastLength: Toast.LENGTH_LONG);
        });
      } else if (isFavouriteClick) {
        setState(() {
          isFavourite = true;
          _checkInFavId = checkInId.toString();
          Fluttertoast.showToast(msg: Strings.toastAddFav, toastLength: Toast.LENGTH_LONG);
        });
      }
    } else if (errorCode == -1) {
      Constants.showSnackBar(scaffoldStateStoreDetail, Strings.errorInServices);
    }
  }

  _handleDeleteCheckInResult(bool isBeenClick, bool isFavouriteClick, int errorCode) {
    _progressDialog(false);
    if (errorCode == 0) {
      if (isBeenClick) {
        setState(() {
          isBeen = false;
          Fluttertoast.showToast(
              msg: Strings.toastRemoveBeen, toastLength: Toast.LENGTH_LONG);
        });
      } else if (isFavouriteClick) {
        setState(() {
          isFavourite = false;
          Fluttertoast.showToast(
              msg: Strings.toastRemoveFav, toastLength: Toast.LENGTH_LONG);
        });
      }
    } else if (errorCode == -1) {
      Constants.showSnackBar(scaffoldStateStoreDetail, Strings.errorInServices);
    }
  }

  _handleDeleteReview(int errorCode) {
    _progressDialog(false);
    if (errorCode == 0) {
      setState(() {
        isLoading = true;
      });
    } else if (errorCode == -1) {
      Constants.showSnackBar(scaffoldStateStoreDetail, Strings.errorInServices);
    }
  }

  _share() {
    String appUrlForAndroid =
        "http://play.google.com/store/apps/details?id=${packageName}";
    String appUrlForIphone = "http://itunes.apple.com/+91/app/${_packageInfo
        .appName}/id$packageName?mt=8";
    final RenderBox box = context.findRenderObject();
    Share.share(
        "Found ${objectModel.businessName} in ${objectModel.area}, ${objectModel
            .city}.\nView offers and more about it by Downloading CrazyRex: ${defaultTargetPlatform ==
            TargetPlatform.iOS ? appUrlForIphone : appUrlForAndroid}",
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  _callApis() {
    print("url --- ${Constants.url}SelectAllBusinessDetailMaster/$_storeId/$_cityId/$_customerId");
    setUpStoreDetailScreenListener = new SetUpStoreDetailScreenListener("${Constants.url}SelectAllBusinessDetailMaster/$_storeId/$_cityId/$_customerId", this);
    setUpStoreDetailScreenListener.loadStoreDetail();
  }

  @override
  void onErrorStoreDetail(onError) {
    print("error in store detail screen");
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onCheckInListener(List<BeenFavStoreModel> checkInList) {
    print("checkIn List------- ${checkInList.toString()}");
    checkInList1 = checkInList;
    if (checkInList.length != 0) {
      for (int i = 0; i < checkInList.length; i++) {
        if (checkInList[i].type == "1") {
          _checkInBeenId = checkInList[i].checkInMasterId;
          isBeen = true;
        } else if (checkInList[i].type == "2") {
          _checkInFavId = checkInList[i].checkInMasterId;
          isFavourite = true;
        }
      }
    } else {
      isBeen = false;
      isFavourite = false;
    }
  }

  @override
  void onGalleryListener(List<String> galleryList) {
    print("gallery List------- ${galleryList.toString()}");
    galleryList1 = galleryList;
  }

  @override
  void onHourListener(List<HourStoreDetailModel> hourList) {
    print("hour List------- ${hourList.toString()}");
    hourList1 = hourList;
  }

  @override
  void onInfoAnswerListener(List<InfoAnswerStoreModel> infoQuestionList) {
    print("infoQuestion List------- ${infoQuestionList.toString()}");
    infoQuestionList1 = infoQuestionList;
  }

  @override
  void onMenuListener(List<String> menuList) {
    print("menu List------- ${menuList.toString()}");
    menuList1 = menuList;
  }

  @override
  void onObjectDataListener(ObjectStoreDetailModel objectStoreDetailModel) {
    print("objectStoreDetail  Model  ------- ${objectStoreDetailModel
        .toString()}");
    objectModel = objectStoreDetailModel;

    _storeName = objectModel.businessName;
    _latitude = double.parse(objectModel.latitude);
    _longitude = double.parse(objectModel.longitude);

    markers = <Marker>[
      new Marker("1", "$_storeName", _latitude, _longitude, color: Colors.red)
    ];

    cameraPosition = new CameraPosition(
        new Location(double.parse(objectModel.latitude),
            double.parse(objectModel.longitude)),
        15.0);
    staticMapUri = staticMapProvider.getStaticUri(
        new Location(double.parse(objectModel.latitude),
            double.parse(objectModel.longitude)),
        15,
        height: 50,
        width: 50,
        mapType: StaticMapViewType.roadmap);

    setState(() {
      isLoading = false;
      isError = false;
    });
  }

  @override
  void onOfferListener(List<OfferListModel> offerList) {
    print("offer List  ------- ${offerList.toString()}");
    offerList1 = offerList;
  }

  @override
  void onReviewListener(List<ReviewStoreModel> reviewList) {
    print("Review List  ------- ${reviewList.toString()}");
    reviewList1 = reviewList;
    if (reviewList1.length != 0) {
      isRate = true;
    } else {
      isRate = false;
    }
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    if (expandedHeight != 0.0) {
      setState(() {
        expandedHeight = 0.0;
      });
    }
    final result = await Navigator.push(context, rout);
    onResume();
    switch(navigateTo){
      case 0:
        setState(() {
          isLoading = false;
        });
        break;
      case 1:
        setState(() {
          isLoading = true;
        });
        break;
    }
  }

  onResume() {
    expandedHeight = 250.0;
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);
          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            setState(() {
              internetConnection = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }
}
