import 'dart:async';

import 'package:crazyrex/model/store_list_model.dart';
import 'package:crazyrex/page/store_detail_screen.dart';
import 'package:crazyrex/parser/all_store_list_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:intl/intl.dart';
import 'package:latlong/latlong.dart';

class StoreListByCategory extends StatefulWidget {
  String businessType;
  int businessTypeMasterId;
  bool isRecentlyJoined = false;
  double latitude, longitude;
  String areaMasterIds;
  String sortType;

  StoreListByCategory(this.businessType, this.businessTypeMasterId,
      this.latitude, this.longitude, this.areaMasterIds, this.sortType) {
    if (businessTypeMasterId == Constants.recentlyJoinedId) {
      isRecentlyJoined = true;
    }
  }

  @override
  _StoreListByCategoryState createState() => _StoreListByCategoryState(
      businessType,
      businessTypeMasterId,
      isRecentlyJoined,
      latitude,
      longitude,
      areaMasterIds,
      sortType);
}

class _StoreListByCategoryState extends State<StoreListByCategory>
    implements AllStoreDataListener {
  String businessType;
  int businessTypeMasterId;
  List<StoreListModel> storeList;
  bool isLoading;
  bool isRecentlyJoined = false;
  bool isError = false;
  double latitude, longitude;
  SetUpAllStoreListenerData setUpAllStoreListenerData;
  String areaMasterIds;
  String sortType;
  int cityId;

  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;

  _StoreListByCategoryState(
      this.businessType,
      this.businessTypeMasterId,
      this.isRecentlyJoined,
      this.latitude,
      this.longitude,
      this.areaMasterIds,
      this.sortType);

  @override
  void initState() {
    super.initState();
    PrefUtil.getIntPreference(Constants.prefCityId).then((int value) {
      cityId = value;
    });
    _internetConnection();
  }

  _internetConnection() {
    if (subscription == null) {
      connectivity = new Connectivity();
      subscription = connectivity.onConnectivityChanged
          .listen((ConnectivityResult result) {
        _connectionStatus = result.toString();
        print(_connectionStatus);

        if (result == ConnectivityResult.wifi ||
            result == ConnectivityResult.mobile) {
          //    _isLoading = true;
          setState(() {
            internetConnection = true;
            isLoading = true;
          });
        } else {
          setState(() {
            internetConnection = false;
          });
        }
      });
    }
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {
                          _internetConnection();
                        });
                      })
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading != null && isLoading) {
          _callApis();
        }
        if (isLoading != null) {
          return isLoading
              ? new Scaffold(
              body: new Center(
                child: new CircularProgressIndicator(),
              ))
              : new Scaffold(
            body: storeList.length != 0 ? _list() : _noData(),
          );
        } else {
          return new Scaffold();
        }
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return Scaffold();
    }
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _list() {
    return new Container(
        child: new ListView.builder(
            itemCount: storeList.length,
            itemBuilder: (BuildContext context, int index) {
              final StoreListModel store = storeList[index];
              return new InkWell(
                onTap: (){handleClickOfItem(store);},
                child: new Card(
                  elevation: 2.0,
                  margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0, index == storeList.length-1?6.0: 0.0),
                  child: new Container(
                    margin: EdgeInsets.fromLTRB(12.0, 10.0, 10.0, 10.0),
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          child: new Row(
                            children: <Widget>[
                              new Card(
                                child: new Container(
                                  height: 75.0,
                                  width: 75.0,
                                  child: new FadeInImage(
                                      fit: BoxFit.cover,
                                      placeholder:
                                      new AssetImage("images/bg33.png"),
                                      image: new NetworkImage(store.xs_ImageName)),
                                ),
                              ),
                              new Expanded(child: new Container(
                                margin: EdgeInsets.only(left: 6.0),
                                child: new Column(
                                  children: <Widget>[
                                    new Row(
                                      children: <Widget>[
                                        new Expanded(flex:4,child: new Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Padding(padding: EdgeInsets.only(top: 5.0)),
                                            new Text(store.businessName, textAlign: TextAlign.start, softWrap: false, style: Theme.of(context).textTheme.body2, maxLines: 1,),
                                            new Padding(padding: EdgeInsets.only(top: 4.0)),
                                            new Row(
                                              children: <Widget>[
                                                new Text(store.businessType, textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption,),
                                                Padding(padding: EdgeInsets.only(left: 3.0)),
                                                vegNonVeg(store)
                                              ],
                                            ),
                                            Padding(padding: new EdgeInsets.only(top: 4.0),),
                                            new Text(store.area, textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption,),
                                            Padding(padding: new EdgeInsets.only(top: 4.0),),
                                          ],
                                        )),
                                        store.totalRating != "0.0"
                                            ? new Expanded(flex: 1,
                                            child: new Column(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: <Widget>[
                                                new Column(
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    store.totalRating != "0.0"
                                                        ? new Icon(Icons.star, color: Colors.green,)
                                                        : new Container(),
                                                    store.totalRating != "0.0"
                                                        ? new Text(store.totalRating, textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption,)
                                                        : new Container(),
                                                  ],
                                                ),
                                              ],
                                            ))
                                            : new Container()
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        new Expanded(child: _txtOpenClose(store)),
                                        store.kilometer != null
                                            ? new Text("${store.kilometer} km", textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption,)
                                            : new Container(),
                                      ],
                                    ),
                                  ],
                                ),
                              ))
                            ],
                          ),
                        ),
                        new Padding(padding: EdgeInsets.only(top: 10.0)),
                        new Container(
                          foregroundDecoration: ViewLineFromRegistration(),
                        ),
                        new Padding(padding: EdgeInsets.only(top: 10.0)),
                        new Container(
                          child: new Row(
                            children: <Widget>[
                              new Padding(padding: EdgeInsets.only(left: 8.0)),
                              new Expanded(child: new Text(
                                  "${store.countOffer} ${Strings.labelOffers}",
                                  textAlign: TextAlign.start,
                                  softWrap: false,
                                  style: Theme
                                      .of(context)
                                      .textTheme
                                      .caption
                                      .apply(color: Colors.myColor))),
                              new Text("${store.offerUsedCount} ${Strings.labelTimeUsed}", textAlign: TextAlign.start, softWrap: false, style: Theme.of(context).textTheme.caption)
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }));
  }
  Widget vegNonVeg(StoreListModel model) {
    if (model.vegNonVeg == 1) {
      //veg
      return new Image.asset("images/ic_veg.png",height: 10.0,width: 10.0,);
    } else if (model.vegNonVeg == 2) {
      //non veg
      return new Image.asset("images/nonveg.png",height: 10.0,width: 10.0,);
    } else if (model.vegNonVeg == 3) {
      // veg and non veg
      return new Row(
        children: <Widget>[
          new Image.asset("images/ic_veg.png",height: 10.0,width: 10.0,),
          Padding(padding: EdgeInsets.only(left: 2.0)),
          new Image.asset("images/nonveg.png",height: 10.0,width: 10.0,),
        ],
      );
    }
    return new Container();
  }
  void setKilometer() {
    if (longitude != null || latitude != null) {
      for (int i = 0; i < storeList.length; i++) {
        storeList[i].kilometer = calculateDistance(latitude, longitude, double.parse(storeList[i].latitude), double.parse(storeList[i].longitude));
      }
    }
  }

  Widget _txtOpenClose(StoreListModel model) {
    String label = "";
    Color color;
    DateFormat dateFormat = new DateFormat.Hm();

    DateTime now = DateTime.now();
    DateTime open = dateFormat.parse(model.openingTime);
    open = new DateTime(now.year, now.month, now.day, open.hour, open.minute);

    DateTime breakStart = dateFormat.parse(model.breakStartTime);
    breakStart = new DateTime(now.year, now.month, now.day, open.hour, open.minute);

    DateTime breakEnd = dateFormat.parse(model.breakEndTime);
    breakEnd = new DateTime(now.year, now.month, now.day, open.hour, open.minute);

    DateTime close = dateFormat.parse(model.closingTime);
    close = new DateTime(now.year, now.month, now.day, close.hour, close.minute);

    if (now.isAfter(open) && now.isBefore(breakStart)) {
      label = "Open now";
      color = Colors.green;
    } else if(now.isAfter(breakEnd) && now.isBefore(close)){
      label = "Open now";
      color = Colors.green;
    }else{
      label = "Closed";
      color = Colors.red;
    }

    return new Text(label, textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption.apply(color: color),);
  }

  _callApis() {
    String url;
    if (areaMasterIds != null) {
      if (areaMasterIds.isEmpty) {
        areaMasterIds = null;
      }
    }
    if (!isRecentlyJoined) {
      url = "${Constants
          .url}SelectAllBusinessMasterBusinessByType/$businessTypeMasterId/$cityId/${areaMasterIds}";
      setUpAllStoreListenerData =
      new SetUpAllStoreListenerData(url, this, false);
    } else {
      url = "${Constants
          .url}SelectAllBusinessMasterBusinessByRecentAdded/$cityId/${areaMasterIds}";
      setUpAllStoreListenerData =
      new SetUpAllStoreListenerData(url, this, true);
    }
    print("url :- $url");
    setUpAllStoreListenerData.loadAllStoreListData();
  }

  Widget _noData() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.store,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyStoreList,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme
                          .of(context)
                          .textTheme
                          .title
                          .apply(color: Colors.myColor)),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  void onStoreListener(List<StoreListModel> stores) {
    print("storeList $stores");
    setState(() {
      isLoading = false;
      storeList = stores;
      if (storeList.length != 0) {
        setKilometer();
        sort(sortType);
      }
    });
  }

  sort(String sortType) {
    if (storeList != null || storeList.length != 0) {
      if (sortType == "nearest") {
        if (storeList[0].kilometer != null) {
          storeList.sort((StoreListModel modelA, StoreListModel modelB) {
            return modelA.kilometer - modelB.kilometer;
          });
        }
      } else if (sortType == "alphabetic") {
        storeList.sort((StoreListModel modelA, StoreListModel modelB) {
          return modelA.businessName
              .toLowerCase()
              .compareTo(modelB.businessName.toLowerCase());
        });
      } else if (sortType == "newest") {
        storeList.sort((StoreListModel modelA, StoreListModel modelB) {
          return modelA.businessMasterId.compareTo(modelB.businessMasterId);
        });
        Iterable inReverse = storeList.reversed;
        storeList = inReverse.toList();
      } else if (sortType == "popular") {
        storeList.sort((StoreListModel modelA, StoreListModel modelB) {
          return double
              .parse(modelA.totalRating)
              .compareTo(double.parse(modelB.totalRating));
        });
        Iterable inReverse = storeList.reversed;
        storeList = inReverse.toList();
      }
    }
  }

  int calculateDistance(double currentLat, double currentLng,double destinationLat, double destinationLng) {
    final Distance distance = new Distance();
    double km;
    km = distance.as(LengthUnit.Kilometer, new LatLng(currentLat, currentLng),
        new LatLng(destinationLat, destinationLng));
    print("km1 cail ${km.ceil()}");
    print("km1 floor ${km.floor()}");
    print("km1 round ${km.round()}");

    return km.round();
  }

  @override
  void onErrorStoreListener(String error) {
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  handleClickOfItem(StoreListModel model) {
    var route = new MaterialPageRoute(
        builder: (BuildContext context) =>
        new StoreDetailScreen(model.businessMasterId));
    _navigateAndReturn(context, route, 0);
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);
          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            setState(() {
              internetConnection = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }


}
