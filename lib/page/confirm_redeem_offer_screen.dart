import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:crazyrex/model/offer_master_obj_model.dart';
import 'package:crazyrex/model/store_list_model.dart';
import 'package:crazyrex/parser/Insert_offer_redeem_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ConfirmRedeemOfferScreen extends StatefulWidget {
  StoreListModel store;
  OfferMasterObjectModel offerModel;

  ConfirmRedeemOfferScreen(this.store, this.offerModel);

  @override
  _ConfirmRedeemOfferScreenState createState() =>
      _ConfirmRedeemOfferScreenState();
}

class _ConfirmRedeemOfferScreenState extends State<ConfirmRedeemOfferScreen> {
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isLoading;
  bool isError = false;
  bool displaySuccess = false;

  String sessionId;
  String CustomerMasterId;

  int customerMembershipTranId;

  BuildContext context;

  _ConfirmRedeemOfferScreenState() {
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value) {
      sessionId = value;
    });

    PrefUtil
        .getStringPreference(Constants.prefCustomerMasterId)
        .then((String value) {
      CustomerMasterId = value;
    });
    PrefUtil.getIntPreference(Constants.prefMembershipTranId).then((int value) {
      customerMembershipTranId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
  }

  @override
  void dispose() {
    subscription = null;
    super.dispose();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        setState(() {
          internetConnection = true;
          isLoading = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, [1]);
      },
      child: _mainLayout(),
    );
  }

  Widget _mainLayout() {
    if (internetConnection != null && internetConnection) {
      return new Scaffold(
        body: new Center(
          child: !displaySuccess ? _build() : success(),
        ),
      );
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return Scaffold();
    }
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {
                      _internetConnection();
                    });
                  })
            ],
          ),
        ),
      ],
    ));
  }

  Widget _build() {
    return new Container(
      margin: EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 0.0),
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            widget.offerModel.xs_ImageName != null
                ? new FadeInImage(
                    placeholder: new AssetImage("images/default1.png"),
                    image: NetworkImage(widget.offerModel.xs_ImageName),
                    fadeInDuration: Duration(milliseconds: 100),
                    width: 60.0,
                    height: 60.0,
                  )
                : new Image.asset(
                    "images/default1.png",
                    width: 60.0,
                    height: 60.0,
                  ),
            Padding(padding: EdgeInsets.only(top: 5.0)),
            new Text(
              widget.offerModel.offerTitle,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline,
            ),
            Padding(padding: EdgeInsets.only(top: 28.0)),
            new Text(
              widget.offerModel.offerContent,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.body1,
            ),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            new MaterialButton(
              onPressed: () {
                try {
                  insetData();
                } catch (e) {
                  print(e);
                  setState(() {
                    isError = true;
                    isLoading = false;
                  });
                }
              },
              child: new Text("CONFIRM"),
              textColor: Colors.white,
              color: Colors.myColor,
            ),
            Padding(padding: EdgeInsets.only(top: 10.0)),
            new FlatButton(
              onPressed: () {
                Navigator.pop(context, [0]);
              },
              child: new Text("CANCLE"),
            )
          ],
        ),
      ),
    );
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text("Please wait")
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);

    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  void insetData() {
    if (internetConnection) {
      _progressDialog(true);
      int errorCode;

      Map<String, dynamic> data = {
        'lstOfferRedeemTran': [
          {
            'SessionId': sessionId,
            'linktoOfferMasterId': widget.offerModel.offerMasterId,
            'linktoCustomerMasterId': CustomerMasterId,
            'linktoUserMasterIdRedeemedBy': 0,
            'linktoSourceMasterId': Constants.SourceMasterId,
            'linktoBusinessMasterId': widget.store.businessMasterId,
            'linktoCustomerMembershipTranId': customerMembershipTranId,
          }
        ]
      };

      var encoder = JSON.encode(data);
      var url = '${Constants.url}InsertOfferRedeemTran';

      Future<int> result = InsertOfferRedeemParser.insertOffer(url, encoder);
      result.then((c) {
        errorCode = c;
        onSuccess(errorCode);
      }).catchError((onError) {
        print("UpdateCustomerMasterCity Error :- ${onError.toString()}");
      });
    } else {
      Fluttertoast.showToast(msg: Strings.noInternetToast);
    }
  }

  void onSuccess(int errorCode) {
    if (errorCode == 0) {
      _progressDialog(false);
      setState(() {
        displaySuccess = true;
      });
    } else {
      Fluttertoast.showToast(msg: Strings.emptyScreenTitle);
    }
  }

  Widget success() {
    return new Container(
      margin: EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 0.0),
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              Strings.congratulation,
              textAlign: TextAlign.center,
              style: Theme
                  .of(context)
                  .textTheme
                  .display1
                  .apply(color: Colors.green),
            ),
            Padding(padding: EdgeInsets.only(top: 5.0)),
            new Icon(
              Icons.check_circle_outline,
              color: Colors.myColor,
              size: 65.0,
            ),
            Padding(padding: EdgeInsets.fromLTRB(28.0, 28.0, 28.0, 0.0)),
            new Text(
              Strings.offerSuccessMessage,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.title,
            ),
            Padding(padding: EdgeInsets.only(top: 28.0)),
            new MaterialButton(
              onPressed: () {
//                var route = new MaterialPageRoute(builder: (BuildContext context) => new OfferDescriptionScreen(isFromStore: false,offerId:widget.offerModel.offerMasterId,businessMasterId:widget.store.businessMasterId,businessName:widget.store.businessName));
                Navigator.pop(context, [1]);
              },
              child: new Text("OK"),
              textColor: Colors.white,
              color: Colors.myColor,
            ),
          ],
        ),
      ),
    );
  }
}
