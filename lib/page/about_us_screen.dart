import 'dart:async';

import 'package:crazyrex/page/privacy_policy_screen.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutUsScreen extends StatefulWidget {
  @override
  _AboutUsScreenState createState() => _AboutUsScreenState();
}

class _AboutUsScreenState extends State<AboutUsScreen> {
  String strDescription = "";

  @override
  Widget build(BuildContext context) {
        return _build();
  }

    Widget _build() {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(Strings.labelAboutUs),
        ),
        body: new Container(
          margin: EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
          child: new ListView(
            children: <Widget>[
              new Center(
                child: new Container(
                  margin: EdgeInsets.only(top: 18.0),
                  child: new Image.asset(
                    "images/logo_with_name.png",
                    height: 65.0,
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 15.0)),
              Text(Strings.contentText, style: Theme.of(context).textTheme.body1,),
              new Padding(padding: EdgeInsets.only(top: 15.0)),
              new Text(
                Strings.txtSupport,
                style: Theme.of(context).textTheme.body1,
              ),
              new Padding(padding: EdgeInsets.only(top: 3.0)),
              new Text(
                Strings.txtSupportEmail,
                style: Theme
                    .of(context)
                    .textTheme
                    .body1
                    .apply(color: Colors.myColor),
              ),
              Padding(padding: EdgeInsets.only(top: 35.0)),
              new InkWell(
                onTap: (){_launchInWebViewOrVC(Constants.FACEBOOK);},
                child: new Row(
                  children: <Widget>[
                    new Image.asset(
                      "images/facebook.png",
                      height: 20.0,
                      width: 20.0,
                    ),
                    new Padding(padding: EdgeInsets.only(left: 5.0)),
                    new Text(
                      Strings.txtFacebook,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .apply(color: Colors.myColor),
                    )
                  ],
                ),
              ),
              new Padding(padding: EdgeInsets.only(top: 10.0)),
              new InkWell(
                onTap: (){_launchInWebViewOrVC(Constants.INSTAGRAM);},
                child: new Row(
                  children: <Widget>[
                    new Image.asset(
                      "images/instagram.png",
                      height: 20.0,
                      width: 20.0,
                    ),
                    new Padding(padding: EdgeInsets.only(left: 5.0)),
                    new Text(
                      Strings.txtInstagram,
                      style: Theme
                          .of(context)
                          .textTheme
                          .body1
                          .apply(color: Colors.myColor),
                    )
                  ],
                ),
              ),
              new Padding(padding: EdgeInsets.only(top: 25.0)),
              new Card(
                margin: EdgeInsets.fromLTRB(0.0, 8.0, 4.0, 4.0),
                elevation: 2.0,
                child: new InkWell(
                  onTap: (){onClick(1);},
                  child: new Container(
                    margin: EdgeInsets.all(20.0),
                    child: new Text(
                      "FAQ",
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.myColor),
                    ),
                  ),
                )
              ),
              new Card(
                margin: EdgeInsets.fromLTRB(0.0, 4.0, 4.0, 4.0),
                elevation: 2.0,
                child: new InkWell(
                  onTap: (){onClick(2);},
                  child: new Container(
                    margin: EdgeInsets.all(20.0),
                    child: new Text(
                      "Privacy Policy",
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.myColor),
                    ),
                  ),
                )
              ),
              new Card(
                margin: EdgeInsets.fromLTRB(0.0, 4.0, 4.0, 8.0),
                elevation: 2.0,
                child: new InkWell(
                  onTap: (){onClick(3);},
                  child: new Container(
                    margin: EdgeInsets.all(20.0),
                    child: new Text(
                      "Terms & Conditions",
                      style: Theme
                          .of(context)
                          .textTheme
                          .body2
                          .apply(color: Colors.myColor),
                    ),
                  ),
                )
              ),
            ],
          ),
        ));
  }

  onClick(int type){
    if(type == 1){
      var route = new MaterialPageRoute(builder: (BuildContext context) =>new PrivacyPolicyScreen(Constants.FAQ,"FAQ"));
      _navigateAndReturn(context, route, 0);
    }else if(type == 2){
      var route = new MaterialPageRoute(builder: (BuildContext context) =>new PrivacyPolicyScreen(Constants.PRIVACY_URL,"Privacy Policy"));
      _navigateAndReturn(context, route, 0);
    }else if(type == 3){
      var route = new MaterialPageRoute(builder: (BuildContext context) =>new PrivacyPolicyScreen(Constants.TERMS_URL,"Terms And Condition"));
      _navigateAndReturn(context, route, 0);
    }
  }

  Future<Null> _launchInWebViewOrVC(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: true);
    } else {
      throw 'Could not launch $url';
    }
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    final result = await Navigator.push(context, rout);
  }

}
