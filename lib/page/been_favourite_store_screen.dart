import 'dart:async';
import 'dart:convert';

import 'package:crazyrex/model/been_fav_store_model.dart';
import 'package:crazyrex/parser/been_fav_store_parser.dart';
import 'package:crazyrex/parser/delete_been_fav_store_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class MyBeenFavStoreScreen extends StatefulWidget {
  final bool isFav;
  final bool isBeen;

  MyBeenFavStoreScreen(this.isFav, this.isBeen);

  @override
  _MyBeenFavStoreScreenState createState() => _MyBeenFavStoreScreenState();
}

class _MyBeenFavStoreScreenState extends State<MyBeenFavStoreScreen> implements BeenFavStoreDataListener{

  final GlobalKey<ScaffoldState> scaffoldStateBeenFavStore = new GlobalKey<ScaffoldState>();
  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  bool isError=false;
  bool isLoading=false;
  bool isFav=false;
  bool isBeen=false;

  SetUpBeenFavStoreListenerData setUpBeenFavStoreListenerData;
  String prefCustomerId;

  List<BeenFavStoreModel> beenStoreList= [];
  List<BeenFavStoreModel> favStoreList =[];

  _MyBeenFavStoreScreenState(){
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((String value){
      prefCustomerId = value;
    });
  }

  @override
  void initState() {

    _internetConnection();
  }
  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        internetConnection = true;
        setState(() {
          if(widget.isFav){
            isFav = true;
          }else if(widget.isBeen){
            isBeen = true;
          }
          isLoading = true;
        });

      } else {
        internetConnection = false;
        setState(() {
        });

      }
    });
  }
//endregion

  @override
  void dispose() {
    subscription == null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(internetConnection != null && internetConnection){
      if(!isError){
        if(isLoading){
          _callApis();
        }
        return isLoading? new Scaffold(
          body: new Center(
            child: new CircularProgressIndicator(),
          ),
        ): isFav
            ?favStoreList.length==0
              ?_emptyScreen()
              :_build()
          :isBeen
            ?beenStoreList.length==0
              ?_emptyScreen()
              :_build()
          :new Scaffold();
      }else{
        return _error();
      }
    }else if(internetConnection != null && !internetConnection){
      return _noInternet();
    }else{
      return new Scaffold();
    }
  }

  Widget _build(){
    return new Scaffold(
      appBar: new AppBar(title: new Text(isFav?Strings.labelTitleFavStoreScreen:Strings.labelTitleBeenStoreScreen),),
      body: new ListView.builder(
          itemCount: isFav?favStoreList.length:beenStoreList.length,
          itemBuilder: ((BuildContext context,int index){
          BeenFavStoreModel model;

          if(isFav){
            model = favStoreList[index];
          }else{
            model = beenStoreList[index];
          }
              return new Dismissible(key: new ObjectKey(isFav?favStoreList[index]:beenStoreList[index]),
                  direction: DismissDirection.endToStart,

                  background: new Container(
                    color: Colors.red,

                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Container(
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Icon(Icons.delete,color: Colors.white,),
                            new Text(Strings.labelDelete,style: Theme.of(context).textTheme.caption.apply(color: Colors.white),)
                          ],
                        ),
                        padding: EdgeInsets.only(right: 12.0),
                        )

                      ],
                    ),
                  ),
                  onDismissed: (direction){
                      if(isFav){
                        favStoreList.removeAt(index);
                      }else{
                        beenStoreList.removeAt(index);
                      }
                      _delete(model.checkInMasterId);
                  }
                  ,child: new Container(
                padding: new EdgeInsets.fromLTRB(12.0,12.0,12.0,0.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Expanded(flex: 1,
                            child: new Container(
                              height: 50.0,
                              width: 50.0,
                              child: model.logoImageName!=null
                                  ?new Image.network(model.logoImageName)
                                  :new Image.asset("images/default1.png"),
                            )
                        ),
                        new Padding(padding: new EdgeInsets.only(left: 8.0)),
                        new Expanded(child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text(model.businessName,style: Theme.of(context).textTheme.body2,maxLines: 1,),
                            new Padding(padding: new EdgeInsets.only(top: 2.0)),
                            new Text(model.address,style: Theme.of(context).textTheme.caption,),
                          ],
                        ),flex: 4,),
                      ],
                    ),
                    new Padding(padding: new EdgeInsets.only(top: 8.0)),
                    new Container(
                      child: new Container(foregroundDecoration: new ViewLineFromRegistration(),),

                    )
                  ],
                ),
              ));
            }
    )));
  }
  Widget _emptyScreen(){
    return new Scaffold(
      appBar: new AppBar(title: new Text(isFav?Strings.labelTitleFavStoreScreen:Strings.labelTitleBeenStoreScreen),),
      body: new Stack(
        children: <Widget>[
          new Center(
            child:  new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(isFav?Icons.favorite:Icons.beenhere,color: Colors.myColor,size: 65.0,),
                  new Padding(padding: new EdgeInsets.only(top: 20.0)),
                  new Text(isFav?Strings.emptyFavouriteScreen:Strings.emptyBeenScreen,style: Theme.of(context).textTheme.title.apply(color: Colors.myColor),)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(padding: EdgeInsets.only(top: 16.0),),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(padding: EdgeInsets.only(top: 27.0),),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }
  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
          ],
        ));
  }

  _callApis() {
    setUpBeenFavStoreListenerData = new SetUpBeenFavStoreListenerData("${Constants.url}SelectAllCheckinMaster/null/$prefCustomerId",this);
    setUpBeenFavStoreListenerData.loadBeenFavStoreData();
  }

  @override
  void onBeenFavStoreListener(List<BeenFavStoreModel> beenFavStoreList) {
    if(isFav){
      if(beenFavStoreList.length != 0){
        for(int i = 0;i<beenFavStoreList.length;i++) {
          if (beenFavStoreList[i].type == "2") {
            BeenFavStoreModel model = new BeenFavStoreModel(
                beenFavStoreList[i].address,
                beenFavStoreList[i].businessName,
                beenFavStoreList[i].checkInMasterId,
                beenFavStoreList[i].logoImageName,
                beenFavStoreList[i].type);
            favStoreList.add(model);
          }
        }
      }
    }else if(isBeen){
      if(beenFavStoreList.length != 0){
        for(int i = 0;i<beenFavStoreList.length;i++) {
          if (beenFavStoreList[i].type == "1") {
            BeenFavStoreModel model = new BeenFavStoreModel(
                beenFavStoreList[i].address,
                beenFavStoreList[i].businessName,
                beenFavStoreList[i].checkInMasterId,
                beenFavStoreList[i].logoImageName,
                beenFavStoreList[i].type);
            beenStoreList.add(model);
          }
        }
      }
    }

    setState(() {
      isError = false;
      isLoading = false;
    });
  }

  @override
  void onErrorBeenFavStore(onError) {
    print("error in been & fav in been fav store screen");
    setState(() {
      isError = true;
      isLoading = false;
    });
  }

  _delete(String checkInMasterId) {
    setState(() {
      int errorCode;
      Map<String,dynamic> body2 = {
        'CheckinMaster':{
          'CheckinMasterId':checkInMasterId
        }
      };
      var encoder = JSON.encode(body2);
      var url = '${Constants.url}DeleteCheckinMaster';

      DeleteCheckInParser checkInParser = new DeleteCheckInParser();
      Future<int> result = checkInParser.deleteCheckIn(url, encoder);
      result.then((c) {
        errorCode = c;
        _handleResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        setState(() {
        });
      });
    });
  }

  _handleResult(int errorCode) {
    setState(() {
    });
    if (errorCode == 0) {
      setState(() {

      });
    }else{
      setState(() {
        favStoreList.clear();
        Constants.showSnackBar(scaffoldStateBeenFavStore, Strings.errorInServices);
        isLoading = true;
      });
    }
  }

}
