import 'dart:async';

import 'package:crazyrex/model/notification_model.dart';
import 'package:crazyrex/page/notification_setting_screen.dart';
import 'package:crazyrex/page/offer_description_screen.dart';
import 'package:crazyrex/page/claim_card_list_screen.dart';
import 'package:crazyrex/page/store_detail_screen.dart';
import 'package:crazyrex/parser/notification_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:intl/intl.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen>with NotificationListListener {
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isLoading;
  bool isError = false;

  List<NotificationModel> notificationList = [];
  SetUpNotificationListener notificationParser;

  String customerMasterId;


  _NotificationScreenState() {
    PrefUtil
        .getStringPreference(Constants.prefCustomerMasterId)
        .then((String value) {
      customerMasterId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
  }

  @override
  void dispose() {
    subscription = null;
    super.dispose();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            //    _isLoading = true;
            setState(() {
              internetConnection = true;
              isLoading = true;
            });
          } else {
            setState(() {
              internetConnection = false;
            });
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          callApi();
        }
        return _build();
      } else {
        return _error();
      }
    } else {
      return _noInternet();
    }
  }

  Widget _build() {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(Strings.notificationScreenTitle),
          actions: <Widget>[
            new InkWell(
              onTap: () {
                var route = new MaterialPageRoute(
                    builder: (BuildContext context) =>
                        new NotificationSettingScreen());
                _navigateAndReturn(context, route, 1);
              },
              child: new Icon(Icons.settings),
            ),
            Padding(padding: EdgeInsets.only(right: 14.0)),
          ],
        ),
        body: isLoading
            ? Scaffold(
                body: new Center(
                  child: new CircularProgressIndicator(),
                ),
              )
            : notificationList.length != 0 ? listView() : _noData());
  }

  Widget listView() {
    return new Container(
        child: new ListView.builder(
            itemCount: notificationList.length,
            itemBuilder: (BuildContext context, int index) {
              final NotificationModel notification = notificationList[index];
              return new Card(elevation: 2.0,
                  margin: new EdgeInsets.fromLTRB(6.0, 6.0, 6.0,
                      index == notificationList.length - 1 ? 6.0 : 0.0),
                  child: new InkWell(
                    onTap: () {
                      _clickOnItem(notification);
                    },
                    child: new Container(
                      padding: EdgeInsets.all(8.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            notification.NotificationTitle,
                            style: Theme.of(context).textTheme.body2,
                          ),
                          Padding(padding: EdgeInsets.only(top: 5.0)),
                          new Text(
                            notification.NotificationText,
                            style: Theme.of(context).textTheme.caption,
                          ),
                          Padding(padding: EdgeInsets.only(top: 5.0)),
                          _date(notification),
                        ],
                      ),
                    ),
                  ));
            }));
  }

  Widget _date(NotificationModel model) {
    String date;
    DateFormat dateFormat = new DateFormat("yyyy-MM-dd");
    DateTime dateTime = dateFormat.parse(model.NotificationDateTime);
    date = new DateFormat("dd-MM-yyyy").format(dateTime);
    return new Text(date,
        textAlign: TextAlign.start, style: Theme.of(context).textTheme.caption);
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {
                      _internetConnection();
                    });
                  })
            ],
          ),
        ),
      ],
    ));
  }

  Widget _noData() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.notifications,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyNotificationList,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme
                      .of(context)
                      .textTheme
                      .title
                      .apply(color: Colors.myColor)),
            ],
          ),
        ),
      ],
    ));
  }

  callApi() {
    String url = "${Constants
        .url}SelectAllNotificationTran/$customerMasterId/2";
    print(url);
    notificationParser = new SetUpNotificationListener(this, url);
    notificationParser.loadNotificationList();
  }

  @override
  void onErrorNotificationList(String error) {
    print(error);
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onGetNotificationList(List<NotificationModel> list) {
    setState(() {
      notificationList = list;
      isLoading = false;
    });
  }

  void _clickOnItem(NotificationModel notification) {
    /*AddBusiness = 1,
                Offer = 2,
                CardRenewal = 3,
                CardActivation = 4,
                CardExpiry = 5,*/
    int type = int.parse(notification.NotificationType);
    if (type == 1) {
      var route = new MaterialPageRoute(builder: (BuildContext context) => new StoreDetailScreen(int.parse(notification.NotificationTypeId)));
      _navigateAndReturn(context, route, 0);
    } else if (type == 2) {
      var route = new MaterialPageRoute(builder: (BuildContext context) => new OfferDescriptionScreen(isFromStore: false,offerId: int.parse(notification.NotificationTypeId),businessMasterId: int.parse(notification.linktoBusinessMasterId),businessName: notification.Business,));
      _navigateAndReturn(context, route, 0);
    } else if (type == 3) {
      var route = new MaterialPageRoute(builder: (BuildContext context) => new ClaimCardListScreen());
      _navigateAndReturn(context, route, 0);
    } else if (type == 4) {
      var route = new MaterialPageRoute(builder: (BuildContext context) => new ClaimCardListScreen());
      _navigateAndReturn(context, route, 0);
    } else if (type == 5) {
      var route = new MaterialPageRoute(builder: (BuildContext context) => new ClaimCardListScreen());
      _navigateAndReturn(context, route, 0);
    }
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);
      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }
}
