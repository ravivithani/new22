import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:crazyrex/model/area_model.dart';
import 'package:crazyrex/model/category_model.dart';
import 'package:crazyrex/page/filter_screen.dart';
import 'package:crazyrex/page/search_screen.dart';
import 'package:crazyrex/page/sort_store_screen.dart';
import 'package:crazyrex/page/tab_store_list_by_category.dart';
import 'package:crazyrex/parser/all_category_parser.dart';
import 'package:crazyrex/parser/select_all_area_master.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:location/location.dart';

class AllStoreTabScreen extends StatefulWidget {
  bool isRecentlyJoined;
  bool isFromCategory;
  int catId;

  @override
  _AllStoreTabScreenState createState() =>
      _AllStoreTabScreenState(isRecentlyJoined);

  AllStoreTabScreen(this.isRecentlyJoined, this.isFromCategory, this.catId);
}

class _AllStoreTabScreenState extends State<AllStoreTabScreen>
    with
        SingleTickerProviderStateMixin,
        AllCategoryDataListener,
        SelectAllAreaListener {
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isError = false;
  bool isLoading = false;

  bool isRecentlyJoined;
  List<AreaModel> areaList;
  List<AreaModel> selectedAreaList = new List();
  SetUpAllCategoryListenerData setUpAllCategoryListenerData;

  SetAllAreaListenerData setAllAreaListenerData;

  List<CategoryModel> categoryModelList = new List();

  TabController tabController;

  Map<String, double> _startLocation;
  Map<String, double> _currentLocation;
  StreamSubscription<Map<String, double>> _locationSubscription;
  Location _location = new Location();
  double latitude, longitude;

  final GlobalKey<ScaffoldState> scaffoldStateStoreTab =
      new GlobalKey<ScaffoldState>();

  int cityId;
  int position;

  String areaMasterIds = "";
  String sortType = "nearest";

  _AllStoreTabScreenState(this.isRecentlyJoined) {
    PrefUtil.getIntPreference(Constants.prefCityId).then((int value) {
      cityId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    try {
      initPlatformState();
    } catch (e) {
      print(e);
    }

    _locationSubscription =
        _location.onLocationChanged.listen((Map<String, double> result) {
      _currentLocation = result;
      if (latitude == null || longitude == null) {
        if (_startLocation != null && _startLocation.isNotEmpty) {
          latitude = _startLocation['latitude'];
          longitude = _startLocation['longitude'];
        }
      }
    });

    _internetConnection();
  }

  initPlatformState() async {
    Map<String, double> location;
    // Platform messages may fail, so we use a try/catch PlatformException.

    try {
      location = await _location.getLocation;
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        Fluttertoast.showToast(
            msg: Strings.appNeedLocationPermission,
            toastLength: Toast.LENGTH_LONG);
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        Fluttertoast.showToast(
            msg: Strings.appNeedLocationPermission,
            toastLength: Toast.LENGTH_LONG);
      }
      location = null;
    }

    _startLocation = location;
    if (latitude == null || longitude == null) {
      latitude = _startLocation['latitude'];
      longitude = _startLocation['longitude'];
    }
  }

  @override
  void dispose() {
    if (subscription != null) {
      subscription == null;
    }
    if (tabController != null) {
      tabController.dispose();
    }
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        internetConnection = true;
        setState(() {
          isLoading = true;
        });
      } else {
        internetConnection = false;
        isLoading = false;
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return _build();
  }

  Widget _build() {
    //region build
    if (categoryModelList.length != 0) {
      if (position == null) {
        if (isRecentlyJoined) {
          position = categoryModelList.length - 1;
        } else if (widget.isFromCategory) {
          for (int i = 0; i < categoryModelList.length; i++) {
            if (widget.catId == categoryModelList[i].businessTypeMasterId) {
              position = i;
            }
          }
        }
        if (position != null) {
          tabController.animateTo(position);
        }
      }
    }
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          _callApis();
        }
        return isLoading
            ? new Scaffold(
                body: new Center(
                child: new CircularProgressIndicator(),
              ))
            : new Scaffold(
                appBar: new AppBar(
                    title: new Text("Stores"),
                    actions: <Widget>[
                      new InkWell(
                        onTap: () {
//                        shortDialog();
                          var route = new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new ShortStoreListScreen(sortType));
                          _navigateAndReturn(context, route, 2);
                        },
                        child: new Container(
                            margin: EdgeInsets.all(10.0),
                            child: new Image.asset(
                              "images/ic_sort.png",
                              height: 30.0,
                              width: 30.0,
                              color: Colors.black87,
                            )),
                      ),
                      new InkWell(
                        onTap: () {
                          var route = new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new FilterScreen(areaList, selectedAreaList));
                          _navigateAndReturn(context, route, 1);
                        },
                        child: new Container(
                            margin: EdgeInsets.all(10.0),
                            child: new Image.asset(
                              selectedAreaList.length != 0
                                  ? "images/ic_filter_filled.png"
                                  : "images/ic_filter.png",
                              height: 30.0,
                              width: 30.0,
                              color: Colors.black87,
                            )),
                      ),
                      new InkWell(
                        onTap: () {
//                        shortDialog();
                          subscription == null;
                          var route = new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new SearchScreen());
                          _navigateAndReturn(context, route, 3);
                        },
                        child: new Container(
                          margin: EdgeInsets.all(10.0),
                          child: new Icon(
                            Icons.search,
                            size: 30.0,
                            color: Colors.black54,
                          ),
                        ),
                      )
                    ],
                    bottom: _tabBar()),
                body: _tabView(),
//                body: new Container(),
              );
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return new Scaffold();
    }
    //endregion
  }

  Widget _tabBar() {
    if (tabController == null) {
      tabController =
          new TabController(length: categoryModelList.length, vsync: this);
    }
    return new TabBar(
      controller: tabController,
      isScrollable: true,
      labelColor: Colors.myColor,
      unselectedLabelColor: Colors.black54,
      labelStyle: Theme.of(context).textTheme.body1,
      unselectedLabelStyle: Theme.of(context).textTheme.body1,
      tabs: categoryModelList.map((CategoryModel model) {
        return new Tab(
          text: model.businessType.toUpperCase(),
        );
      }).toList(),
    );
  }

  Widget _tabView() {
    return new TabBarView(
      controller: tabController,
      children: categoryModelList.map((CategoryModel model) {
        return new StoreListByCategory(
            model.businessType,
            model.businessTypeMasterId,
            latitude,
            longitude,
            areaMasterIds,
            sortType);
      }).toList(),
    );
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {});
                  })
            ],
          ),
        ),
      ],
    ));
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  _callApis() {
    print("current location ${_currentLocation}");

    String url = "${Constants.url}SelectAllBusinessTypeMaster";

    String urlAreaList = "${Constants.url}SelectAllAreaMaster/$cityId";

    setUpAllCategoryListenerData = new SetUpAllCategoryListenerData(url, this);
    setUpAllCategoryListenerData.loadAllCategoryListData();

    setAllAreaListenerData = new SetAllAreaListenerData(urlAreaList, this);
    setAllAreaListenerData.loadAllStoreListData();
  }

  @override
  void onCategoryListener(List<CategoryModel> categoryList) {
    setState(() {
      categoryModelList = categoryList;
      CategoryModel model = new CategoryModel(
          "Recently Joined", null, Constants.recentlyJoinedId);
      categoryModelList.add(model);
      if (tabController == null) {
        tabController =
            new TabController(length: categoryModelList.length, vsync: this);
      }
    });
  }

  @override
  void onAreaListener(List<AreaModel> List) {
    setState(() {
      isLoading = false;
      areaList = List;
    });
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    // 1 for filter screen
    //2 for sort screen
    //3 for search
    position = tabController.index;
    subscription == null;
//    tabController.dispose();
    final result = await Navigator.push(context, rout);
    onResume();
    switch (navigateTo) {
      case 1:
        setState(() {
          tabController.animateTo(position);
          List<Object> result1 = result;
          isLoading = true;
          areaMasterIds = result1[0];
          selectedAreaList = result1[1];
        });
        break;
      case 2:
        setState(() {
          tabController.animateTo(position);
          isLoading = true;
          if (result != null) {
            sortType = result;
          }
        });
        break;
    }
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

  @override
  void onErrorCategoryListener(String error) {
    print(error);
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onErrorSelectAllAreaListener(String error) {
    setState(() {
      isLoading = false;
      isError = true;
    });
  }
}
