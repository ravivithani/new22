import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:crazyrex/model/exist_email_or_phone_model.dart';
import 'package:crazyrex/model/otp_model.dart';
import 'package:crazyrex/parser/exist_email_parser.dart';
import 'package:crazyrex/parser/exist_phone_parser.dart';
import 'package:crazyrex/parser/otp_parser.dart';
import 'package:crazyrex/parser/update_account_parser.dart';
import 'package:date_format/date_format.dart';
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/page/one_time_password.dart';
import 'package:crazyrex/parser/profile_parser.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:image_picker/image_picker.dart';

class EditAccountScreen extends StatefulWidget {
  @override
  _EditAccountScreenState createState() => _EditAccountScreenState();
}

class _EditAccountScreenState extends State<EditAccountScreen> implements ProfileListener,ExistPhoneListener,ExistEmailListener,OneTimePasswordListener {
  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;

  SetUpProfileListener profileListener;

  bool isSelectPhoto = false;
  bool isUrlPhoto = false;
  File selectPhoto;
  String urlPhoto;

  bool isLoading = false;
  bool isError = false;

  UserModel userModel;
  UserModel profileModel;

  String _base64Image;
  String _imageName;

  String bDay;
  bool isEmail=false;
  bool isPhone=false;

  String _sessionId;

  UserModel profileModel1;

  String _values;
  //List<String> _values = new List<String>();

  SetUpExistEmailListener listenerExistEmail;
  SetUpExistPhoneListener listenerExistPhone;
  SetUpOneTimePasswordListener listenerOneTimePassword;
  ExistEmailOrPhoneModel existPhoneModel;
  ExistEmailOrPhoneModel existEmailModel;
  OneTimePasswordModel oneTimePasswordModel;

  final GlobalKey<FormState> _formEditAccountKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldStateEditAccount = new GlobalKey<ScaffoldState>();

  final TextEditingController _firstNameController = new TextEditingController();
  final TextEditingController _lastNameController = new TextEditingController();
  final TextEditingController _phoneNumberController = new TextEditingController();
  final TextEditingController _emailAddressController = new TextEditingController();
  final TextEditingController _birthdayController = new TextEditingController();
  String _selectGender;
  String _male = "Male";
  String _female = "Female";

  FocusNode textFirstFocusNode = new FocusNode();
  FocusNode textSecondFocusNode = new FocusNode();
  FocusNode textThirdFocusNode = new FocusNode();
  FocusNode textForthFocusNode = new FocusNode();

  var _iconEmail;
  var _iconPhone;

  var _iconVerify = new Image.asset("images/Verify.png",color: Colors.red,height: 80.0,width: 80.0);
  var _iconVerified = new Image.asset("images/Verified.png",color: Colors.green,height: 80.0,width: 80.0,);

  DateTime _date = new DateTime.now();
  int groupValue;

  @override
  void initState() {
    super.initState();
    _internetConnection();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        internetConnection = true;
        setState(() {
          isLoading = true;
        });
      } else {
        internetConnection = false;
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    subscription == null;
    super.dispose();
  }

  _EditAccountScreenState() {
    PrefUtil.getIntPreference(Constants.prefRegisterUserMasterId).then((int value) {
      int userId = value;
      print(userId);
      profileListener = new SetUpProfileListener(this, "${Constants.url}SelectRegisteredUserMaster/$userId");
    });
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value) {
      _sessionId = value;
    });
  }

  Future<bool> _back() {
    bool isChange = false;
    print(userModel.toString());
    if(_firstNameController.text != userModel.firstName){
      isChange = true;
    }if(_lastNameController.text != userModel.lastName){
      isChange = true;
    }if(_phoneNumberController.text != userModel.phone){
      isChange = true;
    }if(_emailAddressController.text != userModel.email){
      isChange = true;
    }if(_selectGender != userModel.gender){
      isChange = true;
    }if(bDay != userModel.birthDate){
      isChange = true;
    }if(isSelectPhoto){
      isChange = true;
    }
    if(!isChange){
      Navigator.of(context).pop(true);
      subscription == null;
      return null;
    } else{
      if(_phoneNumberController.text != userModel.phone) {
        return _showBackAlertDialog(true);
      }else{
        return _showBackAlertDialog(false);
      }
    }
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  _showBackAlertDialog(bool phone) {
    showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            content: new Text(Strings.labelAlertTitleOfEditProfile),
            actions: <Widget>[
              new FlatButton(child: new Text(Strings.labelButtonDiscard),
                onPressed: () {
                  subscription == null;
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
              ),
              new FlatButton(child: new Text(Strings.labelButtonSave),
                onPressed: () {
                  Navigator.of(context).pop(true);
                  if(phone) {
                    _showVerifyPhoneAlertDialog();
                  }else{
                    _formEditAccountKey.currentState.validate();
                  }
                },
              ),
            ],
          ),
    ) ?? false;
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: _build(),
        onWillPop: _back
    );
  }

  Widget _build(){
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          profileListener.loadProfile();
        }
        return isLoading ? new Scaffold(
            body: new Center(
              child: new CircularProgressIndicator(),
            ))
            : new Scaffold(
              key: scaffoldStateEditAccount,
              appBar: new AppBar(
                title: new Text(Strings.labelTitleEditAccountScreen),
              ),
              body: _editProfile());

      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return new Scaffold();
    }
  }

  Widget _editProfile() {
    return ListView(
      children: <Widget>[
        new Stack(
          children: <Widget>[
            new Container(
              child: new Column(
                children: <Widget>[
                  new Padding(padding: new EdgeInsets.only(top: 20.0)),
                  new InkWell(
                    onTap: () {
                      _showAddPhotoAlertDialog();
                    },
                    child: isSelectPhoto
                        ? new Container(
                        width: 90.0,
                        height: 90.0,
                        decoration: new BoxDecoration(
                          color: Colors.transparent,
                          shape: BoxShape.circle,
                          image: new DecorationImage(image: new FileImage(selectPhoto),fit: BoxFit.cover),
                        )
                    )
                        : isUrlPhoto
                          ? userModel.profileImageName!=null
                              ? new Container(
                                width: 90.0,
                                height: 90.0,
                                decoration: new BoxDecoration(
                                  color: Colors.transparent,
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(image: new NetworkImage(userModel.profileImageName),fit: BoxFit.cover),
                        )
                    )
                              : new Icon(Icons.account_circle,size: 90.0,color: Colors.black54,)
                          : new Icon(Icons.account_circle,size: 90.0,color: Colors.black54,),
                  ),
                  new Padding(padding: new EdgeInsets.only(top: 2.0)),
                  new Text(Strings.labelEditPhoto),
                  new Container(
                    padding: new EdgeInsets.fromLTRB(20.0, 28.0, 20.0, 20.0),
                    child: new Form(
                        key: _formEditAccountKey,
                        child: new Theme(
                            data: new ThemeData(
                                primaryColor: Colors.myColor,
                                accentColor: Colors.myColor,
                                textSelectionColor: Colors.myColor),
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                new Row(
                                  children: <Widget>[
                                    new Expanded(
                                      child: new Container(
                                        padding:
                                            const EdgeInsets.only(right: 8.0),
                                        child: new TextFormField(
                                          controller: _firstNameController,
                                          keyboardType: TextInputType.text,
                                          onFieldSubmitted: (String value) {
                                            FocusScope.of(context).requestFocus(
                                                textFirstFocusNode);
                                          },
                                          maxLines: 1,
                                          validator: (value) {
                                            if (_firstNameController
                                                .text.isEmpty) {
                                              return Strings.emptyFirstName;
                                            }
                                          },
                                          decoration: new InputDecoration(
                                              labelText: Strings.hintFirstName),
                                        ),
                                      ),
                                      flex: 1,
                                    ),
                                    new Expanded(
                                      child: new Container(
                                        padding: const EdgeInsets.only(left: 8.0),
                                        child: new TextFormField(
                                          maxLines: 1,
                                          focusNode: textFirstFocusNode,
                                          keyboardType: TextInputType.text,
                                          onFieldSubmitted: (String value) {
                                            FocusScope.of(context).requestFocus(
                                                textSecondFocusNode);
                                          },
                                          controller: _lastNameController,
                                          validator: (value) {
                                            if (_firstNameController
                                                .text.isNotEmpty) {
                                              if (_lastNameController
                                                  .text.isEmpty) {
                                                return Strings.emptyLastName;
                                              }
                                            }
                                          },
                                          decoration: new InputDecoration(
                                              labelText: Strings.hintLastName),
                                        ),
                                      ),
                                      flex: 1,
                                    )
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.start,
                                ),
                                new Padding(
                                    padding: new EdgeInsets.only(top: 3.0)),
                                new TextFormField(
                                  controller: _phoneNumberController,
                                  focusNode: textSecondFocusNode,
                                  keyboardType: TextInputType.phone,
                                  onFieldSubmitted: (String value) {
                                    FocusScope
                                        .of(context)
                                        .requestFocus(textThirdFocusNode);
                                  },
                                  maxLines: 1,
                                  validator: (value) {
                                    if (_firstNameController.text.isNotEmpty) {
                                      if (_lastNameController.text.isNotEmpty) {
                                        if (_phoneNumberController.text.isNotEmpty) {
                                          if (Constants.isValidPhone(_phoneNumberController.text)) {
                                            if (_phoneNumberController.text == userModel.phone) {
                                              if (_emailAddressController.text.isEmpty) {
                                                if (internetConnection != null && internetConnection) {
                                                  _progressDialog(true);
                                                  _updateProfile();
                                                } else {
                                                  Constants.showSnackBar(scaffoldStateEditAccount, Strings.noInternetToast);
                                                }
                                              } else {
                                                if (Constants.isValidEmail(_emailAddressController.text)) {
                                                  if (internetConnection != null && internetConnection) {
                                                    _progressDialog(true);
                                                    _updateProfile();
                                                  } else {
                                                    Constants.showSnackBar(scaffoldStateEditAccount, Strings.noInternetToast);
                                                  }
                                                }
                                              }
                                            } else {
                                              _showVerifyPhoneAlertDialog();
                                            }
                                          } else {
                                            return Strings.invalidPhone;
                                          }
                                        } else {
                                          return Strings.emptyCantPhone;
                                        }
                                      }
                                    }
                                  },
                                    decoration: new InputDecoration(
                                      suffixIcon: _phoneNumberController.text.isEmpty
                                          ? null
                                          : _phoneNumberController.text == userModel.phone
                                          ? new IconButton(iconSize: 60.0,icon: _iconVerified, onPressed: null)
                                          : Constants.isValidPhone(_phoneNumberController.text)
                                          ? new IconButton(
                                        iconSize: 50.0,
                                          icon: _iconVerify, onPressed: () {
                                            _onPressedVerify(true, false);
                                          }
                                        )
                                          : null,
                                      labelText: Strings.hintPhoneNumber),
                                ),
                                new Padding(padding: new EdgeInsets.only(top: 3.0)),
                                new TextFormField(
                                  maxLines: 1,
                                  controller: _emailAddressController,
                                  focusNode: textThirdFocusNode,
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (value) {
                                    if (_firstNameController.text.isNotEmpty) {
                                      if (_lastNameController.text.isNotEmpty) {
                                        if (_phoneNumberController.text.isNotEmpty) {
                                          if (Constants.isValidPhone(_phoneNumberController.text)) {
                                            if (_emailAddressController.text.isNotEmpty) {
                                              if (!Constants.isValidEmail(_emailAddressController.text)) {
                                                return Strings.invalidEmail;
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  },
                                  onFieldSubmitted: (String value) {
                                    FocusScope.of(context).requestFocus(textForthFocusNode);
                                  },
                                  decoration: new InputDecoration(
                                      suffixIcon: _emailAddressController.text.isEmpty
                                          ? null
                                          : _emailAddressController.text == userModel.email
                                            ? userModel.isEmailVerify
                                                ? new IconButton(iconSize: 60.0,icon: _iconVerified, onPressed: null)
                                                : new IconButton(iconSize:50.0,icon: _iconVerify, onPressed: (){
                                                      _onPressedVerify(false, true);
                                                    })
                                            : Constants.isValidEmail(_emailAddressController.text)
                                              ? new IconButton(iconSize:50.0,icon: _iconVerify, onPressed: (){
                                                  _onPressedVerify(false, true);
                                                })
                                              : null,
                                      labelText: Strings.hintEmailAddress),
                                ),
                                new Padding(
                                    padding: new EdgeInsets.only(top: 3.0)),
                                new _InputDropdown(
                                  labelText: Strings.hintBirthday,
                                  valueText: _birthdayController.text,
                                  onPressed: () {
                                    _selectDate();
                                  },
                                ),

                                new Padding(padding: new EdgeInsets.only(top: 17.0)),
                                new Text(Strings.hintGender, style: Theme.of(context).textTheme.caption,
                                ),
                                new Row(
                                  children: <Widget>[
                                    new Expanded(
                                        child: RadioListTile<String>(
                                          title: Text(Strings.labelMale),
                                          groupValue: _selectGender,
                                          value: _male,
                                          onChanged: radioGender,
                                        )),
                                    new Expanded(
                                        child: RadioListTile<String>(
                                          title: Text(Strings.labelFemale),
                                          groupValue: _selectGender,
                                          value: _female,
                                          onChanged: radioGender,
                                        )),
                                  ],
                                ),
                                new Padding(
                                    padding: new EdgeInsets.only(top: 5.0)),
                                new MaterialButton(
                                    child: new Text(Strings.labelButtonSave),
                                    textColor: Colors.white,
                                    color: Colors.myColor,
                                    onPressed: () {
                                      _formEditAccountKey.currentState.validate();
                                    })
                              ],
                            ))),
                  ),

                ],
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {});
                  })
            ],
          ),
        ),
      ],
    ));
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  void radioGender(String gender) {
    setState(() {
      _selectGender = gender;
      print(_selectGender);
    });
  }

  Future<Null> _selectDate() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(1918),
        lastDate: new DateTime.now());

    if (picked != null) {
      print("Date Selected $_date");
      setState(() {
        _date = picked;

        String pick = formatDate(picked, [dd, '-', mm, '-', yyyy]);
        _birthdayController.text = pick;
        bDay = formatDate(picked, [yyyy, '-', mm, '-', dd]);

      });
    }
  }

  void _showAddPhotoAlertDialog() {
    showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text(Strings.labelAlertAddPhoto),
          content: new Container(
            height: 100.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    _picker(true, false);
                    Navigator.of(context).pop(true);
                  },
                  child: Text(Strings.labelAlertTakePhoto, textAlign: TextAlign.end),
                ),
                new Padding(padding: new EdgeInsets.only(top: 20.0)),
                InkWell(
                  onTap: () {
                    _picker(false, true);
                    Navigator.of(context).pop(true);
                  },
                  child: Text(Strings.labelAlertChooseGallery, textAlign: TextAlign.left),
                ),
                new Padding(padding: new EdgeInsets.only(top: 20.0)),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop(true);
                  },
                  child: Text(Strings.labelAlertCancelSmall, textAlign: TextAlign.left),
                ),
              ],
            ),
          ),
        ));
  }

  void _showVerifyPhoneAlertDialog() {
    showDialog(
        context: context,
        child: new AlertDialog(
          content: new Text(Strings.labelAlertTitleOfVerifyPhone),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  if(internetConnection!=null && internetConnection){

                    if(Constants.isValidPhone(_phoneNumberController.text)){
                      _progressDialog(true);
                      isPhone = true;
                      isEmail = false;

                      listenerExistPhone = new SetUpExistPhoneListener(this, '${Constants.url}SelectUserMasterByUserNameIsExistOrNot/${_phoneNumberController.text}/2');
                      listenerExistPhone.loadExistPhone();
                    }else{
                      isPhone = false;
                      isEmail = false;
                      _formEditAccountKey.currentState.validate();
                    }
                  }else if(internetConnection!=null && !internetConnection){
                    Constants.showSnackBar(scaffoldStateEditAccount, Strings.noInternetToast);
                  }
                },
                child: new Text(Strings.labelButtonText))
          ],
        ));
  }

  _picker(bool camera, bool gallery) async {
    if (camera) {
      selectPhoto = await ImagePicker.pickImage(source: ImageSource.camera);
    } else if (gallery) {
      selectPhoto = await ImagePicker.pickImage(source: ImageSource.gallery);
    }

    if (selectPhoto != null) {
      List<int> imageBytes = selectPhoto.readAsBytesSync();
      _base64Image = base64Encode(imageBytes);
      _imageName = "img-" +
          new DateTime.now().year.toString() +
          new DateTime.now().month.toString() +
          new DateTime.now().day.toString() +
          new DateTime.now().hour.toString() +
          new DateTime.now().minute.toString() +
          new DateTime.now().second.toString() +
          new DateTime.now().millisecond.toString();
      isSelectPhoto = true;
      setState(() {isSelectPhoto = true;});
    }
  }

  @override
  void onErrorProfile(onError) {
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onSuccessProfile(UserModel model) {
    if(profileModel1!=null){
      setState(() {
        userModel = model;
        profileModel = profileModel1;
        isLoading = false;
        isError = false;
        _setProfileData();
      });
    }else{
      setState(() {
        userModel = model;
        profileModel = model;
        isLoading = false;
        isError = false;
        _setProfileData();
      });
    }

  }

  void _setProfileData() {
    if (!profileModel.isSelectPhoto) {
      if (userModel.profileImageName != null && userModel.profileImageName !="null") {
        isUrlPhoto = true;
        urlPhoto = userModel.profileImageName;
      } else {
        isUrlPhoto = false;
      }
    } else {
      if(profileModel1!=null){
        if (profileModel.profileImageName != null) {
          isSelectPhoto = true;
          selectPhoto = new File(profileModel.profileImageName);
          List<int> imageBytes = selectPhoto.readAsBytesSync();
          _base64Image = base64Encode(imageBytes);
          _imageName = "img-" +
              new DateTime.now().year.toString() +
              new DateTime.now().month.toString() +
              new DateTime.now().day.toString() +
              new DateTime.now().hour.toString() +
              new DateTime.now().minute.toString() +
              new DateTime.now().second.toString() +
              new DateTime.now().millisecond.toString();
        }
      }
    }
    _firstNameController.text = profileModel.firstName;
    _lastNameController.text = profileModel.lastName;
    _phoneNumberController.text = profileModel.phone;
    if (profileModel.email != null && profileModel.email != "") {
      _emailAddressController.text = profileModel.email;
    }else{
      _emailAddressController.text = "";
    }
    if (profileModel.birthDate != null && profileModel.birthDate != "") {
      _birthdayController.text = profileModel.birthDate;
      _birthdayController.text = formatDate(DateTime.parse(profileModel.birthDate), [dd, '-', mm, '-', yyyy]);
      bDay = formatDate(DateTime.parse(profileModel.birthDate), [yyyy, '-', mm, '-', dd]);
    }else{
      bDay = "";
    }
    if (profileModel.gender != null && profileModel.gender != "") {
      if (profileModel.gender == "Female") {
        _selectGender = _female;
      } else {
        _selectGender = _male;
      }
    }else{
      _selectGender = "";
    }
    if (profileModel.email != "" && profileModel.email != null) {
      if (profileModel.isEmailVerify) {
        _iconEmail = _iconVerified;
      } else {
        _iconEmail = _iconVerify;
      }
    } else {
      _iconEmail = null;
    }
    if (profileModel.phone != null && profileModel.phone != "") {
      if (profileModel.isSMSVerify) {
        _iconPhone = _iconVerified;
      } else {
        _iconPhone = _iconVerify;
      }
    } else {
      _iconPhone = null;
    }
  }

  _updateProfile() {
    setState(() {
      int errorCode;

      Map<String, dynamic> body2;
      if (isSelectPhoto) {
        body2 = {
          'RegisteredUserMasterData': {
            'ProfileImage': _imageName,
            'ProfileImageImageNameBytes': _base64Image,
            'SessionId': _sessionId,
            'RegisteredUserMasterId': profileModel.registeredUserMasterId,
            'CustomerMasterId': profileModel.customerMasterId,
            'FirstName': _firstNameController.text,
            'LastName': _lastNameController.text,
            'BirthDate': _birthdayController.text.isNotEmpty ? bDay:null,
            'Gender': _selectGender.isNotEmpty? _selectGender:null,
            'Phone': _phoneNumberController.text,
            'Email': _emailAddressController.text.isNotEmpty? _emailAddressController.text: null,
            'IsEmailVerify': _emailAddressController.text.isNotEmpty? _emailAddressController.text == userModel.email ?userModel.isEmailVerify:false : false,
            'IsSMSVerify': true,
          }
        };
      } else {
        body2 = {
          'RegisteredUserMasterData': {
            'SessionId': _sessionId,
            'RegisteredUserMasterId': profileModel.registeredUserMasterId,
            'CustomerMasterId': profileModel.customerMasterId,
            'FirstName': _firstNameController.text,
            'LastName': _lastNameController.text,
            'BirthDate': _birthdayController.text.isNotEmpty ?bDay:null,
            'Gender': _selectGender.isNotEmpty? _selectGender:null,
            'Phone': _phoneNumberController.text,
            'Email': _emailAddressController.text.isNotEmpty? _emailAddressController.text: null,
            'IsEmailVerify': _emailAddressController.text.isNotEmpty? _emailAddressController.text == userModel.email ?userModel.isEmailVerify:false : false,
            'IsSMSVerify': true,
          },
        };
      }

      var encoder = JSON.encode(body2);
      var url = '${Constants.url}UpdateRegisteredUserMaster';

      UpdateAccountParser updateAccountParser = new UpdateAccountParser();
      Future<int> result = updateAccountParser.updateAccount(url, encoder);
      result.then((c) {
        errorCode = c;
        _handleResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        _progressDialog(false);
      });
    });
  }

  _handleResult(int errorCode) {
    _progressDialog(false);
    if (errorCode == 0) {
      Constants.isLoadingOnResume = true;
      Navigator.of(context).pop(true);
    } else if (errorCode == -2) {
      Constants.showSnackBar(scaffoldStateEditAccount, Strings.errorAccountAlreadyCreatedOnThisNumber);
    } else if (errorCode == -1) {
      Constants.showSnackBar(scaffoldStateEditAccount, Strings.errorInServices);
    }
  }

  _onPressedVerify(bool phone,bool email){
    if(internetConnection!=null && internetConnection) {
      _progressDialog(true);
      if (phone) {
        isPhone = true;
        isEmail = false;
        listenerExistPhone = new SetUpExistPhoneListener(this, '${Constants.url}SelectUserMasterByUserNameIsExistOrNot/${_phoneNumberController.text}/2');
        listenerExistPhone.loadExistPhone();
      } else if (email) {
        isPhone = false;
        isEmail = true;
        listenerExistEmail = new SetUpExistEmailListener(this, '${Constants.url}SelectUserMasterByUserNameIsExistOrNot/${_emailAddressController.text}/1');
        listenerExistEmail.loadExistEmail();
      }
    }else if(internetConnection!=null && !internetConnection){
      Constants.showSnackBar(scaffoldStateEditAccount, "No internet connection");
    }
  }

  @override
  void onErrorExistEmail() {
    print("Error Exist email in Edit Account Screen");
    _progressDialog(false);
  }

  @override
  void onErrorExistPhone() {

    print("Error Exist Phone in Edit Account Screen");
    _progressDialog(false);
  }

  @override
  void onSuccessExistEmail(ExistEmailOrPhoneModel model) {
    existEmailModel = model;
    if(!existEmailModel.selectUserMasterByUserNameIsExistOrNotResult){
      listenerOneTimePassword = new SetUpOneTimePasswordListener(this, '${Constants.url}SelectRegisterUserForgotPassword/${_emailAddressController.text}/Email');
      listenerOneTimePassword.loadOneTimePassword();
    }else{
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStateEditAccount, Strings.existEmail);
    }
  }

  @override
  void onSuccessExistPhone(ExistEmailOrPhoneModel model) {
    existPhoneModel = model;
    if(!existPhoneModel.selectUserMasterByUserNameIsExistOrNotResult){
      listenerOneTimePassword = new SetUpOneTimePasswordListener(this, '${Constants.url}SelectRegisterUserForgotPassword/${_phoneNumberController.text}/Phone');
      listenerOneTimePassword.loadOneTimePassword();
    }else{
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStateEditAccount, Strings.existPhone);
    }
  }

  @override
  void onErrorOneTimePassword(onError) {
    print("Error generate otp in Edit Account Screen -  $onError");
    _progressDialog(false);
  }

  @override
  void onSuccessOneTimePassword(OneTimePasswordModel model) {
    oneTimePasswordModel = model;
    _progressDialog(false);
    if(oneTimePasswordModel.selectRegisterUserForgotPasswordResult != null){
      Constants.OTP = oneTimePasswordModel.selectRegisterUserForgotPasswordResult;
//      Constants.showSnackBar(scaffoldStateEditAccount, Constants.OTP);
      if(isPhone){
        UserModel loginModel1 = new UserModel(null, bDay, profileModel.customerMasterId, null, _emailAddressController.text, null, _firstNameController.text, _lastNameController.text, null,_phoneNumberController.text, _selectGender, profileModel.isEmailVerify, false, profileModel.isSMSVerify, isSelectPhoto?selectPhoto.path:profileModel.profileImageName, null, profileModel.registeredUserMasterId, null, null, null, null,profileModel.sessionId,isSelectPhoto,isUrlPhoto);
        var route = new MaterialPageRoute(builder: (BuildContext context) => new OneTimePasswordScreen(phoneProfile:  loginModel1));
        _navigateAndReturn(context, route, 1);
        subscription == null;
      }else if(isEmail){
        UserModel loginModel1 = new UserModel(null, bDay, profileModel.customerMasterId, null, _emailAddressController.text, null, _firstNameController.text, _lastNameController.text, null,_phoneNumberController.text, _selectGender, profileModel.isEmailVerify, false, profileModel.isSMSVerify, isSelectPhoto?selectPhoto.path:profileModel.profileImageName, null, profileModel.registeredUserMasterId, null, null, null, null,profileModel.sessionId,isSelectPhoto,isUrlPhoto);
        var route = new MaterialPageRoute(builder: (BuildContext context) => new OneTimePasswordScreen(emailProfile:  loginModel1));
        _navigateAndReturn(context, route, 1);
        subscription == null;
      }
      subscription == null;
    }
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    // 1 for edit account
    subscription == null;
    final result = await Navigator.push(context, rout);
    List<Object> result1 = result;//UserModel
    onResume();
    switch(navigateTo){
      case 1:
        setState(() {
          isLoading = true;
          profileModel1 = result1[0];
        });
        break;
    }
  }

  onResume(){
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
          setState(() {
            internetConnection = true;
          });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

}

class _InputDropdown extends StatelessWidget {
  const _InputDropdown({
    Key key,
    this.child,
    this.labelText,
    this.valueText,
    this.valueStyle,
    this.onPressed }) : super(key: key);

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final VoidCallback onPressed;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: onPressed,
      child: new InputDecorator(
        decoration: new InputDecoration(
          labelText: labelText,
        ),
        baseStyle: valueStyle,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text(valueText, style: valueStyle),
            new Icon(Icons.arrow_drop_down,
                color: Theme
                    .of(context)
                    .brightness == Brightness.light
                    ? Colors.grey.shade700
                    : Colors.white70
            ),
          ],
        ),
      ),
    );
  }

}