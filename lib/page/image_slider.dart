import 'package:flutter/material.dart';
import 'package:crazyrex/widget/CarouselImageSlider.dart';

class ImageSlider extends StatefulWidget {
  @override
  _ImageSliderState createState() => _ImageSliderState();
}

class _ImageSliderState extends State<ImageSlider> implements HandleClickOnImage{
  var images = [
    new AssetImage("images/a.png"),
    new AssetImage("images/b.png"),
    new AssetImage("images/c.png"),
  ];

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Scaffold(
        backgroundColor: Colors.white,
//        body: ImageCarousel(),
        body: new Container(
            margin: new EdgeInsets.only(top: 22.0),
            padding: new EdgeInsets.all(5.0),
            height: 200.0,
            child: new ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: new SizedBox(
                child: Carousel(
                  boxFit: BoxFit.cover,
                  autoplay: true,
                  clickListener: this,
                  image: images,
                  indicatorBgPadding: 10.0,
                  dotSize: 5.0,
                  dotSpacing: 15.0,
                  dotBgColor: Colors.transparent,
                  animationCurve: Curves.fastOutSlowIn,
                  animationDuration: Duration(seconds: 2),
                ),
              ),
            )),
      ),
    );
  }

  @override
  void onClickItemOfSlider(int position) {
   print("taped image- ${images[position]}");
  }
}
