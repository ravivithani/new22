import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:crazyrex/page/claim_card_list_screen.dart';
import 'package:crazyrex/parser/claim_card_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:intl/intl.dart';
import 'package:qr_flutter/qr_flutter.dart';

class ClaimCardScreen extends StatefulWidget {

  @override
  _ClaimCardScreenState createState() => _ClaimCardScreenState();
}

class _ClaimCardScreenState extends State<ClaimCardScreen>
    implements ClaimCardDataListener {
  final GlobalKey<ScaffoldState> scaffoldStateClaimCardList =
      new GlobalKey<ScaffoldState>();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  bool isError = false;
  bool isLoading = false;

  SetUpClaimCardListenerData setUpClaimCardListenerData;
  String _customerId;

  String _profileImageName;
  String _phoneNumber;
  String _customerName;
  String _cardNumber;
  String cardNumber;
  String _cardExpiryDate;
  String _membershipTypeName;

  _ClaimCardScreenState() {
    PrefUtil
        .getStringPreference(Constants.prefCustomerMasterId)
        .then((String value) {
      _customerId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
          isLoading = true;
        });
      } else {
        internetConnection = false;
        setState(() {});
      }
    });
  }

  //endregion

  @override
  void dispose() {
    if (subscription != null) {
      subscription == null;
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          _callApi();
        }
        return isLoading
            ? new Scaffold(
                body: new Center(
                  child: new CircularProgressIndicator(),
                ),
              )
            : _build();
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return new Scaffold();
    }
  }

  Widget _build() {
    return new Scaffold(
        appBar: AppBar(
          title: new Text(Strings.labelClaimCardTitle),
        ),
        body: new ListView(
          padding: EdgeInsets.fromLTRB(35.0, 50.0, 35.0, 35.0),
          children: <Widget>[
            claimCard(),
            new Padding(padding: new EdgeInsets.only(top: 15.0)),
            new InkWell(
              child: new Text(
                Strings.labelTitleMembershipListScreen,
                style: Theme
                    .of(context)
                    .textTheme
                    .body2
                    .apply(color: Colors.myColor),
                textAlign: TextAlign.center,
              ),
              onTap: () {
                var route = new MaterialPageRoute(
                    builder: (BuildContext context) =>
                        new ClaimCardListScreen());
                _navigateAndReturn(context, route, 1);
              },
            )
          ],
        ),
      );
  }

  Widget claimCard() {
    double height = MediaQuery
        .of(context)
        .size
        .height / 1.5;
    double width = MediaQuery
        .of(context)
        .size
        .width / 1.3;
    print("$height - $width");
    return new Center(
      child: new Container(
        child: new Card(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(25.0)),
          child: new SizedBox(
              height: height,
              width: width,
              child: new Stack(
                children: <Widget>[
                  new Image.asset(
                    "images/bg33.png",
                    height: height,
                    fit: BoxFit.fitHeight,
                  ),
                  new Column(
                    children: <Widget>[
                      new Flexible(
                          flex: 3,
                          child: new Container(
                            color: Colors.black12,
                            padding: EdgeInsets.all(10.0),
                            child: new Column(
                              children: <Widget>[
                                new Flexible(
                                    flex: 1,
                                    child: new Container(
                                      child: new Image.asset(
                                        "images/logo_with_name.png",
//                                   height: 65.0,
                                      ),
                                    )
                                ),
                                new Flexible(
                                    flex: 2,
                                    child: new Container(
                                      child: new Row(
                                        children: <Widget>[
                                          new Flexible(
                                              flex: 3,
                                              child: new Container(
                                                child: new Column(
                                                  mainAxisAlignment: MainAxisAlignment
                                                      .center,
                                                  crossAxisAlignment: CrossAxisAlignment
                                                      .start,
                                                  children: <Widget>[
                                                    new Row(
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .start,
                                                      children: <Widget>[
                                                        new Icon(Icons.person,color: Colors.black54,),
                                                        new Padding(
                                                            padding: EdgeInsets.only(
                                                                left: 5.0)),
                                                        new Flexible(child: new Text(
                                                          _customerName,
                                                          textAlign: TextAlign.start,
                                                          softWrap: true,
                                                          maxLines: 1,
                                                          overflow: TextOverflow
                                                              .fade,),),
                                                      ],
                                                    ),
                                                    new Row(
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .start,
                                                      children: <Widget>[
                                                        new Icon(Icons.phone,color: Colors.black54),
                                                        new Padding(
                                                            padding: EdgeInsets.only(
                                                                left: 5.0)),
                                                        new Flexible(child: new Text(
                                                          _phoneNumber,
                                                          textAlign: TextAlign.start,
                                                          softWrap: true,
                                                          maxLines: 1,
                                                          overflow: TextOverflow
                                                              .fade,),)
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              )
                                          ),
                                          new Flexible(
                                              flex: 2,
                                              child: new Container(
                                                child: new Center(
                                                  child:
                                                    _profileImageName != null ? new Container(
                                                    width: height/4.5,
                                                    height: height/4.5,
                                                    decoration:
                                                    new BoxDecoration(
                                                      color: Colors.transparent,
                                                      shape: BoxShape.circle,
                                                      image: new DecorationImage(
                                                          image: new NetworkImage(
                                                              _profileImageName),
                                                          fit: BoxFit.cover),
                                                    )):new Icon(
                                                      Icons.account_circle,
                                                      color: Colors.black54,
                                                      size: height/4.5),
                                                ),
                                              )
                                          ),
                                        ],
                                      ),
                                    )
                                ),
                              ],
                            ),
                          )
                      ),
                      new Flexible(
                          flex: 3,
                          child: new Container(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text(
                                    _cardNumber,
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .headline
                                        .apply(color: Colors.myColor),
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                  ),
                                  new Padding(
                                      padding: EdgeInsets.only(top: height/35)),
                                  new QrImage(
                                    data: cardNumber,
                                    size: 100.0,
                                  ),
                                ],
                              )
                          )
                      ),
                      new Expanded(
                          flex: 1,
                          child: new Container(
                            padding: EdgeInsets.only(left: 20.0, right: 20.0),
                            color: Colors.black12,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Expanded(
                                    child: new Text(
                                      _membershipTypeName,
                                      style:
                                      Theme
                                          .of(context)
                                          .textTheme
                                          .body1,
                                      maxLines: 1,)),
                                new Text(
                                  "${Strings.labelValidUpto} $_cardExpiryDate",
                                  style:
                                  Theme
                                      .of(context)
                                      .textTheme
                                      .body1,
                                  maxLines: 1,)
                              ],
                            ),
                          )
                      ),
                    ],
                  ),
                ],
              )
          ),
        ),
      ),
    );
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {});
                  })
            ],
          ),
        ),
      ],
    ));
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  _callApi() {
    setUpClaimCardListenerData = SetUpClaimCardListenerData(
        "${Constants.url}SelectCustomerMembershipTran/$_customerId/1/1", this);
    setUpClaimCardListenerData.loadClaimCard();
  }

  @override
  void onClaimCardData(
      String cardExpiryDate,
      String cardNumber,
      String membershipTypeName,
      String customerName,
      String phoneNumber,
      String profileImageName) {
    this.cardNumber = cardNumber;
    _cardExpiryDate = new DateFormat("dd-MM-yyyy")
        .format(new DateFormat("yyyy-MM-dd").parse(cardExpiryDate));
    _membershipTypeName = membershipTypeName;
    _cardNumber = cardNumber;
    _customerName = customerName;
    _phoneNumber = phoneNumber;
    _profileImageName = profileImageName;

    String s1 = "", s2 = "", s3 = "";
    s1 = cardNumber.substring(0, 4);
    s2 = cardNumber.substring(4, 8);
    s3 = cardNumber.substring(8, 12);

    _cardNumber = s1 + "   " + s2 + "   " + s3;

    setState(() {
      isLoading = false;
      isError = false;
    });
  }

  @override
  void onErrorClaimCard(onError) {
    print("Error in Claim card screen   $onError");

    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
    switch (navigateTo) {
      case 0:
        setState(() {
          isLoading = false;
        });
        break;
      case 1:
        setState(() {
          isLoading = true;
        });
        break;
    }
  }

  onResume() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);
      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }
}
