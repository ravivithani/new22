import 'dart:async';
import 'dart:convert';

import 'package:crazyrex/parser/insert_suggest_store_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:fluttertoast/fluttertoast.dart';

class SuggestScreen extends StatefulWidget {
  @override
  _SuggestScreenState createState() => _SuggestScreenState();
}

class _SuggestScreenState extends State<SuggestScreen> {

  final GlobalKey<FormState> _formSuggestionKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldStateSuggestion = new GlobalKey<
      ScaffoldState>();

  FocusNode textZeroFocusNode = new FocusNode();
  FocusNode textFirstFocusNode = new FocusNode();

  var _storeNameController = new TextEditingController();
  var _storeAddressController = new TextEditingController();
  var _descriptionController = new TextEditingController();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection;


  String _sessionId;
  String _customerId;


  _SuggestScreenState() {
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((
        String value) {
      _customerId = value;
    });
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value) {
      _sessionId = value;
    });
  }

  @override
  void initState() {
    _internetConnection();
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            internetConnection = true;
          } else {
            internetConnection = false;
          }
        });
  }

  //endregion

  Future<bool> _back() {
    return showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            content: new Text(Strings.labelAlertTitleOfEditProfile),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                    Navigator.of(context).pop(false);
                    subscription == null;
                  },
                  child: new Text(Strings.labelButtonDiscard)),
              new FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                    _formSuggestionKey.currentState.validate();
                    subscription == null;
                  },
                  child: new Text(Strings.labelButtonSave)),
            ],
          ),
    ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: new Scaffold(
          key: scaffoldStateSuggestion,
          appBar: new AppBar(
            title: new Text(Strings.labelTitleSuggestStoreScreen),),
          resizeToAvoidBottomPadding: true,
          body: new Stack(
            fit: StackFit.expand,
            children: <Widget>[
              new Center(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    new Container(
                      padding: new EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 15.0),
                      child: new Column(

                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Icon(
                            Icons.store,
                            size: 65.0,
                            color: Colors.myColor,
                          ),
                          new Padding(padding: new EdgeInsets.only(top: 14.0)),
                          new Text(
                            Strings.labelDoYouHaveStoreSuggest, style: Theme
                              .of(context)
                              .textTheme
                              .body2, textAlign: TextAlign.center,),
                          new Padding(padding: new EdgeInsets.only(top: 2.0)),
                          new Text(Strings.labelRecommendUsWithInfo,
                            textAlign: TextAlign.center, style: Theme
                                .of(context)
                                .textTheme
                                .caption,),
                          new Form(
                              key: _formSuggestionKey,
                              child: new Theme(
                                  data: new ThemeData(
                                      primaryColor: Colors.myColor,
                                      accentColor: Colors.myColor,
                                      textSelectionColor: Colors.myColor),
                                  child: new Container(
                                      child: new Column(
                                        mainAxisAlignment: MainAxisAlignment
                                            .center,
                                        crossAxisAlignment: CrossAxisAlignment
                                            .stretch,
                                        children: <Widget>[
                                          new TextFormField(
                                            controller: _storeNameController,
                                            maxLines: 1,
                                            onFieldSubmitted: (String value) {
                                              FocusScope
                                                  .of(context)
                                                  .requestFocus(
                                                  textZeroFocusNode);
                                            },
                                            validator: (value) {
                                              if (_storeNameController.text
                                                  .isEmpty) {
                                                return Strings.emptyStoreName;
                                              }
                                            },
                                            decoration: new InputDecoration(
                                                labelText: Strings
                                                    .labelStoreName),
                                          ),
                                          new TextFormField(
                                            controller: _storeAddressController,
                                            focusNode: textZeroFocusNode,
                                            maxLines: 1,
                                            onFieldSubmitted: (String value) {
                                              FocusScope
                                                  .of(context)
                                                  .requestFocus(
                                                  textFirstFocusNode);
                                            },
                                            validator: (value) {
                                              if (_storeNameController.text
                                                  .isNotEmpty) {
                                                if (_storeAddressController.text
                                                    .isNotEmpty) {
                                                  if (internetConnection !=
                                                      null &&
                                                      internetConnection) {
                                                    _progressDialog(true);
                                                    _insertSuggest();
                                                  } else
                                                  if (internetConnection !=
                                                      null &&
                                                      !internetConnection) {
                                                    Constants.showSnackBar(
                                                        scaffoldStateSuggestion,
                                                        Strings
                                                            .noInternetToast);
                                                  }
                                                } else {
                                                  return Strings
                                                      .emptyStoreAddress;
                                                }
                                              }
                                            },
                                            decoration: new InputDecoration(
                                                labelText: Strings
                                                    .labelStoreAddress),
                                          ),
                                          new TextFormField(
                                            controller: _descriptionController,
                                            focusNode: textFirstFocusNode,
                                            validator: (value) {},
                                            decoration: new InputDecoration(
                                                labelText: Strings
                                                    .labelDescription),
                                          ),
                                          new Padding(
                                              padding: new EdgeInsets.only(
                                                  top: 50.0)),
                                          new Text(Strings.labelHelpUsToExpand,
                                            textAlign: TextAlign.center,
                                            style: Theme
                                                .of(context)
                                                .textTheme
                                                .caption,),

                                          new Padding(
                                              padding: new EdgeInsets.only(
                                                  top: 20.0)),
                                          new MaterialButton(
                                            color: Colors.myColor,
                                            textColor: Colors.white,
                                            child: new Text(
                                                Strings.labelButtonSuggest),
                                            onPressed: () {
                                              _formSuggestionKey.currentState
                                                  .validate();
                                            },
                                          )
                                        ],
                                      ))))

                        ],
                      ),
                    )
                  ],
                ),
              ),

            ],
          ),
        ),
        onWillPop: _storeNameController.text.isNotEmpty ||
            _storeAddressController.text.isNotEmpty ||
            _descriptionController.text.isNotEmpty
            ? _back
            : null
    );
  }

  _insertSuggest() {
    setState(() {
      int errorCode;

      Map<String, dynamic> body2;
      body2 = {
        'SuggestionMaster': {
          'SessionId': _sessionId,
          'StoreName': _storeNameController.text,
          'SuggestedAddress': _storeAddressController.text,
          'Description': _descriptionController.text,
          'linktoCustomerMasterId': _customerId,
        },
      };

      var encoder = JSON.encode(body2);
      var url = '${Constants.url}InsertSuggestionMaster';

      InsertSuggestStoreParser insertSuggestStoreParser = new InsertSuggestStoreParser ();
      Future<int> result = insertSuggestStoreParser.insertSuggestStore(
          url, encoder);
      result.then((c) {
        errorCode = c;
        _handleResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        _progressDialog(false);
      });
    });
  }

  _handleResult(int errorCode) {
    if (errorCode == 0) {
      setState(() {
        _progressDialog(false);
        subscription == null;
        Fluttertoast.showToast(
            msg: Strings.labelSuccessToast, toastLength: Toast.LENGTH_LONG);
        Navigator.pushNamedAndRemoveUntil(
            context, "/HomePage", (Route<dynamic> route) => false);
      });
    } else if (errorCode == -1) {
      _progressDialog(false);
      Constants.showSnackBar(scaffoldStateSuggestion, Strings.errorInServices);
    }
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

}
