import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:crazyrex/model/claim_card_list_model.dart';
import 'package:crazyrex/model/paytm_parameter_model.dart';
import 'package:crazyrex/page/success_buy_claimcard_screen.dart';
import 'package:crazyrex/parser/buy_membership_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class ClaimCardDetailScreen extends StatefulWidget {
  final ClaimCardListModel claimCardModel;
  final bool isRenewal;
  final bool instantActive;
  final int buyOptions;

  ClaimCardDetailScreen(
      {this.claimCardModel,
      this.isRenewal,
      this.buyOptions,
      this.instantActive});

  @override
  _ClaimCardDetailScreenState createState() => _ClaimCardDetailScreenState();
}

class _ClaimCardDetailScreenState extends State<ClaimCardDetailScreen> implements BuyMembershipDataListener{
  static const platform = const MethodChannel("CRXChannel");

  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;
  bool isLoading = false;
  bool isError = false;

  bool isReferred = false;
  String strReferCode = "";
  String strPayableAmount;

  int userId = 0;
  int customerId = 0;
  String sessionId;

  PaytmParameterModel paytmModel;
  int customerMembershipTranId;
  int paymentGatewayTransactionId;

  //region paymentGateway Response params
  String strResponse;
  String status;
  String respCode;
  String respMsg;
  String txnId;
  String txnDate;

  String _firstName;
  String _lastName;
  String _phoneNumber;
  String _referralCode;

  bool isWalletChecked = true;
  bool isTermsChecked = false;

  //region main variable
  String _totalWalletBalance1;
  String _totalCardAmount1;
  double _discount1;
  double _subTotal1;
  double _usedWalletAmount1;
  String _totalPayable1;
  //endregion


  //endregion

  final GlobalKey<FormState> _formClaimDetailKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldStateClaimDetail = new GlobalKey<ScaffoldState>();

  var _referralCodeController = new TextEditingController();
  String _referralUserName;

  SetUpBuyMembershipListenerData setUpBuyMembershipListenerData;
  String _referralCustomerId;

  String _txtReferralUserName="";
  String _walletBalance;
  int _discount;

  var reason;

  _ClaimCardDetailScreenState() {
    PrefUtil.getStringPreference(Constants.prefReferralCode).then((String value) {
      _referralCode = value;
    });
    PrefUtil.getStringPreference(Constants.prefFirstName).then((String value) {
      _firstName = value;
    });
    PrefUtil.getStringPreference(Constants.prefLastName).then((String value1) {
      _lastName = value1;
    });
    PrefUtil
        .getStringPreference(Constants.prefPhone)
        .then((String value) {
      _phoneNumber = value;
    });
    PrefUtil.getIntPreference(Constants.prefRegisterUserMasterId)
        .then((int value) {
      userId = value;
    });
    PrefUtil
        .getStringPreference(Constants.prefCustomerMasterId)
        .then((String value) {
      customerId = int.parse(value);
      print("CustomerId $customerId");
    });
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value) {
      sessionId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
  }

  @override
  void dispose() {
    subscription = null;
    super.dispose();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        setState(() {
          internetConnection = true;
          isLoading = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if(isLoading){
          _callApi();
        }
        return isLoading
            ? new Scaffold(body: new Center(child: new CircularProgressIndicator(),),)
            : _build();
      } else {
        return _error();
      }
    } else {
      return _noInternet();
    }
  }

  Widget _build(){
    return new Scaffold(
      key: scaffoldStateClaimDetail,
      appBar: AppBar(title: Text(Strings.claimCardDetailTitle),),
      body: new ListView(
        shrinkWrap: false,
        children: <Widget>[
          new Stack(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Expanded(child: new Image.asset("images/bg33.png",fit: BoxFit.cover,height: 180.0,))
                ],
              ),
              new Container(
                height: 180.0,
                padding: EdgeInsets.all(16.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    new Container(
                      child: new Row(
                        children: <Widget>[
                          new Image.asset("images/ic_splash_icon_web.png",width: 65.0,height: 65.0,),
                          new Padding(padding: new EdgeInsets.only(left: 5.0)),
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(Strings.labelAppName,style: Theme.of(context).textTheme.headline,),
                              new Text(Strings.labelClaimCardTitle,style: Theme.of(context).textTheme.caption,)
                            ],
                          )
                        ],
                      ),
                    ),
                    new Expanded(child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Text(widget.claimCardModel.membershipTypeName,style: Theme.of(context).textTheme.body2,textAlign: TextAlign.end,),
                        new Padding(padding: new EdgeInsets.only(top: 3.0)),
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Expanded(child: new Text("$_firstName $_lastName",style: Theme.of(context).textTheme.body1,)),
                            new Text("\u20B9${widget.claimCardModel.newCardRate}",style: Theme.of(context).textTheme.body1,)
                          ],
                        ),
                        new Padding(padding: new EdgeInsets.only(top: 1.0)),
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Expanded(child: new Text(_phoneNumber,style: Theme.of(context).textTheme.body1,)),
                            new Text("${Strings.labelValidity} ${widget.claimCardModel.validMonths} ${widget.claimCardModel.validMonths>1?Strings.labelMonths:Strings.labelMonth}",style: Theme.of(context).textTheme.caption,)
                          ],
                        )
                      ],
                    ))
                  ],
                ),
              ),
            ],
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
            color: Colors.white,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                new Text(Strings.labelReferralCode,style: Theme.of(context).textTheme.body2,),
                new Form(key: _formClaimDetailKey,
                    child: new Theme(data: new ThemeData(
                        primaryColor: Colors.myColor,
                        accentColor: Colors.myColor,
                        textSelectionColor: Colors.myColor
                    ), child: new Row(
                      children: <Widget>[
                        new Expanded(child: new TextFormField(
                          controller: _referralCodeController,
                          validator: (value){
                            if(_referralCodeController.text.isNotEmpty){
                              if(_referralCodeController.text.toUpperCase() != _referralCode){
                                if(internetConnection!=null && internetConnection){
                                  setUpBuyMembershipListenerData = SetUpBuyMembershipListenerData("${Constants.url}SelectRegisteredUserMasterByReferralCode/${_referralCodeController.text}",this);
                                  setUpBuyMembershipListenerData.loadReferralCode();
                                  _progressDialog(true);
                                }else if(internetConnection!=null && !internetConnection){
                                  Constants.showSnackBar(scaffoldStateClaimDetail, Strings.noInternetToast);
                                }
                              }else{
                                Constants.showSnackBar(scaffoldStateClaimDetail, Strings.errorOwnReferralCode);
                              }
                            }else{
                              return Strings.emptyReferralCode;
                            }
                          },
                          decoration: InputDecoration(
                              labelText: Strings.hintReferralCodeOption
                          ),
                        )),
                        new Padding(padding: EdgeInsets.only(left: 15.0)),
                        new MaterialButton(onPressed: (){_formClaimDetailKey.currentState.validate();},child: new Text(Strings.labelApply),color: Colors.myColor,textColor: Colors.white,)
                      ],
                    ))),
                new Padding(padding: new EdgeInsets.only(top: 10.0)),
                new Text(_txtReferralUserName,style: Theme.of(context).textTheme.caption.apply(color: Colors.green),),
                new Padding(padding: new EdgeInsets.only(top: 3.0)),
                new InkWell(onTap: (){
                  _showDialogue();
                },
                  child: new Text(Strings.labelHowDoesItWork,style: Theme.of(context).textTheme.caption.apply(color: Colors.myColor),),)
              ],
            ),
          ),
          new Padding(padding: EdgeInsets.only(top: 8.0)),
          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(60.0, 20.0, 30.0, 20.0),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                new Expanded(child: new Text(Strings.labelWallet,style: Theme.of(context).textTheme.body2,)),
                new Text("\u20B9$_totalWalletBalance1",style: Theme.of(context).textTheme.body2,textAlign: TextAlign.end,),
              ],
            ),
          ),
          new Padding(padding: EdgeInsets.only(top: 8.0)),
          new Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(10.0, 20.0, 20.0, 10.0),
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.fromLTRB(10.0, 00.0, 10.0, 0.0),
                  child: new Column(
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          new Padding(padding: EdgeInsets.only(left: 40.0)),
                          new Expanded(child: new Text(Strings.labelTotalAmount,style: Theme.of(context).textTheme.body1,),),
                          new Text("\u20B9$_totalCardAmount1",style: Theme.of(context).textTheme.body2,),
                        ],
                      ),
                      new Padding(padding: EdgeInsets.only(top: 10.0)),
                      new Row(
                        children: <Widget>[
                          new Padding(padding: EdgeInsets.only(left: 40.0)),
                          new Expanded(child: new Text(Strings.labelDiscount,style: Theme.of(context).textTheme.body1,),),
                          new Text("- \u20B9${_discount1.round().toString()}",style: Theme.of(context).textTheme.body2.apply(color: Colors.green),),
                        ],
                      ),
                      new Padding(padding: EdgeInsets.only(top: 10.0)),
                      new Row(
                        children: <Widget>[
                          new Padding(padding: EdgeInsets.only(left: 40.0)),
                          new Expanded(child: new Text(Strings.labelSubTotal,style: Theme.of(context).textTheme.body1,),),
                          new Text("\u20B9${_subTotal1.round().toString()}",style: Theme.of(context).textTheme.body2,),
                        ],
                      ),
                      new Row(
                        children: <Widget>[
                          new Checkbox(value: isWalletChecked, onChanged: _walletBalance=="0"?null:(bool value){onWalletChanged(value);}),
                          new Expanded(child: new Text(Strings.labelWalletAmount,style: Theme.of(context).textTheme.body1,)),
                          new Text("- \u20B9${_usedWalletAmount1.round().toString()}",style: Theme.of(context).textTheme.body2.apply(color: Colors.myColor),)
                        ],
                      ),
                    ],
                  ),
                ),
                new Container(
                  foregroundDecoration: ViewLineFromRegistration(),
                ),
                new Container(
                  padding: EdgeInsets.fromLTRB(50.0, 10.0, 10.0, 0.0),
                  child: new Column(
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          new Expanded(child: new Text(Strings.labelTotalPayable,style: Theme.of(context).textTheme.body2)),
                          new Text("\u20B9${_totalPayable1}",style: Theme.of(context).textTheme.body2,)
                        ],
                      )
                    ],
                  ),
                )//labelDiscount
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 0.0),
            child: new Row(
              children: <Widget>[
                new Checkbox(value: isTermsChecked, onChanged: (bool value){onTermsChanged(value);}),
                new Expanded(child: new Text(Strings.labelAgreeTermsCondition,style: Theme.of(context).textTheme.body1,)),
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0 , 10.0),
            child: new MaterialButton(onPressed: (){
              if(isTermsChecked){
                if(_totalPayable1 !="0"){
                  if(isWalletChecked){
                    if(_usedWalletAmount1 != 0){
                      insertCustomerMembership(3);
                    }else{
                      insertCustomerMembership(2);
                    }
                  }else{
                    insertCustomerMembership(2);
                  }
                }else{
                  insertCustomerMembership(1);
                }
              }else{
                Fluttertoast.showToast(msg: Strings.toastTermsCondition,toastLength: Toast.LENGTH_LONG);
              }
            },child: new Text(_totalPayable1=="0"
                ? Strings.labelButtonPayWallet
                : Strings.labelButtonPayPayment),color: Colors.myColor,textColor: Colors.white,),
          )
        ],
      ),
    );
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {
                      _internetConnection();
                    });
                  })
            ],
          ),
        ),
      ],
    ));
  }

  void _showDialogue(){
    showDialog(context: context,child: new SimpleDialog(
      contentPadding: EdgeInsets.fromLTRB(0.0,10.0,0.0,30.0),
      children: <Widget>[
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 15.0),
          child: new Row(
            children: <Widget>[
              new Expanded(child: new Text(Strings.labelHowDoesItWork,style: Theme.of(context).textTheme.title,)),
              new InkWell(
                onTap: (){Navigator.pop(context);},
                child: new Icon(Icons.close),
              )
            ],
          ),
        ),
        new Container(
          foregroundDecoration: new ViewLineFromRegistration(),
        ),
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 20.0, 15.0, 10.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  child: new Center(child: Text("1",textAlign: TextAlign.center,style: Theme.of(context).textTheme.subhead,),),
                  width: 0.0,
                  height: 35.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color: Colors.myColor,
                      width: 0.5, ), ),
                ),),
              new Expanded(
                  flex: 7,
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 80.0,
                          width: 80.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.myColor,
                              width: 2.0, ), ),
                          child: new Center(child: new Icon(Icons.share,size: 50.0,color: Colors.myColor,)),
                        ),
                      ],
                    ),
                  )),
              new Expanded(
                  flex: 1,
                  child: new Text(" "))
            ],
          ),
        ),
        new Text(Strings.labelFirst,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  child: new Center(child: Text("2",textAlign: TextAlign.center,style: Theme.of(context).textTheme.subhead,),),
                  width: 0.0,
                  height: 35.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color: Colors.myColor,
                      width: 0.5, ), ),
                ),),
              new Expanded(
                  flex: 7,
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 80.0,
                          width: 80.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.myColor,
                              width: 2.0, ), ),
                          child: new Center(child: new Image(image: new AssetImage("images/ic_splash_icon_web.png"),width: 50.0,height: 50.0,color: Colors.myColor,)),
                        ),
                      ],
                    ),
                  )),
              new Expanded(
                  flex: 1,
                  child: new Text(" "))
            ],
          ),
        ),
        new Text(Strings.labelSecond,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
        new Container(
          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  child: new Center(child: Text("3",textAlign: TextAlign.center,style: Theme.of(context).textTheme.subhead,),),
                  width: 0.0,
                  height: 35.0,
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    border: new Border.all(
                      color: Colors.myColor,
                      width: 0.5, ), ),
                ),),
              new Expanded(
                  flex: 7,
                  child: new Center(
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          height: 80.0,
                          width: 80.0,
                          decoration: new BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.myColor,
                              width: 2.0, ), ),
                          child: new Center(child: new Image(image: new AssetImage("images/ic_currency_usd_black_48dp.png"),width: 50.0,height: 50.0,color: Colors.myColor,)),
                        ),
                      ],
                    ),
                  )),
              new Expanded(
                  flex: 1,
                  child: new Text(" "))
            ],
          ),
        ),
        new Text(Strings.labelThird,style: Theme.of(context).textTheme.body1,textAlign: TextAlign.center,),
      ],
    ));
  }

  _callApi() {
    setUpBuyMembershipListenerData = SetUpBuyMembershipListenerData("${Constants.url}SelectRegisteredUserMaster/$userId", this);
    setUpBuyMembershipListenerData.loadWalletBalance();
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text("Please wait")
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  //region buy process
  void insertCustomerMembership(int type) {
    //region key = amount and value = value
    String walletAmount = '';
    dynamic walletAmountValue = '';
    String paytmAmountKey = '';
    dynamic paytmAmountValue = '';
    if (type == 1) {
      String strUsedWalletAmount =  new NumberFormat("##.00").format(_usedWalletAmount1);
      walletAmount = 'WalletAmount';
      walletAmountValue = strUsedWalletAmount;
    } else if (type == 2) {
      strPayableAmount = new NumberFormat("##.00").format(double.parse(_totalPayable1));
      paytmAmountKey = 'PaytmAmount';
      paytmAmountValue = strPayableAmount;
    } else if (type == 3) {
      String strUsedWalletAmount =new NumberFormat("##.00").format(_usedWalletAmount1);
      walletAmount = 'WalletAmount';
      walletAmountValue = strUsedWalletAmount;
      strPayableAmount = new NumberFormat("##.00").format(double.parse(_totalPayable1));
      paytmAmountKey = 'PaytmAmount';
      paytmAmountValue = strPayableAmount;
    }
    //endregion

    //region refer code key and value
    String referralCodeKey = '';
    dynamic referralCodeValue = '';
    if (isReferred) {
      referralCodeKey = 'ReferralCode';
      referralCodeValue = strReferCode;
    }
    //endregion

    //region discount key and value
    String discountKey = '';
    dynamic discountValue = '';
    String strFromDate = widget.claimCardModel.fromDate;
    String strToDate = widget.claimCardModel.toDate;
    if (!widget.isRenewal) {
      if (strFromDate != null && strToDate != null) {
        if (strToDate.compareTo(DateTime.now().toString()) *
                DateTime.now().toString().compareTo(strFromDate) >=
            0) {
          discountKey = 'Discount';
          discountValue = widget.claimCardModel.discount;
        }
      } else {
        discountKey = 'Discount';
        discountValue = widget.claimCardModel.discount;
      }
    }
    //endregion

    String strSubTotal = new NumberFormat("##.00").format(_subTotal1);

    var mapData = {
      'objCustomerMembershipDetails': {
        'IsRenewal': widget.isRenewal,
        walletAmount: walletAmountValue,
        paytmAmountKey: paytmAmountValue,
        'objCustomerMembershipTran': {
          'BuyOptions': widget.buyOptions,
          'SessionId': sessionId,
          'linktoMembershipTypeMasterId':
              widget.claimCardModel.membershipTypeMasterId,
          'linktoCustomerMasterId': customerId,
          'IsCardRenewal': widget.isRenewal,
          'IsDiscountPercentage': widget.claimCardModel.isDiscountPercentage,
          discountKey: discountValue,
          'Rate': strSubTotal,
          'IsEnabled': 0,
          'IsActiveNow': widget.instantActive,
        }
      }
    };

    var encoder = JSON.encode(mapData);
    String url = "${Constants.url}InsertCustomerMembership";

    _progressDialog(true);
    BuyMembershipParser parser = new BuyMembershipParser();
    parser.insertCustomerMembershipTran(url, encoder, type).then((value) {
      Map<String, dynamic> map = value;
      if (type != 1) {
        paytmModel = map['model'];
        customerMembershipTranId = map['CustomerMembershipTranId'];
        paymentGatewayTransactionId = map['paymentGatewayTransactionId'];
        _progressDialog(false);
        startPaymentTransaction().then((value) {
          Map<dynamic, dynamic> response = value;
          if (response != null) {
            strResponse = response['ResponseString'];
            status = response['STATUS'];
            respCode = response['RESPCODE'];
            respMsg = response['RESPMSG'];
            txnId = response['TXNID'];
            txnDate = response['TXNDATE'];
            if (respCode == "01") {
              updateCustomerMembershipTran(type, true, "");
            } else {
              updateCustomerMembershipTran(type, false, respMsg);
            }
          } else {
            setState(() {
              isError = true;
            });
          }
        });
      } else {
        int errorCode = map['ErrorCode'];
        if (errorCode == 0) {
          _progressDialog(false);
          success();
        } else {
          _progressDialog(false);
          setState(() {
            isError = true;
          });
        }
      }
    });

    print(mapData);
  }

  Future<Map<dynamic, dynamic>> startPaymentTransaction() async {
    var value;
    try {
      var map = <String, dynamic>{
        'CALLBACK_URL': paytmModel.callBackUrl,
        'CHANNEL_ID': paytmModel.channelId,
        'Checksum': paytmModel.checksum,
        'INDUSTRY_TYPE_ID': paytmModel.industryTypeId,
        'MERCHANT_KEY': paytmModel.merchantKey,
        'MID': paytmModel.merchantId,
        'WEBSITE': paytmModel.website,
        'IS_PRODUCTION': paytmModel.isProductionMode,
        'ORDER_ID': customerMembershipTranId.toString(),
        'CUST_ID': customerId.toString(),
        'TXN_AMOUNT': strPayableAmount,
      };
      value = await platform.invokeMethod("startPaymentTransaction", map);
    } catch (e) {
      print(e);
    }
    Map<dynamic, dynamic> map = value;
    return map;
  }

  void onWalletChanged(bool value){
    setState(() {
      isWalletChecked = value;
      if(isWalletChecked){
        if(double.parse(_totalWalletBalance1) < double.parse(_subTotal1.toString())){
          _totalWalletBalance1 = "0";
          _usedWalletAmount1 = double.parse(_walletBalance);
          _totalPayable1 = (_subTotal1 - _usedWalletAmount1).round().toString();
        }else{
          _totalWalletBalance1 = (double.parse(_walletBalance) - _subTotal1).toString();
          _usedWalletAmount1 = double.parse(_subTotal1.toString());
          _totalPayable1 = (_subTotal1 - _usedWalletAmount1).round().toString();
        }
      }else{
        _totalWalletBalance1 = (double.parse(_walletBalance)).toString();
        _usedWalletAmount1 = 0.0;
        _totalPayable1 = (_subTotal1 - _usedWalletAmount1).round().toString();
      }
    });
  }

  void onTermsChanged(bool value){
    setState(() {
      isTermsChecked = value;
    });
  }

  void updateCustomerMembershipTran(final int type, final bool isSuccess, final String error) {
    //region key = amount and value = value
    String walletAmount = '';
    dynamic walletAmountValue = '';
    String paytmAmountKey = '';
    dynamic paytmAmountValue = '';
    if (type == 2) {
      strPayableAmount = new NumberFormat("##.00").format(double.parse(_totalPayable1));
      paytmAmountKey = 'PaytmAmount';
      paytmAmountValue = strPayableAmount;
    } else if (type == 3) {
      String strUsedWalletAmount =
          new NumberFormat("##.00").format(_usedWalletAmount1);
      strPayableAmount = new NumberFormat("##.00").format(double.parse(_totalPayable1));

      walletAmount = 'WalletAmount';
      walletAmountValue = strUsedWalletAmount;
      paytmAmountKey = 'PaytmAmount';
      paytmAmountValue = strPayableAmount;
    }

    //region refer code key and value
    String referralCodeKey = '';
    dynamic referralCodeValue = '';
    if (isReferred) {
      referralCodeKey = 'ReferralCode';
      referralCodeValue = strReferCode;
    }
    //endregion

    //region discount key and value
    String discountKey = '';
    dynamic discountValue = '';
    String strFromDate = widget.claimCardModel.fromDate;
    String strToDate = widget.claimCardModel.toDate;
    if (!widget.isRenewal) {
      if (strFromDate != null && strToDate != null) {
        if (strToDate.compareTo(DateTime.now().toString()) *
                DateTime.now().toString().compareTo(strFromDate) >=
            0) {
          discountKey = 'Discount';
          discountValue = widget.claimCardModel.discount;
        }
      } else {
        discountKey = 'Discount';
        discountValue = widget.claimCardModel.discount;
      }
    }
    //endregion

    String strSubTotal = new NumberFormat("##.00").format(_subTotal1);

    var map = {
      'objCustomerMembershipDetails': {
        'IsRenewal': widget.isRenewal,
        walletAmount: walletAmountValue,
        paytmAmountKey: paytmAmountValue,
        referralCodeKey: referralCodeValue,
        'objCustomerMembershipTran': {
          'BuyOptions': widget.buyOptions,
          'SessionId': sessionId,
          'linktoMembershipTypeMasterId':widget.claimCardModel.membershipTypeMasterId,
          'linktoCustomerMasterId': customerId,
          'CustomerMembershipTranId': customerMembershipTranId,
          'IsCardRenewal': widget.isRenewal,
          'IsDiscountPercentage': widget.claimCardModel.isDiscountPercentage,
          discountKey: discountValue,
          'Rate': strSubTotal,
          'IsEnabled': 0,
          'IsActiveNow': widget.instantActive,
        },
        'objPaymentGatewayTransactions': {
          'PaymentGatewayTransactionId': paymentGatewayTransactionId,
          'TransactionDateTime': txnDate,
          'Response': strResponse,
          'IsSuccess': isSuccess,
          'AuthId': txnId,
          'linktoCustomerMasterId': customerId,
          'UpdateDateTime': txnDate,
          'SessionId': sessionId,
        }
      }
    };

    var encoder = JSON.encode(map);
    String url = "${Constants.url}UpdateCustomerMembership";

    _progressDialog(true);
    BuyMembershipParser parser = new BuyMembershipParser();
    parser.updateCustomerMembership(url, encoder).then((errorCode) {
      if (errorCode == 0) {
        if(isSuccess){
          success();
        }else{
          errorDialog(respMsg);
        }
      } else {
        _progressDialog(false);
        Fluttertoast.showToast(msg: Strings.errorInServices);
      }
    }).catchError((error) {
      _progressDialog(false);
      setState(() {
        isError = true;
      });
    });
  }

  void success() {
    var route = new MaterialPageRoute(builder: (BuildContext context) =>new SuccessBuyClaiamCardScreen(widget.claimCardModel.membershipTypeName));
    _navigateAndReturn(context, route, 2);
  }

  void errorDialog(String reason){
    _progressDialog(false);
    showDialog(context: context,child: new SimpleDialog(
      contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      children: <Widget>[
        new Container(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(
                color: Colors.red,
                child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      new Padding(padding: new EdgeInsets.only(top: 30.0)),
                      new Icon(Icons.warning,color: Colors.white,size: 60.0,),
                      new Padding(padding: new EdgeInsets.only(top: 5.0)),
                      new Text(Strings.transactionFailed,style: Theme.of(context).textTheme.title.apply(color: Colors.white),textAlign: TextAlign.center,),
                      new Padding(padding: new EdgeInsets.only(top: 17.0)),
                    ]),
              ),
              new Container(
                height: MediaQuery.of(context).size.height / 4,
                padding: new EdgeInsets.fromLTRB(17.0, 17.0, 17.0, 17.0),
                child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Text(Strings.errorDialogMsg,textAlign: TextAlign.center,style: Theme.of(context).textTheme.body1,),
                      new Padding(padding: new EdgeInsets.only(top: 17.0)),
                      new Text("Reason $reason",style: Theme.of(context).textTheme.caption,textAlign: TextAlign.center,),
                      new Padding(padding: new EdgeInsets.only(top: 17.0)),
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text("OK"),
                        textColor: Colors.myColor,
                      )
                    ]),
              ),
            ],
          ),
        )
      ],
    ));
  }

  @override
  void onErrorReferralCode(onError) {
   print("Error in Claim card detail screen $onError");
   isReferred = false;
   setState(() {
     isError = true;
     isLoading = false;
   });
  }

  @override
  void onReferralCode(String referralUserName,String referralCustomerId) {
    _referralUserName = referralUserName;

    if(referralCustomerId!=null){
      isReferred = true;
      _referralCustomerId = referralCustomerId;
      _txtReferralUserName =  "$_referralUserName${Strings.labelReferralCodeApply}";
      strReferCode = _referralCodeController.text;
    }

    setState(() {
      _progressDialog(false);
    });

  }

  @override
  void onWalletBalance(String walletBalance) {

    _walletBalance = walletBalance;

    //region calculate final
    //region 1st calculation
    _totalCardAmount1 = widget.claimCardModel.newCardRate;
    if(widget.isRenewal){
      if(int.parse(widget.claimCardModel.cardRenewalRate)< int.parse(widget.claimCardModel.newCardRate)){
        _discount1 = double.parse(widget.claimCardModel.newCardRate) - double.parse(widget.claimCardModel.cardRenewalRate);
        _subTotal1 = double.parse(widget.claimCardModel.cardRenewalRate);
      }else{
        _discount1 = 0.0;
        _subTotal1 = double.parse(widget.claimCardModel.cardRenewalRate);
      }
    }else{
      if(widget.claimCardModel.discount!=0){
        if(widget.claimCardModel.toDate!=null && widget.claimCardModel.fromDate !=null ){
          if(widget.claimCardModel.toDate.compareTo(DateTime.now().toString()) * DateTime.now().toString().compareTo(widget.claimCardModel.fromDate) >= 0){
            if(widget.claimCardModel.isDiscountPercentage){
              _discount1 = ((double.parse(widget.claimCardModel.newCardRate) * widget.claimCardModel.discount )/100);
              _subTotal1 = double.parse(_totalCardAmount1) - _discount1;
            }else{
              _discount1 = double.parse(widget.claimCardModel.discount.toString());
              _subTotal1 = double.parse(_totalCardAmount1) - _discount1;
            }
          }else{
            _discount1 = 0.0;
            _subTotal1 = double.parse(_totalCardAmount1) - _discount1;
          }
        }else{
          if(widget.claimCardModel.isDiscountPercentage){
            _discount1 = ((double.parse(widget.claimCardModel.newCardRate) * widget.claimCardModel.discount )/100);
            _subTotal1 = double.parse(_totalCardAmount1) - _discount1;
          }else{
            _discount1 = double.parse(widget.claimCardModel.discount.toString());
            _subTotal1 = double.parse(_totalCardAmount1) - _discount1;
          }
        }
      }else{
        _discount1 = 0.0;
        _subTotal1 = int.parse(_totalCardAmount1) - _discount1;
      }
    }
    //endregion
    //region 2ndCalculation
    if(walletBalance!= null && walletBalance!="0"){
      isWalletChecked = true;
      if(int.parse(walletBalance) <= _subTotal1){
        _totalWalletBalance1 = "0";
        _usedWalletAmount1 = double.parse(walletBalance);
        _totalPayable1 = (_subTotal1 - _usedWalletAmount1).round().toString();
      }else{
        _totalWalletBalance1 = (double.parse(walletBalance) - _subTotal1).toString();
        _usedWalletAmount1 = _subTotal1;
        _totalPayable1 = (_subTotal1 - _usedWalletAmount1).round().toString();
      }
    }else{
      _totalWalletBalance1 = "0";
      _usedWalletAmount1 = 0.0;
      isWalletChecked = false;
      _totalPayable1 = _subTotal1.round().toString();
    }
    //endregion
    //endregion
    print("_totalCardAmount1 $_totalCardAmount1\n _discount1 $_discount1\n _subTotal1 $_subTotal1\n _totalWalletBalance1 $_totalWalletBalance1\n _usedWalletAmount1 $_usedWalletAmount1 \n_totalPayable1 $_totalPayable1");
    setState(() {
      isError = false;
      isLoading = false;
    });
  }
//endregion

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    final result = await Navigator.pushReplacement(context, rout);
  }
}
