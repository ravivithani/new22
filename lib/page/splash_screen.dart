import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:crazyrex/page/notification_screen.dart';
import 'package:crazyrex/page/offer_description_screen.dart';
import 'package:crazyrex/page/store_detail_screen.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info/package_info.dart';

import 'package:crazyrex/model/app_version_model.dart';
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/parser/app_version_parser.dart';
import 'package:crazyrex/parser/login_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>implements AppVersionListener, LoginListener {


  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();

  String message = '',title = '',notificationImage = '',BusinessName = '';
  int id = 0;
  int type = 0;
  int notificationId = 0;
  int linktoBusinessMasterId = 0;

  String groupKey = 'com.arraybit.crazyRex';
  String groupChannelId = 'crazyRex';
  String groupChannelName = 'crazyRex Flutter';
  String groupChannelDescription = 'crazyRex Flutter Offer App';

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

  String version;
  PackageInfo _packageInfo = new PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  String firebaseToken;
  Future<Null> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      version = info.version;

      listener = new SetUpAppVersionListener(this,
          '${Constants.url}SelectApplicationVersionExistOrNot/$version/0');
    });
  }

  changeStatusColor(Color color) async {
    await FlutterStatusbarcolor.setStatusBarColor(color);
  }

  SetUpAppVersionListener listener;
  SetUpLoginListener listenerLogin;
  AppVersionModel modelAppVersion;

  bool isFirstTimeLaunch;
  String phone, password, cityName;
  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;

  bool appVersion;

  UserModel loginModel;

  _SplashScreenState(){
    changeStatusColor(Colors.white);
  }

  @override
  void initState() {

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    _initPackageInfo();
    getToken();
    configureFirebase();
    iosSetting();

    //region SharedPreference Data
    setState(() {
      PrefUtil
          .getBoolPreference(Constants.prefIsFirstTimeLaunch)
          .then((bool value) {
        isFirstTimeLaunch = value;
      });
      PrefUtil.getStringPreference(Constants.prefPhone).then((String value) {
        phone = value;
      });
      PrefUtil.getStringPreference(Constants.prefPassword).then((String value) {
        password = value;
      });
    });
    //endregion

    Timer(Duration(seconds: 0), () => _internetConnection());
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {_connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        listener.loadAppVersion();
      } else {
        subscription == null;
        Navigator.of(context).pushReplacementNamed('/LoginPage');
      }
    });
  }

//endregion

  void _condition() {
    setState(() {
      appVersion = modelAppVersion.selectApplicationVersionExistOrNotResult;
      if (appVersion == true || appVersion != null) {
        if (isFirstTimeLaunch == true || isFirstTimeLaunch != null) {
          if (phone != null || password != null) {
            if(firebaseToken == null){
              firebaseToken = "123456";
            }
            listenerLogin = new SetUpLoginListener(this, '${Constants.url}SelectRegisteredUserMasterByUserName/${phone}/${password}/${firebaseToken}/2');
            listenerLogin.loadLogin();
          } else {
            subscription == null;
            Navigator.of(context).pushReplacementNamed('/LoginPage');
          }
        } else {
          subscription == null;
          Navigator.of(context).pushReplacementNamed('/IntroPage');
        }
      } else {
        subscription == null;
        Navigator.of(context).pushReplacementNamed('/LoginPage');
      }
    });
  }

  @override
  void dispose() {
    subscription == null;
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  //region Design
  @override
  Widget build(BuildContext context) {
    if(firebaseToken == null){
      firebaseToken = "123456";
      PrefUtil.putStringPreference(Constants.prefFcmToken, firebaseToken);
    }
    return new Scaffold(body: _splash());
  }

  Widget _splash() {
    return new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          decoration: new BoxDecoration(color: Colors.white),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 52.0,
                      child: Image(
                        image: new AssetImage("images/ic_splash_icon_web.png"),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  @override
  void onErrorAppVersion() {
    print('Error App Version');
    subscription == null;
    Navigator.of(context).pushReplacementNamed('/LoginPage');
  }

  @override
  void onSuccessAppVersion(AppVersionModel model) {
    modelAppVersion = model;
    _condition();
//    _isLoading = false;
  }

  @override
  void onErrorLogin(onError) {
    print('Error Login Splash Screen :- $onError');
    subscription == null;
    Navigator.of(context).pushReplacementNamed('/LoginPage');
  }

  @override
  void onSuccessLogin(UserModel model) {
    loginModel = model;
    setState(() {
      _login();
    });
  }

  void _login() {
    print(loginModel.toString());
    if (loginModel.registeredUserMasterId != 0) {
      if (loginModel.isSMSVerify) {
        PrefUtil.putStringPreference(Constants.prefPhone, phone);
        PrefUtil.putStringPreference(Constants.prefFirstName, loginModel.firstName);
        PrefUtil.putStringPreference(Constants.prefLastName, loginModel.lastName);
        PrefUtil.putStringPreference(Constants.prefPassword, password);
        PrefUtil.putIntPreference(Constants.prefRegisterUserMasterId, loginModel.registeredUserMasterId);
        PrefUtil.putStringPreference(Constants.prefCustomerMasterId, loginModel.customerMasterId);
        PrefUtil.putBoolPreference(Constants.prefKycDetail, loginModel.isKYCDetails);
        PrefUtil.putStringPreference(Constants.prefSessionId, loginModel.sessionId);
        PrefUtil.putStringPreference(Constants.prefReferralCode, loginModel.referralCode);
        PrefUtil.getStringPreference(Constants.prefCityName).then((String value) {cityName = value;
          if (cityName != null) {
            subscription == null;
            Navigator.of(context).pushReplacementNamed('/HomePage');
          } else {
            subscription == null;
            Navigator.of(context).pushReplacementNamed('/CitySelectionPage');
          }
        });
      } else {
        subscription == null;
        Navigator.of(context).pushReplacementNamed('/LoginPage');
      }
    } else {
      PrefUtil.clearPreference();
      PrefUtil.putBoolPreference(Constants.prefIsFirstTimeLaunch, true);
      subscription == null;
      Navigator.of(context).pushReplacementNamed('/LoginPage');
    }
  }

  Future _showNotification() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(groupChannelId, groupChannelName, groupChannelDescription, importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(0, title, message, platformChannelSpecifics);
  }

  Future _showBigPictureNotification() async {
    //region big picture
    var directory = await getApplicationDocumentsDirectory();

    //region large icon
    var largeIconResponse = await http.get(notificationImage);
    var largeIconPath = '${directory.path}/largeIcon';
    var file = new File(largeIconPath);
    await file.writeAsBytes(largeIconResponse.bodyBytes);
    //endregion

    //region big image
    var bigPictureResponse = await http.get(notificationImage);
    var bigPicture = '${directory.path}/bigPicture';
    file = new File(bigPicture);
    await file.writeAsBytes(bigPictureResponse.bodyBytes);
    var bigPictureStyleInformation = new BigPictureStyleInformation(
        bigPicture,
        BitmapSource.FilePath,
        largeIcon: /*largeIconPath*/'mipmap/ic_launcher',
        largeIconBitmapSource: BitmapSource.Drawable,
        contentTitle: title,
        htmlFormatContentTitle: true,
        summaryText: message,
        htmlFormatSummaryText: true);
    //endregion

    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(groupChannelId, groupChannelName, groupChannelDescription,style: AndroidNotificationStyle.BigPicture,styleInformation: bigPictureStyleInformation);
    var platformChannelSpecifics = new NotificationDetails(androidPlatformChannelSpecifics, null);
    await flutterLocalNotificationsPlugin.show(0, '', '', platformChannelSpecifics);
  }

  showNotification() async {
    var android = new AndroidNotificationDetails(
        groupChannelId, groupChannelName, groupChannelDescription,
        groupKey: groupKey,
        importance: Importance.Max,
        priority: Priority.High,
        largeIcon: notificationImage,
        playSound: true,
        enableVibration: true);
    var ios = new IOSNotificationDetails();
    var platform = new NotificationDetails(android, ios);
    await flutterLocalNotificationsPlugin.show(0, title, "body", platform);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }

    if(type == 1){
      var route = new MaterialPageRoute(builder: (BuildContext context) => new StoreDetailScreen(id));
      _navigateAndReturn(context, route, 0);
    }else if(type == 2){
      var route = new MaterialPageRoute(builder: (BuildContext context) => new OfferDescriptionScreen(offerId: id,businessName: BusinessName,businessMasterId: linktoBusinessMasterId,isFromStore: false,));
      _navigateAndReturn(context, route, 0);
    }else{
      var route = new MaterialPageRoute(builder: (BuildContext context) => new NotificationScreen());
      _navigateAndReturn(context, route, 0);
    }
  }

  //region FireBase
  void configureFirebase(){
    firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> msg) {
        print(msg);
        setData(msg);
      },
      onResume: (Map<String, dynamic> msg) {
        print(msg);
        setData(msg);
      },
      onMessage: (Map<String, dynamic> msg) {
        print(msg);
        setData(msg);
      },
    );
  }

  void setData(Map<String, dynamic> map){
    dynamic data = map['data'];
    Map msg = JSON.decode(data);

    if (msg.containsKey('body')) {
      message = msg['body'];
    }
    if (msg.containsKey('title')) {
      title = msg['title'];
    }
    if (msg.containsKey('notificationImage')) {
      notificationImage = msg['notificationImage'];
    }
    if (msg.containsKey('type')) {
      type = msg['type'];
    }
    if (msg.containsKey('id')) {
      id = msg['id'];
    }
    if (msg.containsKey('notificationId')) {
      notificationId = msg['notificationId'];
    }
    if (msg.containsKey('linktoBusinessMasterId')) {
      linktoBusinessMasterId = msg['linktoBusinessMasterId'];
    }
    if (msg.containsKey('BusinessName')) {
      BusinessName = msg['BusinessName'];
    }

    if(notificationImage.isEmpty){
      _showNotification();
    }else{
      _showBigPictureNotification();
    }

  }

  void getToken(){
    firebaseMessaging.getToken().then((token) {
      firebaseToken = token;
      if(firebaseToken == null){
        PrefUtil.putStringPreference(Constants.prefFcmToken, "123456");
      }else {
        PrefUtil.putStringPreference(Constants.prefFcmToken, firebaseToken);
      }
    });
  }

  void iosSetting(){
    firebaseMessaging.requestNotificationPermissions(const IosNotificationSettings(sound: true, alert: true));
    firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings setting) {
      print("registerd setting ${setting}");
    });
  }
//endregion

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    final result = await Navigator.push(context, rout);
  }
}
