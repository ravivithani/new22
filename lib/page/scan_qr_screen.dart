import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:crazyrex/model/offer_master_obj_model.dart';
import 'package:crazyrex/model/store_list_model.dart';
import 'package:crazyrex/page/claim_card_screen.dart';
import 'package:crazyrex/page/confirm_redeem_offer_screen.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:qr_mobile_vision/qr_camera.dart';
import 'package:simple_permissions/simple_permissions.dart';

class ScanQrScreen extends StatefulWidget {
  StoreListModel store;
  OfferMasterObjectModel offerModel;

  ScanQrScreen(this.store, this.offerModel);

  @override
  _ScanQrScreenState createState() => _ScanQrScreenState(store, offerModel);
}

class _ScanQrScreenState extends State<ScanQrScreen> {
  String errorText = "";
  String qr;
  bool camState = false;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  var textController = new TextEditingController();

  bool isButtonGravityBottom = false;

  StoreListModel model;
  OfferMasterObjectModel offerModel;
  String redeemPin;

  bool isPermissionGranted = false;

  _ScanQrScreenState(this.model, this.offerModel) {
    redeemPin = model.RedeemPin.toString();
  }

  @override
  initState() {
    super.initState();
    checkPermission(Permission.Camera).then((isGranted) {
        setState(() {
          isPermissionGranted = isGranted;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
//   return forTest();
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, [0]);
      },
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text(Strings.ScanQrScreenTitle),
          leading: new InkWell(
            child: new Icon(Icons.arrow_back),
            onTap: () {
              Navigator.pop(context, [0]);
            },
          ),
        ),
        body: new ListView(
          children: <Widget>[
            editText(),
            camera(),
            Padding(padding: EdgeInsets.only(top: 10.0)),
            new FlatButton(onPressed: (){clickOnShowCard();}, child: new Text(Strings.showClaimCard),textColor: Colors.myColor,)
          ],
        ),
      ),
    );
  }

  Widget editText() {
    return new Form(
      key: _formKey,
      child: new Theme(
          data: new ThemeData(
              primaryColor: Colors.myColor,
              accentColor: Colors.myColor,
              textSelectionColor: Colors.myColor),
          child: new Container(
            padding: const EdgeInsets.fromLTRB(50.0, 5.0, 50.0, 0.0),
            child: new Row(
              crossAxisAlignment: !isButtonGravityBottom
                  ? CrossAxisAlignment.end
                  : CrossAxisAlignment.center,
              children: <Widget>[
                new Flexible(
                    child: new TextFormField(
                  controller: textController,
                  obscureText: true,
                  keyboardType: TextInputType.text,
                  decoration: new InputDecoration(labelText: Strings.hintPin),
                  maxLines: 1,
                  validator: (value) {
                    if (textController.text.isNotEmpty) {
                      if (textController.text != redeemPin) {
                        return Strings.invalidEditPin;
                      }
                    } else {
                      return Strings.emptyEditPin;
                    }
                  },
                )),
                Padding(padding: EdgeInsets.only(left: 10.0)),
                new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      redirectToConformationScreen();
                    } else {
                      setState(() {
                        isButtonGravityBottom = true;
                      });
                    }
                  },
                  child: new Text("REDEEM"),
                ),
              ],
            ),
          )),
    );
  }

  Widget camera() {
    try {
      return new SizedBox(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            new Container(
              margin: EdgeInsets.fromLTRB(50.0, 14.0, 50.0, 0.0),
              decoration: BoxDecoration(border: Border.all(color: Colors.myColor, width: 1.0)),
              padding: EdgeInsets.all(4.0),
              child: new SizedBox(
                height: MediaQuery.of(context).size.height / 1.5,
                width: MediaQuery.of(context).size.width,
                child: isPermissionGranted
                    ? qrCode()
                    : new Center(
                        child: new Text("App need camera permission"),
                      ),
              ),
            ),
            isPermissionGranted
                ? new Container(
                    height: 100.0,
                    width: 100.0,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.myColor, width: 2.0)),
                  )
                : new Container(),
          ],
        ),
      );
    } catch (e) {
      print(e);
      return new Center(
        child: new Text("Error"),
      );
    }
  }

  redirectToConformationScreen() {
    var route = new MaterialPageRoute(builder: (BuildContext context) =>new ConfirmRedeemOfferScreen(model, offerModel));
    _navigateAndReturn(context, route, 2);
  }

  void clickOnShowCard(){
    var route = new MaterialPageRoute(builder: (BuildContext context) =>new ClaimCardScreen());
    _navigateAndReturn(context, route, 2);
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    final result = await Navigator.push(context, rout);
    if (result != null) {
      List r = result;
      if (r[0] == 1) {
        Navigator.pop(context, [1]);
      } else if (r[0] == 0) {
        Navigator.pop(context, [0]);
      }
    }
    onResume();
  }

  onResume() {}

  Widget qrCode() {
    Widget widget;
    try {
      if (isPermissionGranted) {
        widget = new QrCamera(
          qrCodeCallback: (code) {
            if (code == model.UniqueId) {
              redirectToConformationScreen();
            } else {
              Fluttertoast.showToast(msg: "Wrong QR Code");
            }
          },
        );
      } else {
        widget = new Center(child: new Text("App need camera permission"));
      }
      return widget;
    } catch (e) {
      print(e);
      return new Text("Error");
    }
  }

  Future<bool> checkPermission(Permission permission) async {
    bool res = await SimplePermissions.checkPermission(permission);
    print("permission is " + res.toString());
    return res;
  }

  Future<bool> requestPermission(Permission permission) async {
    bool res = await requestPermission(permission);
    print("permission is " + res.toString());
    return res;
  }
}
