import 'dart:async';

import 'package:crazyrex/model/history_model.dart';
import 'package:crazyrex/parser/history_parser.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import './tab_refer_and_earn_screen.dart' as first;
import './tab_history_screen.dart' as second;


class ReferAndEarnScreen extends StatefulWidget {
  final bool isMembership;
  final String earnPoint;
  final int membershipLevel;

  ReferAndEarnScreen({Key key, this.isMembership,this.membershipLevel, this.earnPoint})
      : super(key: key);

  @override
  _ReferAndEarnScreenState createState() => _ReferAndEarnScreenState();
}

class _ReferAndEarnScreenState extends State<ReferAndEarnScreen> with SingleTickerProviderStateMixin  implements HistoryDataListener{
  TabController controller;

  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;

  bool isLoading=false;
  bool isError=false;
  String prefReferral;
  List<HistoryModel> list;
  String prefCustomerId;

  SetUpHistoryListenerData setUpHistoryListenerData;


  final GlobalKey<ScaffoldState> scaffoldStateReferAndEarn = new GlobalKey<ScaffoldState>();

  _ReferAndEarnScreenState(){
    PrefUtil.getStringPreference(Constants.prefReferralCode).then((String value){
      prefReferral = value;
    });
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((String value){
      prefCustomerId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
    controller = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    subscription == null;
    controller.dispose();
    super.dispose();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);

          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            internetConnection = true;
            setState(() {
              isLoading = true;
            });
          } else {
            internetConnection = false;
            setState(() {});
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    if(internetConnection!=null && internetConnection){
        if(isLoading){
          _callApi();
        }
        return isLoading? new Scaffold(
            body: new Center(
              child: new CircularProgressIndicator(),
            )):_build();
    }else if(internetConnection!=null && !internetConnection){
      return _noInternet();
    }else{
      return new Scaffold();
    }
  }

  Widget _build(){
    return new Scaffold(
      key: scaffoldStateReferAndEarn,
      appBar: new AppBar(title: new Text(Strings.referAndEarn),
        bottom: new TabBar(
            controller: controller,
            labelColor: Colors.myColor,
            unselectedLabelColor: Colors.black54,
            labelStyle: Theme.of(context).textTheme.body1,
            tabs: <Tab>[
              new Tab(text: Strings.tabReferAndEarn ),
              new Tab(text: Strings.tabHistory,)
            ]
        ),
      ),
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          new first.TabReferAndEarnScreen(isMembership: widget.isMembership,earnPoint: widget.earnPoint,referralCode: prefReferral,membershipLevel: widget.membershipLevel,),
          new second.TabHistoryScreen(list,isError,prefCustomerId)
        ],
      ),
    );
  }

  Widget _noInternet() {
    return new Scaffold(

        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }

  _callApi(){
    print("${Constants.url}SelectAllReferAndEarnTran/$prefCustomerId");
    setUpHistoryListenerData = new SetUpHistoryListenerData("${Constants.url}SelectAllReferAndEarnTran/"+prefCustomerId, this);
    setUpHistoryListenerData.loadHistoryData();
  }

  @override
  void onHistoryListener(List<HistoryModel> historyList) {
    list = historyList;
    print(list.toString());
    isLoading = false;
    isError = false;
    setState(() {

    });
  }

  @override
  void onErrorLogin(onError) {
    print("error history service in tab history---- $onError" );
    setState(() {
      isLoading = false;
      isError = true;
    });
  }
}
