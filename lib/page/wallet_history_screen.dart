import 'dart:async';

import 'package:crazyrex/model/wallet_history_model.dart';
import 'package:crazyrex/parser/wallet_history_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

class WalletHistoryScreen extends StatefulWidget {
  @override
  _WalletHistoryScreenState createState() => _WalletHistoryScreenState();
}

class _WalletHistoryScreenState extends State<WalletHistoryScreen> implements WalletHistoryDataListener{

  final GlobalKey<ScaffoldState> scaffoldStateWalletHistory = new GlobalKey<ScaffoldState>();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  bool isError=false;
  bool isLoading=false;
  String prefCustomerId;

  SetUpWalletHistoryListenerData setUpWalletHistoryListenerData;

  List<WalletHistoryModel> walletList;

  _WalletHistoryScreenState(){
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((String value){
      prefCustomerId = value;
    });
  }

  @override
  void initState() {
    _internetConnection();
  }
  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        internetConnection = true;
        setState(() {
          isLoading = true;
        });

      } else {
        internetConnection = false;
        setState(() {
        });

      }
    });
  }
//endregion

  @override
  void dispose() {
    subscription == null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(internetConnection!=null && internetConnection){
      if(!isError){
        if(isLoading){
          _callApis();
        }
        return isLoading
            ? new Scaffold(
          body: new Center(child: new CircularProgressIndicator(),),
        )
            : walletList.length==0? _emptyScreen() : _build();
      }else{
        return _error();
      }
    }else if(internetConnection!=null && !internetConnection){
      return _noInternet();
    }
    return Container();
  }

  Widget _build(){
    return new Scaffold(
      appBar: AppBar(title: new Text(Strings.labelTitleWalletHistoryScreen),),
      body: new Container(
        child: new Center(
          child: new ListView(
            children: <Widget>[
              new Stack(
                children: <Widget>[
                  new Image(image: AssetImage("images/history.png")),
                  new Container(
                      padding: EdgeInsets.only(top: 70.0),
                      child: new Center(
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Text(Strings.labelWallet, style: Theme
                                .of(context)
                                .textTheme
                                .title,),
                            new Text(walletList[0].currentBalance.toString(), style: Theme
                                .of(context)
                                .textTheme
                                .title,),
                          ],
                        ),
                      )
                  )
                ],
              ),
              new Container(
                child: new ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: walletList.length,
                    itemBuilder: (BuildContext context,int index){
                      WalletHistoryModel model = walletList[index];
                      return new Card(
                        margin: EdgeInsets.fromLTRB(6.0, 6.0, 6.0, index == walletList.length-1 ?6.0: 0.0),
                        elevation: 2.0,
                        child: ListTile(
                          title: new Text(model.description),
                          trailing:  _amount(model),
                          subtitle: new Text("${formatDate(DateTime.parse(model.transactionDate), [dd, '-', mm, '-', yyyy])}"),
                        ),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _emptyScreen(){
    return new Scaffold(
      appBar: new AppBar(title: new Text(Strings.labelTitleWalletHistoryScreen),),
      body: new Stack(
        children: <Widget>[
          new Center(
            child:  new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Image.asset("images/ic_table_search_black.png",color: Colors.myColor,width: 65.0,height: 65.0,),
                  new Padding(padding: new EdgeInsets.only(top: 20.0)),
                  new Text(Strings.labelEmptyWalletHistoryScreen,style: Theme.of(context).textTheme.title.apply(color: Colors.myColor),)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(padding: EdgeInsets.only(top: 16.0),),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(padding: EdgeInsets.only(top: 27.0),),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }
  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
          ],
        ));
  }

  _callApis() {
    setUpWalletHistoryListenerData = new SetUpWalletHistoryListenerData("${Constants.url}SelectAllWalletTransactions/$prefCustomerId",this);
    setUpWalletHistoryListenerData.loadWalletHistory();
  }

  @override
  void onErrorWalletHistory(onError) {
    print("Error in wallet history screen");

    isError = true;
    isLoading = false;
    setState(() {

    });
  }

  @override
  void onWalletHistory(List<WalletHistoryModel> walletHistoryList) {
    walletList = walletHistoryList;

    isError = false;
    isLoading = false;
    setState(() {

    });
  }

  Widget _amount(WalletHistoryModel model) {
    if(model.walletTransactionType==1){
      return new Text("+ \u20B9${model.amount}", style: Theme.of(context).textTheme.body1.apply(color: Colors.green),);
    }else if(model.walletTransactionType==2){
      return new Text("- \u20B9${model.amount}", style: Theme.of(context).textTheme.body1.apply(color: Colors.red),);
    }else{
      return new Text("");
    }

  }
}
