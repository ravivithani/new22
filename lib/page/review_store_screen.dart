import 'dart:async';
import 'dart:convert';

import 'package:crazyrex/model/review_store_model.dart';
import 'package:crazyrex/page/edit_review_store_screen.dart';
import 'package:crazyrex/parser/delete_review_store_parser.dart';
import 'package:crazyrex/parser/review_store_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/widget/star_rating.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';


class ReviewStoreScreen extends StatefulWidget {
  @override
  _ReviewStoreScreenState createState() => _ReviewStoreScreenState();
}

class _ReviewStoreScreenState extends State<ReviewStoreScreen> implements ReviewStoreDataListener {

  final GlobalKey<ScaffoldState> scaffoldStateReviewStore = new GlobalKey<ScaffoldState>();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection = false;

  bool isError=false;
  bool isLoading=false;
  String prefCustomerId;
  String prefSessionId;
  List<ReviewStoreModel> reviewStoreList;

  SetUpReviewStoreListenerData setUpReviewStoreListenerData;

  _ReviewStoreScreenState(){
    PrefUtil.getStringPreference(Constants.prefCustomerMasterId).then((String value){
      prefCustomerId = value;
    });
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value){
      prefSessionId = value;
    });
  }

  @override
  void initState() {
    _internetConnection();
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        internetConnection = true;
        setState(() {
          isLoading = true;
        });

      } else {
        internetConnection = false;
        setState(() {
        });
      }
    });
  }
  //endregion

  @override
  void dispose() {
    subscription == null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(internetConnection!=null && internetConnection){
      if(!isError){
        if(isLoading){
          _callApis();
        }
        return isLoading ? new Scaffold(
          body: new Center(
            child: new CircularProgressIndicator(),
          ),
        )
            :reviewStoreList.length==0?_emptyScreen():_build();
      }else{
        return _error();
      }
    }else if(internetConnection!=null && !internetConnection){
      return _noInternet();
    }
    return Container();
  }

  Widget _build(){
    return new Scaffold(
      appBar: new AppBar(title: new Text(Strings.labelTitleReviewStoreScreen),),
      body: new ListView.builder(
          itemCount: reviewStoreList.length,
          itemBuilder: (BuildContext context,int index){
            ReviewStoreModel model = reviewStoreList[index];
            return new Dismissible(key: new ObjectKey(reviewStoreList[index]),
                /*direction: DismissDirection.endToStart,*/

                background:new Container(
                  color: Colors.orangeAccent,

                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Icon(Icons.edit,color: Colors.white,),
                            new Text(Strings.labelEdit,style: Theme.of(context).textTheme.caption.apply(color: Colors.white),)
                          ],
                        ),
                        padding: EdgeInsets.only(left: 12.0),
                      )

                    ],
                  ),
                ),
                secondaryBackground: new Container(
                  color: Colors.red,

                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Container(
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Icon(Icons.delete,color: Colors.white,),
                            new Text(Strings.labelDelete,style: Theme.of(context).textTheme.caption.apply(color: Colors.white),)
                          ],
                        ),
                        padding: EdgeInsets.only(right: 12.0),
                      )

                    ],
                  ),
                ),

                onDismissed: (DismissDirection direction){
                  if(direction == DismissDirection.endToStart){
                    reviewStoreList.removeAt(index);
                    _delete(model.reviewMasterId);
                  }else if(direction == DismissDirection.startToEnd){
                    reviewStoreList.removeAt(index);
                    var route = new MaterialPageRoute(builder: (BuildContext context) => new EditReviewStoreScreen(editReviewStoreModel: model,));
                    _navigateAndReturn(context, route, 1);
                  }
                },
                child: new Container(
              padding: new EdgeInsets.fromLTRB(12.0,12.0,12.0,0.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  new Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new Expanded(flex: 1,
                          child: new Container(
                            height: 50.0,
                            width: 50.0,
                            child: model.logoImageName!=null
                                ? new Image.network(model.logoImageName)
                                : new Image.asset("images/default1.png")
                          )
                      ),
                      new Padding(padding: new EdgeInsets.only(left: 8.0)),
                      new Expanded(child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(model.businessName,style: Theme.of(context).textTheme.body2,maxLines: 1,),
                          new Padding(padding: new EdgeInsets.only(top: 2.0)),
                          new StarRating(
                            rating: double.parse(model.starRating),
                            color: Colors.myColor,
                          ),
                          new Padding(padding: new EdgeInsets.only(top: 2.0)),
                          new Text(model.review,style: Theme.of(context).textTheme.caption,),
                        ],
                      ),flex: 4,),
                    ],
                  ),
                  new Padding(padding: new EdgeInsets.only(top: 8.0)),
                  new Container(
                    child: new Container(foregroundDecoration: new ViewLineFromRegistration(),),

                  )
                ],
              ),
            ));
      }),
    );
  }

  Widget _emptyScreen(){
    return new Scaffold(
      appBar: new AppBar(title: new Text(Strings.labelTitleReviewStoreScreen),),
      body: new Stack(
        children: <Widget>[
          new Center(
            child:  new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(Icons.tag_faces,color: Colors.myColor,size: 65.0,),
                  new Padding(padding: new EdgeInsets.only(top: 20.0)),
                  new Text(Strings.emptyReviewScreen,style: Theme.of(context).textTheme.title.apply(color: Colors.myColor),)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Colors.myColor,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.noInternetTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(padding: EdgeInsets.only(top: 16.0),),
                  new Text(Strings.noInternetSubText,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(padding: EdgeInsets.only(top: 27.0),),
                  new MaterialButton(
                      color: Colors.myColor,
                      textColor: Colors.white,
                      child: new Text(Strings.labelButtonRetry),
                      onPressed: () {
                        setState(() {});
                      })
                ],
              ),
            ),
          ],
        ));
  }
  Widget _error() {
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.error_outline,
                    color: Colors.redAccent,
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text(Strings.emptyScreenTitle,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
          ],
        ));
  }

  _callApis() {
    setUpReviewStoreListenerData = new SetUpReviewStoreListenerData("${Constants.url}SelectAllReviewMaster/null/$prefCustomerId",this);
    setUpReviewStoreListenerData.loadReviewStoreData();
  }

  @override
  void onErrorReviewStore(onError) {
    print("error in review in review store screen");
    setState(() {
      isError = true;
      isLoading = false;
    });
  }

  @override
  void onReviewStoreListener(List<ReviewStoreModel> reviewList) {
    reviewStoreList = reviewList;
      setState(() {
        isError = false;
        isLoading = false;
      });
  }

  _delete(int reviewMasterId) {
    setState(() {
      int errorCode;
      Map<String,dynamic> body2 = {
        'ReviewMaster':{
          'ReviewMasterId':reviewMasterId,
          'SessionId':prefSessionId
        }
      };
      var encoder = JSON.encode(body2);
      var url = '${Constants.url}DeleteReviewMaster';

      DeleteReviewParser deleteReviewParser = new DeleteReviewParser();
      Future<int> result = deleteReviewParser.deleteReview(url, encoder);
      result.then((c) {
        errorCode = c;
        _handleResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        setState(() {
        });
      });
    });
  }

  _handleResult(int errorCode) {
    setState(() {
    });
    if (errorCode == 0) {
      setState(() {

      });
    }else{
      setState(() {
        reviewStoreList.clear();
        Constants.showSnackBar(scaffoldStateReviewStore, Strings.errorInServices);
        isLoading = true;
      });
    }
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    onResume();
    setState(() {
      isLoading = true;
    });
  }

  onResume(){
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);
      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
        setState(() {
          internetConnection = true;
        });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

}
