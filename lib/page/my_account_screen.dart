import 'dart:async';

import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/page/been_favourite_store_screen.dart';
import 'package:crazyrex/page/city_selection_screen.dart';
import 'package:crazyrex/page/edit_account_screen.dart';
import 'package:crazyrex/page/my_offer_screen.dart';
import 'package:crazyrex/page/notification_setting_screen.dart';
import 'package:crazyrex/page/password_screen.dart';
import 'package:crazyrex/page/purchased_history_screen.dart';
import 'package:crazyrex/page/review_store_screen.dart';
import 'package:crazyrex/page/wallet_history_screen.dart';
import 'package:crazyrex/parser/profile_parser.dart';
import 'package:crazyrex/util/strings.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/widget/view_line_from_registration.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

class MyAccountScreen extends StatefulWidget {
  @override
  _MyAccountScreenState createState() => _MyAccountScreenState();
}

class _MyAccountScreenState extends State<MyAccountScreen>
    implements ProfileListener {
  bool isLoading = false;
  bool isError = false;

  var _connectionStatus = 'Unknown';
  StreamSubscription<ConnectivityResult> subscription;
  var connectivity;
  bool internetConnection;

  SetUpProfileListener profileListener;
  UserModel userModel;

  String cityName;
  int userId;


  _MyAccountScreenState() {

    PrefUtil
        .getIntPreference(Constants.prefRegisterUserMasterId)
        .then((int value) {
      userId = value;
      print(userId);
    });
    PrefUtil.getStringPreference(Constants.prefCityName).then((String value) {
      cityName = value;
    });
  }

  @override
  void initState() {
    _internetConnection();
  }

  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //    _isLoading = true;
        internetConnection = true;
        setState(() {
          isLoading = true;
        });
      } else {
        internetConnection = false;
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    subscription == null;
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return _build();
  }

  Widget _build() {
    if (internetConnection != null && internetConnection) {
      if (!isError) {
        if (isLoading) {
          callApi();
        }
        return new Scaffold(
            appBar: new AppBar(
              actions: <Widget>[
                new IconButton(
                  icon: new Icon(Icons.edit),
                  onPressed: () {
                    var route = new MaterialPageRoute(builder: (BuildContext context) => new EditAccountScreen());
                    _navigateAndReturn(context, route, 1);
                  },
                ),
              ],
              title: new Text(Strings.labelTitleMyAccountScreen),
              bottom: isLoading
                  ? null
                  : new PreferredSize(
                      preferredSize: new Size(55.0, 55.0),
                      child: new Container(
                        padding: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 7.0),
                        child: new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Padding(
                                padding: new EdgeInsets.only(left: 15.0)),
                            userModel.profileImageName != null
                                ? new Container(
                                    width: 50.0,
                                    height: 50.0,
                                    decoration: new BoxDecoration(
                                      color: Colors.transparent,
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                          image: new NetworkImage(
                                              userModel.profileImageName),
                                          fit: BoxFit.cover),
                                    ))
                                : new Icon(Icons.account_circle,size: 50.0,color: Colors.black54,),
                            new Padding(
                                padding: new EdgeInsets.only(left: 15.0)),
                            new Expanded(
                                flex: 7,
                                child: new Container(
                                  padding: new EdgeInsets.only(left: 0.0),
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Text(
                                        (userModel == null ||
                                                userModel.firstName.isEmpty ||
                                                userModel.lastName.isEmpty)
                                            ? "User Name"
                                            : "${userModel.firstName} ${userModel
                                        .lastName}",
                                        style:
                                            Theme.of(context).textTheme.body1,
                                      ),
                                      new Padding(
                                          padding:
                                              new EdgeInsets.only(top: 2.0)),
                                      new Text(
                                        (userModel == null ||
                                                userModel.phone.isEmpty)
                                            ? null
                                            : "${userModel.phone}",
                                        style:
                                            Theme.of(context).textTheme.caption,
                                      ),
                                      (userModel == null ||
                                              userModel.email.isEmpty)
                                          ? new Container()
                                          : new Container(
                                              padding:
                                                  new EdgeInsets.only(top: 2.0),
                                              child: new Text(
                                                  "${userModel.email}",
                                                  style: Theme
                                                      .of(context)
                                                      .textTheme
                                                      .caption),
                                            )
                                    ],
                                  ),
                                ))
                          ],
                        ),
                      )),
            ),
            body: isLoading
                ? new Center(
                    child: new CircularProgressIndicator(),
                  )
                : _myAccount());
      } else {
        return _error();
      }
    } else if (internetConnection != null && !internetConnection) {
      return _noInternet();
    } else {
      return new Scaffold();
    }
  }

  Widget _myAccount() {
    return new Container(
      child: new ListView(
        children: <Widget>[
          new ListTile(
            onTap: () {
              subscription == null;
              var route = new MaterialPageRoute(builder: (BuildContext context) => new CitySelectionScreen(true));
              _navigateAndReturn(context, route, 1);
            },
            trailing: new Text(
              Strings.labelChange,
              style: Theme
                  .of(context)
                  .textTheme
                  .caption
                  .apply(color: Colors.myColor),
            ),
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.location_on,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Text(Strings.labelCitySelect,style: Theme.of(context).textTheme.body1,),
                  new Expanded(
                      flex: 1,
                      child: new Text(" $cityName",style: Theme.of(context).textTheme.body2,)),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),
          new ListTile(
            onTap: () {
              var route = new MaterialPageRoute(builder: (BuildContext context) => new WalletHistoryScreen());
              _navigateAndReturn(context, route, 1);
            },
            trailing: new Text(
              (userModel == null || userModel.walletBalance.isEmpty)
                  ? "\u20B90"
                  : "\u20B9${userModel.walletBalance}",
              style: Theme
                  .of(context)
                  .textTheme
                  .caption
                  .apply(color: Colors.green),
            ),
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.account_balance_wallet,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Expanded(
                      flex: 1,
                      child: new Text(
                        Strings.labelMyWallet,
                        style: Theme.of(context).textTheme.body1,
                      )),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),
          new ListTile(
            onTap: () {
              var route = new MaterialPageRoute(builder: (BuildContext context) => new PurchasedHistoryScreen());
              _navigateAndReturn(context, route, 1);
            },
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.history,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Expanded(
                      flex: 1,
                      child: new Text(
                        Strings.labelPurchasedHistory,
                        style: Theme.of(context).textTheme.body1,
                      )),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),
          new ListTile(
            onTap: () {
              var route = new MaterialPageRoute(
                  builder: (BuildContext context) => new MyOfferScreen(isFromOffer: false,));
              _navigateAndReturn(context, route, 2);
            },
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.local_offer,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Expanded(
                      flex: 1,
                      child: new Text(
                        Strings.labelMyOffer,
                        style: Theme.of(context).textTheme.body1,
                      )),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),
          new ListTile(
            onTap: () {
              var route = new MaterialPageRoute(builder: (BuildContext context) => new MyBeenFavStoreScreen(true,false));
              _navigateAndReturn(context, route, 1);
            },
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.favorite,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Expanded(
                      flex: 1,
                      child: new Text(Strings.labelMyFavourite,
                          style: Theme.of(context).textTheme.body1)),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),
          new ListTile(
            onTap: ()  {

              var route = new MaterialPageRoute(builder: (BuildContext context) => new ReviewStoreScreen());
              _navigateAndReturn(context, route, 1);
            },
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.star,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Expanded(
                      flex: 1,
                      child: new Text(Strings.labelMyReview,
                          style: Theme.of(context).textTheme.body1)),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),
          new ListTile(
            onTap: () {
              var route = new MaterialPageRoute(builder: (BuildContext context) => new MyBeenFavStoreScreen(false,true));
              _navigateAndReturn(context, route, 1);
            },
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.beenhere,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Expanded(
                      flex: 1,
                      child: new Text(Strings.labelBeenThere,
                          style: Theme.of(context).textTheme.body1)),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),
          new ListTile(
            onTap: () {
              var route = new MaterialPageRoute(
                  builder: (BuildContext context) => new PasswordScreen());
              _navigateAndReturn(context, route, 2);
            },
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.lock,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Expanded(
                      flex: 1,
                      child: new Text(Strings.labelChangePassword,
                          style: Theme.of(context).textTheme.body1)),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),
          new ListTile(
            onTap: () {
              var route = new MaterialPageRoute(builder: (BuildContext context) => new NotificationSettingScreen());
              _navigateAndReturn(context, route, 1);
            },
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.notifications,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Expanded(
                      flex: 1,
                      child: new Text(Strings.labelNotificationSettings,
                          style: Theme.of(context).textTheme.body1)),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),
          /*new ListTile(
            onTap: () => {},
            title: new Container(
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.account_box,
                    color: Colors.black45,
                  ),
                  new Padding(padding: new EdgeInsets.only(left: 15.0)),
                  new Expanded(
                      flex: 1,
                      child: new Text(Strings.labelKYCDetails,
                          style: Theme.of(context).textTheme.body1)),
                ],
              ),
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(left: 14.0, right: 14.0),
            child: new Container(
              foregroundDecoration: new ViewLineFromRegistration(),
            ),
          ),*/ //todo kyc details
        ],
      ),
    );
  }

  Widget _noInternet() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.signal_wifi_off,
                color: Colors.myColor,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.noInternetTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
              ),
              new Text(Strings.noInternetSubText,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.body1),
              Padding(
                padding: EdgeInsets.only(top: 27.0),
              ),
              new MaterialButton(
                  color: Colors.myColor,
                  textColor: Colors.white,
                  child: new Text(Strings.labelButtonRetry),
                  onPressed: () {
                    setState(() {});
                  })
            ],
          ),
        ),
      ],
    ));
  }

  Widget _error() {
    return new Scaffold(
        body: new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Icon(
                Icons.error_outline,
                color: Colors.redAccent,
                size: 65.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.0),
              ),
              new Text(Strings.emptyScreenTitle,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.title),
            ],
          ),
        ),
      ],
    ));
  }

  @override
  void onErrorProfile(onError) {
    setState(() {
      isLoading = false;
      isError = true;
    });
  }

  @override
  void onSuccessProfile(UserModel model) {
    setState(() {
      userModel = model;
      isLoading = false;
      isError = false;
    });
  }

  callApi() {
    profileListener = new SetUpProfileListener(
        this, "${Constants.url}SelectRegisteredUserMaster/$userId");
    profileListener.loadProfile();
  }

  _navigateAndReturn(BuildContext context, var rout, int navigateTo) async {
    subscription == null;
    final result = await Navigator.push(context, rout);
    PrefUtil.getStringPreference(Constants.prefCityName).then((String value) {
      cityName = value;
    });
    onResume();
    setState(() {
      isLoading = true;
    });
  }

  onResume(){
    connectivity = new Connectivity();
    subscription = connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);
      if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
          setState(() {
            internetConnection = true;
          });
      } else {
        setState(() {
          internetConnection = false;
        });
      }
    });
  }

}
