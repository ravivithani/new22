import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:crazyrex/model/otp_model.dart';
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/page/password_screen.dart';
import 'package:crazyrex/parser/otp_parser.dart';
import 'package:crazyrex/parser/update_account_verify_username_parser.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:crazyrex/util/pref_util.dart';
import 'package:crazyrex/util/strings.dart';

class OneTimePasswordScreen extends StatefulWidget {
  final UserModel userModelRegistration;
  final String userNameForget;
  final UserModel phoneProfile;
  final UserModel emailProfile;

  @override
  _OneTimePasswordScreenState createState() => _OneTimePasswordScreenState();

  OneTimePasswordScreen(
      {Key key, this.userModelRegistration, this.userNameForget, this.phoneProfile, this.emailProfile,})
      : super(key: key);
}

class _OneTimePasswordScreenState extends State<OneTimePasswordScreen>
    with OneTimePasswordListener, TickerProviderStateMixin {
  final GlobalKey<FormState> _formOtpKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldStateOTP = new GlobalKey<
      ScaffoldState>();

  var _connectionStatus = 'Unknown';
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;
  bool internetConnection;

  UserModel loginModel;
  OneTimePasswordModel oneTimePasswordModel;
  SetUpOneTimePasswordListener listenerOneTimePassword;

  var _otpController = new TextEditingController();

  bool isPhone = false;
  bool isEmail = false;
  bool isForget = false;
  bool isRegister = false;
  bool isProfile = false;
  String userName;
  String _textOtp;

  String _sessionId;

  AnimationController controller;

  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60)
        .toString()
        .padLeft(2, '0')}';
  }


  _OneTimePasswordScreenState() {
    PrefUtil.getStringPreference(Constants.prefSessionId).then((String value) {
      _sessionId = value;
    });
  }

  @override
  void initState() {
    super.initState();
    _internetConnection();
    _general();

    controller = AnimationController(vsync: this,
      duration: Duration(seconds: Constants.resendTimer),
    );

    if (controller.isAnimating) {
      controller.stop();
    } else {
      controller.reverse(
          from: controller.value == 0.0 ? 1.0 : controller.value);
    }
  }

  _general() {
    if (widget.userModelRegistration != null) {
      //region Registration
      loginModel = widget.userModelRegistration;
      userName = loginModel.phone;
      isPhone = true;
      isRegister = true;
      _textOtp = "${Strings.otpSentToPhone} ${userName}";
//      endregion
    } else if (widget.userNameForget != null) {
      //region Forget
      userName = widget.userNameForget;
      isForget = true;
      if (Constants.isValidPhone(userName)) {
        isPhone = true;
        _textOtp = "${Strings.otpSentToPhone} ${userName}";
      } else if (Constants.isValidEmail(userName)) {
        _textOtp = "${Strings.otpSentToEmail} ${userName}";
        isEmail = true;
      }
      //endregion
    } else if (widget.phoneProfile != null) {
      loginModel = widget.phoneProfile;
      userName = loginModel.phone;
      isProfile = true;
      isPhone = true;
      _textOtp = "${Strings.otpSentToPhone} ${userName}";
    } else if (widget.emailProfile != null) {
      loginModel = widget.emailProfile;
      userName = loginModel.email;
      isProfile = true;
      isEmail = true;
      _textOtp = "${Strings.otpSentToEmail} ${userName}";
    }
  }

  //region internet on/off
  _internetConnection() {
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
          _connectionStatus = result.toString();
          print(_connectionStatus);
          if (result == ConnectivityResult.wifi ||
              result == ConnectivityResult.mobile) {
            internetConnection = true;
          } else {
            internetConnection = false;
          }
        });
  }

//endregion

  Future<bool> _back() {
    return showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(
            content: new Text(Strings.labelAlertTitleOfCancelRegistration),
            actions: <Widget>[
              new FlatButton(onPressed: () => Navigator.of(context).pop(false),
                  child: new Text(Strings.alertNegativeButton)),
              new FlatButton(onPressed: () {
                Navigator.of(context).pop(true);
                Navigator.of(context).pop(true);
              }, child: new Text(Strings.alertPositiveButton)),
            ],
          ),
    ) ?? false;
  }

  Future<bool> _back1() {
    if (isPhone) {
      UserModel loginModel1 = new UserModel(
          null,
          loginModel.birthDate,
          loginModel.customerMasterId,
          null,
          loginModel.email,
          null,
          loginModel.firstName,
          loginModel.lastName,
          null,
          loginModel.phone,
          loginModel.gender,
          loginModel.isEmailVerify,
          false,
          loginModel.isSMSVerify,
          loginModel.profileImageName,
          loginModel.referralCode,
          loginModel.registeredUserMasterId,
          null,
          null,
          null,
          null,
          loginModel.sessionId,
          loginModel.isSelectPhoto,
          loginModel.isUrlPhoto);
      Navigator.pop(context, [loginModel1]);
      subscription == null;
    } else if (isEmail) {
      UserModel loginModel1 = new UserModel(
          null,
          loginModel.birthDate,
          loginModel.customerMasterId,
          null,
          loginModel.email,
          null,
          loginModel.firstName,
          loginModel.lastName,
          null,
          loginModel.phone,
          loginModel.gender,
          loginModel.isEmailVerify,
          false,
          loginModel.isSMSVerify,
          loginModel.profileImageName,
          loginModel.referralCode,
          loginModel.registeredUserMasterId,
          null,
          null,
          null,
          null,
          loginModel.sessionId,
          loginModel.isSelectPhoto,
          loginModel.isUrlPhoto);
      Navigator.pop(context, [loginModel1]);
      subscription == null;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: isRegister ? _back : isProfile ? _back1 : null,
      child: _otp(),
    );
  }

  Widget _otp() {
    return new Scaffold(
      key: scaffoldStateOTP,
      resizeToAvoidBottomPadding: false,
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(Strings.labelOtpScreenTitle,
                  style: Theme
                      .of(context)
                      .textTheme
                      .display1
                      .apply(color: Colors.myColor)),
              new Image(
                image: new AssetImage(
                    "images/ic_cellphone_message_black_48dp.png"),
                width: 65.0,
                height: 65.0,
                color: Colors.myColor,
              ),
              new Container(
                padding: new EdgeInsets.fromLTRB(50.0, 15.0, 50.0, 0.0),
                child: Text(_textOtp, style: Theme
                    .of(context)
                    .textTheme
                    .body1, textAlign: TextAlign.center,),
              ),
              new Form(key: _formOtpKey,
                  child: new Theme(data: new ThemeData(
                      primaryColor: Colors.myColor,
                      accentColor: Colors.myColor,
                      textSelectionColor: Colors.myColor
                  ),
                      child: new Container(
                        padding: const EdgeInsets.fromLTRB(
                            50.0, 0.0, 50.0, 0.0),
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            new TextFormField(
                              controller: _otpController,
                              maxLines: 1,
                              keyboardType: TextInputType.number,
                              maxLength: 4,
                              validator: (value) {
                                if (internetConnection == true) {
                                  if (_otpController.text.isNotEmpty) {
                                    if (Constants.OTP == _otpController.text) {
                                      if (isRegister) {
                                        UserModel loginModel1 = new UserModel(
                                            null,
                                            null,
                                            null,
                                            null,
                                            loginModel.email,
                                            null,
                                            loginModel.firstName,
                                            loginModel.lastName,
                                            null,
                                            loginModel.phone,
                                            null,
                                            false,
                                            null,
                                            true,
                                            null,
                                            loginModel.referralCode,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            false,
                                            false);
                                        var route = new MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                            new PasswordScreen(
                                                userModelRegistration: loginModel1));
                                        subscription == null;
                                        Navigator.pushReplacement(
                                            context, route);
                                      } else if (isForget) {
                                        var route = new MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                            new PasswordScreen(
                                              userNameForget: userName,));
                                        subscription == null;
                                        Navigator.pushReplacement(
                                            context, route);
                                      } else if (isProfile) {
                                        if (internetConnection != null &&
                                            internetConnection) {
                                          if (isPhone) {
                                            _progressDialog(true);
                                            _updateProfileVerifyUserName();
                                          } else if (isEmail) {
                                            _progressDialog(true);
                                            _updateProfileVerifyUserName();
                                          }
                                        } else if (internetConnection != null &&
                                            !internetConnection) {
                                          Constants.showSnackBar(
                                              scaffoldStateOTP,
                                              Strings.noInternetToast);
                                        }
                                      }
                                    } else {
                                      return Strings.invalidOTP;
                                    }
                                  } else {
                                    return Strings.emptyOTP;
                                  }
                                } else {
                                  Constants.showSnackBar(scaffoldStateOTP,
                                      Strings.noInternetToast);
                                }
                              },
                              decoration: new InputDecoration(
                                  labelText: Strings.hintOTP
                              ),
                            ),
                            new Padding(
                                padding: new EdgeInsets.only(top: 27.0)),
                            new MaterialButton(
                              color: Colors.myColor,
                              textColor: Colors.white,
                              child: new Text(Strings.labelButtonText),
                              onPressed: () {
                                _formOtpKey.currentState.validate();
                              },
                            ),
                            new Padding(
                                padding: new EdgeInsets.only(top: 55.0)),
                          ],
                        ),
                      ))),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Text(Strings.labelNotReceiveOTPText, style: Theme
                      .of(context)
                      .textTheme
                      .body1),
                  AnimatedBuilder(
                    animation: controller,
                    builder: (BuildContext context, Widget child) {
                      return new InkWell(
                        child: new Text(Strings.labelResend, style: Theme
                            .of(context)
                            .textTheme
                            .body2
                            .apply(
                            color: controller.isAnimating ? Colors.grey : Colors
                                .myColor)),
                        onTap: () {
                          if (internetConnection == true) {
                            if (!controller.isAnimating) {
                              controller.reverse(from: controller.value == 0.0
                                  ? 1.0
                                  : controller.value);
                              _progressDialog(true);
                              if (isPhone) {
                                listenerOneTimePassword =
                                new SetUpOneTimePasswordListener(
                                    this, '${Constants
                                    .url}SelectRegisterUserForgotPassword/${userName}/Phone');
                                listenerOneTimePassword.loadOneTimePassword();
                              } else if (isEmail) {
                                listenerOneTimePassword =
                                new SetUpOneTimePasswordListener(
                                    this, '${Constants
                                    .url}SelectRegisterUserForgotPassword/${userName}/Email');
                                listenerOneTimePassword.loadOneTimePassword();
                              }
                            }
                          } else {
                            Constants.showSnackBar(
                                scaffoldStateOTP, Strings.noInternetToast);
                          }
                        },);
                    },
                  ),
                  controller.isAnimating ? AnimatedBuilder(
                    animation: controller,
                    builder: (BuildContext context, Widget child) {
                      if (timerString == '0:00') {
                        return new Container();
                      } else {
                        return new Center(
                          child: new Container(
                            margin: EdgeInsets.only(left: 5.0),
                            child: Text("($timerString)",
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .body2
                                  .apply(color: Colors.grey),
                            ),
                          ),
                        );
                      }
                    },
                  ) : new Container(),
//                  new Text("($timerString)"),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }

  _progressDialog(bool isLoading) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text(Strings.loadingTitle)
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    showDialog(barrierDismissible: false, context: context, child: dialog);
    if (!isLoading) {
      setState(() {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }
  }

  @override
  void onErrorOneTimePassword(onError) {
    print('Error Resend OTP in OTP Screen - $onError ');
    _progressDialog(false);
//    _isLoading = true;
  }

  @override
  void onSuccessOneTimePassword(OneTimePasswordModel model) {
//    _isLoading = true;
    _progressDialog(false);
    oneTimePasswordModel = model;
    Constants.OTP = oneTimePasswordModel.selectRegisterUserForgotPasswordResult;
  }

  _updateProfileVerifyUserName() {
    setState(() {
      int errorCode;

      Map<String, dynamic> body2;
      if (isEmail) {
        body2 = {
          'RegisteredUserMasterData': {
            'SessionId': _sessionId,
            'RegisteredUserMasterId': loginModel.registeredUserMasterId,
            'Email': loginModel.email,
            'IsEmailVerify': true,
          }
        };
      } else if (isPhone) {
        body2 = {
          'RegisteredUserMasterData': {
            'SessionId': _sessionId,
            'RegisteredUserMasterId': loginModel.registeredUserMasterId,
            'Phone': loginModel.phone,
            'IsSMSVerify': true,
          }
        };
      }

      var encoder = JSON.encode(body2);
      var url = '${Constants.url}UpdateRegisteredUserMasterPhoneEmailVerified';

      UpdateAccountVerifyUserNameParser updateAccountParser = new UpdateAccountVerifyUserNameParser();
      Future<int> result = updateAccountParser.updateAccount(url, encoder);
      result.then((c) {
        errorCode = c;
        _handleResult(errorCode);
      }).catchError((onError) {
        errorCode = -1;
        setState(() {
          _progressDialog(false);
        });
      });
    });
  }

  void _handleResult(int errorCode) {
    setState(() {
      _progressDialog(false);
      if (errorCode == 0) {
        if (isPhone) {
          PrefUtil.putStringPreference(Constants.prefPhone, loginModel.phone);
          UserModel loginModel1 = new UserModel(
              null,
              loginModel.birthDate,
              loginModel.customerMasterId,
              null,
              loginModel.email,
              null,
              loginModel.firstName,
              loginModel.lastName,
              null,
              loginModel.phone,
              loginModel.gender,
              loginModel.isEmailVerify,
              false,
              true,
              loginModel.profileImageName,
              loginModel.referralCode,
              loginModel.registeredUserMasterId,
              null,
              null,
              null,
              null,
              loginModel.sessionId,
              loginModel.isSelectPhoto,
              loginModel.isUrlPhoto);
          Navigator.pop(context, [loginModel1]);
          subscription == null;
        } else if (isEmail) {
          UserModel loginModel1 = new UserModel(
              null,
              loginModel.birthDate,
              loginModel.customerMasterId,
              null,
              loginModel.email,
              null,
              loginModel.firstName,
              loginModel.lastName,
              null,
              loginModel.phone,
              loginModel.gender,
              true,
              false,
              loginModel.isSMSVerify,
              loginModel.profileImageName,
              loginModel.referralCode,
              loginModel.registeredUserMasterId,
              null,
              null,
              null,
              null,
              loginModel.sessionId,
              loginModel.isSelectPhoto,
              loginModel.isUrlPhoto);
          Navigator.pop(context, [loginModel1]);
          subscription == null;
        }
      } else if (errorCode == -2) {
        Constants.showSnackBar(
            scaffoldStateOTP, Strings.errorAccountAlreadyCreatedOnThisNumber);
      } else if (errorCode == -1) {
        Constants.showSnackBar(scaffoldStateOTP, Strings.errorInServices);
      }
    });
  }
}
