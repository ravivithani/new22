import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/app_version_model.dart';

class AppVersionParser implements AppVersionGet{
  String url;

  AppVersionParser(this.url);

  @override
  Future<AppVersionModel> appVersionGet() async{

    http.Response response = await http.get(url,headers: Constants.httpGetHeader,);
    final statusCode = response.statusCode;
    print('statusCode ---------$statusCode ');
    print('response body ---------${response.body}');
    print('response Decoding ---------${JSON.decode(response.body)}');
    Map selectAppVersion = JSON.decode(response.body);

    if (statusCode != 200 || response == null) {
      throw new Exception(
          "An error ocurred : [Status Code : $statusCode]");
    }

    AppVersionModel user = AppVersionModel.Maping(selectAppVersion);
    return user;
  }
}

//listener for onSuccess and onError
abstract class AppVersionListener {
  void onSuccessAppVersion(AppVersionModel model);
  void onErrorAppVersion();
}

class SetUpAppVersionListener{
  AppVersionListener _view;
  AppVersionParser _parser;
  String url;

  SetUpAppVersionListener(this._view, this.url){
   _parser = userParser;
  }

  void loadAppVersion(){
    _parser.appVersionGet().then((c){
      _view.onSuccessAppVersion(c);
    }).catchError((onError) => _view.onErrorAppVersion());
  }

  //this is use for getting interface from ProdCryptoRepository
  AppVersionParser get userParser {
    return new AppVersionParser(url);
  }
}