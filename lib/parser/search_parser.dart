import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/search_model.dart';
import 'package:crazyrex/util/constants.dart';

class SearchParser {
  String url;
  http.Response response;
  SearchListener listener;

  SearchParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    if (statusCode != 200) {
      listener.onError("status code $statusCode");
    }
    try {
      Map array = JSON.decode(response.body);
      List lstSearch = array['SelectAllSearchDetailsResult'];
      listener.onSearchListener(lstSearch.map((c) => new SearchModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onError(e.toString());
    }
  }
}

abstract class SearchListener {
  void onSearchListener(List<SearchModel> searchList);

  void onError(String error);
}

class SetUpSearchListener {
  String url;
  SearchListener searchListener;
  SearchParser searchParser;

  SetUpSearchListener(this.url, this.searchListener) {
    searchParser = parser;
  }

  void loadAllSearchListData() {
    searchParser.callApi();
  }

  SearchParser get parser {
    return new SearchParser(url, searchListener);
  }
}
