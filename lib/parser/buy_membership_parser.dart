import 'dart:async';
import 'dart:convert';

import 'package:crazyrex/model/paytm_parameter_model.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;

class BuyMembershipParser {
  PaytmParameterModel paytmModel;
  int paymentGatewayTransactionId;
  String url;
  http.Response response;
  BuyMembershipDataListener listener;

  BuyMembershipParser({this.url, this.listener});

  Future callWalletApi() async{
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map array = JSON.decode(response.body);
      String walletBalance = array['SelectRegisteredUserMasterResult']['WalletBalance'];

      listener.onWalletBalance(walletBalance);
    } catch (e) {
      print(e);
      listener.onErrorReferralCode(e.toString());
    }
  }

  Future callApi() async{
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map array = JSON.decode(response.body);
      String firstName = array['SelectRegisteredUserMasterByReferralCodeResult']['FirstName'];
      String lastName = array['SelectRegisteredUserMasterByReferralCodeResult']['LastName'];
      String referralCustomerId = array['SelectRegisteredUserMasterByReferralCodeResult']['CustomerMasterId'];

      String referralUserName = "$firstName $lastName";

      listener.onReferralCode(referralUserName,referralCustomerId);
    } catch (e) {
      print(e);
      listener.onErrorReferralCode(e.toString());
    }
  }

  Future<Map<String, dynamic>> insertCustomerMembershipTran(String url, var encoder, int type) async {
    try {
      var client = new http.Client();
      http.Response response = await client.post(url,
          headers: Constants.httpPostHeader,
          body: encoder);
      final statusCode = response.statusCode;

      Map decoder = JSON.decode(response.body);
      int errorCode = decoder['InsertCustomerMembershipResult']['ErrorCode'];
      int customerMembershipTranId = decoder['InsertCustomerMembershipResult']
          ['objCustomerMembershipTran']['CustomerMembershipTranId'];

      if (statusCode != 200 || response == null) {
        return {'ErrorCode': -1};
      }

      if (type != 1) {
        if (customerMembershipTranId != 0) {
          Map objPaymentGatewayTransactions =
              decoder['InsertCustomerMembershipResult']
                  ['objPaymentGatewayTransactions'];
          paymentGatewayTransactionId =
              objPaymentGatewayTransactions['PaymentGatewayTransactionId'];

          Map objPaytm = decoder['InsertCustomerMembershipResult']['objPaytm'];
          paytmModel = PaytmParameterModel.fromMap(objPaytm);
          return {
            'model': paytmModel,
            'CustomerMembershipTranId': customerMembershipTranId,
            'paymentGatewayTransactionId': paymentGatewayTransactionId
          };
        } else {
          return {'ErrorCode': -1};
        }
      }

      return {
        'ErrorCode': errorCode,
        'CustomerMembershipTranId': customerMembershipTranId
      };
    } catch (e) {
      return {'ErrorCode': -1};
    }
  }

  Future<int> updateCustomerMembership(String url, var encoder) async {
    try {
      var client = new http.Client();
      http.Response response = await client.post(url,
          headers: Constants.httpPostHeader,
          body: encoder);

      Map decoder = JSON.decode(response.body);
      int errorCode = decoder['UpdateCustomerMembershipResult']['ErrorCode'];
      return errorCode;
    } catch (e) {
      print(e);
      return -1;
    }
  }


}
abstract class BuyMembershipDataListener{
  void onWalletBalance(String walletBalance);
  void onReferralCode(String referralUserName,String referralCustomerId);
  void onErrorReferralCode(onError);
}

class SetUpBuyMembershipListenerData{
  String url;
  BuyMembershipParser allBuyMembershipParser;
  BuyMembershipDataListener buyMembershipDataListener;


  SetUpBuyMembershipListenerData(this.url, this.buyMembershipDataListener){
    allBuyMembershipParser = buyMembershipParser;
  }

  void loadReferralCode(){
    allBuyMembershipParser.callApi();
  }
  void loadWalletBalance(){
    allBuyMembershipParser.callWalletApi();
  }

  BuyMembershipParser get buyMembershipParser{
    return new BuyMembershipParser(url: url,listener: buyMembershipDataListener);
  }
}