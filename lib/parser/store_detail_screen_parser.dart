import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:crazyrex/model/been_fav_store_model.dart';
import 'package:crazyrex/model/hour_store_detail_model.dart';
import 'package:crazyrex/model/info_question__store_detail_model.dart';
import 'package:crazyrex/model/object_store_detail_model.dart';
import 'package:crazyrex/model/offerlist_model.dart';
import 'package:crazyrex/model/review_store_model.dart';
import 'package:crazyrex/util/constants.dart';

class StoreDetailParser{
  String url;
  http.Response response;
  StoreDetailDataListener listener;

  StoreDetailParser(this.url, this.listener);

  Future callApi() async{
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map storeDetailResult  = JSON.decode(response.body);
      Map selectAllDetailResult = storeDetailResult['SelectAllBusinessDetailMasterResult'];

      List lstGallery = selectAllDetailResult['lstBusinessGalleryTran'];
      List lstHour = selectAllDetailResult['lstBusinessHoursTran'];
      List lstInfoAnswer = selectAllDetailResult['lstBusinessInfoAnswerMaster'];
      List lstMenu = selectAllDetailResult['lstBusinessMenuTran'];
      List lstCheckIn = selectAllDetailResult['lstCheckinMaster'];
      List lstOffer = selectAllDetailResult['lstOfferMaster'];
      List lstReview = selectAllDetailResult['lstReviewMaster'];
      ObjectStoreDetailModel model = ObjectStoreDetailModel.fromMap(selectAllDetailResult['objBusinessMaster']);

      List<String> gallery = [];
      List<String> menu = [];
      for (int i = 0; i < lstGallery.length; i++) {
        gallery.add(lstGallery[i]['sm_ImageName']);
      }
      listener.onGalleryListener(gallery);

      for (int i = 0; i < lstMenu.length; i++) {
        menu.add(lstMenu[i]['sm_ImageName']);
      }
      listener.onMenuListener(menu);
      listener.onHourListener(lstHour.map((c)=> new  HourStoreDetailModel.fromMap(c)).toList());
      listener.onInfoAnswerListener(lstInfoAnswer.map((c)=> new InfoAnswerStoreModel.fromMap(c)).toList());
      listener.onCheckInListener(lstCheckIn.map((c)=> new BeenFavStoreModel.fromMap(c)).toList());
      listener.onOfferListener(lstOffer.map((c)=> new OfferListModel.fromMap(c)).toList());
      listener.onReviewListener(lstReview.map((c)=> new ReviewStoreModel.fromMap(c)).toList());
      listener.onObjectDataListener(model);
    } catch (e) {
      print(e);
      listener.onErrorStoreDetail(e.toString());
    }

  }

  static Future<Map> insertCheckIn(String url, var encoder) async {
    var client = new http.Client();
    http.Response response = await client.post(url,
        headers: Constants.httpPostHeader,
        body: encoder);
    final statusCode = response.statusCode;

    Map decoder = JSON.decode(response.body);
    int errorCode = decoder['InsertCheckinMasterResult']['ErrorCode'];

    if (statusCode != 200 || response == null) {
      return decoder;
    }
    print('ErrorCode $errorCode');
    print('decoder $decoder');
    return decoder;
  }

  static Future<int> deleteCheckIn(String url, var encoder) async {
    var client = new http.Client();
    http.Response response = await client.post(url,
        headers: Constants.httpPostHeader,
        body: encoder);
    final statusCode = response.statusCode;

    Map decoder = JSON.decode(response.body);
    int errorCode = decoder['DeleteCheckinMasterResult']['ErrorCode'];

    if (statusCode != 200 || response == null) {
      return -1;
    }
    print('ErrorCode $errorCode');
    return errorCode;
  }

  static Future<int> deleteReview(String url, var encoder) async{
    var client = new http.Client();
    http.Response response = await client.post(url,
        headers: Constants.httpPostHeader,
        body: encoder);
    final statusCode = response.statusCode;

    Map decoder = JSON.decode(response.body);
    int errorCode = decoder['DeleteReviewMasterResult']['ErrorCode'];

    if (statusCode != 200 || response == null) {
      return -1;
    }
    print('ErrorCode $errorCode');
    return errorCode;
  }
}
abstract class StoreDetailDataListener{
  void onOfferListener(List<OfferListModel> offerList);
  void onMenuListener(List<String> menuList);
  void onGalleryListener(List<String> galleryList);
  void onHourListener(List<HourStoreDetailModel> hourList);
  void onInfoAnswerListener(List<InfoAnswerStoreModel> infoQuestionList);
  void onCheckInListener(List<BeenFavStoreModel> checkInList);
  void onReviewListener(List<ReviewStoreModel> reviewList);
  void onObjectDataListener(ObjectStoreDetailModel objectStoreDetailModel);
  void onErrorStoreDetail(onError);
}
class SetUpStoreDetailScreenListener{
  String url;
  StoreDetailDataListener detailDataListener;
  StoreDetailParser allDetailParser;

  SetUpStoreDetailScreenListener(this.url, this.detailDataListener){
    allDetailParser = detailParser;
  }

  void loadStoreDetail(){
    allDetailParser.callApi();
  }
  StoreDetailParser get detailParser{
    return new StoreDetailParser(url, detailDataListener);
  }

}