import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:crazyrex/model/exist_email_or_phone_model.dart';
import 'package:crazyrex/util/constants.dart';


class ExistEmailParser implements ExistEmailOrPhoneGet{
  String url;

  ExistEmailParser(this.url);

  @override
  Future<ExistEmailOrPhoneModel> existEmailOrPhoneGet() async{
    http.Response response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    print('statusCode ---------$statusCode ');
    print('response body ---------${response.body}');
    print('response Decoding ---------${JSON.decode(response.body)}');
    Map selectExist = JSON.decode(response.body);

    if (statusCode != 200 || response == null) {
      throw new Exception(
          "An error ocurred : [Status Code : $statusCode]");
    }

    ExistEmailOrPhoneModel user = ExistEmailOrPhoneModel.Maping(selectExist);
    return user;
  }
}
abstract class ExistEmailListener{
  void onSuccessExistEmail(ExistEmailOrPhoneModel model);
  void onErrorExistEmail();
}
class SetUpExistEmailListener{
  ExistEmailListener _view;
  ExistEmailParser _parser;
  String url;

  SetUpExistEmailListener(this._view,this.url){
    _parser = userParser;
  }
  void loadExistEmail(){
    _parser.existEmailOrPhoneGet().then((c){
      _view.onSuccessExistEmail(c);
    }).catchError((onError) => _view.onErrorExistEmail());
  }
  ExistEmailParser get userParser{
    return new ExistEmailParser(url);
  }
}
