import 'package:crazyrex/model/wallet_history_model.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:crazyrex/util/constants.dart';

class WalletHistoryParser{
  String url;
  http.Response response;
  WalletHistoryDataListener listener;

  WalletHistoryParser(this.url, this.listener);

  Future callApi() async{
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map array = JSON.decode(response.body);
      List lstStoreList = array['SelectAllWalletTransactionsResult'];
      listener.onWalletHistory(lstStoreList.map((c) => new WalletHistoryModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onErrorWalletHistory(e.toString());
    }
  }
}
abstract class WalletHistoryDataListener{
  void onWalletHistory(List<WalletHistoryModel> walletHistoryList);
  void onErrorWalletHistory(onError);
}
class SetUpWalletHistoryListenerData{
  String url;
  WalletHistoryDataListener historyDataListener;
  WalletHistoryParser allHistoryParser;

  SetUpWalletHistoryListenerData(this.url, this.historyDataListener){
    allHistoryParser = historyParser;
  }
  void loadWalletHistory(){
    allHistoryParser.callApi();
  }

  WalletHistoryParser get historyParser{
    return new WalletHistoryParser(url, historyDataListener);
  }

}