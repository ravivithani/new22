import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/user_model.dart';

class UserMasterParser implements GetUser{
  String url;

  UserMasterParser(this.url);

  @override
  Future<UserModel> getUser() async {
    http.Response response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    Map selectRegister = JSON.decode(response.body);
    Map select = selectRegister['SelectRegisteredUserMasterResult'];

    if (statusCode != 200 || response == null) {
      throw new HandleException(
          "An error ocurred : [Status Code : $statusCode]");
    }

    UserModel user = UserModel.Maping(select);
    return user;
  }

}
abstract class SelectUserListener {
  void onSuccessGetUserData(UserModel model);
  void onErrorGetUserData(onError);
}
class SetUpUserListener{
  SelectUserListener _view;
  UserMasterParser _parser;
  String url;

  SetUpUserListener(this._view, this.url){
    _parser = userParser;
  }

  void loadUserMaster(){
    _parser.getUser().then((c){
      _view.onSuccessGetUserData(c);
    }).catchError((onError) => _view.onErrorGetUserData(onError));
  }

  //this is use for getting interface from ProdCryptoRepository
  UserMasterParser get userParser{
    return new UserMasterParser(url);
  }
}