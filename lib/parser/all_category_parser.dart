import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/category_model.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;

class AllCategoryListParser{
  String url;
  http.Response response;
  AllCategoryDataListener listener;

  AllCategoryListParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get("$url",headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    if(statusCode != 200){
      listener.onErrorCategoryListener("status code $statusCode");
    }
    try {
      Map array = JSON.decode(response.body);
      List lstCategoryList = array['SelectAllBusinessTypeMasterResult'];
      List<CategoryModel> list = lstCategoryList.map((c) =>new CategoryModel.fromMap(c)).toList();
      listener.onCategoryListener(list);
    } catch (e) {
      print(e);
      listener.onErrorCategoryListener(e.toString());
    }

  }
}
abstract class AllCategoryDataListener{
  void onCategoryListener(List<CategoryModel> categoryList);
  void onErrorCategoryListener(String error);
}

class SetUpAllCategoryListenerData{
  String url;
  AllCategoryDataListener allCategoryDataListener;
  AllCategoryListParser allCategoryListParser;


  SetUpAllCategoryListenerData(this.url, this.allCategoryDataListener){
    allCategoryListParser = CategoryParser;
  }
  void loadAllCategoryListData(){
    allCategoryListParser.callApi().then((value){print("value $value");}).catchError((error){allCategoryDataListener.onErrorCategoryListener(error.toString());});
  }

  AllCategoryListParser get CategoryParser{
    return new AllCategoryListParser(url,allCategoryDataListener);
  }

}