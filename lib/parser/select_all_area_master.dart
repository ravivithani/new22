import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/area_model.dart';
import 'package:crazyrex/util/constants.dart';

class SelectAllAreaMasterParser{
  String url;
  http.Response response;
  SelectAllAreaListener listener;

  SelectAllAreaMasterParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map array = JSON.decode(response.body);
      List lstAreaList = array['SelectAllAreaMasterResult'];

      listener.onAreaListener(lstAreaList.map((c) => new AreaModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onErrorSelectAllAreaListener(e.toString());
    }

  }
}

abstract class SelectAllAreaListener{
  void onAreaListener(List<AreaModel> areaList);
  void onErrorSelectAllAreaListener(String error);
}

class SetAllAreaListenerData{
  String url;
  SelectAllAreaListener allAreaListener;
  SelectAllAreaMasterParser allAreaMasterParser;

  SetAllAreaListenerData(this.url, this.allAreaListener){
    allAreaMasterParser = areaParser;
  }

  void loadAllStoreListData(){
    allAreaMasterParser.callApi();
  }

  SelectAllAreaMasterParser get areaParser {
    return new SelectAllAreaMasterParser(url,allAreaListener);
  }

}