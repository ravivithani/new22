import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/review_store_model.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/util/constants.dart';


class ReviewStoreParser{
  String url;
  http.Response response;
  ReviewStoreDataListener listener;

  ReviewStoreParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map array = JSON.decode(response.body);

      List lstStoreList = array['SelectAllReviewMasterResult'];

      listener.onReviewStoreListener(lstStoreList.map((c) => new ReviewStoreModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onErrorReviewStore(e.toString());
    }

  }
}
abstract class ReviewStoreDataListener{
  void onReviewStoreListener(List<ReviewStoreModel> reviewList);
  void onErrorReviewStore(onError);
}

class SetUpReviewStoreListenerData{
  String url;
  ReviewStoreDataListener reviewStoreDataListener;
  ReviewStoreParser allReviewStoreParser;

  SetUpReviewStoreListenerData(this.url, this.reviewStoreDataListener,){
    allReviewStoreParser = reviewStoreParser;
  }

  void loadReviewStoreData(){
    allReviewStoreParser.callApi();
  }
  ReviewStoreParser get reviewStoreParser {
    return new ReviewStoreParser(url,reviewStoreDataListener);
  }
}