import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:crazyrex/model/advertisement_model.dart';
import 'package:crazyrex/model/category_model.dart';
import 'package:crazyrex/model/store_list_model.dart';
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/util/constants.dart';

class HomeScreeDataParser {
  String url;
  http.Response response;

  AdvertisementModel advertisementModel;
  HomeScreenDataListener listener;

  HomeScreeDataParser(this.url,this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map homeDetailsResult = JSON.decode(response.body);
      Map selectAllHomeDetailsResult = homeDetailsResult['SelectAllHomeDetailsResult'];

      List lstAdvertisement = selectAllHomeDetailsResult['lstAdvertisementMaster'];
      List lstBusinessTypeMaster = selectAllHomeDetailsResult['lstBusinessTypeMaster'];
      List lstRecentAddedBusinessMasterDAL = selectAllHomeDetailsResult['lstRecentAddedBusinessMasterDAL'];


      listener.onAdvertisementListener(lstAdvertisement.map((c) => new AdvertisementModel.fromMap(c)).toList());
      listener.onStoreListener(lstRecentAddedBusinessMasterDAL.map((c) => new StoreListModel.fromMap(c)).toList());
      listener.onCategoryListener(lstBusinessTypeMaster.map((c) => new CategoryModel.fromMap(c)).toList());
      UserModel model = UserModel.Maping(selectAllHomeDetailsResult['objRegisteredUserMaster']);
      listener.onUserListener(model);
      listener.onCustomerMembershipListener(selectAllHomeDetailsResult['objCustomerMembershipTran']['CustomerMembershipTranId'],selectAllHomeDetailsResult['objCustomerMembershipTran']['MembershipLevel']);
    } catch (e) {
      print(e);
      listener.onErrorHomeScreenDataListener(e.toString());
    }
  }
}

abstract class HomeScreenDataListener {
  void onAdvertisementListener(List<AdvertisementModel> advertisementList);
  void onCategoryListener(List<CategoryModel> catList);
  void onStoreListener(List<StoreListModel> storeList);
  void onUserListener(UserModel userModel);
  void onCustomerMembershipListener(int membershipTranId,int membershipLevel);
  void onErrorHomeScreenDataListener(String error);
}

class setUpHomeScreenListner {
  String url;
  HomeScreenDataListener homeScreenDataListener;
  HomeScreeDataParser homeScreeDataParser;

  setUpHomeScreenListner(this.homeScreenDataListener, this.url) {
    homeScreeDataParser = homeParser;
  }

  void loadHomeScreenData(){
    homeScreeDataParser.callApi();
  }

  HomeScreeDataParser get homeParser {
    return new HomeScreeDataParser(url,homeScreenDataListener);
  }
}
