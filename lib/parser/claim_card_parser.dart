import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:crazyrex/util/constants.dart';

class ClaimCardParser{
  String url;
  http.Response response;
  ClaimCardDataListener listener;

  ClaimCardParser(this.url, this.listener);

  Future callApi() async{
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map array = JSON.decode(response.body);

      String cardExpiryDate = array['SelectCustomerMembershipTranResult']['CardExpiryDate'];
      String cardNumber = array['SelectCustomerMembershipTranResult']['CardNumber'];
      String membershipTypeName = array['SelectCustomerMembershipTranResult']['MemberShipTypeName'];
      String customerName = array['SelectCustomerMembershipTranResult']['CustomerName'];
      String phoneNumber = array['SelectCustomerMembershipTranResult']['Phone'];
      String profileImageName = array['SelectCustomerMembershipTranResult']['ImageName'];

      listener.onClaimCardData(cardExpiryDate, cardNumber, membershipTypeName,customerName,phoneNumber,profileImageName);
    } catch (e) {
      print(e);
      listener.onErrorClaimCard(e.toString());
    }
  }

}
abstract class ClaimCardDataListener{
  void onClaimCardData(String cardExpiryDate,String cardNumber,String membershipTypeName,String customerName,String phoneNumber,String profileImageName);
  void onErrorClaimCard(onError);
}
class SetUpClaimCardListenerData{
  String url;
  ClaimCardParser allClaimCardParser;
  ClaimCardDataListener cardListenerListener;

  SetUpClaimCardListenerData(this.url, this.cardListenerListener){
    allClaimCardParser = claimCardParser;
  }

  void loadClaimCard(){
    allClaimCardParser.callApi();
  }

  ClaimCardParser get claimCardParser{
    return new ClaimCardParser(url,cardListenerListener );
  }

}