import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/city_list_model.dart';

class SelectCityParser implements GetCitySelection{
  String url;
  SelectCityParser(this.url);

  @override
  Future<List<CityListModel>> getCitySelection() async{
    http.Response response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    Map array = JSON.decode(response.body);
    List responseBody = array['SelectAllCityMasterResult'];

    if (statusCode != 200 || responseBody == null) {
      throw new HandleException("An error ocurred : [Status Code : $statusCode]");
    }

    return responseBody.map((c) => new CityListModel.fromMap(c)).toList();
  }

  static Future<int> insertCity(String url, var encoder) async {
    var client = new http.Client();
    http.Response response = await client.post(url,
        headers: Constants.httpPostHeader,
        body: encoder);
    final statusCode = response.statusCode;

    Map decoder = JSON.decode(response.body);
    int errorCode = decoder['UpdateCustomerMasterCityResult']['ErrorCode'];

    if (statusCode != 200 || response == null) {
      return -1;
    }
    print('ErrorCode $errorCode');
    return errorCode;
  }
}

abstract class CityListener {
  void onSuccessCityList(List<CityListModel> items);

  void onErrorCityList();
}

class SetUpCityListener {
  CityListener _view;
  SelectCityParser _parser;
  String URL;

  SetUpCityListener(this._view, this.URL) {
    _parser = cityParser ;
  }

  void loadCities() {
    _parser.getCitySelection().then((c) {
      _view.onSuccessCityList(c);
    }).catchError((onError) => _view.onErrorCityList());
  }

  //this is use for getting interface from ProdCryptoRepository
  SelectCityParser get cityParser {
    return new SelectCityParser(URL);
  }
}
