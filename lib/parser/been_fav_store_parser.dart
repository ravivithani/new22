import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/been_fav_store_model.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;


class BeenFavStoreParser{
  String url;
  http.Response response;
  BeenFavStoreDataListener listener;

  BeenFavStoreParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get("$url",headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map array = JSON.decode(response.body);

      List lstStoreList = array['SelectAllCheckinMasterResult'];

      listener.onBeenFavStoreListener(lstStoreList.map((c) => new BeenFavStoreModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onErrorBeenFavStore(e.toString());
    }

  }
}
abstract class BeenFavStoreDataListener{
  void onBeenFavStoreListener(List<BeenFavStoreModel> historyList);
  void onErrorBeenFavStore(onError);
}

class SetUpBeenFavStoreListenerData{
  String url;
  BeenFavStoreDataListener beenFavStoreDataListener;
  BeenFavStoreParser allBeenFavStoreParser;

  SetUpBeenFavStoreListenerData(this.url, this.beenFavStoreDataListener,){
    allBeenFavStoreParser = beenFavStoreParser;
  }

  void loadBeenFavStoreData(){
    allBeenFavStoreParser.callApi();
  }
  BeenFavStoreParser get beenFavStoreParser {
    return new BeenFavStoreParser(url,beenFavStoreDataListener);
  }
}