import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/offerlist_model.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/util/constants.dart';

class OfferListParser{
  String url;
  String resultString;
  OfferListListener listener;
  http.Response response;

  OfferListParser(this.url, this.resultString, this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    if(statusCode != 200){
      listener.onErrorOfferListListener("status code $statusCode");
    }
    try {
      Map array = JSON.decode(response.body);
      List lstOffer = array[resultString];
      List<OfferListModel> list = lstOffer.map((c) =>new OfferListModel.fromMap(c)).toList();
      listener.onOfferListener(list);
    } catch (e) {
      print(e);
      listener.onErrorOfferListListener(e.toString());
    }

  }
}

abstract class OfferListListener{
  void onOfferListener(List<OfferListModel> offerList);
  void onErrorOfferListListener(String error);
}

class SetUpOfferListListener{
  String url;
  String resultString;
  OfferListParser listParser;
  OfferListListener listListener;

  SetUpOfferListListener(this.url, this.resultString, this.listListener){
    listParser = parser;
  }
  void loadOfferList(){
    listParser.callApi();
  }
  OfferListParser get parser{
    return new OfferListParser(url,resultString,listListener);
  }
}