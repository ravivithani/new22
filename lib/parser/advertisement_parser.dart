import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/advertisement_model.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;

class AdvertisementParser{
  String url;
  AdvertisementListener listener;

  http.Response response;

  AdvertisementParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get("$url",headers: Constants.httpGetHeader,);
    final statusCode = response.statusCode;
    if(statusCode != 200){
      listener.onErrorAdvertisement("status code $statusCode");
    }
    try {
      Map array = JSON.decode(response.body);
      List lstCategoryList = array['SelectAllAdvertisementMasterDALResult'];
      List<AdvertisementModel> list = lstCategoryList.map((c) =>new AdvertisementModel.fromMap(c)).toList();
      listener.onGetAdvertisementList(list);
    } catch (e) {
      print(e);
      listener.onErrorAdvertisement(e.toString());
    }

  }

}

abstract class AdvertisementListener{
  void onGetAdvertisementList(List<AdvertisementModel> adList);
  void onErrorAdvertisement(String error);
}

class SetUpAdvertisementListener{
  String url;
  AdvertisementParser advertisementParser;
  AdvertisementListener advertisementListener;

  SetUpAdvertisementListener(this.url, this.advertisementListener){
    advertisementParser = parser;
  }

  void loadAds(){
    advertisementParser.callApi();
  }

  AdvertisementParser get parser{
    return new AdvertisementParser(url,advertisementListener);
  }

}