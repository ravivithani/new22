import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:crazyrex/model/exist_email_or_phone_model.dart';
import 'package:crazyrex/util/constants.dart';


class ExistPhoneParser implements ExistEmailOrPhoneGet{
  String url;

  ExistPhoneParser(this.url);

  @override
  Future<ExistEmailOrPhoneModel> existEmailOrPhoneGet() async{
    http.Response response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    print('statusCode ---------$statusCode ');
    print('response body ---------${response.body}');
    print('response Decoding ---------${JSON.decode(response.body)}');
    Map selectExist = JSON.decode(response.body);

    if (statusCode != 200 || response == null) {
      throw new Exception(
          "An error ocurred : [Status Code : $statusCode]");
    }

    ExistEmailOrPhoneModel user = ExistEmailOrPhoneModel.Maping(selectExist);
    return user;
  }
}
abstract class ExistPhoneListener{
  void onSuccessExistPhone(ExistEmailOrPhoneModel model);
  void onErrorExistPhone();
}
class SetUpExistPhoneListener{
  ExistPhoneListener _view;
  ExistPhoneParser _parser;
  String url;

  SetUpExistPhoneListener(this._view,this.url){
    _parser = userParser;
  }
  void loadExistPhone(){
    _parser.existEmailOrPhoneGet().then((c){
      _view.onSuccessExistPhone(c);
    }).catchError((onError) => _view.onErrorExistPhone());
  }
  ExistPhoneParser get userParser{
    return new ExistPhoneParser(url);
  }
}
