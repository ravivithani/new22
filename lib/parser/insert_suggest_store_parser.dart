import 'dart:async';
import 'dart:convert';

import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;

class InsertSuggestStoreParser {

  Future<int> insertSuggestStore(String url, var encoder) async {
    var client = new http.Client();
    http.Response response = await client.post(url,
        headers: Constants.httpPostHeader,
        body: encoder);
    final statusCode = response.statusCode;

    Map decoder = JSON.decode(response.body);
    int errorCode = decoder['InsertSuggestionMasterResult']['ErrorCode'];

    if (statusCode != 200 || response == null) {
      return -1;
    }
    print('ErrorCode $errorCode');
    return errorCode;
  }
}
