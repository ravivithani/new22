import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/util/constants.dart';

class LoginParser implements GetUser{
  String url;

  LoginParser(this.url);

  @override
  Future<UserModel> getUser()async {

    http.Response response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    Map selectRegister = JSON.decode(response.body);
    Map select = selectRegister['SelectRegisteredUserMasterByUserNameResult'];

    if (statusCode != 200 || response == null) {
      throw new HandleException(
          "An error ocurred : [Status Code : $statusCode]");
    }

    UserModel user = UserModel.Maping(select);
    return user;
  }
}

//listener for onSuccess and onError
abstract class LoginListener {
  void onSuccessLogin(UserModel model);
  void onErrorLogin(onError);
}

class SetUpLoginListener{
  LoginListener _view;
  LoginParser _parser;
  String url;

  SetUpLoginListener(this._view, this.url){
    _parser = loginParser;
  }

  void loadLogin(){
    _parser.getUser().then((c){
      _view.onSuccessLogin(c);
    }).catchError((onError) => _view.onErrorLogin(onError));
  }

  //this is use for getting interface from ProdCryptoRepository
  LoginParser get loginParser {
    return new LoginParser(url);
  }
}