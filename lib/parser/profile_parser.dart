import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/user_model.dart';
import 'package:crazyrex/util/constants.dart';

class ProfileParser implements GetUser{
  String url;

  ProfileParser(this.url);

  @override
  Future<UserModel> getUser()async {

    http.Response response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    Map selectRegister = JSON.decode(response.body);
    Map select = selectRegister['SelectRegisteredUserMasterResult'];

    if (statusCode != 200 || response == null) {
      throw new HandleException(
          "An error ocurred : [Status Code : $statusCode]");
    }

    UserModel user = UserModel.Maping(select);
    return user;
  }
}

//listener for onSuccess and onError
abstract class ProfileListener {
  void onSuccessProfile(UserModel model);
  void onErrorProfile(onError);
}

class SetUpProfileListener{
  ProfileListener _view;
  ProfileParser _parser;
  String url;

  SetUpProfileListener(this._view, this.url){
    _parser = profileParser;
  }

  void loadProfile(){
    _parser.getUser().then((c){
      _view.onSuccessProfile(c);
    }).catchError((onError) => _view.onErrorProfile(onError));
  }
  //this is use for getting interface from ProdCryptoRepository
  ProfileParser get profileParser {
    return new ProfileParser(url);
  }
}