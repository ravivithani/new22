import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/referral_code_model.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/user_model.dart';

class ReferralCodeParser implements ReferralCodeGet{
  String url;

  ReferralCodeParser(this.url);

  @override
  Future<ReferralCodeModel> referralCodeGet()async {

    http.Response response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    Map selectRegister = JSON.decode(response.body);
    Map select = selectRegister['SelectRegisteredUserMasterByReferralCodeResult'];

    if (statusCode != 200 || response == null) {
      throw new HandleException(
          "An error ocurred : [Status Code : $statusCode]");
    }
    ReferralCodeModel user = ReferralCodeModel.Maping(select);
    return user;
  }

  @override
  Future<bool> deviceIdExistCall()async {

    http.Response response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    Map selectRegister = JSON.decode(response.body);
    bool select = selectRegister['SelectRegisterUserMasterAndroidDeviceIdExistOrNotResult'];

    if (statusCode != 200 || response == null) {
      throw new HandleException("An error ocurred : [Status Code : $statusCode]");
    }
    return select;
  }
}

//listener for onSuccess and onError
abstract class ReferralCodeListener {
  void onSuccessReferralCode(ReferralCodeModel model);
  void onSuccessDeviceExist(bool isDeviceExist);
  void onErrorReferralCode();
}

class SetUpReferralCodeListener{
  ReferralCodeListener _view;
  ReferralCodeParser _parser;
  String url;

  SetUpReferralCodeListener(this._view, this.url){
    _parser = loginParser;
  }

  void loadReferralCode(){
    _parser.referralCodeGet().then((c){
      _view.onSuccessReferralCode(c);
    }).catchError((onError) => _view.onErrorReferralCode());
  }
  void loadDeviceExist(){
    _parser.deviceIdExistCall().then((c){
      _view.onSuccessDeviceExist(c);
    }).catchError((onError) => _view.onErrorReferralCode());
  }

  //this is use for getting interface from ProdCryptoRepository
  ReferralCodeParser get loginParser {
    return new ReferralCodeParser(url);
  }
}