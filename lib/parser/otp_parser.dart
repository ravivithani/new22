import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/otp_model.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/util/constants.dart';

class OneTimePasswordParser implements OneTimePasswordGet{
  String url;

  OneTimePasswordParser(this.url);

  @override
  Future<OneTimePasswordModel> oneTimePasswordGet() async {
    http.Response response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    print('statusCode ---------$statusCode ');
    print('response body ---------${response.body}');
    print('response Decoding ---------${JSON.decode(response.body)}');
    Map selectExist = JSON.decode(response.body);

    if (statusCode != 200 || response == null) {
      throw new Exception(
          "An error ocurred : [Status Code : $statusCode]");
    }
    OneTimePasswordModel user = OneTimePasswordModel.Maping(selectExist);
    return user;
  }
}
abstract class OneTimePasswordListener{
  void onSuccessOneTimePassword(OneTimePasswordModel model);
  void onErrorOneTimePassword(onError);
}
class SetUpOneTimePasswordListener{
  OneTimePasswordListener _view;
  OneTimePasswordParser _parser;
  String url;

  SetUpOneTimePasswordListener(this._view,this.url){
    _parser = userParser;
  }
  void loadOneTimePassword(){
    _parser.oneTimePasswordGet().then((c){
      _view.onSuccessOneTimePassword(c);
    }).catchError((onError) => _view.onErrorOneTimePassword(onError));
  }
  OneTimePasswordParser get userParser{
    return new OneTimePasswordParser(url);
  }
}
