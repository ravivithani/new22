import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/outlet_list_model.dart';
import 'package:crazyrex/util/constants.dart';

class OutletListParser{
  String url;
  http.Response response;
  OutletListDataListener listener;

  OutletListParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    if(statusCode != 200){
      listener.onErrorOutletList("status code $statusCode");
    }
    try {
      Map array = JSON.decode(response.body);
      List lstStoreList;
        lstStoreList = array['SelectAllBusinessMasterByGroupResult'];

      listener.onOutletListListener(lstStoreList.map((c) => new OutletListModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onErrorOutletList(e.toString());
    }

  }
}
abstract class OutletListDataListener{
  void onOutletListListener(List<OutletListModel> outletList);
  void onErrorOutletList(String error);
}
class SetUpOutletListData{
  String url;
  OutletListDataListener outletListDataListener;
  OutletListParser allOutletListParser;

  SetUpOutletListData(this.url, this.outletListDataListener){
    allOutletListParser = outletListParser;
  }

  voidLoadOutletListData(){
    allOutletListParser.callApi();
  }

  OutletListParser get outletListParser{
    return new OutletListParser(url, outletListDataListener);
  }

}