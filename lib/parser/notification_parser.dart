import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/notification_model.dart';
import 'package:crazyrex/util/constants.dart';

class NotificationParser {
  String url;
  NotificationListListener listener;
  http.Response response;

  NotificationParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    if (statusCode != 200) {
      listener.onErrorNotificationList("status code $statusCode");
    }
    try {
      Map array = JSON.decode(response.body);
      List lstNotification = array['SelectAllNotificationTranResult'];
      List<NotificationModel> list = lstNotification.map((c) => new NotificationModel.fromMap(c)).toList();
      listener.onGetNotificationList(list);
    } catch (e) {
      print(e);
      listener.onErrorNotificationList(e.toString());
    }
  }
}

abstract class NotificationListListener {
  void onGetNotificationList(List<NotificationModel> list);

  void onErrorNotificationList(String error);
}

class SetUpNotificationListener {
  NotificationParser notificationParser;
  NotificationListListener notificationListener;
  String url;


  SetUpNotificationListener(this.notificationListener, this.url){
    notificationParser = parser;
  }

  void loadNotificationList(){
    notificationParser.callApi();
  }

  NotificationParser get parser{
    return new NotificationParser(url,notificationListener);
  }
}
