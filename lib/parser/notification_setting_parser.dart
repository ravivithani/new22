import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/history_model.dart';
import 'package:crazyrex/model/notification_setting_model.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/util/constants.dart';


class NotificationSettingParser{
  String url;
  http.Response response;
  NotificationDataListener listener;

  NotificationSettingParser(this.url, this.listener);

  Future callApi() async{
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    try {
      Map array = JSON.decode(response.body);

      List lstStoreList = array['SelectAllRegisteredUserSettingTranResult'];

      listener.onNotificationSettingListener(lstStoreList.map((c) => new NotificationSettingModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onErrorNotificationSetting(e.toString());
    }
  }

  static Future<int> updateNotificationSetting(String url, var encoder) async {
    var client = new http.Client();
    http.Response response = await client.post(url,
        headers: Constants.httpPostHeader,
        body: encoder);
    final statusCode = response.statusCode;

    Map decoder = JSON.decode(response.body);
    int errorCode = decoder['UpdateRegisteredUserSettingTranResult']['ErrorCode'];

    if (statusCode != 200 || response == null) {
      return -1;
    }
    print('ErrorCode $errorCode');
    return errorCode;
  }

}

abstract class NotificationDataListener{
  void onNotificationSettingListener(List<NotificationSettingModel> notificationSettingModel);
  void onErrorNotificationSetting(onError);

}

class SetUpNotificationSettingListenerData{
  String url;
  NotificationDataListener notificationDataListener;
  NotificationSettingParser allNotificationSettingParser;

  SetUpNotificationSettingListenerData(this.url, this.notificationDataListener){
    allNotificationSettingParser = notificationSettingParser;
  }

  void loadNotificationSettingData(){
    allNotificationSettingParser.callApi();
  }
  NotificationSettingParser get notificationSettingParser {
    return new NotificationSettingParser(url,notificationDataListener);
  }

}