import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/offer_master_obj_model.dart';
import 'package:crazyrex/model/store_list_model.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/util/constants.dart';

class OfferDescriptionParser{
  String url;
  OfferDescriptionListener listener;
  http.Response response;


  OfferDescriptionParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map map = JSON.decode(response.body);
      Map result = map['SelectOfferMasterResult'];

      List lstBusinessMasterDAL = result['lstBusinessMasterDAL'];
      Map objOfferMaster = result['objOfferMaster'];

      OfferMasterObjectModel model = OfferMasterObjectModel.fromMap(objOfferMaster);
      List<StoreListModel> storeList = lstBusinessMasterDAL.map((c) => new StoreListModel.fromMap(c)).toList();

      listener.onGetStoreList(storeList);
      listener.onGetOfferMasterObjectModel(model);

    } catch (e) {
      print(e);
      listener.onErrorOfferDescriptionListener(e.toString());
    }
  }

  static Future<int> offerFavorite(String url, var encoder) async {
    var client = new http.Client();
    http.Response response = await client.post(url,
        headers: Constants.httpPostHeader,
        body: encoder);
    final statusCode = response.statusCode;

    Map decoder = JSON.decode(response.body);
    int errorCode = decoder['InsertCustomerOfferTranResult']['ErrorCode'];

    if (statusCode != 200 || response == null) {
      return -1;
    }
    print('ErrorCode $errorCode');
    return errorCode;
  }

  static Future<int> offerDeleteFavorite(String url, var encoder) async {
    var client = new http.Client();
    http.Response response = await client.post(url,
        headers: Constants.httpPostHeader,
        body: encoder);
    final statusCode = response.statusCode;

    Map decoder = JSON.decode(response.body);
    int errorCode = decoder['DeleteCustomerOfferTranResult']['ErrorCode'];

    if (statusCode != 200 || response == null) {
      return -1;
    }
    print('ErrorCode $errorCode');
    return errorCode;
  }

}
abstract class OfferDescriptionListener{
  void onGetStoreList(List<StoreListModel> storeList);
  void onGetOfferMasterObjectModel(OfferMasterObjectModel offerMasterObjectModel);
  void onErrorOfferDescriptionListener(String error);
}
class SetUpOfferDescriptionListener{
  OfferDescriptionParser offerDescriptionParser;
  OfferDescriptionListener offerDescriptionListener;
  String url;


  SetUpOfferDescriptionListener(this.offerDescriptionListener, this.url){
    offerDescriptionParser = parser;
  }

  void loadOffer(){
    offerDescriptionParser.callApi();
  }

  OfferDescriptionParser get parser{
    return new OfferDescriptionParser(url,offerDescriptionListener);
  }
}