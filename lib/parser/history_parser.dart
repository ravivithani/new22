import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/history_model.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;

class HistoryParser{
  String url;
  http.Response response;
  HistoryDataListener listener;

  HistoryParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map array = JSON.decode(response.body);

      List lstStoreList = array['SelectAllReferAndEarnTranResult'];

      listener.onHistoryListener(lstStoreList.map((c) => new HistoryModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onErrorLogin(e.toString());
    }
  }
}
abstract class HistoryDataListener{
  void onHistoryListener(List<HistoryModel> historyList);
  void onErrorLogin(onError);
}

class SetUpHistoryListenerData{
  String url;
  HistoryDataListener historyDataListener;
  HistoryParser allHistoryParser;

  SetUpHistoryListenerData(this.url, this.historyDataListener,){
    allHistoryParser = historyParser;
  }

  void loadHistoryData(){
    allHistoryParser.callApi();
  }

  HistoryParser get historyParser {
    return new HistoryParser(url,historyDataListener);
  }
}