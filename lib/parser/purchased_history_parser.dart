import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/purchased_history_model.dart';
import 'package:crazyrex/util/constants.dart';

class PurchasedHistoryParser {
  String url;
  PurchasedHistoryListener listener;
  http.Response response;

  PurchasedHistoryParser(this.url, this.listener);

  Future callApi() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    if (statusCode != 200) {
      listener.onErrorPurchasedHistory("status code $statusCode");
    }
    try {
      Map array = JSON.decode(response.body);
      List listHistory = array['SelectAllCustomerPaymentTransactionsResult'];
      List<PurchasedHistoryModel> list =
          listHistory.map((c) => new PurchasedHistoryModel.fromMap(c)).toList();
      listener.onGetPurchasedHistoryList(list);
    } catch (e) {
      print(e);
      listener.onErrorPurchasedHistory(e.toString());
    }
  }
}

abstract class PurchasedHistoryListener {
  void onGetPurchasedHistoryList(List<PurchasedHistoryModel> list);

  void onErrorPurchasedHistory(String error);
}

class SetUpPurchasedHistoryListener {
  PurchasedHistoryListener historyListener;
  PurchasedHistoryParser historyParser;
  String url;

  SetUpPurchasedHistoryListener(this.historyListener, this.url) {
    historyParser = parser;
  }

  void loadPurchasedHistoryList() {
    historyParser.callApi();
  }

  PurchasedHistoryParser get parser {
    return new PurchasedHistoryParser(url, historyListener);
  }
}
