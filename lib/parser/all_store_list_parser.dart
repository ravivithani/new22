import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;
import 'package:crazyrex/model/store_list_model.dart';

class AllStoreListParser{
  String url;
  http.Response response;
  AllStoreDataListener listener;
  bool isRecently;

  AllStoreListParser(this.url, this.listener,this.isRecently);

  Future callApi() async {
    response = await http.get("$url",headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;
    if(statusCode != 200){
      listener.onErrorStoreListener("status code $statusCode");
    }
    try {
      Map array = JSON.decode(response.body);
      List lstStoreList;
      if(isRecently){
            lstStoreList = array['SelectAllBusinessMasterBusinessByRecentAddedResult'];
          }else{
            lstStoreList = array['SelectAllBusinessMasterBusinessByTypeResult'];
          }
      listener.onStoreListener(lstStoreList.map((c) => new StoreListModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onErrorStoreListener(e.toString());
    }

  }
}

abstract class AllStoreDataListener{
  void onStoreListener(List<StoreListModel> storeList);
  void onErrorStoreListener(String error);
}

class SetUpAllStoreListenerData{
  String url;
  AllStoreDataListener allStoreDataListener;
  AllStoreListParser allStoreListParser;
  bool isRecently;

  SetUpAllStoreListenerData(this.url, this.allStoreDataListener,this.isRecently){
    allStoreListParser = storeParser;
  }

  void loadAllStoreListData(){
    allStoreListParser.callApi();
  }

  AllStoreListParser get storeParser {
    return new AllStoreListParser(url,allStoreDataListener,isRecently);
  }

}