import 'dart:async';
import 'dart:convert';
import 'package:crazyrex/model/claim_card_list_model.dart';
import 'package:crazyrex/util/constants.dart';
import 'package:http/http.dart' as http;

class ClaimCardListParser{
  String url;
  ClaimCardListDataListener listener;
  http.Response response;

  ClaimCardListParser(this.url, this.listener);

  Future callApis() async {
    response = await http.get(url,headers: Constants.httpGetHeader);
    final statusCode = response.statusCode;

    try {
      Map array = JSON.decode(response.body);

      List lstStoreList = array['SelectAllMembershipTypeResult'];

      listener.onClaimCardListListener(lstStoreList.map((c) => new ClaimCardListModel.fromMap(c)).toList());
    } catch (e) {
      print(e);
      listener.onErrorClaimCardList(e.toString());
    }
  }

  static Future<int> updateCustomerMembershipTran(String url, var encoder) async {
    var client = new http.Client();
    http.Response response = await client.post(url,
        headers: Constants.httpPostHeader,
        body: encoder);
    final statusCode = response.statusCode;

    Map decoder = JSON.decode(response.body);
    int errorCode = decoder['UpdateCustomerMembershipTranResult']['ErrorCode'];

    if (statusCode != 200 || response == null) {
      return -1;
    }
    print('ErrorCode $errorCode');
    return errorCode;
  }
}

abstract class ClaimCardListDataListener{
  void onClaimCardListListener(List<ClaimCardListModel> claimCardList);
  void onErrorClaimCardList(onError);
}

class SetUpClaimCardListListenerData{
  String url;
  ClaimCardListDataListener claimCardListDataListener;
  ClaimCardListParser allClaimCardListParser;

  SetUpClaimCardListListenerData(this.url, this.claimCardListDataListener,){
    allClaimCardListParser = claimCardListParser;
  }

  void loadClaimCardListData(){
    allClaimCardListParser.callApis();
  }
  ClaimCardListParser get claimCardListParser {
    return new ClaimCardListParser(url,claimCardListDataListener);
  }
}