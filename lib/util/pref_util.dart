import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class PrefUtil{

  static Future<bool> putStringPreference(String key,String value) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(key, value);
    return preferences.commit();
  }
  static Future<String> getStringPreference(String key) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String name = preferences.getString(key);
    return name;
  }
  static Future<bool> putIntPreference(String key,int value) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt(key, value);
    return preferences.commit();
  }
  static Future<int> getIntPreference(String key) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int name = preferences.getInt(key);
    return name;
  }
  static Future<bool> putBoolPreference(String key,bool value) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(key, value);
    preferences.commit();
    return preferences.commit();
  }
  static Future<bool> getBoolPreference(String key) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool name = preferences.getBool(key);
    return name;
  }
  static Future<bool> removePreference(String key) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.remove(key);
    return preferences.commit();
  }
  static Future<bool> clearPreference() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
    return preferences.commit();
  }
}