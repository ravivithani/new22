import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Constants{

  static var appName = "CrazyRex";
  static final String url = "http://10.0.0.114:54080/Service.svc/";
//  static final String url = "http://10.0.0.100/poswServices/Service.svc/";
//  static final String url = "http://crx.arraybit.com/service/service.svc/";

  static String PRIVACY_URL = "http://www.crazyrex.com/privacy-policy.html";
  static String TERMS_URL = "http://www.crazyrex.com/terms&conditions.html";
  static String FAQ = "http://www.crazyrex.com/FAQ.html";
  static String FACEBOOK = "https://www.facebook.com/CrazyRex-790896514437021/";
  static String INSTAGRAM = "https://www.instagram.com/crazyrex.official/";

  static String prefFcmToken = "prefFcmToken";
  static final String prefLogin = "prefLogin";
  static final String prefIsFirstTimeLaunch = "PrefIsFirstTimeLaunch";
  static final String prefPhone = "prefPhone";
  static final String prefPassword = "prefPassword";
  static final String prefRegisterUserMasterId = "prefRegesterUserMasterId";
  static final String prefCustomerMasterId = "prefCustomerMasterId";
  static final String prefFirstName= "prefFirstName";
  static final String prefLastName= "prefLastName";
  static final String prefReferralCode = "prefReferralCode";
  static final String prefProfileImage = "prefProfileImage";
  static final String prefWalletBalance = "prefWalletBalance";
  static final String prefKycDetail = "prefKycDetail";
  static final String prefSessionId = "prefSessionId";
  static final String prefCityName = "prefCityName";
  static final String prefMembershipTranId = "membershipTranId";
  static final String prefCityId = "prefCityId";

  static bool isLoadingOnResume = false;

  static var api_key = "AIzaSyAkocNdxLVO3m9fx1Gl7_Rz84hwTQQXGa4";

  static String OTP = "";

  static int recentlyJoinedId = 1111111111;

  static var SourceMasterId = 3;

  static int resendTimer = 60;

  //region SnackBar
  static void showSnackBar(GlobalKey<ScaffoldState> key,String message){
    if(message.isEmpty) return;
    key.currentState.showSnackBar(new SnackBar(content: new Text(message)));}
//endregion

  static bool isValidEmail(String email){
    bool isEmail = false;
    RegExp exp = new RegExp("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
    Iterable<Match> matches = exp.allMatches(email);
    for (Match m in matches) {
      m.group(0);
      isEmail = true;
    }
    return isEmail;
  }

  static bool isValidPhone(String phone){
    bool isPhone = false;
    RegExp exp = new RegExp('^[0-9]{10}\$');
    Iterable<Match> matches = exp.allMatches(phone);
    for(Match m in matches){
      m.group(0);
      isPhone = true;
    }
    return isPhone;
  }

  static void portrait(){
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  static var httpGetHeader = {"Authorization": "123456"};
  static var httpPostHeader = {
    "Authorization":"123456",
    "Accept": "application/json",
    "Content-type": "application/json"
  };
}