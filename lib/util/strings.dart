class Strings {
  //region intro screen texts
  static String offersAndMore = "Offers & More";
  static String saveMoney = "SAVE MONEY";
  static String makeMoney = "MAKE MONEY";
  static String throughOurNetwork = "through our network";
  static String throughYourNetwork = "through your network";
  static String referAndEarn = "Refer & Earn";
  static String joinCrx = "Join CrazyRex";
  //endregion

  static String loadingTitle = "Please wait";

  //region login screen
    static String welcome = "Welcome";
    static String hintUsername = "Phone or Email";
    static String hintPassword = "Password";
    static String labelButtonSignIn = "SIGN IN";
    static String labelForgetPassword = "Forgot password?";
    static String labelDontHaveAccount = "Don't have an account?";
    static String labelDontHaveAccountSignUp = " Sign Up";
    static String emptyUsername = "Enter phone or email";
    static String emptyPassword = "Enter password";
    static String invalidUsername = "Enter valid phone or email";
    static String wrongUsernameOrPass = "Invalid username or password";
    static String labelButtonUdpate = "UPDATE";
    static String alertAppVersionContent = "Yuhu! An update of this app is available in Play Store. Every update brings more speeds, more security, more feature and much more. \n\nPlease update app to enjoy full featured app with more benefits for you.";
  //endregion

  //region registration
   static String labelSignUp = "Sign Up";
   static String labelAgreement = "By signing up, you agree to CrazyRex";
   static String labelAgreementPrivacyPolicy = "Privacy Policy.";
   static String labelAlreadyHaveAccount= "Already have an account?";
   static String labelAlreadyHaveAccountSignIn= " Sign In";
   static String labelButtonContinue = "CONTINUE";
   static String hintFirstName = "First Name";
   static String hintLastName  = "Last Name";
   static String hintPhoneNumber = "Phone Number";
   static String hintEmailAddressOptional = "Email Address (Optional)";
   static String hintReferCode = "Referral Code (Optional)";
   static String emptyFirstName = "Enter first name";
   static String emptyLastName = "Enter last name";
   static String emptyPhone = "Enter phone number";
   static String invalidPhone = "Enter valid phone number";
   static String invalidEmail = "Enter valid email address";
   static String errorAlreadyHaveAccount = 'User already exists';
   static String errorAlreadyEmailExist = 'Email address is already used';
  //endregion

  ///region how does it work dialog
  static String labelHowDoesItWork = "How it works?";
  static String labelFirst = "Share your referral code.";
  static String labelSecond = "Friends join CrazyRex.\n or \nFriends buy membership.";
  static String labelSecond1 = "Friends apply for CrazyRex account.\n or \nFriends buy CrazyRex membership.";
  static String labelThird = "You and your friends earn money.";
  //endregion

  //region registration with referCode code dialog
    static String alertDialogTitleForSuccessEarn = "Congratulations! CrazyRex points will be added to your account within 24 hrs.";
    static String alertDialogTitleForFailEarn = "Sorry, the sign up reward is only for new users, on new devices. You can still get rewards by referring your friends.";
  //endregion

  //region forget account password
  static String labelForgetPasswordTitle = "Find Your Account";
  static String labelForgetPasswordSubTitle = "Enter your phone or email to search for your account.";
  static String labelButtonNext = "NEXT";
  static String errorAccountNotExist = "Email or Phone doesn\'t exist.";
  //endregion

  //region one time password
  static String labelOtpScreenTitle = "Verification";
  static String otpSentToPhone = "OTP sent to phone number";
  static String otpSentToEmail = "OTP sent to email address";
  static String labelAlertTitleOfCancelRegistration = "Do you want to cancel registration?";
  static String alertPositiveButton = "YES";
  static String alertNegativeButton = "NO";
  static String invalidOTP = "Invalid OTP";
  static String emptyOTP = "Enter OTP";
  static String hintOTP = "OTP";
  static String labelButtonText = "VERIFY";
  static String labelNotReceiveOTPText = "Not received your code? ";
  static String labelResend = " Resend Code";
  static String errorAccountAlreadyCreatedOnThisNumber = "Account is already created on this number";


  //endregion

  //region Password Screen
  static String labelPasswordScreenTitleForCreatePassword = "Create Password";
  static String labelPasswordScreenTitleForResetPassword = "Reset Password";
  static String labelForButtonSignUp = "SIGN UP";
  static String labelForButtonSave = "SAVE";
  static String invalidUserName = "Not valid username";
  static String incorrectPassword = "Password was incorrect";
  static String emptyOldPassword = "Enter old password";
  static String emptyNewPassword = "Enter new password";
  static String emptyRePassword = "Enter re-type password";
  static String hintOldPassword = "Old Password";
  static String hintNewPassword = "New Password";
  static String hintRePassword = "Confirm Password";
  static String invalidPassword = "Enter minimum six digit characters";
  static String requirePassword = "Password required";
  static String rePasswordNotMatch = "Password doesn’t match";
  static String toastSuccessPassword = "Password updated successfully";

  //endregion

  //region no internet screen or toasts
    static String noInternetToast = "No internet connection";
    static String errorInServices = "Oops something went wrong contact your service provider";
    static String noInternetTitle = "Oops!";
    static String labelButtonRetry = "RETRY";
    static String noInternetSubText = "It seems like you are not connected to network.";
  //endregion

  //region empty screen
  static String emptyScreenTitle = "Somthings wents wrong\ntry again later.";
  //endregion

  //region home screen
  static String labelHomeScreenTitle = "CrazyRex";
  static String labelUserName = "User Name";
  static String labelPhone = "Phone";
  static String labelCity = "City";
  static String labelLogOut = "LOGOUT";
  static String labelMyAccount = "My Account";
  static String labelMyOffer = "My Offers";
  static String labelNotifications = "Notifications";
  static String labelSuggestion = "Suggestion";
  static String labelShare = "SHARE";
  static String labelSmallShare = "Share";
  static String labelRateApp = "Rate App";
  static String labelAboutUs = "About Us";
  static String labelShowClaimCard = "Show Claim Card";
  static String labelBeCrazyMember = "Be a crazy member";
  static String labelShowMe = "Show me";
  static String labelOfferCategory = "Offer Categories";
  static String labelRecentlyJoined = "Recently Joined";
  static String labelViewAll = "View all";
  //endregion

  //region citySelection screen
  static String labelCitySelectionScreenTitle = "Select Your City";
  static String hintCityName = "Enter city name";
  //endregion

  //region EditAccount screen
  static String labelAlertTitleOfEditProfile = "Save changes?";
  static String labelAlertTitleOfVerifyPhone = "Verify your phone first.";
  static String labelAlertAddPhoto = "Add Photo!";
  static String labelAlertTakePhoto = "Take Photo";
  static String labelAlertChooseGallery = "Choose from Gallery";
  static String labelAlertCancel = "CANCEL";
  static String labelAlertCancelSmall = "Cancel";
  static String labelButtonDiscard = "DISCARD";
  static String labelButtonSave = "SAVE";
  static String labelTitleEditAccountScreen = "Edit Account";
  static String labelEditPhoto = "Edit Photo";
  static String emptyCantPhone = "Phone number can't be empty";
  static String hintEmailAddress = "Email Address";
  static String hintBirthday = "Birthday";
  static String hintGender = "Gender";
  static String labelMale = "Male";
  static String labelFemale = "Female";
  static String existEmail = "This email is already exist.";
  static String existPhone = "This phone is already exist.";

  //endregion

  //region myAccount screen
  static String labelTitleMyAccountScreen = "My Account";
  static String labelChange = "Change";
  static String labelCitySelect = "You are in ";
  static String labelMyWallet = "My Wallet";
  static String labelPurchasedHistory = "Purchased History";
  static String labelMyFavourite = "My Favourites";
  static String labelMyReview = "My Review";
  static String labelBeenThere = "Been There";
  static String labelChangePassword = "Change Password";
  static String labelNotificationSettings = "Notification Settings";
  static String labelKYCDetails = "KYC Details";
  //endregion

  //region Suggest Store screen
  static String labelTitleSuggestStoreScreen = "Suggest Store";
  static String labelDoYouHaveStoreSuggest = "Do you have any other store suggestion?";
  static String labelRecommendUsWithInfo = "Recommend us with some information about that store where you want to get discount.";
  static String labelHelpUsToExpand = "Help us to expand.";
  static String emptyStoreName = "Enter store name";
  static String emptyStoreAddress = "Enter store address";
  static String labelStoreName = "Store Name";
  static String labelStoreAddress = "Store Address";
  static String labelDescription = "Description";
  static String labelButtonSuggest = "SUGGEST";
  static String labelSuccessToast = "Thank you for your valuable suggestion.";
  //endregion

  //region Search Screen
  static String hintSearch = "Type here to search";
  static String noSearchFound = "No result found.";
  //endregion

  //region Store list
   static String appNeedLocationPermission = "App need location permission";
   static String appNeedLocationPermissionGoToSettings = "App need location permission -> go to app setting and enable location permition";
   static String emptyStoreList = "Stores are coming shortly.\nCome again for updates.";
   static String emptyOfferList = "Offers are coming shortly.\nCome again for updates.";
  //endregion

  //region my offers
  static String labelForTitle = "My Offers";
  static String labelForTabSaved = "SAVED";
  static String labelForTabUsed = "USED";
  static String emptySaved = "No offers are saved.";
  static String emptyUsed = "No offers used till now.";

  //endregion

  //region been fav store
  static String labelTitleFavStoreScreen = "Favourite Stores";
  static String labelTitleBeenStoreScreen = "Been There";
  static String emptyFavouriteScreen = "No favourite store yet.";
  static String emptyBeenScreen = "No place has been visited yet.";
  static String labelDelete = "Delete";
  //endregion

  // region review store
  static String labelTitleReviewStoreScreen = "My Review";
  static String emptyReviewScreen = "No review given yet.";
  static String labelEdit = "Edit";
  //endregion

  // region add - edit review store
  static String labelTitleAddReviewStoreScreen = "Add Review";
  static String labelHowWasExperience = "How was your experience?";
  static String labelWriteReview = "Write A Review";
  static String labelButtonSubmit = "SUBMIT";
  static String labelToastSuccessAddReview = "Review uploaded";
  static String labelToastWriteDescription = "Write your experience";
  static String labelToastAddStarRate = "Add your rate";
  //endregion

  //region history
  static String tabHistory = "HISTORY";
  static String tabReferAndEarn = "REFER & EARN";
  static String labelYourEarning = "Your earnings ";
  static String labelDefaultReferCode = "******";
  static String labelYourReferralCode = "YOUR REFERRAL CODE";
  static String labelCurrentlyNotVisible = "Currently not visible";
  static String labelOnlyActiveMember = "Only active member can refer friends.";
  static String labelEarnMoneyBySharing = "Earn money by sharing your friends.";
  static String labelBuyMembership = "Buy Membership ";
  static String labelFirstGetReferralCode = " first to get your referral code.";
  static String labelShareReferralCodeMessage = "Download CrazyRex and use my refer code ";
  static String labelShareReferralCodeMessage1 = " on sign up & purchase, we BOTH will earn. Then share your refer code and EARN money.";
  static String labelRewardCommission = "Reward commission";
  static String emptyHistoryScreen = "No history available.";
  static String labelEarning = "Total Earning";
  static String labelPendingBalance = "Pending Balance ";
  static String alertRemainingEarnPointMessage = "Your pending balance is where your refer earnings goes before going into your wallet. Once you reach the payout amount it will be transferred into your wallet. Payout Amount: ";
  static String alertRemainingEarnPointMessage1 = " Refer Points.";
  static String labelEarnPointDescriptionFirst= "Bonus received.";
  static String labelEarnPointDescriptionSecond= " used your referral code for membership activation.";
  static String labelEarnPointDescriptionThird= " used your referral code for registration.";
  static String labelEarnPointDescriptionForth= "'s referral code was used.";
  static String labelGotIt= "GOT IT";
  static String toastNotInstalled= "Application not installed";

  //region notification screen
  static String emptyNotificationList = "No notification available.";
  static String notificationScreenTitle = "Notifications";
  //endregion

  //region purchased history
  static String emptyPurchasedHistoryList = "No purchased history found.";
  static String purchasedHistoryScreenTitle = "Purchased History";
  //endregion

//endregion

  //region Notification setting
  static String labelTitleNotificationSettingScreen= "Notification Settings";
  static String labelOfferNotification = "Offer Notification";
  static String labeStoreNotification = "Store Info Notification";
  static String labelNotifyOffer = "We notify you for offers that you don\'t want to miss.";
  static String labelNotifyStore = "We notify you for new store details.";
  //endregion

  // region Notification setting
  static String labelEmptyWalletHistoryScreen= "No wallet history found.";
  static String labelTitleWalletHistoryScreen = "Wallet";
  static String labelWallet = "Wallet Balance";
  //endregion

  //region about us
  static String support = "Support";
  static String supportEmail = "support@CrazyRex.com";
  static String contentText = "CrazyRex is a place which help you to discover offers and deals on food, fashion, beauty, fitness, activities and more around them in your city. \n\n\nExclusive offers with great discounts, what’s not to love? CrazyRex Claim Card holder can get special offers &amp; freebies from registered merchants. Save money every time on every visit.\n";
  static String txtSupport = "For any query mail us at";
  static String txtSupportEmail = "support@crazyrex.com";
  static String txtFacebook = "Like us on Facebook";
  static String txtInstagram= "Follow us on Instagram";
  //endregion

  //region Store detail screen
  static String labelRate = "Rate";
  static String labelBeenHere = "Been Here";
  static String labelFavourite = "Favourite";
  static String toastAddBeen = "Added to 'Been There'";
  static String toastAddFav = "Added to 'Favourite'";
  static String toastRemoveBeen = "Removed from 'Been There'";
  static String toastRemoveFav = "Removed from 'Favourite'";
  static String labelOffers = "Offers";
  static String labelEstimatedSaving = "Estimated Saving ";
  static String labelNA = "N/A ";
  static String labelRedeemed = "Redeemed";
  static String labelUsed = "Used";
  static String labelMenu = "Menu";
  static String labelPhoto = "Photo";
  static String labelPhotos = "Photos";
  static String labelAddress = "Address";
  static String labelOpeningHours = "Opening Hours";
  static String labelBasedOn = "Based on";
  static String labelReviews = "reviews";
  static String labelButtonAddReview = "ADD REVIEW";
  static String labelButtonEditReview = "EDIT REVIEW";
  static String labelAlertReviewContent = "Are you sure want to delete your review?";
  static String labelAlertDelete = "DELETE";
  static String labelHowDidVisitGo = "How did your visit go?";
  static String labelTellEveryOneAbout = "Tell everyone about it!";
  static String labelDirectionNotAvailable = "Direction not available";
  static String labelDetails = "Details";
  static String labelCall = "Call";
  static String labelOutlet = "outlets in";
  static String labelClosed = "Closed";
  static String labelOpenNow = "Open now";
  //endregion

  //region Outlet
  static String labelTitleOutletListScreen = "Outlets";
  static String labelTimeUsed = "times used";
  //endregion

  // region view all review
  static String labelTitleViewAllScreen = "All Review";
  //endregion

  // offer Description
  static String offerDescTitle = "Offer Details";
  static String cardOfferUsageTitle = "Offer usage";
  static String txtHowToUseOffer = "How to use offer?";
  static String txtScanQRCodeAtStore = "Scan the QR code at store.";
  static String txtEnterPinForRedeem = "Let the store employee enter the pin.";
  static String txtShowClaimCardToStore = "Open your Claim Card and let the store employee scan it.";
  static String txtHowToUseFirst = "Show your application with the respective offer to the concerned store employee.";
  static String txtHowToUseSecond= "Once confirmed with the store, you can redeem the offer in any of the below ways.";
  static String offerNotAvailable = "Not available now.";
  static String txtHistory = "History";
  static String branchInfoCardTitle = "Branch Information";
  static String termsAndConditionCardTitle = "Terms & Conditions";

  static String congratulation = "Congratulations!";

  static String offerSuccessMessage = "Offer redeemed successfully.";
  //endregion

  //region scan screen
  static String ScanQrScreenTitle = "Scan QR code or Enter PIN";
  static String emptyEditPin = "Enter PIN";
  static String hintPin = "Enter PIN";
  static String invalidEditPin = "Enter valid PIN";
  static String showClaimCard = "Show Claim Card";
 //endregion
  //endregion

  //region membership list screen
  static String labelTitleMembershipListScreen= "Membership Packages";
  static String labelBenefitsOfBuyingMembership= "Benefits of buying membership";
  static String labelCrazyPrice= "\u2022 Crazy Price! Get everything at a lowest price.";
  static String labelCrazySaver= "\u2022 Crazy Saver! Save more than 50% on offers.";
  static String labelDontMissExclusive= "Don't miss the exclusive freebies.";
  static String labelCrazyEarn= "\u2022 Crazy Earn! Don't forget about referral earnings.";
  static String labelBeCrazyOneAndSave= "Be a crazy one and save around";
  static String labelByJoiningCrzyRex= "By joining CrazyRex, you have read & agree to our";
  static String labelTermsCondition= "Terms & Conditions.";
  static String labelButtonRenew= "RENEW";
  static String labelButtonUpgrade= "UPGRADE";
  static String labelButtonDowngrade= "DOWNGRADE";
  static String labelButtonPurchased= "PURCHASED";
  static String labelButtonBuyNow= "BUY NOW";
  static String labelCurrentlyActive= "Currently active";
  static String labelViewClaimCard= "View Claim Card";
  static String labelDiscount= "Discount";
  static String labelValidity= "Validity";
  static String labelMonth= "month";
  static String labelMonths= "months";
  static String labelExpireOn= "Expire on";
  static String labelActiveFrom= "Active from";
  static String labelAmount= "Amount";
  static String labelAlertPurchaseTitle = "Are you sure?";
  static String labelAlertPurchaseContent = "By activating this";
  static String labelAlertPurchaseContent1 = "membership, your current membership will expire instantly.";
  static String labelAlertActivate = "ACTIVATE";
  static String labelAlertOk = "OK";
  static String labelAlertDoubleBuyContent = "Sorry! you can not deal with more than two memberships.";
  static String buyMembershipSuccessMessage = "Your";
  static String buyMembershipSuccessMessage1 = "membership is activated.\nNow you can enjoy all crazy offers.";
  //endregion
  //region claim activation screen
  static String labelCurrently ="Currently";
  static String labelMembershipIsActiveTill ="membership is active till";
  static String labelMembershipSinceOnly ="Since only one membership can be active at a time please choose one of the option below.";
  static String labelActiveNow ="Active now. Your current membership will expire instantly.";
  static String labelActiveLater ="Activate later. Your new membership will activate after your current membership will expire.";
  //endregion
  //region claim card screen
  static String labelClaimCardTitle ="Claim Card";
  static String labelValidUpto ="Valid up to";
  static String labelAppName ="CrazyRex";
  //endregion
  //region claim card detail
  static String claimCardDetailTitle = "Buy Membership";
  static String labelValid = "Valid";
  static String labelReferralCode = "Referral Code";
  static String labelApply = "APPLY";
  static String hintReferralCodeOption = "Enter referral code (Optional)";
  static String emptyReferralCode = "Enter referral code";
  static String errorOwnReferralCode = "Its your referral code, Enter your friend referral code";
  static String labelReferralCodeApply = "'s referral code applied.";
  static String labelTotalAmount = "Total Amount";
  static String labelSubTotal = "Sub Total";
  static String labelWalletAmount = "Wallet Amount";
  static String labelTotalPayable = "Total Payable";
  static String labelAgreeTermsCondition = "I agree to terms & conditions";
  static String labelButtonPayWallet = "PAY USING WALLET";
  static String labelButtonPayPayment = "PROCEED TO PAY";
  static String toastTermsCondition = "To continue, you must agree to our Terms and Conditions.";

  static String errorDialogMsg = "There was an error while processing your order. Please try again.";

  static String transactionFailed = "Transaction Failed";

  static String beTheFirstUser = "Be the 1st to use this offer";

  //endregion
}
