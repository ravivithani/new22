import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ViewLineFromRegistration extends Decoration {
  @override
  BoxPainter createBoxPainter([VoidCallback onChanged]) {
    return new _ViewLinePainter();
  }
}

class _ViewLinePainter extends BoxPainter {
  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    final paint = new Paint()
      ..strokeWidth = 0.5
      ..color = Colors.black38
      ..style = PaintingStyle.fill;

    final rect = offset & configuration.size;
    canvas.drawLine(new Offset(rect.left, rect.top + rect.height /12), new Offset(rect.right, rect.top + rect.height / 12), paint);
    canvas.restore();
  }
}